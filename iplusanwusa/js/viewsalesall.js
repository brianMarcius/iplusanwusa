$(document).ready(function() {	
	isi_cbokategori();	
});			

function isi_cbokategori(){
	$.ajax({
		type: 'POST', 
		url: 'pages/pricelist/tampilkan_kategori.php',
		success: function(data) {			
			$('#cbokategori').html(data);
			isi_cbobulan();
		}
	});		
}

function isi_cbobulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/monitpenjualan/tampilkan_bulan.php',
		success: function(data) {
			$('#cbobulanthn').html(data); 
			tampil_stock('1');	
		}
	});		
}

$('#cbokategori').change(function(){
	tampil_stock('1');
});

$('#searchby').keyup(function(){
	tampil_stock('1');							
});

$('#cbobulanthn').click(function(){
	tampil_stock('1');							
});

function tampil_stock(pagex){
	var searchby = $("#searchby").val();
	var bln		 = $("#cbobulanthn").val();
	var kategori = $("#cbokategori").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/monitpenjualan/tampildata.php",
		data	: "searchby="+searchby+
					"&bln="+bln+
					"&kategori="+kategori+
					"&page="+pagex,
		success	: function(data){
			$("#data_area").html(data);
			paginationx(pagex);
		}
	});
}	

function paginationx(pagex){
	var searchby	= $("#searchby").val();
	var bln			= $("#cbobulanthn").val();
	var kategori = $("#cbokategori").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/monitpenjualan/paging.php",
		data	: "searchby="+searchby+
					"&bln="+bln+
					"&kategori="+kategori+
					"&page="+pagex,
		success	: function(data){
			$("#page_area").html(data);
		}
	});
}

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampil_stock(pageno);						 
});

function recent(kodebrg,area){
	var kode = kodebrg+"-"+area;

	$.ajax({
		type	: "POST",		
		url		: "pages/monitpenjualan/lintasform.php",
		data	: "kode="+kode,
		success	: function(data){
			window.location.assign('?mod=recentstock');
		}
	});
}