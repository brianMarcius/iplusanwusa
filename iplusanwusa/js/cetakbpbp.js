$(document).ready(function() {
	getdatapengembalian();	
});	

function getdatapengembalian(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/buktipengembalianprol/datapengembalian.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblno").text(data.nosrtjalan);
			$("#lbltgltrx").text(data.tglkirim);
			$("#lblstaff").text(data.staff);
			$("#lblpenerima").text(data.penerima);
			tampildata();
		}
	});
}

function tampildata(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/buktipengembalianprol/tampildata.php",
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function cetaksrtjalan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/buktipengembalianprol/cetakbpbp.php",
		success	: function(data){
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	window.open('?mod=cetbpbp','_blank');
	//window.open("pages/buktipengembalianprol/cetakbpb.php");
}

function goBack() {
    window.history.back();
}