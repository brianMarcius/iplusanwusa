$(document).ready(function() {	
	list_cbperusahaan();
	$("#jammasuk").inputmask("h:s",{ "placeholder": "hh/mm" });
	$("#jampulang").inputmask("h:s",{ "placeholder": "hh/mm" });
});

function list_cbperusahaan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbperusahaan.php',
		success: function(response) {
			$('#cboarea').html(response); 
			$('#cboareacopy').html(response); 
		}
	});	
}

function list_cbohari(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/listhari.php',
		success: function(response) {
			$('#cbohari').html(response); 
		}
	});	
}

function list_shift(){
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/listshift.php',
		data: {
        	kodearea: kodearea
        },
		success: function(response) {
			$('#cboshift').html(response); 
		}
	});	
}

function viewshift(){	
	var	tgl = $("#tglefektif").val();
	var	kodearea = $("#cboarea").val();

	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shift/lihatshift.php',
		data: {
       		tgl: tgl,
       		kodearea: kodearea
       	},
		success: function(response) {
			list_cbohari();
			list_shift();
			$('#tblshift').html(response); 
		}
	});
}

$('#cboarea').change(function(){
	viewshift();
});

$('#tglefektif').change(function(){
	viewshift();
});

function EnterMasuk(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#jampulang").focus();		
	}
}

function EnterPulang(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		addshift();	
		$("#cbohari").focus();
	}
}

function clearaddshift(){
	$("#cbohari").val('');
	$("#cboshift").val('');
	list_cbohari('');
	list_shift('');
	$("#jammasuk").val('');
	$("#jampulang").val('');
}

function addshift(){
	var	tgl = $("#tglefektif").val();
	var	kodearea = $("#cboarea").val();
	var hari	= $("#cbohari").val();
	var shift 	= $("#cboshift").val();
	var jammasuk = $("#jammasuk").val();
	var jampulang	= $("#jampulang").val();
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;			

	$('#warningx').html('');
	var batalkan = false;	
	
	if(tgl.length==0){
		$('#warningx').html('Tanggal efektif belum di input.');
		$("#tglefektif").focus();
		var batalkan = true;
	}

	if(kodearea.length==0){
		$('#warningx').html('Area Kerja belum dipilih.');
		$("#cboarea").focus();
		var batalkan = true;
	}

	if(hari.length==0){
		$('#warningx').html('Hari belum dipilih.');
		$("#cbohari").focus();
		var batalkan = true;
	}

	if(shift.length==0){
		$('#warningx').html('Shift belum dipilih.');
		$("#cboshift").focus();
		var batalkan = true;
	}

	if(jammasuk.length==0){
		$('#warningx').html('Jam masuk belum di input.');
		$("#jammasuk").focus();
		var batalkan = true;
	}

	if(jammasuk.length!=0 && !jammasuk.match(jamValid)){
		$('#warningx').html('Jam masuk tidak valid.');
		$("#jammasuk").focus();
		var batalkan = true;
	}

	if(jampulang.length==0){
		$('#warningx').html('Jam pulang belum di input.');
		$("#jampulang").focus();
		var batalkan = true;
	}

	if(jampulang.length!=0 && !jampulang.match(jamValid)){
		$('#warningx').html('Jam pulang tidak valid.');
		$("#jampulang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/shift/simpanshift.php",
			data	: "tgl="+tgl+	
					"&kodearea="+kodearea+
					"&hari="+hari+	
					"&shift="+shift+	
					"&masuk="+jammasuk+	
					"&pulang="+jampulang,
			dataType: "json",
			success	: function(data){
				if(data.sukses==1){
					viewshift();
					clearaddshift();
				}else{
					$('#warningx').html(data.pesan);
					$("#cbohari").focus();
				}
			}	
		});
	}
}

function addjmlshift(){
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shift/addjmlshift.php',
		data: {
        	kodearea: kodearea
        },
		success: function(response) {
			list_shift();
		}
	});	
}

function minjmlshift(){
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shift/minjmlshift.php',
		data: {
        	kodearea: kodearea
        },
		success: function(response) {
			list_shift();
		}
	});	
}

$('#btn_addjmlshift').click(function(){	
	addjmlshift();						 
});

$('#btn_minjmlshift').click(function(){	
	minjmlshift();						 
});

$('#btn_addshift').click(function(){	
	addshift();						 
});

function edit(kdhari,shift){
	var	tgl = $("#tglefektif").val();
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type	: "POST",
		url		: "pages/payroll/shift/edit.php",
		data	: "kdhari="+kdhari+	
					"&shift="+shift+	
					"&tgl="+tgl+	
					"&kodearea="+kodearea,
		dataType: "json",
		success	: function(data){	
			$('#cbohari').val(data.kdhari);
			$('#cboshift').val(data.shift);
			$('#jammasuk').val(data.jammasuk);
			$('#jampulang').val(data.jampulang);
							
		}
	});
}

function del(kdhari,shift){
	var	tgl = $("#tglefektif").val();
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type	: "POST",
		url		: "pages/payroll/shift/delete.php",
		data	: "kdhari="+kdhari+	
					"&shift="+shift+	
					"&tgl="+tgl+	
					"&kodearea="+kodearea,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				viewshift();
				clearaddshift();			
			}				
		}
	});
}

function goBack() {
    window.history.back();
}

function copyshift(){
	var	tgl = $("#tglefektif").val();
	var	kodearea = $("#cboarea").val();
	var	tglcopy = $("#tglefektifcopy").val();
	var	kodeareacopy = $("#cboareacopy").val();			

	$('#lblwarning_copy').html('');
	$('#lblsuccess_copy').html('');
	var batalkan = false;	
	
	if(tgl.length==0){
		$('#lblwarning_copy').html('Tanggal efektif asal belum di input.');
		var batalkan = true;
	}

	if(kodearea.length==0){
		$('#lblwarning_copy').html('Area Kerja asal belum dipilih.');
		var batalkan = true;
	}

	if(tglcopy.length==0){
		$('#lblwarning_copy').html('Tanggal efektif tujuan belum di input.');
		var batalkan = true;
	}

	if(kodeareacopy.length==0){
		$('#lblwarning_copy').html('Area Kerja tujuan belum dipilih.');
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/shift/copyshift.php",
			data	: "tgl="+tgl+	
					"&kodearea="+kodearea+
					"&tglcopy="+tglcopy+
					"&kodeareacopy="+kodeareacopy,
			dataType: "json",
			success	: function(data){
				if(data.sukses==1){
					$('#lblsuccess_copy').html(data.pesan);
					clearaddshift();
				}else{
					$('#lblwarning_copy').html(data.pesan);
					$("#cboareacopy").focus();
				}
			}	
		});
	}
}

$('#btn_copyshift').click(function(){	
	copyshift();						 
});

function CekJamMasuk() {
	$('#warningx').html('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jammasuk = $("#jammasuk").val();
    if(jammasuk.length!=0 && !jammasuk.match(jamValid)){
    	$('#warningx').html('Jam masuk tidak valid.');
	}
}

function CekJamPulang() {
	$('#warningx').html('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jampulang = $("#jampulang").val();
    if(jampulang.length!=0 && !jampulang.match(jamValid)){
		$('#warningx').html('Jam pulang tidak valid.');
	}
}