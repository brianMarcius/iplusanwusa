$(document).ready(function() {						   
	tampildata('1');
	isi_cbocabang();
	isi_cbojns();
});						   

$(function() {
	$("#txttglmulai").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
	$("#txttglakhir").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
});		

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingdiscperitem/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

function isi_cbojns(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingdiscperitem/tampilkan_customer.php',
		success: function(response) {
			$('#cbojns').html(response); 
		}
	});		
}

$("#cbojns").change(function() {
	tampildata('1');
});

$("#cbocabang").change(function() {
	tampildata('1');
});

$("#cari").keyup(function() {
	tampildata('1');
});


function tampildata(pageno){
	
	setCookie("pagech", pageno);
	
	var cabang = $("#cbocabang").val();
	var jns = $("#cbojns").val();
	var cari = $("#cari").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/settingdiscperitem/tampildata.php",
		data	: "cabang="+cabang+
					"&jns="+jns+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);			
			paging(pageno);						
		}
	});
}	

function paging(pageno){
	
	var cabang = $("#cbocabang").val();
	var jns = $("#cbojns").val();
	var cari = $("#cari").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/settingdiscperitem/paging.php",
		data	: "cabang="+cabang+
					"&jns="+jns+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function edittglmulai(kode,kodearea){
	$("#labelkode_updtglmulai").html(kode);
	$("#lblkodecabmul").html(kodearea);
	$("#txttglmulai").val('');
	$("#inputwarning_updtglakhir").hide();
	$("#updtglmulai-modal").modal('toggle');
	$("#updtglmulai-modal").modal('show');
}

$('#btnupdatetglmulai').click(function(){
	updatetglmulai();
});

function updatetglmulai(){
	var kode = $("#labelkode_updtglmulai").html();
	var kodearea = $("#lblkodecabmul").html();
	var jns = $("#cbojns").val();
	var tglmulai = $("#txttglmulai").val();
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingdiscperitem/updatetglmulai.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&jns="+jns+
					"&tglmulai="+tglmulai,
			success: function(response) {
				var pageno = getCookie("pagech");
				tampildata(pageno);
				$("#inputwarning_updtglmulai").hide();
				$("#updtglmulai-modal").modal('hide');
			}
		});	
	}
}

function edittglakhir(kode,kodearea){
	$("#labelkode_updtglakhir").html(kode);
	$("#lblkodecabakh").html(kodearea);
	$("#txttglakhir").val('');
	$("#inputwarning_updtglakhir").hide();
	$("#updtglakhir-modal").modal('toggle');
	$("#updtglakhir-modal").modal('show');
}

$('#btnupdatetglakhir').click(function(){
	updatetglakhir();
});

function updatetglakhir(){
	var kode = $("#labelkode_updtglakhir").html();
	var kodearea = $("#lblkodecabakh").html();
	var jns = $("#cbojns").val();
	var tglakhir = $("#txttglakhir").val();
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingdiscperitem/updatetglakhir.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&jns="+jns+
					"&tglakhir="+tglakhir,
			success: function(response) {
				var pageno = getCookie("pagech");
				tampildata(pageno);
				$("#inputwarning_updtglakhir").hide();
				$("#updtglakhir-modal").modal('hide');
			}
		});	
	}
}

function editdiscpersen(kode,kodearea){
	$("#labelkode_upddiscpersen").html(kode);
	$("#labelkodeareadisc").html(kodearea);
	$("#txtdiscpersen").val('');
	$("#inputwarning_upddiscpersen").hide();
	$("#upddiscpersen-modal").modal('toggle');
	$("#upddiscpersen-modal").modal('show');
}

$('#btnupdate_upddiscpersen').click(function(){
	updatediscpersen();
});

function updatediscpersen(){
	var kode = $("#labelkode_upddiscpersen").html();
	var kodearea = $("#labelkodeareadisc").html();
	var jns = $("#cbojns").val();
	var discpersen = $("#txtdiscpersen").val();
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingdiscperitem/updatediscpersen.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&jns="+jns+
					"&discpersen="+discpersen,
			success: function(response) {	
				var pageno = getCookie("pagech");
				tampildata(pageno);
				$("#inputwarning_upddiscpersen").hide();
				$("#upddiscpersen-modal").modal('hide');
			}
		});	
	}
}