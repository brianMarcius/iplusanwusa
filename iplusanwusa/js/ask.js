$(document).ready(function() {						   
	tampildata('1');
});						   

$(function() {
	$('#bytgl').daterangepicker({format: 'DD/MM/YYYY'});	
});		

$('#searchby').change(function(){
	var pilihan = $('#searchby').val();
	if(pilihan=='1'){
		$("#searchbykode").hide();
		$("#searchbynama").hide();
		$("#searchbytgl").show();
	}else if(pilihan=='2'){					
		$("#searchbytgl").hide();
		$("#searchbykode").hide();
		$("#searchbynama").show();
		$("#bynama").focus();					
	}else {
		$("#searchbytgl").hide();
		$("#searchbykode").show();
		$("#searchbynama").hide();
		$("#bykode").focus();
	}
});	

$('#btnview').click(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl = $("#bytgl").val();
	var nama = $("#bynama").val();
	var kode = $("#bykode").val();
	var searchby = $("#searchby").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/permintaan/tampildata.php",
		data	: "tgl="+tgl+
					"&nama="+nama+
					"&kode="+kode+
					"&searchby="+searchby+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var tgl = $("#bytgl").val();
	var nama = $("#bynama").val();
	var kode = $("#bykode").val();
	var searchby = $('#searchby').val();
	$.ajax({
		type	: "GET",		
		url		: "pages/permintaan/paging.php",
		data	: "tgl="+tgl+
					"&nama="+nama+
					"&kode="+kode+
					"&searchby="+searchby+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function showdetail(kode){		
	$('#info-modal').modal('toggle');
	$("#info-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan/tampil_detailbrg.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayx").show();
			$("#loading-imgx").show();
		},
		success	: function(data){
			$("#retur-modal").modal('hide');		
			$("#overlayx").hide();
			$("#loading-imgx").hide();
			$("#infone").html(data);
		}	
	});
}

function kirim(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newtransout');
			}
		}
	});
}

$('#btnview').click(function(){
	tampildata('1');						 
});	