$(document).ready(function() {	
	document.getElementById("btnconfirm").disabled = true;
	isi_cbocabang();
});		

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingharga/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbobarang").change(function() {
	setbtnconfirm();
})

$("#cbocabang").change(function() {
	isi_cbobarang();
	setbtnconfirm();
})

$("#txtharga").keyup(function() {
	setbtnconfirm();
})

function isi_cbobarang(){
	var kodecabang = $("#cbocabang").val();
	$.ajax({
		type: 'POST', 
		data: "kodecabang="+kodecabang,
		url: 'pages/settingharga/tampilkan_barang.php',
		success: function(response) {
			$('#cbobarang').html(response); 
		}
	});		
}

function setbtnconfirm(){
	var kodecabang = $("#cbocabang").val();
	var kodebrg = $("#cbobarang").val();
	var harga = $("#txtharga").val();

	if(kodecabang.length>0 && kodebrg.length>0 && harga.length>0){
		document.getElementById("btnconfirm").disabled = false;
	}else{
		document.getElementById("btnconfirm").disabled = true;
	}
}

function simpandata(){
	var kodecabang = $("#cbocabang").val();
	var kodebrg = $("#cbobarang").val();
	var harga = $("#txtharga").val();

		$.ajax({
			type	: "POST", 
			url		: "pages/settingharga/simpan.php",
			data	: "kodecabang="+kodecabang+
						"&kodebrg="+kodebrg+
						"&harga="+harga,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				$("#cbocabang").select2('val','');
				$("#cbobarang").select2('val','');
				$("#txtharga").val('');
				setbtnconfirm();
			}	
		});	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=sethrg");
});

$('#btnconfirm').click(function(){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');	
});

$('#btnsave').click(function(){
	simpandata();		
});
