$(document).ready(function() {
	isi_cbojnscustomer();
	settgldefa();
});	

function isi_cbojnscustomer(){
	$.ajax({
		type: 'POST', 
		url: 'pages/lapjualdetailcust/tampilkan_jenis.php',
		success: function(data) {			
			$('#cbojnscustomer').html(data);
			isi_cbocustomer();
		}
	});		
}

function isi_cbocustomer(){
	var jnscustomer = $('#cbojnscustomer').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapjualdetailcust/tampilkan_customer.php',
		data: 'jnscustomer='+jnscustomer,
		success: function(data) {			
			$('#cbocustomer').html(data);
		}
	});		
}


function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lapjualdetailcust/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx1").val(data.tgltrx1);
			$("#tgltrx2").val(data.tgltrx2);				
			settglarea();
		}
	});
}

function settglarea(){
	
	
	var customer 	= $("#cbocustomer").val();
	var tgltrx1 = $("#tgltrx1").val();
	var tgltrx2 = $("#tgltrx2").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapjualdetailcust/tglarea.php",
		data	: "tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2+
				"&customer="+customer,
		dataType: "json",		
		success	: function(data){
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			$("#lbljmlbln").text(data.jmlbulan);
			$("#lblnamacust").text(data.namacust);
			$("#lblkodecust").text(data.kodecust);
			$("#lblalamatcust").text(data.alamatcust);
			tampildata();
		}
	});
}



$('#tgltrx1').change(function(){
	tampildata();
	settglarea();
});

$('#tgltrx2').change(function(){
	tampildata();
	settglarea();
});


$('#cbojnscustomer').change(function(){
	isi_cbocustomer();
});

$('#cbocustomer').change(function(){
	settglarea();
	tampildata();
});

function tampildata(){
	var customer 	= $("#cbocustomer").val();
	var tgltrx1 	= $("#tgltrx1").val();				
	var tgltrx2 	= $("#tgltrx2").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapjualdetailcust/tampildata.php",
		data	: "tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2+
				"&customer="+customer,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail_jl(ID,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=jldetail');
			}
		}
	});
}

/*
function print(){
	
	var customer 	= $("#cbocustomer").val();
	var tgltrx1 	= $("#tgltrx1").val();				
	var tgltrx2 	= $("#tgltrx2").val();	
	alert(customer);
	$.ajax({
		type	: "POST", 
		url		: "pages/lapjualdetailcust/cetakdetailtrx.php",
		data	: "tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2+
				"&customer="+customer,
		success	: function(data){
			alert(data);
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	window.open("pages/lapjualdetailcust/cetakdetailtrx.php");
}
*/