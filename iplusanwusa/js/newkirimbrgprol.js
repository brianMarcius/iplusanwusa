$(document).ready(function() {		
	isi_cbotujuan();
});		

function isi_cbotujuan(){	
	$.ajax({
		type: "POST", 
		url: "pages/pengirimanbrgprolanis/tampilkan_tujuan.php",
		success: function(response) {			
			$("#cbotujuan").html(response); 
			isi_cbopengirim();	
		}
	});		
}

function isi_cbopengirim(){
	var area = $('#cboarea').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/pengirimanbrgprolanis/tampilkan_pengirim.php',
		data: 'area='+area,
		success: function(response) {
			$('#cbopengirim').html(response); 
			tampillistbrg();
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/pengirimanbrgprolanis/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);			
		}
	});
}

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/pengirimanbrgprolanis/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/pengirimanbrgprolanis/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/pengirimanbrg/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function setsatuan(kdsat){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/pengirimanbrgprolanis/tampilkan_satuanx.php',
		data: "kodebrg="+kodebrg+
				"&kodesatuan="+kdsat,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var tujuan  = $("#cbotujuan").val();
	var tgled  	= $("#txted").val();
	
	$("#lblwarning_save").text('');
	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
		return false;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
		return false;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
		return false;
	}
	
	if(tujuan.length==0){
		$('#warningx').html('Tujuan pengiriman tidak valid.');
		var batalkan = true;
		return false;
	}
	
	if(tgled.length==0){
		$('#warningx').html('Tgl Kadaluarsa masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pengirimanbrgprolanis/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&tujuan="+tujuan+
					"&tgled="+tgled+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){
				$("#warningx").html(data);
				var pesansuksesx = $("#warningx").html();
				var pesansukses = pesansuksesx.trim();
				if(pesansukses.length==0){
					tampillistbrg();
					 clearaddbrg();
					 //setbtnconfirm();
				}
			}	
		});
	}
}

function simpandata(){
	var kodetujuan	= $("#cbotujuan").val();
	var pengirim	= $("#cbopengirim").val();
	var tglkirim	= $("#txttglkirim").val();
	var perihal		= $("#cbountuk").val();
	var ket			= $("#txtket").val();
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		$("#cbotujuan").focus();
		var batalkan = true;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		$("#cbopengirim").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pengirimanbrgprolanis/simpan.php",
			data	: "kodetujuan="+kodetujuan+
						"&pengirim="+pengirim+
						"&tglkirim="+tglkirim+
						"&perihal="+perihal+
						"&ket="+ket,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){		
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);	
				$("#lblsukses").text(data.sukses);
			}	
		});	
	}	
}		

$('#info-ok').click(function(){
	var sukses = $("#lblsukses").text();
	if(sukses==1){
		tampillistbrg();
		$("#cbopengirim").val('');
		$("#cbopengirim").select2("val","");
		$("#cbotujuan").val('');
		$("#cbotujuan").select2("val","");
		$("#cbountuk").val('');
		$("#cbountuk").select2("val","0");
		$("#txttglkirim").val('');
		$("#kodeminta").val('');
		window.location.assign("?mod=srtjln");
	}
});

$('#btnconfirm').click(function(){							
	var perihal		= $("#cbountuk").val();
	var kodetujuan	= $("#cbotujuan").val();
	var pengirim	= $("#cbopengirim").val();		
	var tglkirim	= $("#txttglkirim").val();
	
	$('#warningx').html('');
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(tglkirim.length==0){
		$("#lblwarning_save").text('Tanggal belum diisi!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/pengirimanbrgprolanis/datecompare.php",
			data	: "tglkirim="+tglkirim,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Kirim melebihi tanggal sekarang.');
					return false;
				}else {
					if(data.adarec==0){
						$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
						return false;
					}else {
						if(data.stok<0){
							$("#lblwarning_save").text('Jumlah pengiriman melebihi stok yang ada.');
							tampillistbrg();
							return false;
						}else {	
							
							if(data.jmlkirim==0){
								$("#lblwarning_save").text('Jumlah pengiriman masih ada yang nol.');
								tampillistbrg();
								return false;
							}else{							
								if(data.sat==0){
									$("#lblwarning_save").text('Satuan barang ada yg tidak valid.');
									tampillistbrg();
									return false;
								}else {	
									$("#confirm-modal").modal('toggle');
									$("#confirm-modal").modal('show');	
								}	
							}
						}
					}					
				}
			}
		});
	}
	
});

function del(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/pengirimanbrgprolanis/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){			
			tampillistbrg();							
		}
	});
}

function edit(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/pengirimanbrgprolanis/edit.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			$("#cbobarang").val(data.kodebrg);
			$("#barang").val(data.namabrg);
			$("#txtjml").val(data.jmlbrg);
			// $("#txted").val(data.tgled);
			setsatuan(data.kodesatuan);			
		}
	});
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#txted").val('');
}

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

function clearall(){
	$.ajax({
		type	: "POST", 
		url		: "pages/pengirimanbrgprolanis/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();		
			document.getElementById("cbotujuan").disabled = false;
			isi_cbotujuan();
			$('#warningx').html('');
			$("#lblwarning_save").text('');
		}
	});
}

function goBack() {
    window.history.back();
}
