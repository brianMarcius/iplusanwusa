$(document).ready(function() {	
	document.getElementById("btnconfirm").disabled = true;
	tampillistbrg();
	isi_cbosupplier();
	isi_cbobarang();
	isi_cbosatuan();
});		

$(function() {
	$("#txttglbeli").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
	$("#txttglexp").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
});	

function isi_cbosupplier(){
	$.ajax({
		type: 'POST', 
		url: 'pages/pembelian/tampilkan_supplier.php',
		success: function(response) {
			$('#cbosupplier').html(response); 
		}
	});		
}

	
$('#cbosupplier').change(function(){
	var kodesupplier = $('#cbosupplier').val();			
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/getdatasupplier.php",
		data	: "kodesupplier="+kodesupplier,			
		dataType: "json",
		success	: function(data){
			$('#txtalamat').val(data.alamat);
			$('#txtnotelp').val(data.notelp);
			setbtnconfirm();
		}
	});					 
});	

$('#cboviabayar').change(function(){
	var viabayar = $('#cboviabayar').val();

	if(viabayar==2) {
		document.getElementById('cbobank').disabled = false;
		document.getElementById('txtnorek').disabled = false;
		$("#bank_add").show();
		tampilkan_bank();
	}
	else {
		document.getElementById('cbobank').disabled = true;
		document.getElementById('txtnorek').disabled = true;
		$("#bank_add").hide();
		$("#cbobank").select2('val','');
		$("#txtnorek").val('');
	}
});	

function tampilkan_bank() {
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/tampilkan_bank.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbobank").html(data);
		}
	});
}

function myfunction(obj){
	var cek = document.getElementById(obj).checked;
	if(cek==true){
		addpay(obj);
	}else{
		delpay(obj);
	}
}

function addpay(kode){	
	var kodesupplier = $("#txtkodesupplier").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pelunasan/addpay.php",
		data	: "kode="+kode+
					"&kodesupplier="+kodesupplier,
		dataType: "json",
		success	: function(data){
			$("#txtjmlhutang").val(data.jmlhutang);
			getsaldohutang();
		}
	});
}

function delpay(kode){	
	var kodesupplier = $("#txtkodesupplier").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pelunasan/delpay.php",
		data	: "kode="+kode+
					"&kodesupplier="+kodesupplier,
		dataType: "json",
		success	: function(data){
			$("#txtjmlhutang").val(data.jmlhutang);
			getsaldohutang();
		}
	});
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function setbtnconfirm(){
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/getdatacount.php",
		dataType: "json",
		success	: function(data){	
			var nonota = $('#txtnonota').val();	
			var kodesupplier = $('#cbosupplier').val();	
			var tglbeli = $('#txttglbeli').val();
			if(nonota.length>0 && kodesupplier.length>0 && tglbeli.length>0 && data.jmlrec>0){
				document.getElementById("btnconfirm").disabled = false;
			}else{
				document.getElementById("btnconfirm").disabled = true;
			}
		}
	});
}

function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var harga	= $("#txtharga").val();
	var nobatch = $('#txtnobatch').val();
	var tglexp 	= $('#txttglexp').val();
			

	$('#warningx').html('');
	var batalkan = false;

	if(harga<=0){
		$('#warningx').html('Harga tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum di pilih');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(nobatch.length==0){
		$('#warningx').html('No Produksi Batch belum diisi.');
		$("#txtnobatch").focus();
		var batalkan = true;
	}
	
	if(tglexp.length==0){
		$('#warningx').html('Tgl expired belum diisi.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pembelian/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat+	
					"&nobatch="+nobatch+	
					"&tglexp="+tglexp+	
					"&harga="+harga,
			timeout	: 3000,
			success	: function(data){				
 				 tampillistbrg();
				 clearaddbrg();
				 getrptotal();
				 getrphutang();
				 setbtnconfirm();
			}	
		});
	}
}

function getrphutang(){
	var jmlbayar	= $("#txtjmlbayar").val();
	var disc  = $("#txtdisc").val();
	var ppn  = $("#txtppn").val();
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/getrphutang.php",
		data	: "jmlbayar="+jmlbayar+
					"&disc="+disc+
					"&ppn="+ppn,
		dataType: "json",
		success	: function(data){			
			$("#txtsaldohutang").val(data.jmlhutang);
		}
	});
}

function getrpdisc(){
	var total	= $("#txttot").val();
	var disc 	= $("#txtdiscpersen").val();
	var totaldisc = Math.ceil((disc * total) / 100);
	$("#txtdisc").val(totaldisc);
	getrphutang();
}

function getrpppn(){
	var total	= $("#txttot").val();
	var disc 	= $("#txtdisc").val();
	var ppn 	= $("#txtppnpersen").val();
	var totalppn = Math.ceil((ppn * (total - disc))/ 100);
	$("#txtppn").val(totalppn);
	getrphutang();
}

function getrptotal(){
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/getrptotal.php",
		dataType: "json",
		success	: function(data){			
			$("#txttot").val(data.tot);
			getrpdisc();
			getrpppn();
		}
	});
}

function clearaddbrg(){
	$("#cbobarang").select2('val','');
	$("#txtjml").val('');
	$("#cbosatuan").select2('val','');
	$("#txtharga").val('');
	$("#txtnobatch").val('');
	$("#txttglexp").val('');
}

function simpandata(){
	var kodesupplier = $("#cbosupplier").val();
	var nonota	= $("#txtnonota").val();
	var tglbeli		= $("#txttglbeli").val();
	var viabayar	= $("#cboviabayar").val();
	var jmlbayar	= $("#txtjmlbayar").val();
	var jmlhutang	= $("#txtsaldohutang").val();
	var disc 		= $("#txtdisc").val();
	var ppn 		= $("#txtppn").val();
	var batalkan = false;

	if(viabayar==2) {
		var bank = $("#cbobank").val();
		var norek = $("#txtnorek").val();
	}
	else {
		var bank = '';
		var norek = '';
	}
	
	if(kodesupplier.length==0){
		$("#lblwarning_save").text('Nama Supplier belum dipilih.');
		$("#cbosupplier").focus();
		var batalkan = true;
	}
	if(nonota.length==0){
		$("#lblwarning_save").text('No Nota belum di inputkan.');
		$("#nonota").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pembelian/simpan.php",
			data	: "kodesupplier="+kodesupplier+
						"&nonota="+nonota+
						"&tglbeli="+tglbeli+
						"&viabayar="+viabayar+
						"&bank="+bank+
						"&norek="+norek+
						"&jmlbayar="+jmlbayar+
						"&disc="+disc+
						"&ppn="+ppn+
						"&jmlhutang="+jmlhutang,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampillistbrg();
				$("#txtnonota").val('');
				$("#cbosupplier").val('');
				$("#txttglbeli").val('');
				$("#txtalamat").val('');
				$("#txtnotelp").val('');			
				$("#cboviabayar").val('');			
				$("#cbobank").val('');			
				$("#txtnorek").val('');			
				$("#txtjmlbayar").val('0');
				$("#txtsaldohutang").val('0');
				setbtnconfirm();
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newbuy");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				getrphutang();
				getrptotal();
				setbtnconfirm();				
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var kodesupplier = $("#cbosupplier").val();
	var nonota	= $("#txtnonota").val();	
	var jmlbayarx	= $("#txtjmlbayar").val();
	var jmlbayar	= jmlbayarx.substr(0, 2);
	var jmlhutangx	= $("#txtsaldohutang").val();
	var jmlhutang	= jmlhutangx.substr(0, 2);	
	var tglbeli		= $("#txttglbeli").val();
	var viabayar	= $("#cboviabayar").val();

	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(viabayar==2) {
		var bank = $("#cbobank").val();
		var norek = $("#txtnorek").val();

		if(bank.length==0){
			$("#lblwarning_save").text('Nama Bank belum dipilih.');
			var batalkan = true;
			return false;
		}
		if(norek.length==0){
			$("#lblwarning_save").text('No Rekening belum dipilih.');
			var batalkan = true;
			return false;
		}

	}
	
	if(kodesupplier.length==0){
		$("#lblwarning_save").text('Nama Supplier belum dipilih.');
		var batalkan = true;
		return false;
	}
	if(nonota.length==0){
		$("#lblwarning_save").text('No Nota belum di inputkan.');
		var batalkan = true;
		return false;
	}
	if(jmlhutang<0){
		$("#lblwarning_save").text('Jml bayar melebihi jml rupiah beli.');
		var batalkan = true;
		return false;
	}
	if(jmlbayar<=0){
		$("#lblwarning_save").text('Jml bayar masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/pembelian/datecompare.php",
			data	: "tglbeli="+tglbeli,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Nota melebihi tanggal sekarang.');
					return false;
				}
				else {
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

// $('#vendor_add').click(function(){
// 	window.location.assign('?mod=newvend');
// });

$("#txtjmlbayar").keyup(function(){
	 getrphutang();
});

$("#txtdiscpersen").keyup(function(){
	 getrpdisc();
	 getrpppn();
});

$("#txtppnpersen").keyup(function(){
	 getrpppn();
});

$("#txtnonota").keyup(function(){
	setbtnconfirm();
});	

function regnewbank(){
	var bank	 	= $("#txtbank").val();	
	var batalkan 	= false;

	if(bank<=1){
		$('#warningreg').html('Nama bank harus di inputkan!!');
		$("#txtbank").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/pelunasan/addbank.php",
			data	: "bank="+bank,
			timeout	: 3000,
			success	: function(data){
				$('#warningreg').show();
				$('#warningreg').html(data)
				tampilkan_bank();
				$("#txtbank").val('');
				
			}	
		});
	}	
}


$('#bank_add').click(function(){		
	$("#regnewbank-modal").modal('toggle');
	$("#regnewbank-modal").modal('show');
	$('#warningreg').hide();
});

$('#btnadd').click(function(){
		regnewbank();
});