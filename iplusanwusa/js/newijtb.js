$(document).ready(function() {
   isi_cbokaryawan();
	isi_cbocabang();
	isi_cbodivisi();
	isi_cbojabatan();
	tampil_data();
});						   

$(function() {
	$("#txttgllahir").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
	$("[data-mask]").inputmask();
});

function isi_cbokota(){
	$.ajax({
		type: 'POST', 
		url: 'pages/ijintb/tampilkan_kota.php',
		success: function(response) {
			$('#cbokotalahir').html(response); 
		}
	});	
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/ijintb/tampilkan_cabang.php',
		success: function(response) {
			//alert(response);
			$('#cbocabang').html(response); 
		}
	});		
}

function isi_cbodivisi(){
	$.ajax({
		type: 'POST', 
		url: 'pages/ijintb/tampilkan_divisi.php',
		success: function(response) {
			$('#cbodivisi').html(response); 
		}
	});	
}
function isi_cbojabatan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/ijintb/tampilkan_jabatan.php',
		success: function(response) {
			$('#cbojabatan').html(response); 
		}
	});	
}

function simpandata(){
	var karyawan 	= $("#cbokaryawan").val();
	var divisi 		= $("#txtdivisi").val();
	var lamaijin 	= $("#txtlamaijin").val();
	var hari		= $("#cbohari").val();
	var tglmulai 	= $("#txttglmulai").val();
	var tglselesai 	= $("#txttglselesai").val();
	var alasan 	 	= $("#cboalasan").val();
	var keterangan 	= $("#txtket").val();
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/ijintb/simpan.php",
			data	: {
				karyawan:karyawan,
				divisi:divisi,
				tglmulai:tglmulai,
				tglselesai:tglselesai,
				alasan:alasan,
				keterangan:keterangan
			},
	
			success	: function(data){	
				if (data==0) {
					toastr["error"]("Insert failed ","Error");	
					$("#confirm-modal").modal('hide');
				}else{
					toastr["success"]("Insert Success", "Success");
					$("#confirm-modal").modal('hide');
					window.location.assign("?mod=ijtb");
				}			
				/*
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				*/
			}	
		});
	}	
}

$('#btnconfirm').click(function(){
	$("#warningx").text('');
	var karyawan 	= $("#cbokaryawan").val();
	var divisi 		= $("#txtdivisi").val();
	var lamaijin 	= $("#txtlamaijin").val();
	var hari		= $("#cbohari").val();
	var tglmulai 	= $("#txttglmulai").val();
	var tglselesai 	= $("#txttglselesai").val();
	var alasan 	 	= $("#cboalasan").val();
	var keterangan 	= $("#txtket").val();
	var batalkan = false;

	if(karyawan.length==0){
		$("#warningx").text('karyawan masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(divisi.length==0){
		$("#warningx").text('divisi masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(lamaijin.length==0){
		$("#warningx").text('lamaijin masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(hari.length==0){
		$("#warningx").text('hari belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(tglmulai.length==0){
		$("#warningx").text('Tgl mulai masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(tglselesai.length==0){
		$("#warningx").text('Tgl selesai belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(alasan.length==0){
		$("#warningx").text('alasan masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(keterangan.length==0){
		$("#warningx").text('keterangan masih kosong!');
		var batalkan = true;
		return false;
	}
	
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
});

$('#btnok').click(function(){
	window.location.assign('?mod=empl');
});

function tampil_data(){		
	$.ajax({
		type		: "POST",
		url			: "pages/ijintb/cari.php",
		dataType	: "json",
		success		: function(data){
			if(data.jmldata>0){					
				$("#txtidijintb").val(data.idijintb);
				$("#txtnik").val(data.nik);
				$("#txtnama").val(data.nama);
				$("#txtpin").val(data.idfinger);
				$("#cbokotalahir").select2('val',data.tempatlahir);
				$("#txttgllahir").val(data.tgllahir);
				$("#cbojekel").val(data.jnskelamin);
				$("#alamat").val(data.alamat);				
				$("#txttelp").val(data.notelp);
				$("#cbocabang").val(data.kodearea);
				$("#cbodivisi").val(data.kodedivisi);
				$("#cbojabatan").val(data.kodejabatan);
				$("#tglmasuk").val(data.tglmasuk);	
			}
		}
	});
}

$('#divisi_add').click(function(){
	$("#adddivisi-modal").modal('toggle');
	$("#adddivisi-modal").modal('show');
	$('#txtdivisibaru').focus();
});

$('#btnupdatediv').click(function(){
	var divisibaru = $("#txtdivisibaru").val();
	$("#warningdiv").hide();
	var batalkan = false;
	
	if(divisibaru.length==0){
		$("#warningdiv").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/ijintb/adddivisi.php",
			data	: "divisibaru="+divisibaru,
			timeout	: 3000,
			success	: function(data){				
				$("#adddivisi-modal").modal('hide');
				isi_cbodivisi();
			}	
		});
	}		
});

$('#jabatan_add').click(function(){
	$("#addjabatan-modal").modal('toggle');
	$("#addjabatan-modal").modal('show');
	$('#txtjabatanbaru').focus();
});

$('#btnupdatejab').click(function(){
	var jabatanbaru = $("#txtjabatanbaru").val();
	$("#warningjab").hide();
	var batalkan = false;
	
	if(jabatanbaru.length==0){
		$("#warningjab").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/ijintb/addjabatan.php",
			data	: "jabatanbaru="+jabatanbaru,
			timeout	: 3000,
			success	: function(data){				
				$("#addjabatan-modal").modal('hide');
				isi_cbojabatan();
			}	
		});
	}					
});

function isi_cbokaryawan() {
	$.ajax({
		type: 'POST', 
		url: 'pages/ijintb/tampilkan_karyawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response); 
		}
	});		
}

$("#cbokaryawan").change(function(){
	var karyawan = $('#cbokaryawan').val();
	$.ajax({
		type: 'POST',
		url: 'pages/ijintb/tampilkan_divisi.php',
		data:{
			idkaryawan:karyawan
		},
		success:function(response) {
			$('#txtdivisi').val(response);
		}
	})
})

$('#txttglmulai').blur(function(){
	var lamaijin = $('#txtlamaijin').val();
	var hari = $('#cbohari').val();
	var tglmulai = $('#txttglmulai').val();
	if (tglmulai.length!==0) {
		$.ajax({
			type:'POST',
			url: 'pages/ijintb/tampilkan_tglselesai.php',
			data: {
				lamaijin:lamaijin,
				hari:hari,
				tglmulai:tglmulai
			},
			success:function(response){
				$('#txttglselesai').val(response);
			}
		})
	}
})