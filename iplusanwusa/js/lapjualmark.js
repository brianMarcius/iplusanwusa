$(document).ready(function() {
	isi_cbomarketing();					
});	

function isi_cbomarketing(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappenjualanmark/tampilkan_marketing.php',
		success: function(data) {			
			$('#cbomarketing').html(data);
			isi_cbojnscustomer();
		}
	});		
}

function isi_cbojnscustomer(){
	$.ajax({
		type: 'POST', 
		url: 'pages/lappenjualanmark/tampilkan_jenis.php',
		success: function(data) {
			$('#cbojnscustomer').html(data); 
				settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lappenjualanmark/tgldefa.php",
		dataType: "json",		
		success	: function(data){				
			$("#tgltrx").val(data.tgltrx);
			settglarea();
		}
	});
}

function settglarea(){
	
	var tgltrx = $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappenjualanmark/tglarea.php",
		data	: "tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cbomarketing').change(function(){							  
	tampildata();
	settglarea();
});

$('#tgltrx').change(function(){
	tampildata();
	settglarea();
});

$('#cbotrx').change(function(){
	tampildata();
});

$('#cbojnscustomer').change(function(){
	tampildata();
});

function tampildata(){
	
	var marketing = $("#cbomarketing").val();
	var tgltrx = $("#tgltrx").val();
	var jnstrx = $("#cbotrx").val();		
	var jnscustomer = $("#cbojnscustomer").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappenjualanmark/tampildata.php",
		data	: "marketing="+marketing+
				"&tgltrx="+tgltrx+
				"&jnstrx="+jnstrx+
				"&jnscustomer="+jnscustomer,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail_jl(ID,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=jldetailmark');
			}
		}
	});
}