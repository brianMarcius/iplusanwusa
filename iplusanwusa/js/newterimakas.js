$(document).ready(function() {		
	isian_default();
});	

function isian_default(){
	$.ajax({
		type: 'POST', 
		url: 'pages/penerimaankas/setdefault.php',
		dataType: "json",
		success	: function(data){
			$('#txttgltrx').val(data.tglskg);		
		}
	});		
}

function simpandata(){
	var kode		= $("#txtkode").val();
	var idtrx		= $("#cbonamatrx").val();
	var tgltrx 		= $("#txttgltrx").val();
	var jmlrptrx 	= $("#txtjmlrptrx").val();
	var ket 		= $("#txtket").val();
	
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/penerimaankas/simpan.php",
			data	: "kode="+kode+
					"&idtrx="+idtrx+	
					"&tgltrx="+tgltrx+					
					"&jmlrptrx="+jmlrptrx+
					"&ket="+ket,
			timeout	: 3000,
			success	: function(data){			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
			}	
		});
	}	
}

$('#btnconfirm').click(function(){
	
	$("#warningx").text('');
		
	var kode		= $("#txtkode").val();
	var idtrx		= $("#cbonamatrx").val();
	var tgltrx 		= $("#txttgltrx").val();
	var jmlrptrx 	= $("#txtjmlrptrx").val();
	var ket 		= $("#txtket").val();
	
	var batalkan = false;
		
	if(idtrx.length==0){
		$("#warningx").text('Nama transaksi belum dipilih!');
		var batalkan = true;
		return false;
	}	
		
	if(jmlrptrx.length==0){
		$("#warningx").text('Jumlah rupiah transaksi masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(ket.length==0){
		$("#warningx").text('Keterangan harus diisi!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	$("#txtkode").val('');
	$("#cbonamatrx").select2('val','');
	$("#txtjmlrptrx").val('');
	$("#txtket").val('');
});
