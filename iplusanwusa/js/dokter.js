$(document).ready(function() {
	tampildata('1');
});	

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

$('#cari').keyup(function(){
	tampildata('1');
});

function tampildata(pageno){
	setCookie("nopagedr", pageno);
	if(getCookie("pagedr").length>0) {
		var pageno = getCookie("pagedr");
	}
	else {
		var pageno = pageno;
	}
	if(getCookie("caridr").length>0) {
		var kode = getCookie("caridr");
	}
	else {
		var kode = $("#cari").val();		
	}
	
	$.ajax({
		type	: "GET",		
		url		: "pages/dokter/tampildata.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	if(getCookie("caridr").length>0) {
		var kode = getCookie("caridr");
	}
	else {
		var kode = $("#cari").val();		
	}
	$.ajax({
		type	: "GET",		
		url		: "pages/dokter/paging.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			delCookie("caridr");
			delCookie("pagedr");
		}
	});
}

function edit(kode){
	setCookie("pagedr", getCookie("nopagedr"));
	setCookie("caridr", $("#cari").val());
	
	$.ajax({
		type	: "POST", 
		url		: "pages/dokter/lintasform.php",
		data	: "kode="+kode,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newdokter');
			}
		}	
	});	
}

function hapus(kode){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkode").val(kode);
}

$('#btnsave').click(function(){
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/dokter/hapus.php",
		data	: "kode="+kode,
		dataType: "json",
		success	: function(data){
			if(data.result==0){
				$("#confirm-modal").modal('hide');
				tampildata('1');				
			}				
		}	
	});	
});
