$(document).ready(function() {	
	document.getElementById("btnconfirm").disabled = true;
	tampillistbrg();
	isi_cbotujuan();
	isi_cbopemohon();
	isi_cbobarang();
	isi_cbosatuan();
});		

function isi_cbotujuan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/permintaan/tampilkan_tujuan.php',
		success: function(response) {
			$('#cbotujuan').html(response); 
		}
	});		
}

$('#cbotujuan').change(function(){
	setbtnconfirm();
});	

function isi_cbopemohon(){
	$.ajax({
		type: 'POST', 
		url: 'pages/permintaan/tampilkan_pemohon.php',
		success: function(response) {
			$('#cbopemohon').html(response); 
		}
	});		
}

$('#cbopemohon').change(function(){
	setbtnconfirm();
});	

function isi_cbobarang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/permintaan/tampilkan_barang.php',
		success: function(response) {
			$('#cbobarang').html(response); 
		}
	});		
}

function isi_cbosatuan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/permintaan/tampilkan_satuan.php',
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function setbtnconfirm(){
	$.ajax({
		type	: "POST",
		url		: "pages/permintaan/getdatacount.php",
		dataType: "json",
		success	: function(data){	
			var tujuan = $('#cbotujuan').val();	
			var pemohon = $('#cbopemohon').val();	
			
			if(tujuan.length>0 && pemohon.length>0 && data.valid>0){
				document.getElementById("btnconfirm").disabled = false;
			}else{
				document.getElementById("btnconfirm").disabled = true;
			}
		}
	});
}

function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/permintaan/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){				
				tampillistbrg();
				 clearaddbrg();
				 setbtnconfirm();
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#cbobarang").select2("val","");
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#cbosatuan").select2("val","");
}

function simpandata(){
	var kodetujuan	= $("#cbotujuan").val();
	var pemohon		= $("#cbopemohon").val();
	var batalkan = false;
	
	if(kodetujuan.length==0){
		$("#lblwarning_save").text('Ditujukan belum dipilih!');
		$("#cbotujuan").focus();
		var batalkan = true;
	}
	if(pemohon.length==0){
		$("#lblwarning_save").text('Pemohon belum dipilih!');
		$("#cbopemohon").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/permintaan/simpan.php",
			data	: "kodetujuan="+kodetujuan+
						"&pemohon="+pemohon,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampillistbrg();
				$("#cbopemohon").val('');
				$("#cbopemohon").select2("val","");
				$("#cbotujuan").val('');
				$("#cbotujuan").select2("val","");
				setbtnconfirm();
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newminta");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/permintaan/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				setbtnconfirm();				
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var kodetujuan	= $("#cbotujuan").val();
	var pemohon		= $("#cbopemohon").val();		

	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(kodetujuan.length==0){
		$("#lblwarning_save").text('Ditujukan Ke belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(pemohon.length==0){
		$("#lblwarning_save").text('Pemohon belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');	
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});