$(document).ready(function() {
	isi_cboarea();
});

function isi_cboarea(){
	var area = $('#cboarea').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/lapstokhbs/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {
			$('#cboarea').html(data);				
			isi_cbokategori();
		}
	});		
}

function isi_cbokategori(){
	$.ajax({
		type	: "POST", 
		url		: "pages/lapstokhbs/tampilkan_kategorix.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbokategori").html(data);	
			tampildata();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapstokhbs/tglarea.php",
		data	: "area="+area,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cboarea').change(function(){
	settglarea();
});

$('#cbokategori').change(function(){
	tampildata();
});

function tampildata(){	
	
	var area = $("#cboarea").val();
	var kat = $("#cbokategori").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapstokhbs/tampildata.php",
		data	: "area="+area+
				"&kat="+kat,
		success	: function(data){				
			$("#tampildata").html(data);
			
		}
	});
}	
