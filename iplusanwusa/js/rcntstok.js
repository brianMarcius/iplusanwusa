$(document).ready(function() {
	tampildata('1');
});

$(function() {
	$('#bytgl').daterangepicker({format: 'DD/MM/YYYY'});	
});	

function tampildata(pageno){
	var bytgl = $("#bytgl").val();		
		
	$.ajax({
		type	: "GET",		
		url		: "pages/stock/tampildatax.php",
		data	: "tgl="+bytgl+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var bytgl = $("#bytgl").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/stock/pagingx.php",
		data	: "tgl="+bytgl+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

$('#btnview').click(function() {
	tampildata('1');
});