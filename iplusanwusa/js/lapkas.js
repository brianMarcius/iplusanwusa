$(document).ready(function() {
	isi_cboarea();
	settgldefa();
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapkas/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);
			isi_cbojnscustomer();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lapkas/tgldefa.php",
		dataType: "json",		
		success	: function(data){				
			$("#tgltrx").val(data.tgltrx);
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapkas/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}


$('#cboarea').change(function(){							  
	tampildata();
	settglarea();
});

$('#tgltrx').change(function(){
	tampildata();
	settglarea();
});

$('#cbotrx').change(function(){
	tampildata();
});

function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapkas/tampildata.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		success	: function(data){				
			$("#tampildata").html(data);
			setlapkas();
		}
	});
}

function setlapkas(){
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/lapkas/insertlapkas.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",
		success	: function(data){
			//$("#lblalamat").text(data.result);
			if(data.result==1){				
				return true;
				//window.open('media.php'+data.hlink,'_blank');
			}
			
		}	
	});
}

function cetaklapkas(){
	window.open("pages/lapkas/cetaklapkas.php");
}