$(document).ready(function() {	
	tampillistbrg();
	setbtnconfirm();
});		

function setbtnconfirm(){
	var kodepenolakan = $("#txtkodepengiriman").val();
	var pengirim = $("#txtpengirim").val();

	if(kodepenolakan.length>0 && pengirim.length>0){
		document.getElementById("btnconfirm").disabled = false;
	}else{
		document.getElementById("btnconfirm").disabled = true;
	}
}		

$("#txtkodepengiriman").keydown(function(event) {
	if(event.which==13 || event.which==9) {
		tampillistbrg();
		isi_pengirim();
		setbtnconfirm();
		return false;
	}
})

$("#txtkodepengiriman").blur(function() {
	tampillistbrg();
	setbtnconfirm();
})

function tampillistbrg(){
	var penolakan = $("#txtkodepengiriman").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/penolakan/tampillistbrg.php",
		data 	: "penolakan="+penolakan,
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function isi_pengirim(){
	var pengiriman 	= $("#txtkodepengiriman").val();
	
	$.ajax({
		type: 'POST', 
		dataType: 'json',
		data: 'kodekirim='+pengiriman,
		url: 'pages/penolakan/tampilkan_karyawan.php',
		success: function(data) {
			$('#txtpengirim').val(data.pengirim);
		}
	});		
}

function simpandata(){
	var pengirim 	= $("#lblidsender_confirm").text();
	var pengiriman 	= $("#lblpengiriman_confirm").text();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/penolakan/simpan.php",
		data	: "pengirim="+pengirim+
				  "&pengiriman="+pengiriman,
		timeout	: 3000,
		beforeSend	: function(){		
			$("#overlayx").show();
			$("#loading-imgx").show();			
		},
		success	: function(data){
			$("#overlayx").hide();
			$("#loading-imgx").hide();			
			$("#confirm-modal").modal('hide');
			$("#info-modal").modal('toggle');
			$("#info-modal").modal('show');
			$("#infone").html(data);
			$("#cboperusahaan").val('');
			$("#txtpengirim").val('');
			$("#txtkodepengiriman").val('');
			tampillistbrg();
		}	
	});		
}		

$('#btnconfirm').click(function(){
	var pengirim = $("#txtpengirim").val();
	var pengiriman = $("#txtkodepengiriman").val();	

	$.ajax({
		type	: "POST", 
		url		: "pages/penolakan/cari.php",
		data	: "pengiriman="+pengiriman,
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			
			if(data.jmlrec==0) {
				$("#lblwarning_save").text('Tidak ada data.');
				return false;
			}
			else {
				$("#lblwarning_save").text('');	
				$("#confirm-modal").modal('toggle');
				$("#confirm-modal").modal('show');
				$("#lblidsender_confirm").text(pengirim);
				$("#lblpengiriman_confirm").text(pengiriman);
			}
		}	
	});		

});

$('#btnsave').click(function(){
	simpandata();		
});
