$(document).ready(function() {						   
	tampildata('1');
});						   
/*
$(function() {
	$('#caribytgl').daterangepicker({format: 'DD/MM/YYYY'});	
});
*/

$('#caribytgl').change(function(){							
	tampildata('1');						 
});	

$('#caribytgl2').change(function(){
	tampildata('1');						 
});	

$('#cari').keyup(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl = $("#caribytgl").val();
	var tgl2 = $("#caribytgl2").val();
	var kode = $("#cari").val();
	
	if((tgl.length==0 && tgl2.length==0) || (tgl.length>0 && tgl2.length>0)){
	
		$.ajax({
			type	: "GET",		
			url		: "pages/penjualan/tampildata.php",
			data	: "tgl="+tgl+	
						"&tgl2="+tgl2+
						"&kode="+kode+
						"&page="+pageno,
			success	: function(data){
				$("#tampildata").html(data);
				paging(pageno);
			}
		});
		
	}
}	

function paging(pageno){
	var tgl = $("#caribytgl").val();
	var tgl2 = $("#caribytgl2").val();
	var kode = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/penjualan/paging.php",
		data	: "tgl="+tgl+	
					"&tgl2="+tgl2+
					"&kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function showdetail(kode){		
	$('#info-modal').modal('toggle');
	$("#info-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/penjualan/tampil_detailbrg.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayx").show();
			$("#loading-imgx").show();
		},
		success	: function(data){
			$("#retur-modal").modal('hide');		
			$("#overlayx").hide();
			$("#loading-imgx").hide();
			$("#infone").html(data);
		}	
	});
}

function tampillistbrg(){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/penjualan/tampildetail.php",
		//data	: "kode=",
		timeout	: 3000,
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

function retur(ID){		

	$.ajax({
		type	: "POST", 
		url		: "pages/penjualan/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newretsal');
			}
		}
	});
}

function brgretur(kode){		
	$('#retur-modal').modal('toggle');
	$("#retur-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/penjualan/tampil_detailretur.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayy").show();
			$("#loading-imgy").show();
		},
		success	: function(data){
			$("#info-modal").modal('hide');		
			$("#overlayy").hide();
			$("#loading-imgy").hide();
			$("#inforetur").html(data);
		}	
	});
}
