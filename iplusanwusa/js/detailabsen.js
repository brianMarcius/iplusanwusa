$(document).ready(function() {	
	getdataabsen();
});		

function getdataabsen(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/payroll/absen/dataabsensi.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblnotelp").text(data.notelp);
			$("#lblid").text(data.idkaryawan);
			$("#lblnmkry").text(data.nmkaryawan);
			$("#jmlhadir").text(data.jmlhadir);
			$("#jmltelat").text(data.jmltelat);
			$("#jmlijin").text(data.jmlijin);
			$("#jmlalpa").text(data.jmlalpa);
			$("#jmllibur").text(data.jmllibur);
			$("#jmldump").text(data.jmldump);
			tampillistabsen();
		}
	});
}

function tampillistabsen(){
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/absen/tampillistabsen.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblabsen").html(data);
		}
	});
}

function cetakabsendetail(){	
	window.open("pages/payroll/absen/cetakabsendetail.php");
}

function ubahketlain(tanggal,idkaryawan,ketlain,valtelat){
	$("#ubahketlain-modal").modal('toggle');
	$("#ubahketlain-modal").modal('show');
	$("#warningketlain").hide();
	$('#txttanggal').val(tanggal);
	$('#txtidkaryawan').val(idkaryawan);
	$('#txtketlain').val(ketlain);
	$("#cbopottelat").val('');
	if(valtelat==0){
		$("#cbopottelat").hide();
	} else {
		$("#cbopottelat").show();
	}
	$('#txtketlain').focus();
}

$('#btnupdateketlain').click(function(){
	var tanggal = $("#txttanggal").val();
	var idkaryawan = $("#txtidkaryawan").val();
	var ketlain = $("#txtketlain").val();
	var pottelat = $("#cbopottelat").val();
	$("#warningketlain").hide();
	var batalkan = false;
	
	if(ketlain.length==0){
		$("#warningketlain").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/absen/ubahketlain.php",
			data	: "idkaryawan="+idkaryawan+
						"&ketlain="+ketlain+
						"&pottelat="+pottelat+
						"&tanggal="+tanggal,
			timeout	: 3000,
			success	: function(data){				
				$("#ubahketlain-modal").modal('hide');
				tampillistabsen();
			}	
		});
	}					
});