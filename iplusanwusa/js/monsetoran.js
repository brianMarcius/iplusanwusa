$(document).ready(function() {						   
	tampil_setor('1');
	list_cbbulan();
	list_cbtahun();
});			


function list_cbbulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/monitsetoran/cbbulan.php',
		success: function(response) {
			$('#cbobulan').html(response);
		}
	});	
}

function list_cbtahun(){
	$.ajax({
		type: 'POST', 
		url: 'pages/monitsetoran/cbtahun.php',
		success: function(response) {
			$('#cbotahun').html(response);
		}
	});	
}

function tampil_setor(pagex){
	var searchby	= $("#searchby").val();
	var bulan		= $("#cbobulan").val();
	var tahun		= $("#cbotahun").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/monitsetoran/tampildata.php",
		data	: "searchby="+searchby+
					"&bulan="+bulan+
					"&tahun="+tahun+
					"&page="+pagex,
		success	: function(data){
			$("#data_area").html(data);
			paginationx(pagex);
		}
	});
}	

function paginationx(pagex){
	var searchby	= $("#searchby").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/monitsetoran/paging.php",
		data	: "searchby="+searchby+
					"&page="+pagex,
		success	: function(data){
			$("#page_area").html(data);
		}
	});
}

$('#cbobulan').change(function(){
	tampil_setor('1');
});

$('#cbotahun').change(function(){
	tampil_setor('1');
});

$('#searchby').keyup(function(){
	tampil_setor('1');							
});


/*
function showdetail(kdbrg,kdarea){		
	$.ajax({
		type	: "POST", 
		url		: "pages/monitsetoran/lintasform.php",
		data	: "kodearea="+kdarea+
					"&kodebrg="+kdbrg,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=vwhisstok');
			}
		}
	});
}
*/