$(document).ready(function() {
	tampildata('1');
});	

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

$('#cari').keyup(function(){
	tampildata('1');
});

function tampildata(pageno){
	setCookie("nopageempl", pageno);
	if(getCookie("pageempl").length>0) {
		var pageno = getCookie("pageempl");
	}
	else {
		var pageno = pageno;
	}
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}
	
	$.ajax({
		type	: "GET",		
		url		: "pages/supplier/tampildata.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}
	$.ajax({
		type	: "GET",		
		url		: "pages/supplier/paging.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			delCookie("cariempl");
			delCookie("pageempl");
		}
	});
}

function edit(kode_supplier){
	
	setCookie("pageempl", getCookie("nopageempl"));
	setCookie("cariempl", $("#cari").val());
	
	$.ajax({
		type	: "POST", 
		url		: "pages/supplier/lintasform.php",
		data	: "kode="+kode_supplier,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newsuppl');
			}
		}	
	});	
}

function hapussplr(kode_supplier){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkode").val(kode_supplier);
}

function hapus(){
	var kode	 	= $("#txtkode").val();
	var batalkan 	= false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/supplier/hapus.php",
			data	: "kode="+kode,
			dataType: "json",
			success	: function(data){
			$("#confirm-modal").modal('hide');
			tampildata('1');
			}	
		});
	}	
}

$('#btnsave').click(function(){
	hapus();
});
