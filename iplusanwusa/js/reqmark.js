$(document).ready(function() {		
	isi_cboarea();	
});						   

$(function() {
	$('#tgltrx').daterangepicker({format: 'DD/MM/YYYY'});	
});		

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/permintaan_marketing/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);
			tampildata('1');
		}
	});		
}

$('#cboarea').click(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl = $("#tgltrx").val();
	var area = $("#cboarea").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/permintaan_marketing/tampildata.php",
		data	: "tgl="+tgl+	
					"&area="+area+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);			
		}
	});
}	

function paging(pageno){
	var tgl = $("#tgltrx").val();
	var area = $("#cboarea").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/permintaan_marketing/paging.php",
		data	: "tgl="+tgl+	
					"&area="+area+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function showdetail(kode){		
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan_marketing/lintasform.php",
		data	: "kode="+kode,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newdetreq');
			}
		}
	});
}

function goBack() {
    window.history.back();
}