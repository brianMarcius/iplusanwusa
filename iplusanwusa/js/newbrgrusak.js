$(document).ready(function() {	
	// isi_cbotujuan();		
	tampillistbrg();	
	
});		

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/barangrusakso/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/barangrusakso/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/barangrusakso/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){	
				$('#warningx').html(data);
				tampillistbrg();
				 clearaddbrg();
				 //setbtnconfirm();
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
}

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/barangrusakso/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				//setbtnconfirm();				
			}				
		}
	});
}

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$('#btnconfirm').click(function(){		
	var tgl 		= $("#txttgl").val();

	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(tgl.length==0){
		$("#lblwarning_save").text('Tanggal belum diisi!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/barangrusakso/datecompare.php",
			data	: "tgl="+tgl,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl melebihi tanggal sekarang.');
					return false;
				}else {
					if(data.adarec==0){
						$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
						return false;
					}else {
						$("#confirm-modal").modal('toggle');
						$("#confirm-modal").modal('show');	
					}					
				}
			}
		});
	}
	
});

function simpandata(){
	var tgl 		= $("#txttgl").val();
	var ket			= $("#txtket").val();
	var batalkan = false;
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/barangrusakso/simpan.php",
			data	: "tgl="+tgl+
						"&ket="+ket,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampillistbrg();
				$("#txttgl").val('');
			}	
		});	
	}	
}	

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newbrgrusak");
});

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/barangrusakso/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}
$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/barangrusakso/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}
function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}