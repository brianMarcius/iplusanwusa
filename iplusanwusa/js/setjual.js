$(document).ready(function() {	
						  
	isi_cbojnscustomer();	 
	delCookie("nopagex");		
	
});						   

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbojnscustomer(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpenjualan/tampilkan_customer.php',
		success: function(response) {
			$('#cbojns').html(response); 
			tampildata('1');
		}
	});		
}

$("#cbojns").change(function() {
	tampildata('1');
})

$("#cari").keyup(function() {				  
	tampildata('1');
})

function tampildata(pageno){
	
	setCookie("nopagex", pageno);
	var kode = $("#cari").val();
	var jns = $("#cbojns").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/settingpenjualan/tampildata.php",
		data	: "kode="+kode+
					"&jns="+jns+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			var pageno = getCookie("nopagex");
			paging(pageno);
		}
	});
}	

function paging(pageno){
	
	var kode = $("#cari").val();
	var jns = $("#cbojns").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/settingpenjualan/paging.php",
		data	: "kode="+kode+
					"&jns="+jns+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function del(kode){
	$("#kodehapus").val(kode);
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
}

$('#btndel').click(function(){
	hapus();
});

function hapus(){
	var kode = $("#kodehapus").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpenjualan/hapus.php',
		data: "kode="+kode,
		success: function(response) {
			var pageno = getCookie("nopagex");			
			tampildata(pageno);
			$("#confirm-modal").modal('hide');
		}
	});		
}

function editsaldo(kdcust){	
		
	$.ajax({
		type	: "POST", 
		url		: "pages/settingpenjualan/lintasform.php",
		data	: "kode="+kdcust,
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newsetpiut');
			}
		}
	});
	
}

$('#btnupdate_updsaldo').click(function(){
	updatesaldo();
});

function updatesaldo(){
	var kode = $("#labelkode_updsaldo").html();
	var saldo = $("#txtsaldo").val();
	var batalkan = false;
	if(saldo < 0) {
		$("#inputwarning_updsaldo").show();
		$("#inputwarning_updsaldo").text("Nilai yang dimasukkan tidak valid");
		batalkan = true;
		return false;
	}
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingpenjualan/updatesaldo.php',
			data: "kode="+kode+
					"&saldo="+saldo,
			success: function(response) {
				var pageno = getCookie("nopagex");
				tampildata(pageno);					
				$("#inputwarning_updsaldo").hide();
				$("#updsaldo-modal").modal('hide');
			}
		});	
	}
}

function editmaxpiutang(kode){
	$("#labelkode_updmaxpiutang").html(kode);
	$("#txtmaxpiutang").val('');
	$("#inputwarning_updmaxpiutang").hide();
	$("#updmaxpiutang-modal").modal('toggle');
	$("#updmaxpiutang-modal").modal('show');
}

$('#btnupdate_updmaxpiutang').click(function(){
	updatemaxpiutang();
});

function updatemaxpiutang(){
	var kode = $("#labelkode_updmaxpiutang").html();
	var maxpiutang = $("#txtmaxpiutang").val();	
	var batalkan = false;

	if(maxpiutang < 0) {
		$("#inputwarning_updmaxpiutang").show();
		$("#inputwarning_updmaxpiutang").text("Nilai yang dimasukkan tidak valid");
		batalkan = true;
		return false;
	}

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingpenjualan/updatemaxpiutang.php',
			data: "kode="+kode+
					"&maxpiutang="+maxpiutang,
			success: function(response) {
				var pageno = getCookie("nopagex");
				tampildata(pageno);
				$("#inputwarning_updmaxpiutang").hide();
				$("#updmaxpiutang-modal").modal('hide');
			}
		});	
	}
}

function blokir(kode){
	$("#kodeblokir").val(kode);
	$("#blokir-modal").modal('toggle');
	$("#blokir-modal").modal('show');
}

$('#btnblokir').click(function(){
	blokircust();
});

function blokircust(){
	var kode = $("#kodeblokir").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpenjualan/blokir.php',
		data: "kode="+kode,
		success: function(response) {
			var pageno = getCookie("nopagex");			
			tampildata(pageno);
			$("#blokir-modal").modal('hide');
		}
	});		
}

function unblokir(kode){
	$("#kodeunblokir").val(kode);
	$("#unblokir-modal").modal('toggle');
	$("#unblokir-modal").modal('show');
}

$('#btnunblokir').click(function(){
	unblokircust();
});

function unblokircust(){
	var kode = $("#kodeunblokir").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpenjualan/unblokir.php',
		data: "kode="+kode,
		success: function(response) {
			var pageno = getCookie("nopagex");			
			tampildata(pageno);
			$("#unblokir-modal").modal('hide');
		}
	});		
}