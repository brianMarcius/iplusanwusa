$(document).ready(function() {						   
	tampildata('1');
});						   
/*
$(function() {
	$('#tglnota').daterangepicker({format: 'DD/MM/YYYY'});	
});		
*/
$('#tglnota').change(function(){
	tampildata('1');						 
});	

$('#tglnota2').change(function(){
	tampildata('1');						 
});

$('#txtkode').keyup(function(){
	tampildata('1');						 
});	

$('#btnview').click(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl = $("#tglnota").val();
	var tgl2 = $("#tglnota2").val();
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/returpenjualan/tampildata.php",
		data	: "tgl="+tgl+	
					"&tgl2="+tgl2+
					"&kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var tgl = $('#tglnota').val();
	var tgl2 = $("#tglnota2").val();
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/returpenjualan/paging.php",
		data	: "tgl="+tgl+	
					"&tgl2="+tgl2+
					"&kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function showdetail(kode){		
	$('#info-modal').modal('toggle');
	$("#info-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/tampil_detailbrg.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayx").show();
			$("#loading-imgx").show();
		},
		success	: function(data){
			$("#retur-modal").modal('hide');		
			$("#overlayx").hide();
			$("#loading-imgx").hide();
			$("#infone").html(data);
		}	
	});
}

function tampillistbrg(){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/tampildetail.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

function receipt(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newretur');
			}
		}
	});
}

function edit(ID){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newretur');
			}
		}
	});
}

function hapus(ID){			
	$('#confirm-modal').modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkodebeli").val(ID);
}

$('#btnsave').click(function(){
	var kode = $("#txtkodebeli").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/hapus_trx.php",
		data	: "kode="+kode,
		dataType: "json",
		success	: function(data){
			$("#confirm-modal").modal('hide');
			$('#info-modal').modal('toggle');
			$("#info-modal").modal('show');
			$("#infone").html(data.pesan);
			if(data.sukses==1){
				tampildata('1');
			}
		}
	});
});

function showdetail_rj(ID,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=rjdetail');
			}
		}
	});
}

function showdetail_jl(ID,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=jldetail');
			}
		}
	});
}

function cetak(ID,kdcust){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=printrj'); 
			}
		}	
	});	
}