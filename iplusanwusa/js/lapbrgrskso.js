$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapbrgrusakso/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);				
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lapbrgrusakso/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx").val(data.tgltrx);				
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx= $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbrgrusakso/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cboarea').change(function(){	
	settglarea();
});

$('#tgltrx').change(function(){
	settglarea();
});

function tampildata(){	
	var area = $("#cboarea").val();
	var tgltrx= $("#tgltrx").val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbrgrusakso/tampildata.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	
