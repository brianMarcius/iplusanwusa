$(document).ready(function() {
	getdatalembur();
	$("#txtmulai").inputmask("h:s",{ "placeholder": "hh/mm" });
	$("#txtselesai").inputmask("h:s",{ "placeholder": "hh/mm" });
});		

function getdatalembur(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/payroll/rekaplembur/datalembur.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblnotelp").text(data.notelp);
			$("#lblid").text(data.idkaryawan);
			$("#lblnmkry").text(data.nmkaryawan);
			tampil_list_lembur();
		}
	});
}

function tampil_list_lembur(){
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/rekaplembur/tampil_list_lembur.php",
		success	: function(data){
			$("#tbllembur").html(data);
		}
	});
}

function cetaklemburdetail(){	
	window.open("pages/payroll/rekaplembur/cetaklemburdetail.php");
}

function ubahlembur(tanggal,idkaryawan,mulai,selesai,ket){
	$("#ubahlembur-modal").modal('toggle');
	$("#ubahlembur-modal").modal('show');
	$("#warninglembur").text('');
	$('#txttanggal').val(tanggal);
	$('#txtidkaryawan').val(idkaryawan);
	$('#txtmulai').val(mulai);
	$('#txtselesai').val(selesai);
	$('#cboket').val(ket);
	$('#txtmulai').focus();
}

function CekJamMulai() {
	$("#warninglembur").text('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jammulai = $("#txtmulai").val();
    if(jammulai.length!=0 && !jammulai.match(jamValid)){
		$('#warninglembur').text('Jam mulai lembur tidak valid');
	}
}

function CekJamSelesai() {
	$("#warninglembur").text('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jamselesai = $("#txtselesai").val();
    if(jamselesai.length!=0 && !jamselesai.match(jamValid)){
		$('#warninglembur').text('Jam selesai lembur tidak valid');
	}
}

$('#btnupdatelembur').click(function(){
	var tanggal = $("#txttanggal").val();
	var idkaryawan = $("#txtidkaryawan").val();
	var mulai = $("#txtmulai").val();
	var selesai = $("#txtselesai").val();
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
	var ket = $("#cboket").val();

	$("#warninglembur").text('');
	var batalkan = false;
	
	if(mulai.length==0){
		$("#warninglembur").text("Jam mulai lembur belum diisi");
		var batalkan = true;
		return false;
	}

	if(mulai.length!=0 && !mulai.match(jamValid)){
		$('#warninglembur').text('Jam mulai lembur tidak valid');
		var batalkan = true;
		return false;
	}

	if(selesai.length==0){
		$("#warninglembur").text("Jam selesai lembur belum diisi");
		var batalkan = true;
		return false;
	}

	if(selesai.length!=0 && !selesai.match(jamValid)){
		$('#warninglembur').text('Jam selesai lembur tidak valid');
		var batalkan = true;
		return false;
	}

	if(ket.length==0){
		$("#warninglembur").text("Keterangan lembur belum dipilih");
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/rekaplembur/ubahlembur.php",
			data	: "idkaryawan="+idkaryawan+
						"&mulai="+mulai+
						"&selesai="+selesai+
						"&ket="+ket+
						"&tanggal="+tanggal,
			success	: function(){				
				$("#ubahlembur-modal").modal('hide');
				tampil_list_lembur();
			}	
		});
	}					
});

$('#btntutup').click(function() {
	$.ajax({
		type: "POST",		
		url: "pages/payroll/rekaplembur/unsetsess_lembur.php",
		success	: function(){
			window.location.assign('?mod=rkplembur');
		}
	});
});