$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){
	var marketing = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapstokbrgmark/tampilkan_area.php',
		data: 'marketing='+marketing,
		success: function(data) {			
			$('#cboarea').html(data);	
		}
	});		
}

function settglarea(){	
	var area = $("#cboarea").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapstokbrgmark/tglarea.php",
		data	: "area="+area,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			
		}
	});
}

$('#cboarea').change(function(){	
	tampildata();
});

function tampildata(){	
	var area = $("#cboarea").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapstokbrgmark/tampildata.php",
		data	: "marketing="+area,
		success	: function(data){				
			$("#tampildata").html(data);
			settglarea();
		}
	});
}	

function showdetail(kdbrg,kdarea){	
	$.ajax({
		type	: "POST", 
		url		: "pages/historystokmark/lintasform.php",
		data	: "kodearea="+kdarea+
					"&kodebrg="+kdbrg,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=vwhisstok');
			}
		}
	});
}