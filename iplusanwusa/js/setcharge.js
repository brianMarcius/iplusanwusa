$(document).ready(function() {						   
	tampildata('1');
});	

$("#cari").keyup(function() {
	tampildata('1');
})

function tampildata(pageno){
	var cabang = $("#cbocabang").val();
	var cari = $("#cari").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/biayatambahan/tampildata.php",
		data	: "cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
		}
	});
}	

function editnilai(kode){
	$("#labelkode").html(kode);
	$("#txtnilai").val('');
	$("#inputwarning").hide();
	$("#updnilai-modal").modal('toggle');
	$("#updnilai-modal").modal('show');
}

$('#btnupdate').click(function(){
	updatenilai();
});

function updatenilai(){
	var kode = $("#labelkode").text();
	var nilai = $("#txtnilai").val();
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/biayatambahan/updatenilai.php',
			data: "kode="+kode+
					"&nilai="+nilai,
			success: function(response) {
				tampildata('1');
				$("#inputwarning").hide();
				$("#updnilai-modal").modal('hide');
			}
		});	
	}
}