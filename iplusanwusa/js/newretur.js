$(document).ready(function() {	
	//document.getElementById("btnconfirm").disabled = true;
	
	tampildata_retur();
	tampillistbrg();
	$("#cek").prop("checked",true);
	//inserttemp();
});		

function inserttemp(){
	$.ajax({
		type		: "POST",
		url			: "pages/returpembelian/inserttemp.php",
		success		: function(data){
			tampillistbrg();
		}
	});
}

function tampildata_retur(){
	$.ajax({
		type		: "POST",
		url			: "pages/returpembelian/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#txtnonota").val(data.nonota);
			$("#txtnamasupplier").val(data.nama_supplier);
			$("#txtalamat").val(data.alamat);
			$("#txttglretur").val(data.tglretur);
		}
	});
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function isi_cbosatuan(kodebrg){
	$.ajax({
		type: 'POST', 
		data: "kodebrg="+kodebrg,
		url: 'pages/returpembelian/tampilkan_satuan.php',
		success: function(response) {
			$('#cbosat').html(response); 
		}
	});		
}

function simpandata(){
	var tglretur = $('#txttglretur').val();	
	var ket = $('#txtket').val();	
	var batalkan = false;	
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returpembelian/simpan.php",
			data	: "tglretur="+tglretur+
						"&ket="+ket,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampillistbrg();
				$("#txttglretur").val('');
				$("#txtket").val('');				
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newretur");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();				
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var tglretur = $('#txttglretur').val();	
	var ket = $('#txtket').val();	

	$("#lblwarning_save").text('');
	var batalkan = false;
		
	if(tglretur.length==0){
		$("#lblwarning_save").text('Tgl Retur belum diisi.');
		var batalkan = true;
		return false;
	}
	if(ket.length==0){
		$("#lblwarning_save").text('Keterangan belum diisi.');
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/returpembelian/datecompare.php",
			data	: "tglretur="+tglretur,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Retur melebihi tanggal sekarang.');
					return false;
				}
				else {
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});

$('#btnsave').click(function(){
	simpandata();		
});

function editjml(kdbrg){
	$("#updjml-modal").modal('toggle');
	$("#updjml-modal").modal('show');	
	$("#txtjml").val('');
	$('#inputwarning_updjml').hide();
	$("#lblkodebrg_updjml").text(kdbrg);
	$("#txtjml").focus();
}

$("#btnupdate_updjml").click(function(){									   
	var kodebrg		= $("#lblkodebrg_updjml").text();
	var jml			= $("#txtjml").val();	

	var batalkan = false;
	
	if(jml<=0){
		$("#inputwarning_updjml").show();
		$("#inputwarning_updjml").text('Jumlah barang tidak valid.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",		
			url		: "pages/returpembelian/editjml.php",
			data	: "kodebrg="+kodebrg+
						"&jml="+jml,
			dataType: "json",			
			success	: function(data){
				if(data.sukses==0){
					$('#inputwarning_updjml').show();
					$('#inputwarning_updjml').text(data.pesan);
				}else{
					$("#updjml-modal").modal('hide');
					tampillistbrg();
				}				
			}
		});
	}
});

function editsat(kdbrg){
	$("#updsat-modal").modal('toggle');
	$("#updsat-modal").modal('show');	
	isi_cbosatuan(kdbrg);
	$('#inputwarning_updsat').hide();
	$("#lblkodebrg_updsat").text(kdbrg);
}

$("#btnupdate_updsat").click(function(){									   
	var kodebrg		= $("#lblkodebrg_updsat").text();
	var sat			= $("#cbosat").val();	

	var batalkan = false;
	
	if(sat<=0){
		$("#inputwarning_updsat").show();
		$("#inputwarning_updsat").text('Satuan tidak valid.');
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",		
			url		: "pages/returpembelian/editsat.php",
			data	: "kodebrg="+kodebrg+
						"&sat="+sat,
			dataType: "json",			
			success	: function(data){
				if(data.sukses==0){
					$('#inputwarning_updsat').show();
					$('#inputwarning_updsat').text(data.pesan);
				}else{
					$("#updsat-modal").modal('hide');
					tampillistbrg();
				}				
			}
		});
	}
});
