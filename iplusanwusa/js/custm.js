$(document).ready(function() {
	tampildata('1');
	isi_cbokerja();
	isi_cbojenis();
});	

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}
function isi_cbokerja(){
	$.ajax({
		type: 'POST', 
		url: 'pages/customer/tampilkan_pekerjaan2.php',
		success: function(data) {
			$('#cbokerja').html(data);
			if(getCookie("profesiempl").length>0) {
				$("#cbokerja").val(getCookie("profesiempl"));
			}
		}
	});		
}
function isi_cbojenis(){
	$.ajax({
		type: 'POST', 
		url: 'pages/customer/tampilkan_jenis2.php',
		success: function(data) {
			$('#cbojenis').html(data); 
			if(getCookie("jnsempl").length>0) {
				$("#cbojenis").val(getCookie("jnsempl"));
			}
		}
	});		
}
$('#cbokerja').click(function(){
	tampildata('1');
});
$('#cbojenis').click(function(){
	tampildata('1');
});

$('#cari').keyup(function(){
	tampildata('1');
});

function tampildata(pageno){
	setCookie("nopageempl", pageno);
	if(getCookie("pageempl").length>0) {
		var pageno = getCookie("pageempl");
	}
	else {
		var pageno = pageno;
	}
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}
	if(getCookie("profesiempl").length>0) {
		var kerja = getCookie("profesiempl");
	}
	else {
		var kerja = $("#cbokerja").val();		
	}
	if(getCookie("jnsempl").length>0) {
		var jenis = getCookie("jnsempl");
	}
	else {
		var jenis = $("#cbojenis").val();		
	}
		
	$.ajax({
		type	: "GET",		
		url		: "pages/customer/tampildata.php",
		data	: "kode="+kode+
					"&kerja="+kerja+
					"&jenis="+jenis+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}
	if(getCookie("profesiempl").length>0) {
		var kerja = getCookie("profesiempl");
	}
	else {
		var kerja = $("#cbokerja").val();		
	}
	if(getCookie("jnsempl").length>0) {
		var jenis = getCookie("jnsempl");
	}
	else {
		var jenis = $("#cbojenis").val();		
	}
	
	$.ajax({
		type	: "GET",		
		url		: "pages/customer/paging.php",
		data	: "kode="+kode+
					"&kerja="+kerja+
					"&jenis="+jenis+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			delCookie("cariempl");
			delCookie("pageempl");
			delCookie("profesiempl");
			delCookie("jnsempl");
		}
	});
}

function edit(kode_customer){
	
	setCookie("pageempl", getCookie("nopageempl"));
	setCookie("cariempl", $("#cari").val());
	setCookie("profesiempl", $("#cbokerja").val());
	setCookie("jnsempl", $("#cbojenis").val());
	
	$.ajax({
		type	: "POST", 
		url		: "pages/customer/lintasform.php",
		data	: "kode="+kode_customer,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newcustm');
			}
		}	
	});	
}

function hapuscstmr(kode_customer){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkode").val(kode_customer);
}

function hapus(){
	var kode	 	= $("#txtkode").val();
	var batalkan 	= false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/customer/hapus.php",
			data	: "kode="+kode,
			dataType: "json",
			success	: function(data){
			$("#confirm-modal").modal('hide');
			tampildata('1');
			}	
		});
	}	
}

$('#btnsave').click(function(){
	hapus();
});