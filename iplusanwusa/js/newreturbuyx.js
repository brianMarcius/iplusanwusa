$(document).ready(function() {	
	//document.getElementById("btnconfirm").disabled = true;
	isi_cbosupplier();
	isian_default();
	tampillistbrg();
	
	//isi_cbobarang();
	//$("#txttglretur").focus();
});		

//$(function() {
	//$("#txttglbeli").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
	//$("#txttglexp").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
//});	

function isian_default(){
	$.ajax({
		type: 'POST', 
		url: 'pages/returpembelian/setdefault.php',
		dataType: "json",
		success	: function(data){			
			var edit=data.kodebeli;
			if(edit.length>0){
				$('#txtkodebeli').val(data.kodebeli);
				$('#txtnonota').val(data.nonota);
				$('#txttglbeli').val(data.tglbeli);
				$('#txttglretur').val(data.tgljthtempo);
				$('#cbosupplier').select2('val',data.kodesupplier);
				$('#txtalamat').val(data.alamat);
				$('#txtdiscpersen').val(data.disctot);
				$('#txtdisc').val(data.rpdisctot);
				$('#txtppnpersen').val(data.ppn);
				$('#txtppn').val(data.rpppn);
				$('#txtsaldohutang').val(data.rpgrandtot);
			}else{
				$('#txttglbeli').val(data.tglbeli);				
			}
			
		}
	});		
}

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

function EnterBatch(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#txttglexp").focus();		
	}
}

function EnterHarga(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#txtjml").focus();		
	}
}

function EnterJml(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#cbosatuan").focus();				
	}
}

function EnterDisc(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#txtdiscrp_peritem").focus();		
	}
}

function getdiscrp_peritem(){
	var harga	= $("#txtharga").val();
	var jml 	= $("#txtjml").val();
	var disc 	= $("#txtdisc_peritem").val();
	var discrp = Math.ceil((harga * jml * disc) / 100);
	$("#txtdiscrp_peritem").val(discrp);
}

$("#txtdisc_peritem").keyup(function(){
	 getdiscrp_peritem();
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/returpembelian/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
		}
	});		
}

function getkodebrg(kodebrg,namabrg){
	$('#txtkodebrg').val(kodebrg);
	$('#txtnamabrg').val(namabrg);
	isi_cbosatuan('');
	$("#barang-modal").modal('hide');
	$("#txtnobatch").focus();
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/returpembelian/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

$("#txtcaribarang").keyup(function(){
	 tampil_masterbrg(1);
});

$("#txttglexp").change(function(){
	$("#txtharga").focus();
});

function isi_cbosupplier(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/returpembelian/tampilkan_supplier.php',
		success: function(response) {
			$('#cbosupplier').html(response); 
		}
	});		
}
/*
function isi_cbobarang(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/returpembelian/tampilkan_barang.php',
		success: function(response) {
			$('#cbobarang').html(response); 
		}
	});		
}
*/
function isi_cbosatuan(kode){
	var kodebrg = $('#txtkodebrg').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/returpembelian/tampilkan_satuan.php',
		data	: "kodebrg="+kodebrg+
					"&kodesatuan="+kode,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}
/*
$('#cbobarang').change(function(){
	isi_cbosatuan();
});		
*/	
$('#cbosupplier').change(function(){
	var kodesupplier = $('#cbosupplier').val();			
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/getdatasupplier.php",
		data	: "kodesupplier="+kodesupplier,			
		dataType: "json",
		success	: function(data){
			$('#txtalamat').val(data.alamat);
			$('#txtnotelp').val(data.notelp);
			//setbtnconfirm();
		}
	});					 
});	

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function addbarang(){
	
	var kodebeli = $("#txtkodebeli").val();
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var harga	= $("#txtharga").val();
	var disc	= $("#txtdisc_peritem").val();
	var rpdisc	= $("#txtdiscrp_peritem").val();	

	$('#warningx').html('');
	var batalkan = false;	
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum di pilih');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(harga<=0){
		$('#warningx').html('Harga tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(disc<0){
		$('#warningx').html('Disc tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(rpdisc<0){
		$('#warningx').html('Rp Disc tidak valid.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returpembelian/addbarang.php",
			data	: "kodebeli="+kodebeli+	
					"&kodebrg="+kodebrg+
					"&jml="+jml+
					"&kodesatuan="+kodesat+	
					"&harga="+harga+
					"&disc="+disc+
					"&rpdisc="+rpdisc,
			dataType: "json",
			success	: function(data){	
				if(data.sukses==1){
					 tampillistbrg();
					 clearaddbrg();
					 getrphutang();
				}else{
					$('#warningx').text(data.pesan);
				}
			}	
		});
	}
}

function getrphutang(){
	var disc  = $("#txtdiscpersen").val();
	var ppn  = $("#txtppnpersen").val();
	//alert('PPN :'+ppn);
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/getrphutang.php",
		data	: "disc="+disc+
					"&ppn="+ppn,
		dataType: "json",
		success	: function(data){
			//alert('RP PPN :'+data.rpppn);
			$("#txtdisc").val(data.rpdisc);
			$("#txtppn").val(data.rpppn);
			$("#txtsaldohutang").val(data.jmlhutang);			
		}
	});
}

function clearaddbrg(){
	$("#txtnamabrg").val('');
	$("#txtkodebrg").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	isi_cbosatuan('');
	$("#txtharga").val('');
	$("#txtdisc_peritem").val('');
	$("#txtdiscrp_peritem").val('');	
}

function simpandata(){
	var kodesupplier = $("#cbosupplier").val();
	var koderetur	 = $("#txtkoderetur").val();
	var tglretur	= $("#txttglretur").val();	
	var kodebeli	 = $("#txtkodebeli").val();
	var disc 		= $("#txtdiscpersen").val();
	var rpdisc 		= $("#txtdisc").val();
	var ppn 		= $("#txtppnpersen").val();	
	var rpppn 		= $("#txtppn").val();
	var grandtot	= $("#txtsaldohutang").val();
	var ket 		= $("#txtket").val();
	var batalkan = false;	
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returpembelian/simpan.php",
			data	: "kodesupplier="+kodesupplier+
						"&koderetur="+koderetur+
						"&tglretur="+tglretur+
						"&kodebeli="+kodebeli+
						"&disc="+disc+
						"&rpdisc="+rpdisc+
						"&ppn="+ppn+
						"&rpppn="+rpppn+
						"&grandtot="+grandtot+
						"&ket="+ket,
			dataType: "json",
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);
				tampillistbrg();				
				if(data.lanjut==1){
					$("#txtnonota").val('');
					$("#txtkodebeli").val('');
					$("#txttglbeli").val('');
					$("#txtkoderetur").val('');
					$("#txttglretur").val('');
					$("#cbosupplier").select2('val','');				
					$("#txtalamat").val('');
					$("#txtdiscpersen").val('0');
					$("#txtdisc").val('0');
					$("#txtppnpersen").val('0');	
					$("#txtppn").val('0');
					$("#txtsaldohutang").val('0');	
					$("#txtket").val('');
				}
			}	
		});	
	}	
}		

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				getrphutang();			
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var tglretur	= $("#txttglretur").val();	
	var ket 		= $("#txtket").val();	
	
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(tglretur.length==0){
		$("#lblwarning_save").text('Tgl Retur belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(ket.length==0){
		$("#lblwarning_save").text('Alasan Retur harus diisi.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/returpembelian/datecompare.php",
			data	: "tglretur="+tglretur,
			dataType: "json",
			success	: function(data){
				if(data.lanjut==0){
					$("#lblwarning_save").text(data.pesan);
					return false;
				}else{
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$("#txtdiscpersen").keyup(function(){
	getrphutang();
});

$("#txtppnpersen").keyup(function(){
	 getrphutang();
});

function regnewbank(){
	var bank	 	= $("#txtbank").val();	
	var batalkan 	= false;

	if(bank<=1){
		$('#warningreg').html('Nama bank harus di inputkan!!');
		$("#txtbank").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/pelunasan/addbank.php",
			data	: "bank="+bank,
			timeout	: 3000,
			success	: function(data){
				$('#warningreg').show();
				$('#warningreg').html(data)
				tampilkan_bank();
				$("#txtbank").val('');
				
			}	
		});
	}	
}


$('#bank_add').click(function(){		
	$("#regnewbank-modal").modal('toggle');
	$("#regnewbank-modal").modal('show');
	$('#warningreg').hide();
});

$('#btnadd').click(function(){
	regnewbank();
});

function goBack() {
    window.history.back();
}

function edit(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/edit.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){	
			$('#txtkodebrg').val(data.kodebrg);
			$('#txtnamabrg').val(data.namabrg);
			$('#txtharga').val(data.hargabrg);
			$('#txtjml').val(data.jmlbrg);
			isi_cbosatuan(data.kodesatuan);
			$('#txtdisc_peritem').val(data.disc);
			$('#txtdiscrp_peritem').val(data.rpdisc);
							
		}
	});
}
