$(document).ready(function() {						   
	tampil_stock('1');	
});			

function tampil_stock(pagex){
	var searchby	= $("#searchby").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/stock/tampildata.php",
		data	: "searchby="+searchby+
					"&page="+pagex,
		success	: function(data){
			$("#data_area").html(data);
			paginationx(pagex);
		}
	});
}	

function paginationx(pagex){
	var searchby	= $("#searchby").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/stock/paging.php",
		data	: "searchby="+searchby+
					"&page="+pagex,
		success	: function(data){
			$("#page_area").html(data);
		}
	});
}

$('#searchby').keyup(function(){
	tampil_stock('1');							
});

function recent(kodebrg,area){
	var kode = kodebrg+"-"+area;

	$.ajax({
		type	: "POST",		
		url		: "pages/stock/lintasform.php",
		data	: "kode="+kode,
		success	: function(data){
			window.location.assign('?mod=recentstock');
		}
	});
}