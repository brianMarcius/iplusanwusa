$(document).ready(function(){
	if(getCookie("tgl_lembur_1").length>0) {
		$('#tanggal1').val(getCookie("tgl_lembur_1"));
		delCookie("tgl_lembur_1");
	}
	if(getCookie("tgl_lembur_2").length>0) {
		$('#tanggal2').val(getCookie("tgl_lembur_2"));
		delCookie("tgl_lembur_2");
	}
	list_perusahaan();
	tampillembur();
});

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function list_perusahaan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/rekaplembur/list_perusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
			if(getCookie("areatmp").length>0) {
				$('#cboarea').val(getCookie("areatmp"));
				delCookie("areatmp");
			}
		}
	});	
}

function tampillembur(){
	if(getCookie("tgl_lembur_1").length>0) {
		var tanggal1 = getCookie("tgl_lembur_1");
	}
	else {
		var tanggal1 = $("#tanggal1").val();		
	}

	if(getCookie("tgl_lembur_2").length>0) {
		var tanggal2 = getCookie("tgl_lembur_2");
	}
	else {
		var tanggal2 = $("#tanggal2").val();		
	}

	if(getCookie("areatmp").length>0) {
		var unit = getCookie("areatmp");
	}
	else {
		var unit = $("#cboarea").val();		
	}

	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/rekaplembur/tampil_lembur.php',
		data: {
          	tanggal1: tanggal1,
          	tanggal2: tanggal2,
          	unit: unit
        },
		success: function(response) {
			$('#tbllembur').html(response); 
		}
	});	
}

function cetaklembur(){
	var tanggal1 = $("#tanggal1").val();
	var tanggal2 = $("#tanggal2").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(tanggal1.length==0){
		alert("Tanggal 1 belum dipilih");
		var batalkan = true;
		return false;
	}

	if(tanggal2.length==0){
		alert("Tanggal 2 belum dipilih");
		var batalkan = true;
		return false;
	}

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type: "POST",		
			url: "pages/payroll/rekaplembur/setsess_cetak.php",
			data: {
				tanggal1: tanggal1,
				tanggal2: tanggal2,
				area: area
			}
		});

		window.open("pages/payroll/rekaplembur/cetak_lembur.php");
	}
}

function showdetail(idkaryawan,kodearea,tgl1,tgl2){		
	setCookie("tgl_lembur_1", $("#tanggal1").val());
	setCookie("tgl_lembur_2", $("#tanggal2").val());
	setCookie("areatmp", $("#cboarea").val());
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/rekaplembur/setsess_lembur.php",
		data	: {
					idkaryawan: idkaryawan,
					kodearea: kodearea,
					tgl1: tgl1,
					tgl2: tgl2
		},
		success	: function(){
			window.location.assign('?mod=detlembur');
		}
	});
}