$(document).ready(function() {
	isi_cboasal();	
});	

function isi_cboasal(){
	var asal = $('#cboasal').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappengiriman/tampilkan_asal.php',
		data: 'area='+asal,
		success: function(data) {			
			$('#cboasal').html(data);	
			isi_cbotujuan();
			settgldefa();
		}
	});		
}

function isi_cbotujuan(){
	var tujuan = $('#cbotujuan').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappengiriman/tampilkan_tujuan.php',
		data: 'area='+tujuan,
		success: function(data) {			
			$('#cbotujuan').html(data);			
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lappengiriman/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx1").val(data.tgltrx1);
			$("#tgltrx2").val(data.tgltrx2);				
			settglarea();
		}
	});
}

function settglarea(){
	
	var asal = $("#cboasal").val();
	var tujuan = $("#cbotujuan").val();
	var tgltrx1 = $("#tgltrx1").val();	
	var tgltrx2 = $("#tgltrx2").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappengiriman/tglarea.php",
		data	: "asal="+asal+
				"&tujuan="+tujuan+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cboasal').click(function(){	
	tampildata();
});

$('#cbotujuan').click(function(){
	tampildata();
});

$('#tgltrx1').change(function(){
	tampildata();
});

$('#tgltrx2').change(function(){
	tampildata();
});

function tampildata(){
	
	var asal = $("#cboasal").val();
	var tujuan = $("#cbotujuan").val();	
	var tgltrx1 = $("#tgltrx1").val();
	var tgltrx2 = $("#tgltrx2").val();
		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappengiriman/tampildata.php",
		data	: "asal="+asal+
				"&tujuan="+tujuan+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function detail(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/lappengiriman/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=transdetail');
			}
		}
	});
}