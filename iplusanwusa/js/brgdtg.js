$(document).ready(function() {	
	setdefa();	
});						   

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function setdefa(){
	$.ajax({
		type: 'POST', 
		url: 'pages/barangdtg/setdefa.php',
		dataType: "json",		
		success: function(data) {
			$('#tgltrx').val(data.tgltrx); 
			tampildata('1');
		}
	});		
}

$("#tgltrx").change(function() {
	tampildata('1');
});

$("#cari").keyup(function(){
	tampildata('1');						 
});	

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampildata(pageno);						 
});

function tampildata(pageno){	
	
	setCookie("pagech", pageno);
	var tgltrx = $("#tgltrx").val();
	var cari = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/barangdtg/tampildata.php",
		data	: "tgltrx="+tgltrx+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			var pageno = getCookie("pagech");
			paging(pageno);
		}
	});
}	

function paging(pageno){	
	
	var tgltrx = $("#tgltrx").val();
	var cari = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/barangdtg/paging.php",
		data	: "tgltrx="+tgltrx+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}