$(document).ready(function() {
	getdatapengiriman();	
});	

function getdatapengiriman(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/presuratjalan/datapengiriman.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lbltujuan").text(data.unittujuan);
			$("#lblalamattujuan").text(data.alamattujuan);
			$("#lbltelptujuan").text(data.notelptuj);
			$("#lblno").text(data.nosrtjalan);
			$("#lbltgltrx").text(data.tglkirim);
			$("#lblstaffgdg").text(data.staffgudang);
			$("#lblpengirim").text(data.pengirim);
			tampildata();
		}
	});
}

function tampildata(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/presuratjalan/tampildata.php",
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function cetaksiapkirim(){
	$.ajax({
		type	: "POST", 
		url		: "pages/presuratjalan/cetaksrtsiapkirim.php",
		success	: function(data){
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	window.open("pages/presuratjalan/cetaksrtsiapkirim.php");
}