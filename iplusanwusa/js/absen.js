$(document).ready(function(){
	list_cbperusahaan();
	list_cbbulan();
	list_cbtahun();
	isi_cbokaryawan();
	gettglskg();
	tampilabsen1();
	tampilabsen2();
});

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function gettglskg(){
	//var	tglmulai = $("#tglmulai").val();

	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/absen/gettglskg.php",
		//data	: "tglmulai="+tglmulai,
		dataType: "json",
		success	: function(data){
			$('#tglkerja').val(data.tglskg);
		}	
	});
}

function tampilabsen1(){	
	var	tglkerja = $("#tglkerja").val();
	var	unit = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/absen/tampil_absen.php',
		data: {
          	tanggal: tglkerja,
          	unit: unit
        },
		success: function(response) {
			$('#tblabsensi1').html(response); 
		}
	});	
}

function tampilabsen2(){
	if(getCookie("bulantmp").length>0) {
		var bulan = getCookie("bulantmp");
	}
	else {
		var bulan = $("#cbbulan").val();		
	}
	if(getCookie("tahuntmp").length>0) {
		var tahun = getCookie("tahuntmp");
	}
	else {
		var tahun = $("#cbtahun").val();		
	}
	if(getCookie("areatmp").length>0) {
		var unit = getCookie("areatmp");
	}
	else {
		var unit = $("#cboarea").val();		
	}
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/absen/tampil_absen2.php',
		data: {
          	bulan: bulan,
          	tahun: tahun,
          	unit: unit
        },
		success: function(response) {
			$('#tblabsensi2').html(response); 
		}
	});	
}

function cetakabsen1(){
	var tgl	= $("#tglkerja").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(tgl.length==0){
		alert("Tanggal belum di input");
		var batalkan = true;
		return false;
	}

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		window.open("pages/payroll/absen/cetakabsen1.php");
	}
}

function cetakabsen2(){
	var bulan = $("#cbbulan").val();
	var tahun = $("#cbtahun").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(bulan.length==0){
		alert("Bulan belum dipilih");
		var batalkan = true;
		return false;
	}

	if(tahun.length==0){
		alert("Tahun belum dipilih");
		var batalkan = true;
		return false;
	}

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		window.open("pages/payroll/absen/cetakabsen2.php");
	}
}

function clearall(){
	$.ajax({
		timeout	: 3000,
		success	: function(data){
			$("#cbokaryawan").select2('val','');
			$("#txtarea").val('');
			$("#kodearea").val('');
			$("#tglabsen").val('');
			$("#cboshift").val('');
			$("#jammasuk").val('');
			$("#jampulang").val('');
			$("#cboket").val('');
			$("#ketlain").val('');
		}
	});
}

function simpanabsen(){
	var idkaryawan	= $("#cbokaryawan").val();
	var tglkerja	= $("#tglabsen").val();
	var shift		= $("#cboshift").val();
	var mulai		= $("#jammasuk").val();	
	var selesai		= $("#jampulang").val();		
	var unit		= $("#kodearea").val();
	var status		= $("#cboket").val();
	var ketlain		= $("#ketlain").val();
	var batalkan = false;	
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/absen/simpanabsen.php",
			data: {
          		idkaryawan: idkaryawan,
          		tglkerja: tglkerja,
          		shift: shift,
          		mulai: mulai,
          		selesai: selesai,
          		unit: unit,
          		status: status,
          		ketlain: ketlain
        	},
			timeout	: 3000,
			success	: function(data){	
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);		
			}	
		});	
	}	
}

function isi_cbokaryawan(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/absen/tampil_karyawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response);	
		}
	});		
}

$('#cbokaryawan').change(function(){
	$("#jammasuk").inputmask("h:s",{ "placeholder": "hh/mm" });
	$("#jampulang").inputmask("h:s",{ "placeholder": "hh/mm" });
	var idkaryawan = $('#cbokaryawan').val();			
	$.ajax({
		type	: "POST",
		url		: "pages/payroll/absen/getdatakaryawan.php",
		data	: "idkaryawan="+idkaryawan,			
		dataType: "json",
		success	: function(data){
			$('#txtarea').val(data.unit);
			$('#kodearea').val(data.kodearea);
			list_shift();
			//setbtnconfirm();
		}
	});					 
});	

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

$('#btnconfirm').click(function(){
	var idkaryawan	= $("#cbokaryawan").val();
	var tglkerja	= $("#tglabsen").val();
	var shift		= $("#cboshift").val();
	var mulai		= $("#jammasuk").val();	
	var selesai		= $("#jampulang").val();		
	var unit		= $("#kodearea").val();
	var status		= $("#cboket").val();
	var ketlain		= $("#ketlain").val();	
	var jamValid 	= /([01]\d|2[0-3]):([0-5]\d)/;
	
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(idkaryawan.length==0){
		$("#lblwarning_save").text('Nama Karyawan belum di pilih.');
		var batalkan = true;
		return false;
	}
	
	if(tglkerja.length==0){
		$("#lblwarning_save").text('Tanggal belum di inputkan.');
		var batalkan = true;
		return false;
	}

	if(status=='HADIR' && shift.length==0){
		$("#lblwarning_save").text('Shift belum di pilih.');
		var batalkan = true;
		return false;
	}
	
	if(status=='HADIR' && mulai.length==0){
		$("#lblwarning_save").text('Jam masuk belum di inputkan.');
		var batalkan = true;
		return false;
	}

	if(mulai.length!=0 && !mulai.match(jamValid)){
		$("#lblwarning_save").text('Jam masuk tidak valid.');
		var batalkan = true;
		return false;
	}
	
	if(status=='HADIR' && selesai.length==0){
		$("#lblwarning_save").text('Jam pulang belum di inputkan.');
		var batalkan = true;
		return false;
	}

	if(selesai.length!=0 && !selesai.match(jamValid)){
		$("#lblwarning_save").text('Jam pulang tidak valid.');
		var batalkan = true;
		return false;
	}

	if(unit.length==0){
		$("#lblwarning_save").text('Kode area belum di inputkan.');
		var batalkan = true;
		return false;
	}

	if(status.length==0){
		$("#lblwarning_save").text('Keterangan belum di pilih.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			success	: function(data){
				$("#confirm-modal").modal('toggle');
				$("#confirm-modal").modal('show');	
			}
		});
	}
});

$('#btnsave').click(function(){
	simpanabsen();		
});

$('#btnokinfo').click(function(){
	$("#info-modal").modal('hide');
	isi_cbokaryawan();
	$("#cbokaryawan").select2('val','');
	$("#txtarea").val('');
	$("#kodearea").val('');
	$("#tglabsen").val('');
	$("#cboshift").val('');
	$("#jammasuk").val('');
	$("#jampulang").val('');
	$("#cboket").val('');
	$("#ketlain").val('');
	clearall();
	window.location("media.php?mod=inputabsen");
});

function list_cbperusahaan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/absen/list_perusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
			if(getCookie("areatmp").length>0) {
				$('#cboarea').val(getCookie("areatmp"));
				delCookie("areatmp");
			}
		}
	});	
}

function list_cbbulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbbulan.php',
		success: function(response) {
			$('#cbbulan').html(response);
			if(getCookie("bulantmp").length>0) {
				$('#cbbulan').val(getCookie("bulantmp"));
				delCookie("bulantmp");
			}
		}
	});	
}

function list_cbtahun(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbtahun.php',
		success: function(response) {
			$('#cbtahun').html(response);
			if(getCookie("tahuntmp").length>0) {
				$('#cbtahun').val(getCookie("tahuntmp"));
				delCookie("tahuntmp");
			} 
		}
	});	
}

function list_shift(){
	var	kodearea = $("#kodearea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/listshift.php',
		data: {
        	kodearea: kodearea
        },
		success: function(response) {
			$('#cboshift').html(response); 
		}
	});	
}

function showdetail(kode){		
	setCookie("bulantmp", $("#cbbulan").val());
	setCookie("tahuntmp", $("#cbtahun").val());
	setCookie("areatmp", $("#cboarea").val());
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/absen/lintasform.php",
		data	: "kode="+kode,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=detabsen');
			}
		}
	});
}

function ubahketlain(tanggal,idkaryawan,ketlain,valtelat){
	$("#ubahketlain-modal").modal('toggle');
	$("#ubahketlain-modal").modal('show');
	$("#warningketlain").hide();
	$('#txttanggal').val(tanggal);
	$('#txtidkaryawan').val(idkaryawan);
	$('#txtketlain').val(ketlain);
	$("#cbopottelat").val('');
	if(valtelat==0){
		$("#cbopottelat").hide();
	} else {
		$("#cbopottelat").show();
	}
	$('#txtketlain').focus();
}

$('#btnupdateketlain').click(function(){
	var tanggal = $("#txttanggal").val();
	var idkaryawan = $("#txtidkaryawan").val();
	var ketlain = $("#txtketlain").val();
	var pottelat = $("#cbopottelat").val();
	$("#warningketlain").hide();
	var batalkan = false;
	
	if(ketlain.length==0){
		$("#warningketlain").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/absen/ubahketlain.php",
			data	: "idkaryawan="+idkaryawan+
						"&ketlain="+ketlain+
						"&pottelat="+pottelat+
						"&tanggal="+tanggal,
			timeout	: 3000,
			success	: function(data){				
				$("#ubahketlain-modal").modal('hide');
				tampilabsen1();
			}	
		});
	}					
});

function CekJamMasuk() {
	$("#lblwarning_save").text('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jammasuk = $("#jammasuk").val();
    if(jammasuk.length!=0 && !jammasuk.match(jamValid)){
    	$("#lblwarning_save").text('Jam masuk tidak valid.');
	}
}

function CekJamPulang() {
	$("#lblwarning_save").text('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jampulang = $("#jampulang").val();
    if(jampulang.length!=0 && !jampulang.match(jamValid)){
		$("#lblwarning_save").text('Jam pulang tidak valid.');
	}
}

function goBack() {
    delCookie("bulantmp");
    delCookie("tahuntmp");
    delCookie("areatmp");
    window.history.back();
}

function isishift(kodearea,tanggal,idkaryawan,mulai,selesai){
	$("#isishift-modal").modal('toggle');
	$("#isishift-modal").modal('show');
	$("#warningshift").hide();
	$('#kodearea').val(kodearea);
	$('#txttanggals').val(tanggal);
	$('#txtidkaryawans').val(idkaryawan);
	$('#txtmulai').val(mulai);
	$('#txtselesai').val(selesai);
	list_shift();
}

$('#btnisishift').click(function(){
	var kodearea = $("#kodearea").val();
	var tanggal = $("#txttanggals").val();
	var idkaryawan = $("#txtidkaryawans").val();
	var mulai = $("#txtmulai").val();
	var selesai = $("#txtselesai").val();
	var shift = $("#cboshift").val();
	$("#warningshift").hide();
	var batalkan = false;
	
	if(shift==0){
		$("#warningshift").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/absen/isishift.php",
			data	: "idkaryawan="+idkaryawan+
						"&kodearea="+kodearea+
						"&mulai="+mulai+
						"&selesai="+selesai+
						"&shift="+shift+
						"&tanggal="+tanggal,
			timeout	: 3000,
			success	: function(data){				
				$("#isishift-modal").modal('hide');
				tampilabsen1();
			}	
		});
	}					
});