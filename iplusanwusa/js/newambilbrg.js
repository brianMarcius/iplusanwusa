$(document).ready(function() {	
	isi_cbomarketing();	
	tampillistbrg();
});		

function isi_cbomarketing(){
	
	$.ajax({
		type: 'POST', 
		url: 'pages/pengambilanbrg/tampilkan_marketing.php',
		success: function(response) {
			$('#cbomarketing').html(response); 
		}
	});		
}


function isi_cbosatuan(){
	var kodebrg = $('#txtkodebrg').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/pengambilanbrg/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function setsatuan(kdsat){
	var kodebrg = $('#txtkodebrg').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/pengambilanbrg/tampilkan_satuanx.php',
		data: "kodebrg="+kodebrg+
				"&kodesatuan="+kdsat,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function tampillistbrg(){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pengambilanbrg/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);			
		}
	});
}

function addbarang(){
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	
	$("#lblwarning_save").text('');
	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
		return false;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
		return false;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#txtkodebrg").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pengambilanbrg/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){
				$("#warningx").html(data);
				var pesansuksesx = $("#warningx").html();
				var pesansukses = pesansuksesx.trim();
				if(pesansukses.length==0){
					tampillistbrg();
					 clearaddbrg();
				}
			}	
		});
	}
}

function clearaddbrg(){
	$("#txtkodebrg").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
}

function simpandata(){
	var marketing	= $("#cbomarketing").val();
	var tglambil	= $("#txttglambil").val();
	var ket			= $("#txtket").val();
	var batalkan = false;
	
	if(marketing.length==0){
		$("#lblwarning_save").text('Marketing belum dipilih!');
		$("#cbomarketing").focus();
		var batalkan = true;
	}
	
	if(tglambil.length<3){
		$('#lblwarning_save').text('Tgl Pengambilan tidak valid.');
		$("#txttglambil").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pengambilanbrg/simpan.php",
			data	: "marketing="+marketing+
						"&tglambil="+tglambil+
						"&ket="+ket,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){	
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);	
				$("#lblsukses").text(data.sukses);
			}	
		});	
	}	
}		

$('#info-ok').click(function(){
	var sukses = $("#lblsukses").text();
	if(sukses==1){
		tampillistbrg();
		$("#cbomarketing").val('');
		$("#cbomarketing").select2("val","");
		$("#txttglambil").val('');
		window.location.assign("?mod=ttb");
	}
});

function del(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/pengambilanbrg/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){			
			tampillistbrg();							
		}
	});
}

function edit(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/pengambilanbrg/edit.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			$("#txtkodebrg").val(data.kodebrg);
			$("#barang").val(data.namabrg);
			$("#txtjml").val(data.jmlbrg);
			setsatuan(data.kodesatuan);			
		}
	});
}

$('#btnconfirm').click(function(){
	var marketing	= $("#cbomarketing").val();
	var tglambil	= $("#txttglambil").val();
	
	$('#warningx').html('');
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(marketing.length==0){
		$("#lblwarning_save").text('Marketing belum dipilih!');
		$("#cbomarketing").focus();
		var batalkan = true;
	}
	
	if(tglambil.length<3){
		$('#lblwarning_save').text('Tgl Pengambilan tidak valid.');
		$("#txttglambil").focus();
		var batalkan = true;
		return false;
	}
		
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/pengambilanbrg/datecompare.php",
			data	: "tglambil="+tglambil,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Kirim melebihi tanggal sekarang.');
					return false;
				}else {
					if(data.adarec==0){
						$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
						return false;
					}else {
						if(data.stok<0){
							$("#lblwarning_save").text('Jumlah pengiriman melebih stok yang ada.');
							tampillistbrg();
							return false;
						}else {	
							
							if(data.jmlkirim==0){
								$("#lblwarning_save").text('Jumlah pengiriman masih ada yang nol.');
								tampillistbrg();
								return false;
							}else{							
								if(data.sat==0){
									$("#lblwarning_save").text('Satuan barang ada yg tidak valid.');
									tampillistbrg();
									return false;
								}else {	
									$("#confirm-modal").modal('toggle');
									$("#confirm-modal").modal('show');	
								}	
							}
						}
					}					
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/pengambilanbrg/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/pengambilanbrg/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}
function getkodebrg(kodebrg,namabrg){
	$('#txtkodebrg').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

function clearall(){
	$.ajax({
		type	: "POST", 
		url		: "pages/pengambilanbrg/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();	
			$("#cbomarketing").val('');
			$("#cbomarketing").select2("val","");
			$("#txttglambil").val('');
			$("#txtket").val('');
			$('#warningx').html('');
			$("#lblwarning_save").text('');
		}
	});
}

function goBack() {
    window.history.back();
}