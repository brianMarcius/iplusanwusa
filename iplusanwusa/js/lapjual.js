$(document).ready(function() {
	isi_cboarea();					
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappenjualan/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);
			isi_cbojnscustomer();
		}
	});		
}

function isi_cbojnscustomer(){
	$.ajax({
		type: 'POST', 
		url: 'pages/lappenjualan/tampilkan_jenis.php',
		success: function(data) {
			$('#cbojnscustomer').html(data); 
				settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lappenjualan/tgldefa.php",
		dataType: "json",		
		success	: function(data){				
			$("#tgltrx").val(data.tgltrx);
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappenjualan/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}


$('#cboarea').change(function(){							  
	tampildata();
	settglarea();
});

$('#tgltrx').change(function(){
	tampildata();
	settglarea();
});

$('#cbotrx').change(function(){
	tampildata();
});

$('#cbojnscustomer').change(function(){
	tampildata();
});

function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();
	var jnstrx = $("#cbotrx").val();		
	var jnscustomer = $("#cbojnscustomer").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappenjualan/tampildata.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx+
				"&jnstrx="+jnstrx+
				"&jnscustomer="+jnscustomer,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail_jl(ID,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=jldetail');
			}
		}
	});
}