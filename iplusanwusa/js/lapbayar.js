$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappembayaran/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);			
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lappembayaran/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx").val(data.tgltrx);	
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx= $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappembayaran/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text('Tgl. '+$('#tgltrx').val());
			isi_cbokasir();
			
		}
	});
}

function isi_cbokasir(){
	
	var area = $("#cboarea").val();
	var tgltrx= $("#tgltrx").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappembayaran/tampilkan_kasir.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		success	: function(data){				
			$('#cbokasir').html(data);			
			tampildata();
		}
	});
}

$('#cboarea').change(function(){	
	settglarea();
});

$('#tgltrx').change(function(){
	settglarea();
});

$('#cbokasir').change(function(){	
	tampildata();
});

$('#cbosaleonly').change(function(){	
	tampildata();
});


function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();	
	var kasir = $("#cbokasir").val();
	var saleonly = $("#cbosaleonly").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappembayaran/tampildata.php",
		data	: "area="+area+
				"&kasir="+kasir+
				"&saleonly="+saleonly+
				"&tgltrx="+tgltrx,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	