$(document).ready(function() {	
	getdatapengiriman();
});		

function getdatapengiriman(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/returpenjualan/datapenjualan_rj.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblnotelp").text(data.notelp);
			$("#lbltglrj").text(data.tglretur);
			$("#lblnoretjual").text(data.noretur);
			$("#lblcust").text(data.customer);
			$("#lblalamatcust").text(data.alamatcust);
			$("#lblnotelpcust").text(data.telpcust);
			$("#lblnotajual").text(data.nojual);
			$("#lblcustomer").text(data.customer);
			$("#lblmark").text(data.marketing);
			tampillistbrg();
		}
	});
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/tampillistbrg_rj.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function goBack() {
    window.history.back();
}