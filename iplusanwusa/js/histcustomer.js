$(document).ready(function() {						   
	tampil_stock('1');	
});			

function tampil_stock(pagex){
	var searchby	= $("#searchby").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/hiscustomer/tampildata.php",
		data	: "searchby="+searchby+
					"&page="+pagex,
		success	: function(data){
			$("#data_area").html(data);
			paginationx(pagex);
		}
	});
}	

function paginationx(pagex){
	var searchby	= $("#searchby").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/hiscustomer/paging.php",
		data	: "searchby="+searchby+
					"&page="+pagex,
		success	: function(data){
			$("#page_area").html(data);
		}
	});
}

$('#searchby').keyup(function(){
	tampil_stock('1');							
});

function showdetail(ID){	
	$.ajax({
		type	: "POST", 
		url		: "pages/hiscustomer/lintasform.php",
		data	: "kodecust="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=vwhistpiutang');
			}
		}
	});
}