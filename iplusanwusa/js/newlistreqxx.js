$(document).ready(function() {	
	getdatapengiriman();
	isi_cbogudang();
});		

function getdatapengiriman(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/daftarpermintaan/datapermintaan.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblnotelp").text(data.notelp);
			$("#lblno").text(data.nosrtjalan);
			$("#lbltgltrx").text(data.tglminta);
			tampillistbrg();
		}
	});
}

function tampillistbrg(){
	var kodegudang = $("#cbogudang").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/daftarpermintaan/tampillistbrg.php",
		data	: "kodegudang="+kodegudang,
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function goBack() {
    window.history.back();
}

function cetakpermintaan(){
	/*
	var kodegudang = $("#cbogudang").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/daftarpermintaan/cetakpermintaan.php",
		data	: "kodegudang="+kodegudang,
		success	: function(data){
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	*/
	window.open('media.php?mod=cetdp');
	//window.open("pages/daftarpermintaan/cetakpermintaan.php");
}

function isi_cbogudang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settinggudang/tampil_gudang.php',
		success: function(response) {
			$('#cbogudang').html(response); 
		}
	});		
}

function getpic(){	
	var gudang 	= $("#cbogudang").val();
	$.ajax({
		type: "POST", 
		url: "pages/settinggudang/picgudang.php",
		data	: "kodegudang="+gudang,
		dataType: "json",
		success: function(data) {
			$('#txtnamapic').val(data.pic);
		}
	});		
}

$('#cbogudang').change(function(){
	getpic();							
	tampillistbrg();						 
});	

$('#btnconfirm').click(function(){								
	$.ajax({
		type	: "POST", 
		url		: "pages/daftarpermintaan/dicekdulu.php",
		dataType: "json",
		timeout	: 3000,
		success	: function(data){								
			if(data.sukses==1){
				$("#confirm-modal").modal('toggle');
				$("#confirm-modal").modal('show');
				$('#lblwarning_tolak').text('');
			}else{
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);	
			}
		}	
	});							
										
});

$('#btnsave').click(function(){
	var ket = $("#txtket").val();	
	var batalkan = false;
	
	if(ket.length<5){
		$('#lblwarning_tolak').text('Keterangan harus diisi dengan lengkap.');
		$("#txtket").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/daftarpermintaan/tolak.php",
			data	: "ket="+ket,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){		
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);					
				if(data.sukses==1){
					window.history.back();
				}
			}	
		});	
	}	
							 
});
