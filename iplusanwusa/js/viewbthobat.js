$(document).ready(function() {	
	tampil_data('1');	
});			

$('#cbosortby').change(function(){
	tampil_data('1');
});

$('#searchby').keyup(function(){
	tampil_data('1');							
});

function tampil_data(pagex){
	var searchby = $("#searchby").val();
	var sortby = $("#cbosortby").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/monitkebutuhanobat/tampildata.php",
		data	: "searchby="+searchby+
					"&sortby="+sortby+
					"&page="+pagex,
		success	: function(data){
			$("#data_area").html(data);
			paginationx(pagex);
		}
	});
}	

function paginationx(pagex){
	var searchby = $("#searchby").val();
	var sortby = $("#cbosortby").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/monitkebutuhanobat/paging.php",
		data	: "searchby="+searchby+
					"&sortby="+sortby+
					"&page="+pagex,
		success	: function(data){
			$("#page_area").html(data);
		}
	});
}

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampil_data(pageno);						 
});