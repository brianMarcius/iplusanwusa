$(document).ready(function() {						   
	tampildata('1');
	isi_cbocabang();
});						   

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpembelian/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbocabang").change(function() {
	tampildata('1');
})

$('#btnview').click(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var cabang = $("#cbocabang").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/settingpembelian/tampildata.php",
		data	: "cabang="+cabang+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
						

		}
	});
}	

function paging(pageno){
	var cabang = $("#cbocabang").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/settingpembelian/paging.php",
		data	: "cabang="+cabang+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
				

		}
	});
}

function editmaxbl(kode,kodearea){
	$("#labelkode_updmaxbl").html(kode);
	$("#labelkodearea").html(kodearea);
	$("#txtmaxbl").val('');
	$("#inputwarning_updmaxbl").hide();
	$("#updmaxbl-modal").modal('toggle');
	$("#updmaxbl-modal").modal('show');
}

$('#btnupdate_updmaxbl').click(function(){
	updatemaxbl();
});

function updatemaxbl(){
	var kode = $("#labelkode_updmaxbl").html();
	var cabang = $("#labelkodearea").html();
	var maxbl = $("#txtmaxbl").val();
	var batalkan = false;

	if(maxbl <= 0) {
		$("#inputwarning_updmaxbl").show();
		$("#inputwarning_updmaxbl").text("Nilai yang dimasukkan tidak valid");
		batalkan = true;
		return false;
	}
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingpembelian/updatemaxbeli.php',
			data: "kode="+kode+
					"&cabang="+cabang+
					"&maxbl="+maxbl,
			success: function(response) {
				
				tampildata('1');
				$("#inputwarning_updmaxbl").hide();
				$("#updmaxbl-modal").modal('hide');
			}
		});	
	}
}

function changelock(kode,kodearea){	
	
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpembelian/changelock.php',
		data: "kode="+kode+
				"&cabang="+kodearea,
		success: function(response) {
			//tampildata('1');
			$("#inputwarning_updmaxbl").hide();
			$("#updmaxbl-modal").modal('hide');
		}
	});		
}

