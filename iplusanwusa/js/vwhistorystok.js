$(document).ready(function() {						   
	getdata();
});		

function getdata(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/historystok/datastok.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblnamabrg").val(data.namabrg);
			$("#lblsatuan").val(data.satuan);
			tampilhistory();
		}
	});
}

function tampilhistory(){
	$.ajax({
		type	: "POST", 
		url		: "pages/historystok/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function goBack() {
    window.history.back();
}