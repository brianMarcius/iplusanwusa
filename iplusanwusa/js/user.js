$(document).ready(function() {						   
	tampildata('1');
});						   

$('#txtkode').keyup(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/user/tampildata.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
						

		}
	});
}	

function paging(pageno){
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/user/paging.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
				

		}
	});
}

function edit(ID){	

	$.ajax({
		type	: "POST", 
		url		: "pages/user/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newuser');
			}
		}
	});
}

function confirmdel(ID) {
	$("#namauser").html(ID);
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
}

$("#btndel").click(function() {
	var usern = $("#namauser").html();
	del(usern);
	$("#confirm-modal").modal('hide');
})

function del(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/user/hapus.php",
		data	: "username="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				tampildata('1');
			}
		}
	});
}

function resetuser(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/user/resetuser.php",
		data	: "username="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				tampildata('1');
			}
		}
	});
}