$(document).ready(function() {			
	isi_cbopengirim();
	isian_default();
});	

function isi_cbopengirim(){
	$.ajax({
		type: 'POST', 
		url: 'pages/modalkasir/tampilkan_pengirim.php',
		success: function(response) {
			$('#cbopengirim').html(response); 
		}
	});		
}

function isian_default(){
	$.ajax({
		type: 'POST', 
		url: 'pages/modalkasir/setdefault.php',
		dataType: "json",
		success	: function(data){
			$('#txttglsetor').val(data.tglskg);		
		}
	});		
}

function simpandata(){
	var kode			= $("#txtkode").val();
	var pengirim		= $("#cbopengirim").val();
	var tglsetor 		= $("#txttglsetor").val();
	var jmlsetor 	 	= $("#txtjmlsetor").val();
	
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/modalkasir/simpan.php",
			data	: "pengirim="+pengirim+
					"&kode="+kode+					
					"&tglsetor="+tglsetor+					
					"&jmlsetor="+jmlsetor,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
			}	
		});
	}	
}

$('#btnconfirm').click(function(){
	
	$("#warningx").text('');
		
	var kode			= $("#txtkode").val();
	var pengirim		= $("#cbopengirim").val();
	var tglsetor 		= $("#txttglsetor").val();
	var jmlsetor 	 	= $("#txtjmlsetor").val();
	
	var batalkan = false;
	
	if(pengirim.length==0){
		$("#warningx").text('Karyawan masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(jmlsetor.length==0){
		$("#warningx").text('Jumlah setoran masih kosong!');
		var batalkan = true;
		return false;
	}
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	$("#txtkode").val('');
	$("#cbopengirim").select2('val','');
	$("#txtjmlsetor").val('');
});
