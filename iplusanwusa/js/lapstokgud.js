$(document).ready(function() {
	isi_cbogudang();		
});	

function isi_cbogudang(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapstokgudang/tampilkan_gudang.php',
		success: function(data) {			
			$('#cbogudang').html(data);	
			settglarea();
		}
	});		
}

function settglarea(){		
	var gudang = $("#cbogudang").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/lapstokgudang/tglarea.php",
		data	: "gudang="+gudang,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbljdllap").text(data.jdllap);
			$("#lbltgltrx").text(data.tgltrx);		
			tampildata();
		}
	});
}

$('#cbogudang').change(function(){	
	tampildata();
});

function tampildata(){	
	var gudang = $("#cbogudang").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/lapstokgudang/tampildata.php",
		data	: "gudang="+gudang,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail(kdbrg,gudang){	
	$.ajax({
		type	: "POST", 
		url		: "pages/historystokgudang/lintasform.php",
		data	: "gudang="+gudang+
					"&kodebrg="+kdbrg,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=vwhisstokgud');
			}
		}
	});
}
