$(document).ready(function() {		
	isi_cboarea();	
});						   

$(function() {
	$('#tgltrx').daterangepicker({format: 'DD/MM/YYYY'});	
});		

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/daftarpermintaan/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);
			isi_cbogudang();
		}
	});		
}

function isi_cbogudang(){
	var area = $('#cbogudang').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/daftarpermintaan/tampilkan_gudang.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cbogudang').html(data);
			tampildata('1');
		}
	});		
}

$('#cboarea').click(function(){
	tampildata('1');						 
});	

$('#cbogudang').click(function(){
	tampildata('1');						 
});	

$('#tgltrx').change(function(){
	tampildata('1');						 
});

function tampildata(pageno){
	var tgl = $("#tgltrx").val();
	var area = $("#cboarea").val();
	var gudang = $("#cbogudang").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/daftarpermintaan/tampildata.php",
		data	: "tgl="+tgl+	
					"&area="+area+
					"&gudang="+gudang+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);			
		}
	});
}	

function paging(pageno){
	var tgl = $("#tgltrx").val();
	var area = $("#cboarea").val();
	var gudang = $("#cbogudang").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/daftarpermintaan/paging.php",
		data	: "tgl="+tgl+	
					"&area="+area+
					"&gudang="+gudang+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function showdetail(kode){		
	$.ajax({
		type	: "POST", 
		url		: "pages/daftarpermintaan/lintasform.php",
		data	: "kode="+kode,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			alert(data.result);
			if(data.result==1){
				window.location.assign('?mod=newdetreq');
			}
		}
	});
}

function kirimbrg(kode){		
	$.ajax({
		type	: "POST", 
		url		: "pages/daftarpermintaan/lintasform.php",
		data	: "kode="+kode,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newkirimbrg');
			}
		}
	});
}
