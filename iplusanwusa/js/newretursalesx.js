$(document).ready(function() {	
	tampildata_retur();
	inserttemp();
});		

$(function() {
	$("#txttglretur").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
});	

$("#txttglretur, #txtket").keyup(function() {
	setbtnconfirm();
})

function inserttemp(){
	$.ajax({
		type		: "POST",
		url			: "pages/returpenjualan/inserttemp.php",
		success		: function(data){
			console.log(data);
			tampillistbrg();
		}
	});
}

function tampildata_retur(){
	$.ajax({
		type		: "POST",
		url			: "pages/returpenjualan/cari.php",
		dataType	: "json",
		success		: function(data){
			
			$("#txtnonota").val(data.nonota);
			$("#txtnamacustomer").val(data.nama_customer);
			$("#txtalamat").val(data.alamat);
			$("#txtnotelp").val(data.notelp);
			$("#txtkodecustomer").val(data.kodecustomer);
		}
	});
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function isi_cbosatuan(kodesat){
	$.ajax({
		type: 'POST', 
		data: "kodesat="+kodesat,
		url: 'pages/returpenjualan/tampilkan_satuan.php',
		success: function(response) {
			$('#cbosat').html(response); 
		}
	});		
}

function setbtnconfirm(){
	$.ajax({
		type	: "POST",
		url		: "pages/returpenjualan/getdatacount.php",
		dataType: "json",
		success	: function(data){	
			var tglretur = $('#txttglretur').val();	
			var ket = $('#txtket').val();	
			
			if(tglretur.length>0 && ket.length>0 && data.jmlrec>0){
				document.getElementById("btnconfirm").disabled = false;
			}else{
				document.getElementById("btnconfirm").disabled = true;
			}
		}
	});
}

function simpandata(){
	var tglretur = $('#txttglretur').val();	
	var ket = $('#txtket').val();
	var cek	= $('#cek').val();
	var kodecustomer = $('#txtkodecustomer').val();
	var batalkan = false;

	if(tglretur.length==0){
		$("#lblwarning_save").text('Tgl Retur belum diisi.');
		$("#txttglretur").focus();
		var batalkan = true;
	}
	if(ket.length==0){
		$("#lblwarning_save").text('Keterangan belum diisi.');
		$("#txtket").focus();
		var batalkan = true;
	}
	alert(cek);
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returpenjualan/simpan.php",
			data	: "tglretur="+tglretur+
						"&ket="+ket+
						"&kodecustomer="+kodecustomer+
						"&cek="+cek,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampillistbrg();
				$("#txttglretur").val('');
				$("#txtket").val('');
				setbtnconfirm();
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newretsal");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/returpenjualan/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				setbtnconfirm();				
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var tglretur = $('#txttglretur').val();	
	var ket = $('#txtket').val();	

	$("#lblwarning_save").text('');
	var batalkan = false;
		
	if(tglretur.length==0){
		$("#lblwarning_save").text('Tgl Retur belum diisi.');
		var batalkan = true;
		return false;
	}
	if(ket.length==0){
		$("#lblwarning_save").text('Keterangan belum diisi.');
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/returpenjualan/datecompare.php",
			data	: "tglretur="+tglretur,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Retur melebihi tanggal sekarang.');
					return false;
				}
				else {
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});

$('#btnsave').click(function(){
	simpandata();		
});

function editjml(kdbrg){
	$("#updjml-modal").modal('toggle');
	$("#updjml-modal").modal('show');	
	$("#txtjml").val('');
	$('#inputwarning_updjml').hide();
	$("#lblkodebrg_updjml").text(kdbrg);
	$("#txtjml").focus();
}

$("#btnupdate_updjml").click(function(){									   
	var kodebrg		= $("#lblkodebrg_updjml").text();
	var jml			= $("#txtjml").val();	

	var batalkan = false;
	
	if(jml<=0){
		$("#inputwarning_updjml").show();
		$("#inputwarning_updjml").text('Jumlah barang tidak valid.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",		
			url		: "pages/returpenjualan/editjml.php",
			data	: "kodebrg="+kodebrg+
						"&jml="+jml,
			dataType: "json",			
			success	: function(data){
				if(data.sukses==0){
					$('#inputwarning_updjml').show();
					$('#inputwarning_updjml').text(data.pesan);
				}else{
					$("#updjml-modal").modal('hide');
					tampillistbrg();
				}				
			}
		});
	}
});

function editsat(kdbrg, kodesat){
	$("#updsat-modal").modal('toggle');
	$("#updsat-modal").modal('show');	
	isi_cbosatuan(kodesat);
	$('#inputwarning_updsat').hide();
	$("#lblkodebrg_updsat").text(kdbrg);
}

$("#btnupdate_updsat").click(function(){									   
	var kodebrg		= $("#lblkodebrg_updsat").text();
	var sat			= $("#cbosat").val();	

	var batalkan = false;
	
	if(sat<=0){
		$("#inputwarning_updsat").show();
		$("#inputwarning_updsat").text('Satuan tidak valid.');
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",		
			url		: "pages/returpenjualan/editsat.php",
			data	: "kodebrg="+kodebrg+
						"&sat="+sat,
			dataType: "json",			
			success	: function(data){
				if(data.sukses==0){
					$('#inputwarning_updsat').show();
					$('#inputwarning_updsat').text(data.pesan);
				}else{
					$("#updsat-modal").modal('hide');
					tampillistbrg();
				}				
			}
		});
	}
});
