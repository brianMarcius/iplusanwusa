$(document).ready(function() {	
	document.getElementById("btnconfirm").disabled = true;
	isi_cbocabang();
	isi_cbosupplier();
});		

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpembelian/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbocabang, #cbosupplier").change(function() {
	setbtnconfirm();
})

$("#txtsaldoawal, #txtmaxhtg, #txtmaxbl").keyup(function() {
	setbtnconfirm();
})

function isi_cbosupplier(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpembelian/tampilkan_supplier.php',
		success: function(response) {
			$('#cbosupplier').html(response); 
		}
	});		
}

function setbtnconfirm(){
	var kodecabang = $("#cbocabang").val();
	var kodesplr = $("#cbosupplier").val();
	var saldoawal = $("#txtsaldoawal").val();
	var maxhtg = $("#txtmaxhtg").val();
	var maxbl = $("#txtmaxbl").val();

	if(kodecabang.length>0 && kodesplr.length>0 && saldoawal.length>0 && maxbl.length>0 && maxhtg.length>0){
		document.getElementById("btnconfirm").disabled = false;
	}else{
		document.getElementById("btnconfirm").disabled = true;
	}
}

function simpandata(){
	var kodecabang = $("#cbocabang").val();
	var kodesplr = $("#cbosupplier").val();
	var saldoawal = $("#txtsaldoawal").val();
	var maxhtg = $("#txtmaxhtg").val();
	var maxbl = $("#txtmaxbl").val();

		$.ajax({
			type	: "POST", 
			url		: "pages/settingpembelian/simpan.php",
			data	: "kodecabang="+kodecabang+
						"&kodesplr="+kodesplr+
						"&saldoawal="+saldoawal+
						"&maxbl="+maxbl+
						"&maxhtg="+maxhtg,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				$("#cbocabang").select2('val','');
				$("#cbosupplier").select2('val','');
				$("#txtsaldoawal").val('');
				$("#txtmaxhtg").val('');
				$("#txtmaxbl").val('');
				setbtnconfirm();
			}	
		});	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=setbeli");
});

$('#btnconfirm').click(function(){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');	
});

$('#btnsave').click(function(){
	simpandata();		
});
