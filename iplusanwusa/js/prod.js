$(document).ready(function() {
	tampilkategori();			
});	

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function tampilkategori(){
	$.ajax({
		type	: "POST", 
		url		: "pages/barang/tampilkan_kategorix.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbokategori").html(data);	
			tampildata('1');
		}
	});
}

$('#cari').keyup(function(){
	tampildata('1');
});

$('#cbokategori').change(function(){
	tampildata('1');
});

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampildata(pageno);						 
});

function tampildata(pageno){
	
	setCookie("nopageprod", pageno);
	if(getCookie("pageprod").length>0) {
		var pageno = getCookie("pageprod");
		setCookie("nopageprod", pageno);
	}
	else {
		var pageno = pageno;
	}
	if(getCookie("cariprod").length>0) {
		var kode = getCookie("cariprod");
		$("#cari").val(getCookie("cariprod"));
	}
	else {
		var kode = $("#cari").val();		
	}
	if(getCookie("katprod").length>0) {
		//alert(getCookie("katprod"));
		var kategori = getCookie("katprod");
		$("#cbokategori").val(getCookie("katprod"));
	}
	else {
		var kategori = $("#cbokategori").val();		
	}
	
	$.ajax({
		type	: "GET",		
		url		: "pages/barang/tampildata.php",
		data	: "kode="+kode+
					"&kategori="+kategori+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var kode = $("#cari").val();			
	var kategori = $("#cbokategori").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/barang/paging.php",
		data	: "kode="+kode+
					"&kategori="+kategori+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			delCookie("cariprod");
			delCookie("pageprod");
			delCookie("katprod");
		}
	});
}

function editbrg(kodebrg){
	setCookie("pageprod", getCookie("nopageprod"));
	setCookie("cariprod", $("#cari").val());
	setCookie("katprod", $("#cbokategori").val());
	
	$.ajax({
		type	: "POST", 
		url		: "pages/barang/lintasform.php",
		data	: "kode="+kodebrg,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newprod');
			}
		}	
	});	
}

function hapusbrg(kodebrg){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkode").val(kodebrg);
}

function hapus(){
	var kode	 	= $("#txtkode").val();
	var batalkan 	= false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/barang/hapus.php",
			data	: "kode="+kode,
			dataType: "json",
			success	: function(data){
			
			$("#confirm-modal").modal('hide');
			var pageno = getCookie("nopageprod");
			tampildata(pageno);
			}	
		});
	}	
}

$('#btnsave').click(function(){
	hapus();
});

function detail(kodebrg){
	$("#detail-modal").modal('show');
	$("#txtkode").val(kodebrg);
	tabelsatuandetail();
}

function tabelsatuandetail(){
	var kode	 	= $("#txtkode").val();

	$.ajax({
		type	: "POST", 
		url		: "pages/barang/tampilsatuandetail.php",
		data	: "kode="+kode,
		timeout	: 3000,
		success	: function(data){
			$("#tblsatuandetail").html(data);
		}
	});
}

function cetakbrg(){
	var kategori = $("#cbokategori").val();

	if(kategori.length==0){
		window.open("pages/barang/cetakbrg.php");
	}
	else {
		$.ajax({
			type	: "POST", 
			url		: "pages/barang/lintasreport.php",
			data	: "kategori="+kategori,
			timeout	: 3000,
			success	: function(data){
				//$("#tblsatuandetail").html(data);
			}
		});
		window.open("pages/barang/cetakbrg2.php");
	}
}
