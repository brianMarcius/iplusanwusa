$(document).ready(function() {						   
	tampildata('1');
});						   

$(function() {
	$('#tglnota').daterangepicker({format: 'DD/MM/YYYY'});	
});		

$('#txtkode').keyup(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl = $("#tglnota").val();
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/returpembelian/tampildata.php",
		data	: "tgl="+tgl+	
					"&kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var tgl = $('#tglnota').val();
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/returpembelian/paging.php",
		data	: "tgl="+tgl+	
					"&kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function showdetail(kode){		
	$('#info-modal').modal('toggle');
	$("#info-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/tampil_detailbrg.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayx").show();
			$("#loading-imgx").show();
		},
		success	: function(data){
			$("#retur-modal").modal('hide');		
			$("#overlayx").hide();
			$("#loading-imgx").hide();
			$("#infone").html(data);
		}	
	});
}

function tampillistbrg(){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/tampildetail.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

function receipt(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newretur');
			}
		}
	});
}

function edit(ID){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newretur');
			}
		}
	});
}

function hapus(ID){			
	$('#confirm-modal').modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkodebeli").val(ID);
}

$('#btnsave').click(function(){
	var kode = $("#txtkodebeli").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/hapus_trx.php",
		data	: "kode="+kode,
		dataType: "json",
		success	: function(data){
			$("#confirm-modal").modal('hide');
			$('#info-modal').modal('toggle');
			$("#info-modal").modal('show');
			$("#infone").html(data.pesan);
			if(data.sukses==1){
				tampildata('1');
			}
		}
	});
});