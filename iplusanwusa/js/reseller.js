$(document).ready(function() {
	tampildata('1');	
});	

$('#cari').keyup(function(){
	tampildata('1');
});

function tampildata(pageno){
	
	var kode = $("#cari").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/marginreseller/tampildata.php",
		data	: "kode="+kode+
				"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){		
	var kode = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/marginreseller/paging.php",
		data	: "kode="+kode+
				"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function editmarginpersen(kode){
	$("#labelkode_updmarginpersen").html(kode);
	$("#txtmarginpersen").val('');
	$("#inputwarning_updmarginpersen").hide();
	$("#updmarginpersen-modal").modal('toggle');
	$("#updmarginpersen-modal").modal('show');
}

$('#btnupdate_updmarginpersen').click(function(){
	updatemarginpersen();
});

function updatemarginpersen(){
	var kode = $("#labelkode_updmarginpersen").html();
	var marginpersenx = $("#txtmarginpersen").val();
	var marginpersen = marginpersenx.replace(',','.');
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/marginreseller/updatemarginpersen.php',
			data: "kode="+kode+
					"&marginpersen="+marginpersen,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_updmarginpersen").hide();
				$("#updmarginpersen-modal").modal('hide');
			}
		});	
	}
}

function editmarginrp(kode){
	$("#labelkode_updmarginrp").html(kode);
	$("#txtmarginrp").val('');
	$("#inputwarning_updmarginrp").hide();
	$("#updmarginrp-modal").modal('toggle');
	$("#updmarginrp-modal").modal('show');
}

$('#btnupdate_updmarginrp').click(function(){
	updatemarginrp();
});

function updatemarginrp(){
	var kode = $("#labelkode_updmarginrp").html();
	var marginrpy = $("#"+kode).val();
	var marginrpx = marginrpy.substr(0,marginrpy.length-3);
	var marginrp = $("#txtmarginrp").val();
	var max = (parseInt(marginrpx.substr(0,(marginrpx.length-2)))+1)*100;
	var min = parseInt(marginrpx.substr(0,(marginrpx.length-2)))*100;
	var batalkan = false;

	if(marginrp<min && marginrpy!=0) {
		$("#inputwarning_updmarginrp").html("Margin Rp melebihi batas minimal");
		$("#inputwarning_updmarginrp").show();
		batalkan = true;
	}

	if(marginrp>max && marginrpy!=0) {
		$("#inputwarning_updmarginrp").html("Margin Rp melebihi batas maksimal");
		$("#inputwarning_updmarginrp").show();
		batalkan = true;
	}

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/marginreseller/updatemarginrp.php',
			data: "kode="+kode+
					"&marginrp="+marginrp,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_updmarginrp").hide();
				$("#updmarginrp-modal").modal('hide');
			}
		});	
	}
}

function edithpp(kode){
	$("#labelkode_updhpp").html(kode);
	$("#txthpp").val('');
	$("#inputwarning_updhpp").hide();
	$("#updhpp-modal").modal('toggle');
	$("#updhpp-modal").modal('show');
}

$('#btnupdate_updhpp').click(function(){
	updatehpp();
});

function updatehpp(){
	var kode = $("#labelkode_updhpp").html();
	var hpp = $("#txthpp").val();
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/marginreseller/updatehpp.php',
			data: "kode="+kode+
					"&hpp="+hpp,
			success: function(response) {
				
				tampildata('1');
				$("#inputwarning_updhpp").hide();
				$("#updhpp-modal").modal('hide');
			}
		});	
	}
}

