$(document).ready(function() {
	viewshiftkry();
	tampil_karyawan();
	$('#warningx').html('');
});

function tampil_karyawan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shiftkry/listkaryawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response);
		}
	});	
}

function viewshiftkry(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shiftkry/lihatshiftkryroll.php',
		success: function(response) {
			$('#tblshiftkry').html(response); 
		}
	});
}

function ubah(tanggal,shift,idkaryawan){
	$("#ubahkry-modal").modal('toggle');
	$("#ubahkry-modal").modal('show');
	tampil_karyawan();
	$("#warningkry").hide();
	$('#txtshift').val(shift);
	$('#txttanggal').val(tanggal);
	$('#cbokaryawan').select2('val',idkaryawan);
	$('#cbokaryawan').focus();
}

$('#cbokaryawan').change(function(){
	$("#warningkry").hide();
});

$('#btnupdatekry').click(function(){
	var shift = $("#txtshift").val();
	var tanggal = $("#txttanggal").val();
	var newidkaryawan = $("#cbokaryawan").val();
	var tglkerja = $("#tglkerja").val();
	$("#warningkry").hide();
	var batalkan = false;
	
	if(newidkaryawan.length==0){
		$("#warningkry").show();
		var batalkan = true;
		return false;
	}	

	if(tglkerja.length==0){
		$("#warningkry").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/shiftkry/rollkaryawan.php",
			data	: "newidkaryawan="+newidkaryawan+
						"&shift="+shift+
						"&tglkerja="+tglkerja+
						"&tanggal="+tanggal,
			timeout	: 3000,
			success	: function(data){				
				$("#ubahkry-modal").modal('hide');
				viewshiftkry();
			}	
		});
	}					
});