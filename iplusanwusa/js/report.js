$(document).ready(function() {
	isi_cbosupplier();
	isi_cbocustomer();
});						   

$(function() {
	$('#tglreport').daterangepicker({format: 'DD/MM/YYYY'});	
});	

function isi_cbosupplier(){
	$.ajax({
		type: 'POST', 
		url: 'pages/report/tampilkan_supplier.php',
		success: function(response) {
			$('#cbosupplier').html(response); 
		}
	});		
}

function isi_cbocustomer(){
	$.ajax({
		type: 'POST', 
		url: 'pages/report/tampilkan_customer.php',
		success: function(response) {
			$('#cbocustomer').html(response); 
		}
	});		
}

$('#btnview').click(function(){
	report();
});	

function tampil_data(){
	var tgl 		= $('#tglreport').val();
	// var kdprshn 	= $("#cboperusahaan").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/report/tampildata_pembelian.php",
		data	: "tgl="+tgl,
		success	: function(data){
			$("#report_area").html(data);			
		}
	});
}	

function report(){
	var tgl 		= $('#tglreport').val();
	var supplier 	= $('#cbosupplier').val();
	var customer	= $('#cbocustomer').val();
	var nonota 		= $('#txtnonota').val();
	var report		= $('#txtreport').val();
	$.ajax({
		type	: "GET", 
		url		: "pages/report/lintasreport.php",
		data	: "tgl="+tgl+
					"&supplier="+supplier+
					"&customer="+customer+
					"&nonota="+nonota+
					"&report="+report,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign(data.hlink);
			}
		}	
	});	
}

