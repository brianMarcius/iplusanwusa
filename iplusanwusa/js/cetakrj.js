$(document).ready(function() {
	isi_cboarea();					
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/resepprolanis/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/resepprolanis/tgldefa.php",
		dataType: "json",		
		success	: function(data){				
			$("#tgltrx").val(data.tgltrx);
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/resepprolanis/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}


$('#cboarea').change(function(){							  
	tampildata();
	settglarea();
});

$('#tgltrx').change(function(){
	tampildata();
	settglarea();
});

$('#cbotrx').change(function(){
	tampildata();
});

function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();
	var jnstrx = $("#cbotrx").val();			
	
	$.ajax({
		type	: "GET",		
		url		: "pages/resepprolanis/tampildata.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx+
				"&jnstrx="+jnstrx,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail_prol(ID,nobpjs){	
	$.ajax({
		type	: "POST", 
		url		: "pages/resepprolanis/lintasform.php",
		data	: "kode="+ID+
					"&nobpjs="+nobpjs,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=rpdetail');
			}
		}
	});
}