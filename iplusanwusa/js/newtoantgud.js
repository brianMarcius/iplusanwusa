$(document).ready(function() {	
	isi_cbogudangasal();	
	tampillistbrg();
});		

function isi_cbogudangasal(){
	
	$.ajax({
		type: "POST", 
		url: "pages/toantargudang/tampilkan_gudangasal.php",
		success: function(response) {
			$("#cbogudangasal").html(response); 
			isi_cbogudangtujuan();
		}
	});		
}

function isi_cbogudangtujuan(){
	$("#cbogudangtujuan").select2("val","");
	var gudangasal = $("#cbogudangasal").val();
	$.ajax({
		type: "POST", 
		url: "pages/toantargudang/tampilkan_gudangtujuan.php",
		data: "gudangasal="+gudangasal,
		success: function(data) {
			$("#cbogudangtujuan").html(data); 
		}
	});		
}

function isi_cbosatuan(){
	var kodebrg = $('#txtkodebrg').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/toantargudang/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function setsatuan(kdsat){
	var kodebrg = $('#txtkodebrg').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/toantargudang/tampilkan_satuanx.php',
		data: "kodebrg="+kodebrg+
				"&kodesatuan="+kdsat,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

$('#cbogudangasal').change(function(){				
	cleartemp();
	isi_cbogudangtujuan();	
});

function tampillistbrg(){
	var gudangasal = $('#cbogudangasal').val();
	$.ajax({
		type	: "POST", 
		url		: "pages/toantargudang/tampillistbrg.php",
		data	: "gudangasal="+gudangasal,
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);			
		}
	});
}

function addbarang(){
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var gudangasal = $('#cbogudangasal').val();
	
	$("#lblwarning_save").text('');
	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
		return false;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
		return false;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#txtkodebrg").focus();
		var batalkan = true;
		return false;
	}
	
	if(gudangasal.length==0){
		$('#warningx').html('Gudang Asal belum dipilih.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/toantargudang/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat+
					"&gudangasal="+gudangasal,
			timeout	: 3000,
			success	: function(data){
				$("#warningx").html(data);
				var pesansuksesx = $("#warningx").html();
				var pesansukses = pesansuksesx.trim();
				if(pesansukses.length==0){
					tampillistbrg();
					 clearaddbrg();
				}
			}	
		});
	}
}

function clearaddbrg(){
	$("#txtkodebrg").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
}

function simpandata(){
	var gudangasal = $('#cbogudangasal').val();
	var gudangtujuan = $('#cbogudangtujuan').val();
	var tglto	= $("#txttglto").val();
	var batalkan = false;
	
	if(gudangasal.length==0){
		$("#lblwarning_save").text('Gudang Asal belum dipilih!');
		$("#cbogudangasal").focus();
		var batalkan = true;
	}
	
	if(gudangtujuan.length==0){
		$("#lblwarning_save").text('Gudang Tujuan belum dipilih!');
		$("#cbogudangtujuan").focus();
		var batalkan = true;
	}
	
	if(tglto.length<3){
		$('#lblwarning_save').text('Tgl Pengambilan tidak valid.');
		$("#txttglto").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/toantargudang/simpan.php",
			data	: "gudangasal="+gudangasal+
						"&gudangtujuan="+gudangtujuan+
						"&tglto="+tglto,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){	
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);	
				$("#lblsukses").text(data.sukses);
			}	
		});	
	}	
}		

$('#info-ok').click(function(){
	var sukses = $("#lblsukses").text();
	if(sukses==1){
		tampillistbrg();
		$("#cbogudangasal").select2("val","");
		$("#cbogudangtujuan").select2("val","");
		$("#txttglto").val('');
		window.location.assign("?mod=ttbtoag");
	}
});

function cleartemp(){	
	$.ajax({
		type	: "POST",
		url		: "pages/toantargudang/deltemp.php",
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
			}
		}
	});
}

function del(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/toantargudang/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){			
			tampillistbrg();							
		}
	});
}

function edit(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/toantargudang/edit.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			$("#txtkodebrg").val(data.kodebrg);
			$("#barang").val(data.namabrg);
			$("#txtjml").val(data.jmlbrg);
			setsatuan(data.kodesatuan);			
		}
	});
}

$('#btnconfirm').click(function(){
	var gudangasal = $('#cbogudangasal').val();
	var gudangtujuan = $('#cbogudangtujuan').val();
	var tglto	= $("#txttglto").val();
	
	$('#warningx').html('');
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(gudangasal.length==0){
		$("#lblwarning_save").text('Gudang Asal belum dipilih!');
		$("#cbogudangasal").focus();
		var batalkan = true;
	}
	
	if(gudangtujuan.length==0){
		$("#lblwarning_save").text('Gudang Tujuan belum dipilih!');
		$("#cbogudangtujuan").focus();
		var batalkan = true;
	}
	
	if(tglto.length<3){
		$('#lblwarning_save').text('Tgl Pengambilan tidak valid.');
		$("#txttglto").focus();
		var batalkan = true;
		return false;
	}
		
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/toantargudang/datecompare.php",
			data	: "tglto="+tglto+
						"&gudangasal="+gudangasal,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Kirim melebihi tanggal sekarang.');
					return false;
				}else {
					if(data.adarec==0){
						$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
						return false;
					}else {
						if(data.stok<0){
							$("#lblwarning_save").text('Jumlah pengiriman melebih stok yang ada.');
							tampillistbrg();
							return false;
						}else {	
							
							if(data.jmlkirim==0){
								$("#lblwarning_save").text('Jumlah pengiriman masih ada yang nol.');
								tampillistbrg();
								return false;
							}else{							
								if(data.sat==0){
									$("#lblwarning_save").text('Satuan barang ada yg tidak valid.');
									tampillistbrg();
									return false;
								}else {	
									$("#confirm-modal").modal('toggle');
									$("#confirm-modal").modal('show');	
								}	
							}
						}
					}					
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);	
});

$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();
	var kodearea = $('#cbogudangasal').val();
	$.ajax({
		type: 'GET', 
		url: 'pages/toantargudang/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&kodearea="+kodearea+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/toantargudang/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

function getkodebrg(kodebrg,namabrg){
	$('#txtkodebrg').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

function clearall(){
	$.ajax({
		type	: "POST", 
		url		: "pages/toantargudang/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();	
			$("#cbogudangasal").select2("val","");
			$("#cbogudangtujuan").select2("val","");
			$("#txttglto").val('');
			$('#warningx').html('');
			$("#lblwarning_save").text('');
		}
	});
}

function goBack() {
    window.history.back();
}