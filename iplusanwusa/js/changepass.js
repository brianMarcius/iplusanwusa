$('#txtoldpassword').val('');

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

$('#txtoldpassword').keyup(function(){
	delay(function(){
		pass = $('#txtoldpassword').val();
		$.ajax({
			type:'post',
			url:'pages/updateacc/checkpass.php',
			data:{pass:pass},
			success:function(result) {
				if (result=='ok') {
					$('#keterangan').css('color','green').html('Password benar');
					$('#txtnewpassword').attr('disabled', false).focus();
				}else{
					$('#keterangan').css('color','red').html('Password salah');
				}
			}
		})
	},900)		
});

$('#txtrepassword').keyup(function(){
	delay(function(){
		newpass = $('#txtnewpassword').val();
		repass = $('#txtrepassword').val();
		if (newpass==repass) {
			$('#ketrepassword').css('color','green').html('Password cocok');
		}else{
			$('#ketrepassword').css('color','red').html('Password tidak cocok');
		}
	},900);
})

$('#btnsubmit').click(function(){
	newpass = $('#txtnewpassword').val();
	repass = $('#txtrepassword').val();
	username = $('#txtusername').val();
	if (newpass==repass) {
		$.ajax({
			type:'post',
			url:'pages/updateacc/updatepass.php',
			data:{
				username:username,
				newpass:newpass
			},
			success:function(result){
				if (result>0) {
					toastr["success"]("Password successfully changed", "Success");
					$('#txtrepassword,#txtnewpassword').val('');
					$('#txtoldpassword').val('');
					$('#keterangan,#ketrepassword').html('');
				}else{
					toastr["error"]("Password failed to be changed","Error");	
				}
			}
		})
	}else{
		toastr["error"]("Password tidak cocok","Error");
		$('#txtrepassword').focus();
	}
})