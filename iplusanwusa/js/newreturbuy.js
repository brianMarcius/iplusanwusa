$(document).ready(function() {	
	isian_default();
});		

function isian_default(){
	$.ajax({
		type: 'POST', 
		url: 'pages/returpembelian/setdefault.php',
		dataType: "json",
		success	: function(data){			
			var edit=data.kodebeli;
			if(edit.length>0){
				$('#txtkodebeli').val(data.kodebeli);
				$('#txtnonota').val(data.nonota);
				$('#txttglbeli').val(data.tglbeli);
				$('#txttglretur').val(data.tgljthtempo);
				$('#txtkodesupplier').val(data.kodesupplier);
				$('#txtnamasupplier').val(data.namasupplier);
				$('#txtalamat').val(data.alamat);
				$('#txtdiscpersen').val(data.disctot);
				$('#txtdisc').val(data.rpdisctot);
				$('#txtppnpersen').val(data.ppn);
				$('#txtppn').val(data.rpppn);
				$('#txtsaldohutang').val(data.rpgrandtot);
				$('#txtrppenerimaan').val(data.rppenerimaan);
				tampillistbrg();
			}else{
				$('#txttglbeli').val(data.tglbeli);				
			}			
		}
	});		
}

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/returpembelian/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/returpembelian/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

function getkodebrg(kodebrg,namabrg){
	$('#txtkodebrg').val(kodebrg);
	$('#txtnamabrg').val(namabrg);
	$("#barang-modal").modal('hide');
	edit(kodebrg,0,0);
}

function edit(ID,hrg,hrgppn){
	var kodebeli = $('#txtkodebeli').val();
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/edit.php",
		data	: "kodebrg="+ID+
					"&kodebeli="+kodebeli+
					"&hargabrg="+hrg+
					"&hargappn="+hrgppn,
		dataType: "json",
		success	: function(data){	
			$('#txtkodebrg').val(data.kodebrg);
			$('#txtnamabrg').val(data.namabrg);
			$('#txtharga').val(data.hargabrg);
			$('#txthargappn').val(data.hargappn);
			$('#txtjml').val(data.jmlbrg);
			isi_cbosatuan(data.kodesatuan);
			$('#txtdisc_peritem').val(data.disc);
			$('#txtdiscrp_peritem').val(data.rpdisc);
							
		}
	});
}

$("#txtcaribarang").keyup(function(){
	 tampil_masterbrg(1);
});

function isi_cbosatuan(kode){
	var kodebrg = $('#txtkodebrg').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/returpembelian/tampilkan_satuan.php',
		data	: "kodebrg="+kodebrg+
					"&kodesatuan="+kode,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
			getrphutang();
		}
	});
}

function addbarang(){

	var kodebeli = $("#txtkodebeli").val();
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var harga	= $("#txtharga").val();
	var hargappn= $("#txthargappn").val();
	var disc	= $("#txtdisc_peritem").val();
	var rpdisc	= $("#txtdiscrp_peritem").val();	

	$('#warningx').html('');
	var batalkan = false;	
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum di pilih');
		$("#txtjml").focus();
		var batalkan = true;
	}
	/*
	if(harga<=0){
		$('#warningx').html('Harga tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	*/
	if(disc<0){
		$('#warningx').html('Disc tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(rpdisc<0){
		$('#warningx').html('Rp Disc tidak valid.');
		$("#txtharga").focus();
		var batalkan = true;
	}

	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}

	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returpembelian/addbarang.php",
			data	: "kodebeli="+kodebeli+	
					"&kodebrg="+kodebrg+
					"&jml="+jml+
					"&kodesatuan="+kodesat+	
					"&harga="+harga+
					"&hargappn="+hargappn+
					"&disc="+disc+
					"&rpdisc="+rpdisc,
			dataType: "json",
			success	: function(data){	
				
				if(data.sukses==1){
					 tampillistbrg();
					 clearaddbrg();
				}else{
					$('#warningx').text(data.pesan);
				}
			}	
		});
	}
}

function getrphutang(){
	var disc  = $("#txtdiscpersen").val();
	var ppn  = $("#txtppnpersen").val();
	//alert('PPN :'+ppn+', Disc:'+disc);
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/getrphutang.php",
		data	: "disc="+disc+
					"&ppn="+ppn,
		dataType: "json",
		success	: function(data){
			//alert('RP PPN :'+data.rpppn);
			$("#txtdisc").val(data.rpdisc);
			$("#txtppn").val(data.rpppn);
			$("#txtsaldohutang").val(data.jmlhutang);			
			$("#txtrppenerimaan").val(data.rppenerimaan);	
		}
	});
}

function clearaddbrg(){
	$("#txtnamabrg").val('');
	$("#txtkodebrg").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	isi_cbosatuan('');
	$("#txtharga").val('');
	$("#txtdisc_peritem").val('');
	$("#txtdiscrp_peritem").val('');
	$("#txthargappn").val('');
}

function simpandata(){
	var kodesupplier = $("#txtkodesupplier").val();
	var tglretur	= $("#txttglretur").val();	
	var kodebeli	 = $("#txtkodebeli").val();
	var disc 		= $("#txtdiscpersen").val();
	var rpdisc 		= $("#txtdisc").val();
	var ppn 		= $("#txtppnpersen").val();	
	var rpppn 		= $("#txtppn").val();
	var grandtot	= $("#txtsaldohutang").val();
	var pembayaran	= $("#txtrppenerimaan").val();
	var ket 		= $("#txtket").val();
	var batalkan = false;	
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returpembelian/simpan.php",
			data	: "kodesupplier="+kodesupplier+
						"&tglretur="+tglretur+
						"&kodebeli="+kodebeli+
						"&disc="+disc+
						"&rpdisc="+rpdisc+
						"&ppn="+ppn+
						"&rpppn="+rpppn+
						"&grandtot="+grandtot+
						"&pembayaran="+pembayaran+
						"&ket="+ket,
			dataType: "json",
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);
			}	
		});	
	}	
}		

function del(ID,hrg,hrgppn){
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/hapus.php",
		data	: "kodebrg="+ID+
					"&hargabrg="+hrg+
					"&hargappn="+hrgppn,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();	
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var tglretur	= $("#txttglretur").val();	
	var ket 		= $("#txtket").val();	
	
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(tglretur.length==0){
		$("#lblwarning_save").text('Tgl Retur belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(ket.length==0){
		$("#lblwarning_save").text('Alasan Retur harus diisi.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/returpembelian/datecompare.php",
			data	: "tglretur="+tglretur,
			dataType: "json",
			success	: function(data){
				if(data.lanjut==0){
					$("#lblwarning_save").text(data.pesan);
					return false;
				}else{
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$("#txtdiscpersen").keyup(function(){
	getrphutang();
});

$("#txtppnpersen").keyup(function(){
	 getrphutang();
});

$('#btnok').click(function(){
	 goBack();	
});

function goBack() {
    window.history.back();
}

$('#cbosatuan').change(function(){
	konversi();
});

$('#txtjml').keyup(function(){
	konversi();
});

function konversi(){	
	var kodebrg = $("#txtkodebrg").val();
	var jmlretur = $("#txtjml").val();
	var kodesatuan = $("#cbosatuan").val();
	var kodebeli = $("#txtkodebeli").val();
	
	$.ajax({
		type	: "POST",
		url		: "pages/returpembelian/konversi.php",
		data	: "kodebrg="+kodebrg+
					"&jmlretur="+jmlretur+
					"&kodesatuan="+kodesatuan+
					"&kodebeli="+kodebeli,
		dataType: "json",
		success	: function(data){	
			$('#txtharga').val(data.hargabrg);
			$('#txtdiscrp_peritem').val(data.rpdisc);
			$('#txthargappn').val(data.hargappn);			
		}
	});
	
}
