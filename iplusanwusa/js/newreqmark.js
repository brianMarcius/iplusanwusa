$(document).ready(function() {							 
	tampillistbrg();	
});		

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/permintaan_marketing/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan_marketing/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function addbarang(){
	var tglsp  	= $("#txttglpermintaan").val();
	var kodebrg = $("#cbobarang").val();
	var tujuan = '01';
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var ket		= $("#txtket").val();

	$('#warningx').html('');
	var batalkan = false;

	if(tglsp.length==0){
		$('#warningx').html('Tgl Permintaan masih kosong.');
		$("#txttglpermintaan").focus();
		var batalkan = true;
	}
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/permintaan_marketing/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&tujuan="+tujuan+
					"&kodesatuan="+kodesat+
					"&tglsp="+tglsp+
					"&ket="+ket,
			timeout	: 3000,
			success	: function(data){	
				$('#warningx').html(data);
				var pesansuksesx = $("#warningx").html();
				var pesansukses = pesansuksesx.trim();
				if(pesansukses.length==0){
					tampillistbrg();
					 clearaddbrg();
				}
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#txtket").val('');
}

function simpandata(){
	var kodetujuan	= '01';	
	var tglsp  	 = $("#txttglpermintaan").val();
	var batalkan = false;

	if(tglsp.length==0){
		$('#lblwarning_save').text('Tgl Permintaan masih kosong.');
		var batalkan = true;
	}
		
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/permintaan_marketing/simpan.php",
			data	: "kodetujuan="+kodetujuan+
					"&tglsp="+tglsp,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				clearaddbrg();
				tampillistbrg();
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newmintamark");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/permintaan_marketing/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();			
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var kodetujuan	= '01';
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(kodetujuan.length==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/permintaan_marketing/datecompare.php",
			dataType: "json",
			success	: function(data){
				if(data.adarec==0){
					$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
					return false;
				}else {
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}									
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$('#txttglpermintaan').change(function(){
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();
		}
	});
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	var tujuan = '01';	
	$.ajax({
		type: 'GET', 
		url: 'pages/permintaan_marketing/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&tujuan="+tujuan+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}
$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/permintaan_marketing/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');	
});