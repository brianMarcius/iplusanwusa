$(document).ready(function() {			
	isi_cbopengirim();
	isian_default();
	tampil_banktujuan();
});	

function isi_cbopengirim(){
	$.ajax({
		type: 'POST', 
		url: 'pages/setoranmarketing/tampilkan_pengirim.php',
		success: function(response) {
			$('#cbopengirim').html(response); 
		}
	});		
}

$('#cbopengirim').change(function(){
	setbtnconfirm();
});	

function isian_default(){
	$.ajax({
		type: 'POST', 
		url: 'pages/setoranmarketing/setdefault.php',
		dataType: "json",
		success	: function(data){
			$('#txttglsetor').val(data.tglskg);		
		}
	});		
}
function tampil_banktujuan(){				
	$.ajax({
		type	: "POST", 
		url		: "pages/setoranmarketing/tampilkan_bank.php",
		success	: function(data){
			$("#cbobank_tujuan").html(data);
	
		}
	});
}

function isi_cbobank(){				
	$.ajax({
		type	: "POST", 
		url		: "pages/setoranmarketing/tampilkan_namabank.php",
		success	: function(data){
			$("#cbobank").html(data);
	
		}
	});
}

function simpandata(){
	var kode			= $("#txtkode").val();
	var pengirim		= $("#cbopengirim").val();
	var tglsetor 		= $("#txttglsetor").val();
	var jmlsetor 	 	= $("#txtjmlsetor").val();
	var bank_tujuan 	= $("#cbobank_tujuan").val();
	var norek_tujuan	= $("#txtnorek_tujuan").val();
	
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/setoranmarketing/simpan.php",
			data	: "pengirim="+pengirim+
					"&kode="+kode+					
					"&tglsetor="+tglsetor+					
					"&jmlsetor="+jmlsetor+
					"&bank_tujuan="+bank_tujuan+
					"&norek_tujuan="+norek_tujuan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
			}	
		});
	}	
}

$('#btnconfirm').click(function(){
	
	$("#warningx").text('');
		
	var kode			= $("#txtkode").val();
	var pengirim		= $("#cbopengirim").val();
	var tglsetor 		= $("#txttglsetor").val();
	var jmlsetor 	 	= $("#txtjmlsetor").val();
	var bank_tujuan 	= $("#cbobank_tujuan").val();
	var norek_tujuan	= $("#txtnorek_tujuan").val();
	
	var batalkan = false;
	
	if(pengirim.length==0){
		$("#warningx").text('Karyawan masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(jmlsetor.length==0){
		$("#warningx").text('Jumlah setoran masih kosong!');
		var batalkan = true;
		return false;
	}
	/*
	if(bank_tujuan.length==0){
		$("#warningx").text('Bank tujuan masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(norek_tujuan.length==0){
		$("#warningx").text('Norek tujuan masih kosong!');
		var batalkan = true;
		return false;
	}
	*/
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	$("#txtkode").val('');
	$("#cbopengirim").select2('val','');
	$("#txttglsetor").val('');
	$("#txtjmlsetor").val('');
	$("#cbobank_tujuan").val('');
	$("#txtnorek_tujuan").val('');

});

$('#norek_add').click(function(){
	$("#addnorek-modal").modal('toggle');
	$("#addnorek-modal").modal('show');
	isi_cbobank();
	$('#txtnorek').focus();
});

$('#btnupdate').click(function(){
	var kodebank = $("#cbobank").val();						   
	var norek = $("#txtnorek").val();
	$("#warningnorek").hide();
	var batalkan = false;
	
	if(norek.length==0){
		$("#warningnorek").show();
		var batalkan = true;
		return false;
	}	
	
	if(kodebank.length==0){
		$("#warningnorek").show();
		$("#warningnorek").text('Nama Bank belum dipilih!');
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/setoranmarketing/addnorek.php",
			data	: "kodebank="+kodebank+
						"&norekbaru="+norek,
			timeout	: 3000,
			success	: function(data){				
				$("#addnorek-modal").modal('hide');
				tampil_banktujuan();
			}	
		});
	}		
});
