$(document).ready(function() {						   
	tampildata('1');
});						   

$(function() {
	$('#tgltrx').daterangepicker({format: 'DD/MM/YYYY'});	
});		

$('#tgltrx').blur(function(){
	tampildata('1');						 
});	

$('#txtkode').keyup(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl = $("#tgltrx").val();
	var kode = $("#txtkode").val();
	var searchby = $("#searchby").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/barangdatang/tampildata.php",
		data	: "tgl="+tgl+	
					"&kode="+kode+
					"&searchby="+searchby+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
						

		}
	});
}	

function paging(pageno){
	var tgl = $('#tgltrx').val();
	var kode = $("#txtkode").val();
	var searchby = $('#searchby').val();
	$.ajax({
		type	: "GET",		
		url		: "pages/barangdatang/paging.php",
		data	: "tgl="+tgl+	
					"&kode="+kode+
					"&searchby="+searchby+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
				

		}
	});
}

function showdetail(kode){		
	$('#info-modal').modal('toggle');
	$("#info-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/barangdatang/tampil_detailbrg.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayx").show();
			$("#loading-imgx").show();
		},
		success	: function(data){
			$("#retur-modal").modal('hide');		
			$("#overlayx").hide();
			$("#loading-imgx").hide();
			$("#infone").html(data);
		}	
	});
}

function tampillistbrg(){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/barangdatang/tampildetail.php",
		//data	: "kode=",
		timeout	: 3000,
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

function receive(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/barangdatang/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newreceiveprod');
			}
		}
	});
}

function brgretur(kode){		
	$('#retur-modal').modal('toggle');
	$("#retur-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/barangdatang/tampil_detailretur.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayy").show();
			$("#loading-imgy").show();
		},
		success	: function(data){
			$("#info-modal").modal('hide');		
			$("#overlayy").hide();
			$("#loading-imgy").hide();
			$("#inforetur").html(data);
		}	
	});
}
