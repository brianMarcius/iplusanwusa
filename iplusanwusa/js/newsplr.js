$(document).ready(function() {		
	tampil_data();
});

$(function() {
	$("#txttglkerjasama").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
});							   

function simpandata(){
	var kode		= $("#txtkode").val();
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= '';
	var telp 	 	= $("#txttelp").val();
	var npwp 	 	= $("#txtnpwp").val();
	var tglkerjasama = $("#txttglkerjasama").val();
	
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/supplier/simpan.php",
			data	: "kodesplr="+kode+	
					"&nama="+nama+					
					"&kota="+kota+
					"&alamat="+alamat+
					"&telp="+telp+
					"&npwp="+npwp+
					"&tglkerjasama="+tglkerjasama,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				window.location.assign('?mod=suppl');
				/*
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				*/
			}	
		});
	}	
}
$('#btnconfirm').click(function(){

	$("#warningx").text('');
	
	var kode		= $("#txtkode").val();
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= '';
	var telp 	 	= $("#txttelp").val();
	var npwp 	 	= $("#txtnpwp").val();
	var tglkerjasama = $("#txttglkerjasama").val();
	var batalkan = false;

	if(nama.length==0){
		$("#warningx").text('Nama masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(alamat.length==0){
		$("#warningx").text('Alamat masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(telp.length==0){
		$("#warningx").text('Nomor Telepon masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	/*
	$("#txtnama").val('');
	$("#alamat").val('');
	$("#txttelp").val('');
	$("#txtnpwp").val('');
	$("#txttglkerjasama").val('');
	*/
});

$('#txtnama').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#alamat').focus();
	}
});
$('#alamat').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#txttelp').focus();
	}
});

function tampil_data(){		

	$.ajax({
		type		: "POST",
		url			: "pages/supplier/cari.php",
		dataType	: "json",
		success		: function(data){
			
			$("#txtkode").val(data.kode_supplier);
			$("#txtnama").val(data.nama_supplier);		
			$("#alamat").val(data.alamat);
			$("#txttelp").val(data.notelp);
			$("#txtnpwp").val(data.npwp);
			$("#txttglkerjasama").val(data.tglkerjasama);
		}
	});
}