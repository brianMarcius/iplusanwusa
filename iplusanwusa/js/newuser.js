$(document).ready(function() {	
	isi_cbokaryawan();
	inserttemp();
});

function inserttemp() {
	$.ajax({
		type: 'POST', 
		url: 'pages/user/inserttemp.php',
		success: function(response) {
			tampillistmenu();
		}
	});		
}

function tampildata() {
	$.ajax({
		type		: "POST",
		url			: "pages/user/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#cbokaryawan").select2('val',data.idkaryawan);
			$("#txtusername").val(data.username);
			var username = data.username;
			if(username.length>0){
				$('#cbokaryawan').attr("disabled", true); 
				$('#txtusername').attr("disabled", true);
			}else{
				$('#cbokaryawan').attr("disabled", false); 
				$('#txtusername').attr("disabled", false);
			}
			$("#txtpassword").val(data.password);
			$("#txtrepassword").val(data.repassword);
			$("input:radio[name='radio-level'][value='"+data.level+"']").iCheck("check");
			$("input:radio[name='radio-blokir'][value='"+data.blokir+"']").iCheck("check");
			if(data.id_anak!='') {
				var id_anak = data.id_anak;
				id_anak.forEach(id_anakchecked);
				function id_anakchecked(item, index) {
					$("input:checkbox[value='"+item+"']").prop("checked",true);
				}
			}
			if(data.id_induk!='') {
				var id_induk = data.id_induk;
				id_induk.forEach(id_indukchecked);
				function id_indukchecked(itemx, indexx) {
					$("input:checkbox[value='"+itemx+"']").prop("checked",true);
				}
			}
		}
	});	
}

function actinduk(r, ID) {
	if(r.checked==true) {
		addmenuinduk(ID);
	}
	else {
		delmenuinduk(ID);
	}
}

function addmenuinduk(ID) {
	$.ajax({
		type		: "POST",
		url			: "pages/user/addmenuinduk.php",
		data 		: "id_induk="+ID,
		dataType	: "json",
		success		: function(data){
			var id_anak = data.id_anak;
			id_anak.forEach(id_anakchecked);
			function id_anakchecked(item, index) {
				$("input:checkbox[value='"+item+"']").prop("checked",true);
			}
		}
	});	
}

function delmenuinduk(ID) {
	$.ajax({
		type		: "POST",
		url			: "pages/user/delmenuinduk.php",
		data 		: "id_induk="+ID,
		dataType	: "json",
		success		: function(data){
			var id_anak = data.id_anak;
			id_anak.forEach(id_anakchecked);
			function id_anakchecked(item, index) {
				$("input:checkbox[value='"+item+"']").prop("checked",false);
			}
		}
	});	
}

function actanak(r, ID) {
	if(r.checked==true) {
		addmenuanak(ID);
	}
	else {
		delmenuanak(ID);
	}
}

function addmenuanak(ID) {
	$.ajax({
		type		: "POST",
		url			: "pages/user/addmenuanak.php",
		data 		: "id_anak="+ID,
		dataType	: "json",
		success		: function(data){
			$("input:checkbox[value='"+data.id_anak+"']").prop("checked",true);
		}
	});	
}

function delmenuanak(ID) {
	$.ajax({
		type		: "POST",
		url			: "pages/user/delmenuanak.php",
		data 		: "id_anak="+ID,
		dataType	: "json",
		success		: function(data){
			$("input:checkbox[value='"+data.id_anak+"']").prop("checked",false);
		}
	});	
}

function isi_cbokaryawan() {
	$.ajax({
		type: 'POST', 
		url: 'pages/user/tampilkan_karyawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response); 
		}
	});		
}

function tampillistmenu() {
	$.ajax({
		type: 'POST', 
		url: 'pages/user/tampillistmenu.php',
		success: function(response) {
			$('#tblmenu').html(response);
			tampildata();
		}
	});		
}

$('#btnsave').click(function(){
	simpandata();		
});

function simpandata(){
	var nik 		= $("#cbokaryawan").val();
	var username	= $("#txtusername").val();
	var password	= $("#txtpassword").val();
	var level 		= 'admin';
	var blokir 		= 'N';
	var batalkan = false;
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/user/simpan.php",
			data	: "nik="+nik+
						"&username="+username+
						"&password="+password+
						"&level="+level+
						"&blokir="+blokir,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				var respon = data.substr(4,6);

				if(respon=="Simpan" || respon=="Update") {
					$("#info-ok, #info-close").click(function() {
						window.location.assign("?mod=user");
					});
				}
			}	
		});	
	}	
}		

$('#btnconfirm').click(function(){
	var nik 		= $("#cbokaryawan").val();
	var username	= $("#txtusername").val();
	var password	= $("#txtpassword").val();
	var repassword	= $("#txtrepassword").val();
	var level 		= 'admin';
	var blokir 		= 'N';

	$("#lblwarning_save").text('');
	var batalkan = false;
		
	if(nik.length==0){
		$("#lblwarning_save").text('Nama Karyawan belum dipilih.');
		var batalkan = true;
		return false;
	}
	if(username.length==0){
		$("#lblwarning_save").text('Username belum diisi.');
		var batalkan = true;
		return false;
	}

	if(password.length==0){
		$("#lblwarning_save").text('Password belum diisi.');
		var batalkan = true;
		return false;
	}

	if(repassword.length==0){
		$("#lblwarning_save").text('Ulangi password belum diisi.');
		var batalkan = true;
		return false;
	}

	if(password!=repassword){
		$("#lblwarning_save").text('Ulangi password tidak sama.');
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/user/getdatacount.php",
			dataType: "json",
			success	: function(data){
				if(data.result<=0){
					$("#lblwarning_save").text('Hak Akses belum dipilih.');
					return false;
				}
				else {
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});
