$(document).ready(function() {	
	isi_cbokategori();
	isi_cbokandungan();
	tampildata('1');
});						   

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbokategori(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingkandungan/tampilkan_kategori.php',
		success: function(response) {
			$('#cbokategori').html(response);			
		}
	});		
}

function isi_cbokandungan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingkandungan/tampilkan_subkategori.php',
		success: function(response) {
			$('#cbokandungan').html(response); 
			$('#cbokandungan2').html(response);
		}
	});		
}

$('#cbotampilan').change(function() {
	$.ajax({
		type: 'POST', 
		url: 'pages/settingkandungan/hapus.php',
		success: function(response) {
			tampildata('1');
		}
	});			
});

$('#cbokategori').change(function() {
	$.ajax({
		type: 'POST', 
		url: 'pages/settingkandungan/hapus.php',
		success: function(response) {
			tampildata('1');
		}
	});
});

$('#cbokandungan').change(function() {
	$.ajax({
		type: 'POST', 
		url: 'pages/settingkandungan/hapus.php',
		success: function(response) {
			tampildata('1');
		}
	});
});

$('#txtcari').keyup(function() {
	tampildata('1');
});

function tampildata(pageno){	
	
	setCookie("pagech", pageno);
	var tampilan 	= $("#cbotampilan").val();
	var kategori 	= $("#cbokategori").val();
	var kandungan 	= $("#cbokandungan").val();
	var keyword		= $("#txtcari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/settingkandungan/tampildata.php",
		data	: "kategori="+kategori+
					"&kandungan="+kandungan+
					"&tampilan="+tampilan+
					"&keyword="+keyword+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			var pageno = getCookie("pagech");
			paging(pageno);
		}
	});
}	

function paging(pageno){		
	var tampilan 	= $("#cbotampilan").val();
	var kategori 	= $("#cbokategori").val();
	var kandungan 	= $("#cbokandungan").val();
	var keyword		= $("#txtcari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/settingkandungan/paging.php",
		data	: "kategori="+kategori+
					"&kandungan="+kandungan+
					"&tampilan="+tampilan+
					"&keyword="+keyword+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function changelock(kodebrg){	
	
	var checkbox= $('#'+kodebrg+'').prop('checked');
	
	if(checkbox==true){
		var checkbox=1;
	}else{
		var checkbox=0;
	}
	
	$.ajax({
		type: 'POST', 
		url: 'pages/settingkandungan/changelock.php',
		data: "kodebrg="+kodebrg+
				"&checkbox="+checkbox,
		success: function(response) {}
	});		
}

$('#btnconfirm').click(function() {
	var kandungan = $("#cbokandungan2").val();
	$("#warningx").text('');
	var batalkan = false;

	if(kandungan.length==0) {
		$("#warningx").show();
		$("#warningx").text("Kandungan Utama belum ditentukan");
		batalkan = true;
		return false;
	}
	if(batalkan==false) {		
		$.ajax({
			type: 'POST', 
			url: 'pages/settingkandungan/cekdata.php',
			dataType: "json",		
			success	: function(data){				
				if(data.ada>0){
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');
				}else{
					$("#warningx").show();
					$("#warningx").text("Tidak ada obat yang dipilih");
				}
			}
		});		
	}
});

$('#btnsave').click(function(){								
	var kandungan = $("#cbokandungan2").val();
	var batalkan = false;

	if(kandungan.length==0) {
		batalkan = true;
		return false;
	}

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingkandungan/update.php',
			data: "kandungan="+kandungan,
			success: function(response) {				
				$("#confirm-modal").modal('hide');
				tampildata('1');
			}
		});	
	}
});