$(document).ready(function() {
	tampilbarang();
	tampilsatuan();
	tampil_data();
});						   

function tampilsatuan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/harga/tampilkan_satuan.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbonamasatuan").html(data);
		}
	});
}

function tampilbarang(){
	$.ajax({
		type	: "POST", 
		url		: "pages/harga/tampilkan_barang.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbobarang").html(data);
		}
	});
}

$("#cbobarang").change(function() {
	tampilhrg();
})

function simpandata(){
	var brg 		= $("#cbobarang").val();
	var jns 		= $("#cbojns").val();
	var harga	 	= $("#txtharga").val();
	var satuan  	= $("#cbonamasatuan").val();
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/harga/simpan.php",
			data	: "brg="+brg+
					"&jns="+jns+
					"&harga="+harga+
					"&satuan="+satuan,
			timeout	: 3000,
			success	: function(data){
				tampilhrg();
				$("#cbobarang").val('');
				$("#cbonamasatuan").val('');
				$("#cbojns").val('');
				$("#txtharga").val('');
			}	
		});
	}	
}

$('#btn_addhrg').click(function(){
	
	$("#warningx").text('');

	var brg 		= $("#cbobarang").val();
	var jns 		= $("#cbojns").val();
	var harga	 	= $("#txtharga").val();
	var satuan  	= $("#cbonamasatuan").val();
	var batalkan = false;
	
	if(brg.length==0){
		$("#warningx").text('Barang belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(jns.length==0){
		$("#warningx").text('Jenis Customer belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(harga.length==0){
		$("#warningx").text('Harga barang belum di isi!');
		var batalkan = true;
		return false;
	}
	
	if(satuan.length==0){
		$("#warningx").text('Satuan belum dipilih!');
		var batalkan = true;
		return false;
	}
		
	if(batalkan==false){					
		simpandata();
	}	
});

function tampilhrg(){
	var kodebrg = $("#cbobarang").val();
	$.ajax({
		type		: "POST",
		url			: "pages/harga/tampilharga.php",
		data 		: "kodebrg="+kodebrg,
		success		: function(data){
			$("#tblhrg").html(data);
		}
	});
}


function tampil_data(){		
	$.ajax({
		type		: "POST",
		url			: "pages/harga/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#cbobarang").val(data.kodebrg);
			/*if(data.kodebrg.length>0) {
				document.getElementById('cbobarang').disabled = true;
			}*/
			tampilhrg();
		}
	});
}

function editharga(kodebrg, jnscustomer){
	$.ajax({
		type		: "POST",
		url			: "pages/harga/edit.php",
		data 		: "kodebrg="+kodebrg+
						"&jnscustomer="+jnscustomer,
		dataType	: "json",
		success		: function(data){
			$("#cbobarang").select2('val',kodebrg);
			$("#cbojns").val(data.jnscustomer);
			$("#txtharga").val(data.harga);
			$("#cbonamasatuan").val(data.satuan);
		}
	});
}

function hapus(kodebrg,jnscustomer){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkode").val(kodebrg);
	$("#txtjns").val(jnscustomer);
}

function hapusharga(){
	var kodebrg	 	= $("#txtkode").val();
	var jnscustomer	= $("#txtjns").val();

	var batalkan 	= false;

	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/harga/hapus.php",
			data	: "kodebrg="+kodebrg+
						"&jnscustomer="+jnscustomer,
			dataType: "json",
			success	: function(data){
				$("#confirm-modal").modal('hide');
				tampilhrg();
			}	
		});
	}	
}

$('#btnsave').click(function(){
	hapusharga();
});