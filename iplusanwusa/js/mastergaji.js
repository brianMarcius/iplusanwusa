$(document).ready(function(){
	isi_cbokaryawan();
});

function isi_cbokaryawan(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/mastergaji/tampil_karyawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response);	
		}
	});		
}

$('#cbokaryawan').change(function(){
	var idkaryawan = $('#cbokaryawan').val();			
	$.ajax({
		type	: "POST",
		url		: "pages/payroll/mastergaji/getdatakaryawan.php",
		data	: "idkaryawan="+idkaryawan,			
		dataType: "json",
		success	: function(data){
			$('#txtjabatan').val(data.jabatan);
			$('#kodejabatan').val(data.kodejabatan);
			$('#txtdivisi').val(data.divisi);
			$('#kodedivisi').val(data.kodedivisi);
			$('#txtarea').val(data.unit);
			$('#kodearea').val(data.kodearea);
			tampildata();
		}
	});					 
});

function tampildata(){
	var idkaryawan = $('#cbokaryawan').val();
	
	$.ajax({
		type	: "POST",		
		url		: "pages/payroll/mastergaji/tampildata.php",
		data	: "idkaryawan="+idkaryawan,
		success	: function(data){
			//$('#cbogudang').val(data.gudang);
			//$('#cbopic').val(data.pic);
			$("#tampildata").html(data);
		}
	});
}

function ubah(idkaryawan,jenis,jumlah){
	$("#ubahjml-modal").modal('toggle');
	$("#ubahjml-modal").modal('show');
	$("#warningjml").hide();
	if (jenis=="norek") {
		$("#txtjumlah").hide();
		$("#txtnorek").show();
		$('#txtnorek').val(jumlah);
	} else {
		$("#txtjumlah").show();
		$("#txtnorek").hide();
		$('#txtjumlah').val(jumlah);
	}
	if(jenis=="norek"){
		$("#judul-modal").html("No Rekening");
	}
	if(jenis=="gapok"){
		$("#judul-modal").html("Gaji Pokok");
	}
	if(jenis=="uangmakan"){
		$("#judul-modal").html("Uang Makan");
	}
	if(jenis=="tunjab"){
		$("#judul-modal").html("Tunjangan Jabatan");
	}
	if(jenis=="tunjbpjstk"){
		$("#judul-modal").html("Tunjangan BPJS Tenaga Kerja");
	}
	if(jenis=="tunjbpjskes"){
		$("#judul-modal").html("Tunjangan BPJS Kesehatan");
	}
	if(jenis=="insentif"){
		$("#judul-modal").html("Insentif");
	}
	if(jenis=="lembur"){
		$("#judul-modal").html("Lembur per jam");
	}
	if(jenis=="pph"){
		$("#judul-modal").html("Potongan PPH 21");
	}
	if(jenis=="koperasi"){
		$("#judul-modal").html("Potongan Koperasi");
	}
	if(jenis=="bpjs"){
		$("#judul-modal").html("Potongan Iuran BPJS");
	}
	if(jenis=="potbpjskes"){
		$("#judul-modal").html("Potongan BPJS Kesehatan Perusahaan");
	}
	if(jenis=="potbpjstk"){
		$("#judul-modal").html("Potongan BPJS Tenaga Kerja Perusahaan");
	}
	if(jenis=="lain"){
		$("#judul-modal").html("Potongan Kredit");
	}
	$('#txtidkaryawan').val(idkaryawan);
	$('#txtjenis').val(jenis);
}

$('#ubahjml-modal').on('shown.bs.modal', function() {
	$('#txtjumlah').focus();
})

$('#btnupdatejml').click(function(){
	simpandata();
});

function EnterSimpan(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		simpandata();
	}
}

function simpandata(){
	var idkaryawan = $("#txtidkaryawan").val();
	var jenis = $("#txtjenis").val();
	var norek = $("#txtnorek").val();
	var jumlah = $("#txtjumlah").val();
	$("#warningjml").hide();
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/mastergaji/simpan.php",
			data	: "idkaryawan="+idkaryawan+
						"&jenis="+jenis+
						"&norek="+norek+
						"&jumlah="+jumlah,
			timeout	: 3000,
			success	: function(data){				
				$("#ubahjml-modal").modal('hide');
				tampildata();
			}	
		});
	}
}