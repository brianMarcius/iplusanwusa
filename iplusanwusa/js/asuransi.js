$(document).ready(function() {
	tampildata('1');
	//isi_cbojns();
});	

// function isi_cbojns() {
// 	$.ajax({
// 		type	: "GET",		
// 		url		: "pages/harga/tampilkan_jnscustomer.php",
// 		success	: function(data){
// 			$("#cbojns").html(data);
// 		}
// 	});
// }


// $('#cbojns').change(function(){
// 	tampildata('1');
// });

function tampildata(pageno){
	// var kode = $("#cbojns").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/marginasuransi/tampildata.php",
		data	: "page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	// var kode = $("#cbojns").val();		

	$.ajax({
		type	: "GET",		
		url		: "pages/marginasuransi/paging.php",
		data	: "page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function editbatasbawah(kode,batasbawah,batasatas){
	$("#labelkode_updbatasbawah").html(kode);
	$("#txtbatasbawah").val('');
	$("#lblbatasbawah").html(batasbawah);
	$("#lblbatasatas").html(batasatas);
	$("#inputwarning_updbatasbawah").hide();
	$("#updbatasbawah-modal").modal('toggle');
	$("#updbatasbawah-modal").modal('show');
}

$('#btnupdate_updbatasbawah').click(function(){
	updatebatasbawah();
});

function updatebatasbawah(){
	var kode = $("#labelkode_updbatasbawah").html();
	var batasbawah = $("#lblbatasbawah").html();
	var batasatas = $("#lblbatasatas").html();
	var txtx = $("#txtbatasbawah").val();
	var txt = txtx.replace(',','.');
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/marginasuransi/updatebatasbawah.php',
			data: "kode="+kode+
					"&batasbawah="+batasbawah+
					"&batasatas="+batasatas+
					"&txt="+txt,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_updbatasbawah").hide();
				$("#updbatasbawah-modal").modal('hide');
			}
		});	
	}
}

function editbatasatas(kode,batasbawah,batasatas){
	$("#labelkode_updbatasatas").html(kode);
	$("#txtbatasatas").val('');
	$("#lblbatasbawah").html(batasbawah);
	$("#lblbatasatas").html(batasatas);
	$("#inputwarning_updbatasatas").hide();
	$("#updbatasatas-modal").modal('toggle');
	$("#updbatasatas-modal").modal('show');
}

$('#btnupdate_updbatasatas').click(function(){
	updatebatasatas();
});

function updatebatasatas(){
	var kode = $("#labelkode_updbatasatas").html();
	var batasbawah = $("#lblbatasbawah").html();
	var batasatas = $("#lblbatasatas").html();
	var txtx = $("#txtbatasatas").val();
	var txt = txtx.replace(',','.');
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/marginasuransi/updatebatasatas.php',
			data: "kode="+kode+
					"&batasbawah="+batasbawah+
					"&batasatas="+batasatas+
					"&txt="+txt,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_updbatasatas").hide();
				$("#updbatasatas-modal").modal('hide');
			}
		});	
	}
}

function editmargin(kode,batasbawah,batasatas){
	$("#labelkode_updmargin").html(kode);
	$("#txtmargin").val('');
	$("#lblbatasbawah").html(batasbawah);
	$("#lblbatasatas").html(batasatas);
	$("#inputwarning_updmargin").hide();
	$("#updmargin-modal").modal('toggle');
	$("#updmargin-modal").modal('show');
}

$('#btnupdate_updmargin').click(function(){
	updatemargin();
});

function updatemargin(){
	var kode = $("#labelkode_updmargin").html();
	var batasbawah = $("#lblbatasbawah").html();
	var batasatas = $("#lblbatasatas").html();
	var txtx = $("#txtmargin").val();
	var txt = txtx.replace(',','.');
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/marginasuransi/updatemargin.php',
			data: "kode="+kode+
					"&batasbawah="+batasbawah+
					"&batasatas="+batasatas+
					"&txt="+txt,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_updmargin").hide();
				$("#updmargin-modal").modal('hide');
			}
		});	
	}
}
