$(document).ready(function() {		
	edit_data();
});						   

function simpandata(){
	var kode		= $("#txtkode").val();
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();	
	var telp 	 	= $("#txttelp").val();
	var ktp 	 	= $("#txtktp").val();
	var tgllahir 	= $("#txttgllahir").val();
	
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/dokter/simpan.php",
			data	: "kodedokter="+kode+
					"&nama="+nama+					
					"&alamat="+alamat+
					"&telp="+telp+
					"&ktp="+ktp+				
					"&tgllahir="+tgllahir,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				window.location.assign('?mod=listdokter');
				/*
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				*/
			}	
		});
	}	
}
$('#btnconfirm').click(function(){
	
	$("#warningx").text('');
		
	var kode		= $("#txtkode").val();
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();	
	var telp 	 	= $("#txttelp").val();
	var ktp 	 	= $("#txtktp").val();
	var tgllahir 	= $("#txttgllahir").val();
	
	var batalkan = false;
	
	if(nama.length==0){
		$("#warningx").text('Nama masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(alamat.length==0){
		$("#warningx").text('Alamat masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(telp.length==0){
		$("#warningx").text('Nomor Telepon masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	/*
	$("#txtkode").val('');
	$("#txtnama").val('');
	$("#alamat").val('');
	$("#txttelp").val('');
	$("#txtktp").val('');	
	$("#txttgllahir").val('');
	*/
});


$('#txtnama').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#alamat').focus();
	}
});
$('#alamat').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#txttelp').focus();
	}
});
$('#txttelp').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('# txtktp').focus();
	}
});

function edit_data(){		
	$.ajax({
		type		: "POST",
		url			: "pages/dokter/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#txtkode").val(data.kode);
			$("#txtnama").val(data.nama);
			$("#alamat").val(data.alamat);
			$("#txttelp").val(data.notelp);
			$("#txtktp").val(data.ktp);			
			$("#txttgllahir").val(data.tgllahir);
		}
	});
}



