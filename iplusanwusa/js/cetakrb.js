$(document).ready(function() {
	getdatapengambilan();	
});	

function getdatapengambilan(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/cetakrb/datapengambilan.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblno").text(data.nonota);
			$("#lblnoretbeli").text(data.koderetur);
			$("#lbltgltrx").text(data.tglnota);
			$("#lblsupplier").text(data.supplier);
			$("#lblalamatsup").text(data.alamatsup);
			// $("#lblpenerima").text(data.penerima);
			tampildata();
		}
	});
}

function tampildata(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakrb/tampildata.php",
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function cetretbeli(){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetakrb/cetakrb.php",
		success	: function(data){
			if(data.result==1){				
				// window.open('?mod=cetakrb','_blank');
			}
		}	
	});
	window.open('?mod=cetakrb','_blank');
}

function goBack() {
    window.history.back();
}