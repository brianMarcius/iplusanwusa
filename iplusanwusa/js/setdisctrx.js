$(document).ready(function() {						   
	tampildata('1');
	isi_cbocabang();
});						   

$(function() {
	$("#txttglmulai").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
	$("#txttglakhir").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
});		

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingdiscpertrx/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbocabang").change(function() {
	tampildata('1');
})

$("#cari").keyup(function() {
	tampildata('1');
})

$('#btnview').click(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var cabang = $("#cbocabang").val();
	var cari = $("#cari").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/settingdiscpertrx/tampildata.php",
		data	: "cabang="+cabang+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
						

		}
	});
}	

function paging(pageno){
	var cabang = $("#cbocabang").val();
	var cari = $("#cari").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/settingdiscpertrx/paging.php",
		data	: "cabang="+cabang+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
				

		}
	});
}
function edittglmulai(kode,kodearea){
	$("#labelkode_updtglmulai").html(kode);
	$("#lblkodecabmul").html(kodearea);
	$("#txttglmulai").val('');
	$("#inputwarning_updtglakhir").hide();
	$("#updtglmulai-modal").modal('toggle');
	$("#updtglmulai-modal").modal('show');
}

$('#btnupdatetglmulai').click(function(){
	updatetglmulai();
});

function updatetglmulai(){
	var jns = $("#labelkode_updtglmulai").html();
	var kodearea = $("#lblkodecabmul").html();
	var tglmulai = $("#txttglmulai").val();
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingdiscpertrx/updatetglmulai.php',
			data: "kodearea="+kodearea+
					"&jns="+jns+
					"&tglmulai="+tglmulai,
			success: function(response) {				
				tampildata('1');
				$("#inputwarning_updtglmulai").hide();
				$("#updtglmulai-modal").modal('hide');
			}
		});	
	}
}

function edittglakhir(kode,kodearea){
	$("#labelkode_updtglakhir").html(kode);
	$("#lblkodecabakh").html(kodearea);
	$("#txttglakhir").val('');
	$("#inputwarning_updtglakhir").hide();
	$("#updtglakhir-modal").modal('toggle');
	$("#updtglakhir-modal").modal('show');
}

$('#btnupdatetglakhir').click(function(){
	updatetglakhir();
});

function updatetglakhir(){
	var jns = $("#labelkode_updtglakhir").html();
	var kodearea = $("#lblkodecabakh").html();
	var tglakhir = $("#txttglakhir").val();
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingdiscpertrx/updatetglakhir.php',
			data: "kodearea="+kodearea+
					"&jns="+jns+
					"&tglakhir="+tglakhir,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_updtglakhir").hide();
				$("#updtglakhir-modal").modal('hide');
			}
		});	
	}
}

function editdiscpersen(kode,kodearea){
	$("#labelkode_upddiscpersen").html(kode);
	$("#labelkodeareadisc").html(kodearea);
	$("#txtdiscpersen").val('');
	$("#inputwarning_upddiscpersen").hide();
	$("#upddiscpersen-modal").modal('toggle');
	$("#upddiscpersen-modal").modal('show');
}

$('#btnupdate_upddiscpersen').click(function(){
	updatediscpersen();
});

function updatediscpersen(){
	var kodearea = $("#labelkodeareadisc").html();
	var jns = $("#labelkode_upddiscpersen").html();
	var discpersen = $("#txtdiscpersen").val();
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingdiscpertrx/updatediscpersen.php',
			data: "kodearea="+kodearea+
					"&jns="+jns+
					"&discpersen="+discpersen,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_upddiscpersen").hide();
				$("#upddiscpersen-modal").modal('hide');
			}
		});	
	}
}