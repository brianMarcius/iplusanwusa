$(document).ready(function() {				   
	isi_cbosupplier();
});		

$(function() {

	"use strict";
	
	$("#chkincppn").on('ifUnchecked', function(event) {
		document.getElementById("txtharga").disabled = false;
		document.getElementById("txthargappn").disabled = true;
		$("#txthargappn").val('');
		$("#txtppnpersen").val('10');		
	});
	
	$("#chkincppn").on('ifChecked', function(event) {
		document.getElementById("txtharga").disabled = true;
		document.getElementById("txthargappn").disabled = false;	
		$("#txtharga").val('');
		$("#txtppnpersen").val('0');
	});
	
	$("#txttglexp").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		
});
 
function isi_cbosupplier(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/pembelian/tampilkan_supplier.php',
		success: function(response) {
			$('#cbosupplier').html(response); 
			isian_default();	
		}
	});		
}

function isian_default(){
	$.ajax({
		type: 'POST', 
		url: 'pages/pembelian/setdefault.php',
		dataType: "json",
		success	: function(data){			
			var edit=data.kodebeli;
			if(edit.length>0){
				$('#txtkodebeli').val(data.kodebeli);
				$('#txtnonota').val(data.nonota);
				$('#txttglbeli').val(data.tglbeli);
				$('#txttgljthtempo').val(data.tgljthtempo);
				$('#cbosupplier').select2('val',data.kodesupplier);
				$('#txtalamat').val(data.alamat);
				$('#txtdiscpersen').val(data.disctot);
				$('#txtdisc').val(data.rpdisctot);
				$('#txtppnpersen').val(data.ppn);
				$('#txtppn').val(data.rpppn);
				$('#txtsaldohutang').val(data.rpgrandtot);
				
				if(data.checkbox==1){
					$("#chkincppn").iCheck("check");
				}else{					
					$("#chkincppn").iCheck("uncheck");
				}
			}else{				
				$('#txttglbeli').val(data.tglbeli);	
				$('#txttgljthtempo').val(data.tglblndepan);				
			}
			tampillistbrg();
			$("#txtnonota").focus();
		}
	});		
}

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		$("#txtcaribarang").val('');
		tampil_masterbrg(1);		
	}
}

$("#txtnamabrg").click(function(){
	var namabrg = $("#txtnamabrg").val();
	
	if(namabrg.length==0){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		$("#txtcaribarang").val('');
		tampil_masterbrg(1);
	}
});

function EnterHarga(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#txttglexp").focus();		
	}
}

function EnterHargappn(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#txttglexp").focus();		
	}
}

function EnterEd(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#txtjml").focus();		
	}
}

function EnterJml(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#cbosatuan").focus();				
	}
}

function EnterDisc(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#txtdiscrp_peritem").focus();		
	}
}

function EnterRpdisc(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		addbarang();	
		$("#txtnamabrg").focus();
	}
}

function getdiscrp_peritem(){
	var harga	= $("#txtharga").val();
	var hargappn= $("#txthargappn").val();
	var jml 	= $("#txtjml").val();
	var disc 	= $("#txtdisc_peritem").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/pembelian/getdiscrp.php',
		data: "harga="+harga+
				"&hargappn="+hargappn+
				"&jml="+jml+
				"&disc="+disc,	
		dataType: "json",
		success: function(data) {
			$("#txtdiscrp_peritem").val(data.discrp);
		}
	});	
	//var discrp = Math.ceil((harga * jml * disc) / 100);	
}

$("#txtdisc_peritem").keyup(function(){
	 getdiscrp_peritem();
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/pembelian/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
		}
	});		
}

function getkodebrg(kodebrg,namabrg){
	$('#txtkodebrg').val(kodebrg);
	$('#txtnamabrg').val(namabrg);
	isi_cbosatuan('');
	$("#barang-modal").modal('hide');	
	//$("#txtharga").focus();
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/pembelian/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);			
			$("#txtcaribarang").focus();
		}
	});
}

$("#txtcaribarang").keyup(function(){
	 tampil_masterbrg(1);
});

$("#txttglexp").change(function(){
	$("#txtjml").focus();
});

function isi_cbosatuan(kode){
	var kodebrg = $('#txtkodebrg').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/pembelian/tampilkan_satuan.php',
		data	: "kodebrg="+kodebrg+
					"&kodesatuan="+kode,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

$('#cbosupplier').change(function(){
	var kodesupplier = $('#cbosupplier').val();			
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/getdatasupplier.php",
		data	: "kodesupplier="+kodesupplier,			
		dataType: "json",
		success	: function(data){
			$('#txtalamat').val(data.alamat);
			$('#txtnotelp').val(data.notelp);
			//setbtnconfirm();
		}
	});					 
});	

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function clearall(){
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();
			$("#txtrppot").val('0');
		}
	});
}

function addbarang(){
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var harga	= $("#txtharga").val();
	var hargappn = $('#txthargappn').val();
	var disc	= $("#txtdisc_peritem").val();
	var rpdisc	= $("#txtdiscrp_peritem").val();	
	var tglexp 	= $('#txttglexp').val();			

	$('#warningx').html('');
	var batalkan = false;	
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0 || jml.length==0){
		$('#warningx').html('Jumlah pembelian masih kosong.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(disc<0){
		$('#warningx').html('Disc tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(rpdisc<0){
		$('#warningx').html('Rp Disc tidak valid.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	/*
	if(harga<=0 && hargappn<=0){
		$('#warningx').html('No Produksi Batch belum diisi.');
		$("#txtnobatch").focus();
		var batalkan = true;
	}
	*/
	if(tglexp.length==0){
		$('#warningx').html('Tgl expired belum diisi.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pembelian/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat+	
					"&tglexp="+tglexp+	
					"&harga="+harga+
					"&hargappn="+hargappn+	
					"&disc="+disc+
					"&rpdisc="+rpdisc,
			dataType: "json",
			success	: function(data){	
				
				if(data.sukses==0){
					$('#warningx').html(data.pesan);
					$("#txtjml").focus();
				}else{
					 tampillistbrg();
					 clearaddbrg();
					 getrphutang();
				}
			}	
		});
	}
}

function getrphutang(){
	var rppot  = $("#txtrppot").val();
	var disc  = $("#txtdiscpersen").val();
	var ppn  = $("#txtppnpersen").val();
	//alert('PPN :'+ppn);
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/getrphutang.php",
		data	: "disc="+disc+
					"&ppn="+ppn+
					"&rppot="+rppot,
		dataType: "json",
		success	: function(data){
			$("#txtdisc").val(data.rpdisc);
			$("#txtppn").val(data.rpppn);
			$("#txtsaldohutang").val(data.jmlhutang);			
		}
	});
}

function clearaddbrg(){
	$("#txtnamabrg").val('');
	$("#txtkodebrg").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	isi_cbosatuan('');
	$("#txtharga").val('');
	$("#txthargappn").val('');
	$("#txttglexp").val('');
	$("#txtdisc_peritem").val('');
	$("#txtdiscrp_peritem").val('');	
}

function simpandata(){
	
	var kodesupplier = $("#cbosupplier").val();
	var kodebeli	 = $("#txtkodebeli").val();
	var nonota		= $("#txtnonota").val();
	var tglbeli		= $("#txttglbeli").val();
	var tgljthtempo	= $("#txttgljthtempo").val();
	var jmlbayar	= 0;
	var jmlhutang	= $("#txtsaldohutang").val();
	var rppot 		= $("#txtrppot").val();
	var disc 		= $("#txtdiscpersen").val();
	var rpdisc 		= $("#txtdisc").val();
	var ppn 		= $("#txtppnpersen").val();	
	var rpppn 		= $("#txtppn").val();
	var batalkan = false;	
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pembelian/simpan.php",
			data	: "kodesupplier="+kodesupplier+
						"&kodebeli="+kodebeli+
						"&nonota="+nonota+
						"&tglbeli="+tglbeli+
						"&tgljthtempo="+tgljthtempo+
						"&jmlbayar="+jmlbayar+
						"&rppot="+rppot+
						"&disc="+disc+
						"&rpdisc="+rpdisc+
						"&ppn="+ppn+
						"&rpppn="+rpppn+
						"&jmlhutang="+jmlhutang,
			timeout	: 3000,
			success	: function(data){	
				
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);		
			}	
		});	
	}	
}		

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				getrphutang();			
			}				
		}
	});
}

$('#btnconfirm').click(function(){
								
	var nonota	= $("#txtnonota").val();
	var tglbeli		= $("#txttglbeli").val();
	var tgljthtempo	= $("#txttgljthtempo").val();	
	var kodesupplier = $("#cbosupplier").val();		
	var jmlbayar	= 0;
	var jmlhutangx	= $("#txtsaldohutang").val();
	var jmlhutang	= jmlhutangx.substr(0, 2);	
	
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(nonota.length==0){
		$("#lblwarning_save").text('No Faktur belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(tglbeli.length==0){
		$("#lblwarning_save").text('Tgl Faktur belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(tgljthtempo.length==0){
		$("#lblwarning_save").text('Tgl Jth Tempo pembayaran belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(kodesupplier.length==0){
		$("#lblwarning_save").text('Nama Supplier belum dipilih.');
		var batalkan = true;
		return false;
	}	
	
	if(jmlhutang<0){
		$("#lblwarning_save").text('Jml bayar melebihi jml rupiah beli.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/pembelian/datecompare.php",
			data	: "tglbeli="+tglbeli,
			dataType: "json",
			success	: function(data){
				if(data.lanjut==0){
					$("#lblwarning_save").text(data.pesan);
					return false;
				}else{
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});

$('#btnokinfo').click(function(){
	window.location.assign("?mod=printbd");
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$("#txtdiscpersen").keyup(function(){
	getrphutang();
});

$("#txtrppot").keyup(function(){
	getrphutang();
});

$("#txtppnpersen").keyup(function(){
	 getrphutang();
});

function regnewbank(){
	var bank	 	= $("#txtbank").val();	
	var batalkan 	= false;

	if(bank<=1){
		$('#warningreg').html('Nama bank harus di inputkan!!');
		$("#txtbank").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/pelunasan/addbank.php",
			data	: "bank="+bank,
			timeout	: 3000,
			success	: function(data){
				$('#warningreg').show();
				$('#warningreg').html(data)
				tampilkan_bank();
				$("#txtbank").val('');
				
			}	
		});
	}	
}

$('#bank_add').click(function(){		
	$("#regnewbank-modal").modal('toggle');
	$("#regnewbank-modal").modal('show');
	$('#warningreg').hide();
});

$('#btnadd').click(function(){
	regnewbank();
});

function goBack() {
    window.history.back();
}

function edit(ID,bonus){
	$.ajax({
		type	: "POST",
		url		: "pages/pembelian/edit.php",
		data	: "kodebrg="+ID+
					"&bonus="+bonus,
		dataType: "json",
		success	: function(data){	
			$('#txtkodebrg').val(data.kodebrg);
			$('#txtnamabrg').val(data.namabrg);			
			$('#txttglexp').val(data.tglexpired);
			$('#txtharga').val(data.hargabrg);
			$('#txthargappn').val(data.hargappn);
			$('#txtjml').val(data.jmlbrg);
			isi_cbosatuan(data.kodesatuan);
			$('#txtdisc_peritem').val(data.disc);
			$('#txtdiscrp_peritem').val(data.rpdisc);
			if(data.hargappn>0){
				document.getElementById("txtharga").disabled = true;
				document.getElementById("txthargappn").disabled = false;
			}else{
				document.getElementById("txtharga").disabled = false;
				document.getElementById("txthargappn").disabled = true;
			}							
		}
	});
}

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});