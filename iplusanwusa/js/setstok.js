$(document).ready(function() {	
	isi_cbocabang();					   	
});						   

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingstok/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
			tampildata('1');
		}
	});		
}

$("#cbocabang").change(function() {
	tampildata('1');
})

$("#cari").keyup(function(){
	tampildata('1');						 
});	

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampildata(pageno);						 
});

function tampildata(pageno){	
	
	setCookie("pagech", pageno);
	var cabang = $("#cbocabang").val();
	var cari = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/settingstok/tampildata.php",
		data	: "cabang="+cabang+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			var pageno = getCookie("pagech");
			paging(pageno);
		}
	});
}	

function paging(pageno){	
	
	var cabang = $("#cbocabang").val();
	var cari = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/settingstok/paging.php",
		data	: "cabang="+cabang+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function editsaldo(kode){
	
	setCookie("pagechtamp", getCookie("pagech"));
	
	$("#labelkode_updsaldo").html(kode);
	$("#txtsaldo").val('');
	$("#inputwarning_updsaldo").hide();
	$("#updsaldo-modal").modal('toggle');
	$("#updsaldo-modal").modal('show');
	$("#txtsaldo").focus();	
}

$('#btnupdate_updsaldo').click(function(){
	updatesaldo();
});

function updatesaldo(){	
	var kode = $("#labelkode_updsaldo").html();
	var saldo = $("#txtsaldo").val();
	var kodearea = $("#cbocabang").val();
	var batalkan = false;
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingstok/updatesaldo.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&saldo="+saldo,
			success: function(response) {
				var pageno = getCookie("pagech");			
				tampildata(pageno);
				$("#inputwarning_updsaldo").hide();
				$("#updsaldo-modal").modal('hide');
			}
		});	
	}
}

function editminstok(kode){
	setCookie("pagechtamp", getCookie("pagech"));
	
	$("#labelkode_updminstok").html(kode);
	$("#txtminstok").val('');
	$("#inputwarning_updminstok").hide();
	$("#updminstok-modal").modal('toggle');
	$("#updminstok-modal").modal('show');
}

$('#btnupdate_updminstok').click(function(){
	updateminstok();
});

function updateminstok(){
	var kode = $("#labelkode_updminstok").html();
	var minstok = $("#txtminstok").val();
	var kodearea = $("#cbocabang").val();
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingstok/updateminstok.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&minstok="+minstok,
			success: function(response) {
				var pageno = getCookie("pagech");			
				tampildata(pageno);
				$("#inputwarning_updminstok").hide();
				$("#updminstok-modal").modal('hide');
			}
		});
	}
}

function editmaxstok(kode){	
	setCookie("pagechtamp", getCookie("pagech"));
	$("#labelkode_updmaxstok").html(kode);
	$("#txtmaxstok").val('');
	$("#inputwarning_updmaxstok").hide();
	$("#updmaxstok-modal").modal('toggle');
	$("#updmaxstok-modal").modal('show');
}

$('#btnupdate_updmaxstok').click(function(){
	updatemaxstok();
});

function updatemaxstok(){
	var kode = $("#labelkode_updmaxstok").html();
	var maxstok = $("#txtmaxstok").val();
	var kodearea = $("#cbocabang").val();
	var batalkan = false;

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingstok/updatemaxstok.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&maxstok="+maxstok,
			success: function(response) {
				var pageno = getCookie("pagech");			
				tampildata(pageno);
				$("#inputwarning_updmaxstok").hide();
				$("#updmaxstok-modal").modal('hide');
			}
		});
	}
}

function changelock(kode,kodearea){		
	
	$.ajax({
		type: 'POST', 
		url: 'pages/settingstok/changelock.php',
		data: "kode="+kode+
				"&cabang="+kodearea,
		success: function(response) {
			
			//setCookie("pagech", pageno);
			
			//tampildata(pageno);
		}
	});		
}