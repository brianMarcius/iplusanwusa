$(document).ready(function() {						   
	tampildata('1');
});						   

$('#btnview').click(function(){
	tampildata('1');						 
});	

$("#txtsearching").keyup(function(){
	tampildata('1');							  
});	

function tampildata(pageno){
	var kode 		= $("#txtsearching").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/pembayaran/tampildata.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var kode 		= $("#txtsearching").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/pembayaran/paging.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function showdetail(kode){	
	$('#info-modal').modal('toggle');
	$("#info-modal").modal('show');	
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaran/tampildetail.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayx").show();
			$("#loading-imgx").show();
		},
		success	: function(data){
			$("#overlayx").hide();
			$("#loading-imgx").hide();
			$("#infone").html(data);
		}	
	});
}

function setpayment(kode){		
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaran/lintasform.php",
		data	: "kode="+kode,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=entpaym');
			}
		}
	});
}

