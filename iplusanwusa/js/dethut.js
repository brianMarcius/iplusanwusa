$(document).ready(function() {	
	getdata();
});	

function getdata(){	
	$.ajax({
		type	: "POST", 
		url		: "pages/detailhutang/getdata.php",
		dataType: "json",
		success	: function(data){
			$("#txtkodesupplier").val(data.kode_supplier);
			$("#txtnamasupplier").val(data.nama_supplier);
			$("#txtalamat").val(data.alamat);
			tampil_list();
		}
	});
}

function tampil_list(){			
	$.ajax({
		type	: "POST", 
		url		: "pages/detailhutang/tampil_listhutang.php",
		success	: function(data){
			$("#tblhutang").html(data);	
		}
	});	
}

function goBack() {
    window.history.back();
}