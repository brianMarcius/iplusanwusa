$(document).ready(function(){
	isi_cbobulan();
	isi_cbotahun();
	isi_cboarea();
	tampilgaji();
});

function isi_cbobulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/reportgaji/listbulan.php',
		success: function(response) {
			$('#cbobulan').html(response);
		}
	});	
}

function isi_cbotahun(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/reportgaji/listtahun.php',
		success: function(response) {
			$('#cbotahun').html(response);
		}
	});	
}

function isi_cboarea(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbperusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
		}
	});	
}

function tampilgaji(){	
	var	bulan = $("#cbobulan").val();
	var	tahun = $("#cbotahun").val();
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/reportgaji/tampilgaji.php',
		data: {
          	bulan: bulan,
          	tahun: tahun,
          	kodearea: kodearea
        },
		success: function(response) {
			$('#tblgaji').html(response); 
		}
	});	
}

$('#cbobulan').change(function(){
	tampilgaji();				 
});

$('#cbotahun').change(function(){
	tampilgaji();				 
});

$('#cboarea').change(function(){
	tampilgaji();				 
});

function cetakslipgaji(){
	window.open("pages/payroll/reportgaji/cetakslipgaji.php");
}

function cetakrptgaji(){
	window.open("pages/payroll/reportgaji/cetakrptgaji.php");
}

function ubah(idkaryawan,bulan,tahun,jumlah,jenis){
	$("#ubahgaji-modal").modal('toggle');
	$("#ubahgaji-modal").modal('show');
	$("#warningjml").hide();
	if(jenis=="pph"){
		$("#judul-modal").html("Potongan PPH 21");
	}
	if(jenis=="koperasi"){
		$("#judul-modal").html("Potongan Koperasi");
	}
	if(jenis=="bpjs"){
		$("#judul-modal").html("Potongan Iuran BPJS");
	}
	if(jenis=="kredit"){
		$("#judul-modal").html("Potongan Kredit");
	}
	$('#txtjumlah').val(jumlah);
	$('#txtidkaryawan').val(idkaryawan);
	$('#txtbulan').val(bulan);
	$('#txttahun').val(tahun);
	$('#txtjenis').val(jenis);
}

$('#ubahgaji-modal').on('shown.bs.modal', function() {
	$('#txtjumlah').focus();
})

$('#btnupdatejml').click(function(){
	ubahdata();
});

function EnterUbah(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		ubahdata();
	}
}

function ubahdata(){
	var jumlah = $("#txtjumlah").val();
	var idkaryawan = $("#txtidkaryawan").val();
	var bulan = $("#txtbulan").val();
	var tahun = $("#txttahun").val();
	var jenis = $("#txtjenis").val();
	$("#warningjml").hide();						
	
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/reportgaji/update.php",
		data	: "idkaryawan="+idkaryawan+
					"&bulan="+bulan+
					"&tahun="+tahun+
					"&jenis="+jenis+
					"&jumlah="+jumlah,
		timeout	: 3000,
		success	: function(data){				
			$("#ubahgaji-modal").modal('hide');
			tampilgaji();
		}	
	});
}