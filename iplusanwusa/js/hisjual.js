$(document).ready(function() {
						   
	isi_cboarea();	
});	

function isi_cboarea(){		
	
	$.ajax({
		type: 'POST', 
		url: 'pages/hisjual/tampilkan_area.php',
		success: function(data) {				
			$('#cboarea').html(data);				
			isi_cbocustomer();
		}
	});		
}

function isi_cbocustomer(){		
	
	var area = $("#cboarea").val();
	var tgltrx1 = $("#tgltrx1").val();	
	var tgltrx2 = $("#tgltrx2").val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/hisjual/tampilkan_customer.php',
		data: "kodearea="+area+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2,
		success: function(data) {				
			$('#cbocustomer').html(data);	
			settgldefa();
		}
	});		
	
}


function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/hisjual/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx1").val(data.tgltrx1);
			$("#tgltrx2").val(data.tgltrx2);				
			tampildata();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx1 = $("#tgltrx1").val();	
	var tgltrx2 = $("#tgltrx2").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/hisjual/tglarea.php",
		data	: "area="+area+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
		}
	});
}

$('#cboarea').change(function(){	
	isi_cbocustomer();						  
});

$('#tgltrx1').change(function(){
	tampildata();
});

$('#tgltrx2').change(function(){
	tampildata();
});

$('#cbocustomer').change(function(){	
	tampildata();
});


function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx1 = $("#tgltrx1").val();
	var tgltrx2 = $("#tgltrx2").val();
	var kodecustomer = $("#cbocustomer").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/hisjual/tampildata.php",
		data	: "area="+area+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2+
				"&kodecustomer="+kodecustomer,
		success	: function(data){				
			$("#tampildata").html(data);
			settglarea();
		}
	});
	
}	
