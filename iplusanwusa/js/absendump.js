$(document).ready(function(){
	list_cbperusahaan();
	gettglskg();
	tampilabsen();
});

function list_cbperusahaan(){
	$.ajax({
		type: 'POST',
		url: 'pages/payroll/absen/list_perusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
		}
	});	
}

function gettglskg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/absen/gettglskg.php",
		dataType: "json",
		success	: function(data){
			$('#tglkerja').val(data.tglskg);
		}	
	});
}

function tampilabsen(){	
	var	tglkerja = $("#tglkerja").val();
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/absen/tampil_absen_dump.php',
		data: {
          	tanggal: tglkerja,
          	kodearea: kodearea
        },
		success: function(response) {
			$('#tblabsensi').html(response); 
		}
	});	
}

function cetakabsendump(){
	var tgl	= $("#tglkerja").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(tgl.length==0){
		alert("Tanggal belum di input");
		var batalkan = true;
		return false;
	}

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type: 'POST', 
			url: 'pages/payroll/absen/lintasrpt_absendump.php',
			data: {
          		tanggal: tgl,
          		kodearea: area
        	}
		});

		window.open("pages/payroll/absen/cetakabsendump.php");
	}
}
