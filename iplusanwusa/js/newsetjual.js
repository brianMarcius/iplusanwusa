$(document).ready(function() {	
	document.getElementById("btnconfirm").disabled = true;
	isi_cbocabang();
});		

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpenjualan/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbocustomer").change(function() {
	setbtnconfirm();
})

$("#cbocabang, #cbojns").change(function() {
	isi_cbocustomer();
	setbtnconfirm();
})

$("#txtsaldoawal, #txtmaxpiutang").keyup(function() {
	setbtnconfirm();
})

function isi_cbocustomer(){
	var kodearea = $("#cbocabang").val();
	var jns = $("#cbojns").val();
	$.ajax({
		type: 'POST', 
		data: "jns="+jns+
				"&kodearea="+kodearea,
		url: 'pages/settingpenjualan/tampilkan_customer.php',
		success: function(response) {
			$('#cbocustomer').html(response); 
		}
	});		
}

function setbtnconfirm(){
	var kodecabang = $("#cbocabang").val();
	var kodecstmr = $("#cbocustomer").val();
	var saldoawal = $("#txtsaldoawal").val();
	var maxpiutang = $("#txtmaxpiutang").val();

	if(kodecabang.length>0 && kodecstmr.length>0 && saldoawal.length>0 && maxpiutang.length>0){
		document.getElementById("btnconfirm").disabled = false;
	}else{
		document.getElementById("btnconfirm").disabled = true;
	}
}

function simpandata(){
	var kodecabang = $("#cbocabang").val();
	var kodecstmr = $("#cbocustomer").val();
	var saldoawal = $("#txtsaldoawal").val();
	var maxpiutang = $("#txtmaxpiutang").val();

		$.ajax({
			type	: "POST", 
			url		: "pages/settingpenjualan/simpan.php",
			data	: "kodecabang="+kodecabang+
						"&kodecstmr="+kodecstmr+
						"&saldoawal="+saldoawal+
						"&maxpiutang="+maxpiutang,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				$("#cbocabang").select2('val','');
				$("#cbocustomer").select2('val','');
				$("#txtsaldoawal").val('');
				$("#txtmaxpiutang").val('');
				setbtnconfirm();
			}	
		});	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=setjual");
});

$('#btnconfirm').click(function(){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');	
});

$('#btnsave').click(function(){
	simpandata();		
});
