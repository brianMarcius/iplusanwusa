$(document).ready(function() {						   
	getdata();
});		

function getdata(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/hiscustomer/datacust.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblcust").val(data.namacust);
			$("#lblalamat").val(data.alamat);
			$("#lblkodecust").val(data.kodecust);
			tampilhistory();
		}
	});
}

function tampilhistory(){
	$.ajax({
		type	: "POST", 
		url		: "pages/hiscustomer/tampillisttransaksi.php",
		timeout	: 3000,
		success	: function(data){
			$("#tbltransaksi").html(data);
		}
	});
}

function goBack() {
    window.history.back();
}