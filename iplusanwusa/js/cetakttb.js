$(document).ready(function() {
	getdatapengambilan();	
});	

function getdatapengambilan(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/tandaterima/datapengambilan.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblno").text(data.nosrtjalan);
			$("#lbltgltrx").text(data.tglkirim);
			$("#lblstaffgdg").text(data.staffgudang);
			$("#lblpenerima").text(data.penerima);
			tampildata();
		}
	});
}

function tampildata(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/tandaterima/tampildata.php",
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function cetaksrtjalan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/tandaterima/cetakttb.php",
		success	: function(data){
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	//window.open("pages/tandaterima/cetakttb.php");
	window.open('?mod=cetttb','_blank');
}

function goBack() {
    window.history.back();
}