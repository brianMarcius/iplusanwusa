$(document).ready(function() {	
	isi_cbocabang();
	edit_data();
});						   

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/customer/tampilkan_cabang.php',
		success: function(response) {
			//alert(response);
			$('#cbocabang').html(response); 
		}
	});		
}

$(document).ready(function() {	
	edit_data();
});	

function simpandata(){
	var kode		= $("#txtkode").val();
	var kdcabang	= $("#cbocabang").val();
	var jns 		= '6';
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= $("#txtkota").val();
	var ktp 	 	= $("#txtktp").val();
	var telp 	 	= $("#txttelp").val();
	
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/customer/simpan.php",
			data	: "kdcabang="+kdcabang+	
					"&nama="+nama+
					"&jns="+jns+
					"&kodecstmr="+kode+
					"&kota="+kota+
					"&ktp="+ktp+
					"&alamat="+alamat+
					"&telp="+telp,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
			}	
		});
	}	
}
$('#btnconfirm').click(function(){
	
	$("#warningx").text('');
		
	var kode		= $("#txtkode").val();
	var kdcabang	= $("#cbocabang").val();
	var jns 		= $("#cbojns").val();
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= $("#txtkota").val();
	var ktp 	 	= $("#txtktp").val();
	var telp 	 	= $("#txttelp").val();
	var batalkan = false;
	
	if(kdcabang.length==0){
		$("#warningx").text('Cabang belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(jns.length==0){
		$("#warningx").text('Jenis Customer belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(nama.length==0){
		$("#warningx").text('Nama masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(alamat.length==0){
		$("#warningx").text('Alamat masih kosong!');
		var batalkan = true;
		return false;
	}
	if(kota.length==0){
		$("#warningx").text('Kota masih kosong!');
		var batalkan = true;
		return false;
	}
	if(telp.length==0){
		$("#warningx").text('Nomor Telepon masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	$("#txtkode").val('');
	$("#cbocabang").val('');
	$("#cbojns").val('');
	$("#txtnama").val('');
	$("#txtkota").val('');
	$("#txtktp").val('');
	$("#alamat").val('');
	$("#txttelp").val('');
});

/*
$('#txtnama').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#txtktp').focus();
	}
});
$('#txtktp').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#alamat').focus();
	}
});
$('#alamat').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#txttelp').focus();
	}
});
*/
function edit_data(){		
	$.ajax({
		type		: "POST",
		url			: "pages/customer/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#txtkode").val(data.kode_customer);
			$("#cbocabang").val(data.kodearea);
			$("#cbojns").val(data.jnscustomer);
			$("#txtnama").val(data.nama_customer);
			$("#txtkota").val(data.kota);
			$("#txtktp").val(data.ktp);
			$("#alamat").val(data.alamat);
			$("#txttelp").val(data.notelp);
		}
	});
}



