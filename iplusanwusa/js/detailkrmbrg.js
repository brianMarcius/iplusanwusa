$(document).ready(function() {
	gettrx();
	insert_temp();
});	
	
function gettrx() {
	$.ajax({
		type	: "GET", 
		url		: "pages/lappengiriman/gettrx.php",
		success	: function(data){
			$("#txtnotrx").val(data);
		}
	});
}

function insert_temp() {
	$.ajax({
		type	: "POST", 
		url		: "pages/lappengiriman/inserttemp.php",
		success	: function(data){
			tampil_listbarang();
		}
	});
}

function tampil_listbarang(){

	$.ajax({
		type	: "GET", 
		url		: "pages/lappengiriman/tampilbarang.php",
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

$('#btnok').click(function(){		
	window.location.assign('?mod=lapkirim');
});