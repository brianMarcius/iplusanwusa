$(document).ready(function() {	
	isi_cboarea();	
	tampil_list();
	isi_cbokaryawan();
});	

function isi_cboarea(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/pembayaranviamark/tampilkan_area.php',
		success: function(data) {			
			$('#cboarea').html(data);			
		}
	});		
}

function isi_cbojnscustomer(){
	$.ajax({
		type: 'POST', 
		url: 'pages/pembayaranviamark/tampilkan_jenis.php',
		success: function(data) {
			$('#cbojnscustomer').html(data); 			
		}
	});		
}

function isi_cbocustomer(){
	var jnscustomer = $("#cbojnscustomer").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/pembayaranviamark/tampilkan_customer.php',
		data: 'jnscustomer='+jnscustomer,
		success: function(data) {
			$('#cbocustomer').html(data); 
		}
	});		
}

function isi_cbokaryawan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/pembayaranviamark/tampilkan_karyawan.php',
		success: function(data) {
			$('#cbopenerima').html(data); 
		}
	});		
}

$('#cboarea').change(function(){
	isi_cbojnscustomer();
	tampil_list();
});	

$('#cbojnscustomer').change(function(){
	isi_cbocustomer();
});	

$('#cbocustomer').change(function(){
	tampil_list();
});

$(function() {
	$("#txttglbayar").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
});	

function tampil_list(){		
	var kodearea = $("#cboarea").val();
	var kodecustomer = $("#cbocustomer").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaranviamark/tampil_listplns.php",
		data	: "kodecustomer="+kodecustomer+
					"&kodearea="+kodearea,
		success	: function(data){
			$("#tblplns").html(data);			
			gettotpiut();
		}
	});
	
}

function tampil_banktujuan(){				
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaranviamark/tampilkan_bank.php",
		success	: function(data){
			$("#cbobank_tujuan").html(data);
		}
	});
}

$("#cbosistembyr").change(function() {
	var viabayar = $("#cbosistembyr").val();	
	
	if(viabayar!=2){
		$("#cbobank_tujuan").val('');
		$("#txtnorek_tujuan").val('');
//		$("#txtownrek").val('');
		tampil_banktujuan();
		$("#bank_add").hide();
		document.getElementById("cbobank_tujuan").disabled = true;
		document.getElementById("txtnorek_tujuan").disabled = true;
//		document.getElementById("txtownrek").disabled = true;
	}else{		
		document.getElementById("cbobank_tujuan").disabled = false;
		document.getElementById("txtnorek_tujuan").disabled = false;
//		document.getElementById("txtownrek").disabled = false;
		tampil_banktujuan();
		$("#bank_add").show();
	}			
});

$("#txtjmlbayar").keyup(function() {
	getsaldopiutang();
});

function datediff(tgl) {
	var dx = tgl.split("/");
	var d = dx[1]+"/"+dx[0]+"/"+dx[2];
	var now = new Date();
	var tglx = new Date(d);
	var a = now.getTime() - tglx.getTime();
	var diff = Math.floor(a/(1000*60*60*24));
	return diff;
}

$('#btnconfirm').click(function(){
	var tglbayar 	= $("#txttglbayar").val();
	var sistembyr	= $("#cbosistembyr").val();
	var banktujuan	= $("#cbobank_tujuan").val();
	var norektujuan = $("#txtnorek_tujuan").val();
//	var pemilikrek	= $("#txtownrek").val();
	var jmlpiutangx 	= $("#txtjmlpiutang").val();
	var jmlpiutang	= jmlpiutangx.substring(0,1);
	var jmlbayarx 	= $("#txtjmlbayar").val();
	var jmlbayar	= jmlbayarx.substring(0,2);
	var saldopiutangx= $("#txtsaldopiutang").val();
	var saldopiutang	= saldopiutangx.substring(0,2);
	var marketing	= $("#cbopenerima").val();
	var kodearea	= $("#cboarea").val();

	$("#lblwarning_save").text('');
	
	if(kodearea.length==0){
		$("#lblwarning_save").text('Nama Unit belum dipilih.');
		$("#cboarea").focus();
		return false;
	}
	
	if(jmlpiutang<=0){
		$("#lblwarning_save").text('Tidak ada nota yang dipilih.');
		$("#txtjmlbayar").focus();
		return false;
	}							
							
	if(tglbayar.length==0){
		$("#lblwarning_save").text('Tgl Bayar belum diisi.');
		$("#txttglbayar").focus();
		return false;
	}

	if(datediff(tglbayar)<0) {
		$("#lblwarning_save").text('Tgl Bayar melebihi tanggal sekarang.');
		$("#txttglbayar").focus();
		return false;
	}
	
	if(sistembyr.length==0){
		$("#lblwarning_save").text('Cara Pembayaran belum dipilih.');
		$("#cbosistembyr").focus();
		return false;
	}
	
	if(sistembyr==2){
		if(banktujuan.length==0){
			$("#lblwarning_save").text('Bank Tujuan belum dipilih.');
			$("#cbobank_tujuan").focus();
			return false;
		}
		if(norektujuan.length==0){
			$("#lblwarning_save").text('No Rekening belum diisi.');
			$("#txtnorek_tujuan").focus();
			return false;
		}
		// if(pemilikrek.length==0){
		// 	$("#lblwarning_save").text('Nama Pemilik Rekening masih kosong.');
		// 	$("#txtownrek").focus();
		// 	return false;
		// }
	}
	
	if(jmlbayar<=0){
		$("#lblwarning_save").text('Jml Bayar tidak valid.');
		$("#txtjmlbayar").focus();
		return false;
	}
	
	if(saldopiutang<0){
		$("#lblwarning_save").text('Jml bayar melebihi sisa piutang lalu.');
		$("#txtjmlbayar").focus();
		return false;
	}	
	
	if(marketing.length==0){
		$("#lblwarning_save").text('Nama Marketing belum dipilih.');
		$("#cbopenerima").focus();
		return false;
	}	

	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#overlayx").hide();
	$("#loading-imgx").hide();
	
});

function simpandata(){	
	
	var kodecustomer	= $("#cbocustomer").val();
	var tglbayar 	= $("#txttglbayar").val();
	var sistembyr	= $("#cbosistembyr").val();
	var banktujuan	= $("#cbobank_tujuan").val();
	var norektujuan = $("#txtnorek_tujuan").val();
//	var pemilikrek	= $("#txtownrek").val();
	var jmlbayar 	= $("#txtjmlbayar").val();
	var marketing	= $("#cbopenerima").val();
	var kodearea	= $("#cboarea").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaranviamark/simpan.php",
		data	: "kodecustomer="+kodecustomer+
					"&tglbayar="+tglbayar+
					"&sistembyr="+sistembyr+
					"&banktujuan="+banktujuan+
					"&norektujuan="+norektujuan+
					//"&pemilikrek="+pemilikrek+
					"&jmlbayar="+jmlbayar+
					"&marketing="+marketing+
					"&kodearea="+kodearea,
		timeout	: 3000,
		beforeSend	: function(){		
			$("#overlayx").show();
			$("#loading-imgx").show();			
		},
		success	: function(data){
			$("#lblwarning_save").text(data);
		}	
	});		
}		

$('#btnsave').click(function(){
	simpandata();	
	$("#confirm-modal").modal('hide');
	$("#txttglbayar").val('');
	$("#txtnorek_tujuan").val('');
//	$("#txtownrek").val('');
	$("#txtjmlbayar").val('');
	$("#cbopenerima").select2("val","");
	tampil_list();	
	getsaldopiutang();
	gettotpiut();
});							 

$('#btnclose').click(function(){
	window.history.back();
});	

function myfunction(obj){	
	var cek = document.getElementById(obj).checked;	
	if(cek==true){
		addpay(obj);
	}else{
		delpay(obj);
	}
}

function gettotpiut(){	
	var kodecustomer = $("#cbocustomer").val();
	var kodearea = $("#cboarea").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaranviamark/gettotpiut.php",
		data	: "kodecustomer="+kodecustomer+
					"&kodearea="+kodearea,
		dataType: "json",
		success	: function(data){
			$("#txtjmlpiutang").val(data.jmlpiutang);
			$("#txtsaldopiutang").val(data.jmlpiutang);
		}
	});
}

function addpay(kode){	
	var kodecustomer = $("#cbocustomer").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaranviamark/addpay.php",
		data	: "kode="+kode+
					"&kodecustomer="+kodecustomer,
		dataType: "json",
		success	: function(data){
			$("#txtjmlpiutang").val(data.jmlpiutang);
			getsaldopiutang();
		}
	});
}

function delpay(kode){	
	var kodecustomer = $("#cbocustomer").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaranviamark/delpay.php",
		data	: "kode="+kode+
					"&kodecustomer="+kodecustomer,
		dataType: "json",
		success	: function(data){
			$("#txtjmlpiutang").val(data.jmlpiutang);
			getsaldopiutang();
		}
	});
}

function getsaldopiutang(){
	var jmlpiutanglalu 	= $("#txtjmlpiutang").val();
	var jmlbayar  		= $("#txtjmlbayar").val();
	
	$.ajax({
		type		: "POST",
		url			: "pages/pembayaranviamark/getsaldopiutang.php",
		data		: "jmlpiutanglalu="+jmlpiutanglalu+
						"&jmlbayar="+jmlbayar,	
		dataType	: "json",
		success		: function(data){
			$("#txtsaldopiutang").val(data.jmlpiutang);
		}
	});
}

function regnewbank(){
	var bank	 	= $("#txtbank").val();	
	var batalkan 	= false;

	if(bank<=1){
		$('#warningreg').html('Nama bank harus di inputkan!!');
		$("#txtbank").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/pembayaranviamark/addbank.php",
			data	: "bank="+bank,
			timeout	: 3000,
			success	: function(data){
				$('#warningreg').show();
				$('#warningreg').html(data)
				tampil_banktujuan();
				$("#txtbank").val('');
				
			}	
		});
	}	
}


$('#bank_add').click(function(){		
	$("#regnewbank-modal").modal('toggle');
	$("#regnewbank-modal").modal('show');
	$('#warningreg').hide();
});

$('#btnadd').click(function(){
	regnewbank();
});