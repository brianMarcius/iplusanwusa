$(document).ready(function() {
	tabelsatuan();
	tampilsatuan();
	tampilkategori();
	edit_data();
});						   

function tampilkategori(){
	$.ajax({
		type	: "POST", 
		url		: "pages/barang/tampilkan_kategori.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbokategori").html(data);
		}
	});
}

function tampilsubkategori(kodesub){	
	var kodekategori = $("#cbokategori").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/barang/tampilkan_subkategori.php",
		data	: "kodekategori="+kodekategori+
				"&kodesub="+kodesub,
		timeout	: 3000,
		success	: function(data){
			$("#cbosubkategori").html(data);
		}
	});
}

function tampilsatuan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/barang/tampilkan_satuan.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbonamasatuan").html(data);
		}
	});
}

function tabelsatuan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/barang/tampilsatuan.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblsatuan").html(data);
		}
	});
}

function edit_data(){		
	$.ajax({
		type		: "POST",
		url			: "pages/barang/cari.php",
		dataType	: "json",
		success		: function(data){
			tabelsatuan();
			$("#txtnama").val(data.namabrg);
			$("#txtkode").val(data.kodebrg);
			$("#txtbarcode").val(data.barcode);
			var kategori = data.kodekategori;
			$("#cbokategori").val(kategori.trim());	
			var subkat = data.subkategori;
			tampilsubkategori(subkat.trim());			
		}
	});
}

function simpandata(){
	var nama 		= $("#txtnama").val();
	var kode 		= $("#txtkode").val();
	var barcode	 	= $("#txtbarcode").val();
	var kategori  	= $("#cbokategori").val();
	var subkategori = $("#cbosubkategori").val();
	var isi 	 	= $("#txtisi").val();
	var satuan 	 	= $("#cbonamasatuan").val();
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/barang/simpan.php",
			data	: "nama="+nama+
					"&kode="+kode+
					"&barcode="+barcode+
					"&kategori="+kategori+
					"&subkategori="+subkategori+
					"&isi="+isi+
					"&satuan="+satuan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				window.location.assign('?mod=prod');
				/*
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				*/
			}	
		});
	}	
}

$('#btnconfirm').click(function(){
	
	$("#warningx").text('');

	var nama 		= $("#txtnama").val();
	var kode 		= $("#txtkode").val();
	var barcode	 	= $("#txtbarcode").val();
	var kategori  	= $("#cbokategori").val();
	var isi 	 	= $("#txtisi").val();
	var satuan 	 	= $("#cbonamasatuan").val();
	var batalkan = false;
		
	if(nama.length==0){
		$("#warningx").text('Nama masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(kategori.length==0){
		$("#warningx").text('Kategori belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	//tabelsatuan();
});

$('#btnok').click(function(){
	location.reload();
});

$('#cbokategori').change(function(){
	tampilsubkategori('');
});

$('#btn_addsatuan').click(function(){
	simpansatuan();	
});

function simpansatuan(){
	var isi 		= $("#txtisi").val();
	var satuan 		= $("#cbonamasatuan").val();
	
	var batalkan = false;
	$("#warningx").text('');
	
	if(satuan.length==0){
		var batalkan = true;	
		$("#warningx").text('Silakan diisi satuannya!');
		return false;
	}
	
	if(isi<=0){
		var batalkan = true;	
		$("#warningx").text('Nilai isi tidak valid!');
		return false;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/barang/addsatuan.php",
			data	: "isi="+isi+
					"&satuan="+satuan,
			timeout	: 3000,
			success	: function(data){
				tabelsatuan();
				$("#txtisi").val('');
				$("#cbonamasatuan").select2('val','');
				$("#warningx").html(data);
			}	
		});
	}	
}

function delsatuan(ID){
	$("#warningx").text('');
	$.ajax({
		type	: "POST",
		url		: "pages/barang/delsatuan.php",
		data	: "kodesatuan="+ID,
		success	: function(data){
			tabelsatuan();
		}
	});
}

function regnewunit(){
	var kategori	= $("#txtkategori").val();	
	var batalkan 	= false;
		
	if(kategori.length==0){
		$('#warningreg').show();
		$('#warningreg').html('Kategori harus di inputkan!!');
		$("#txtkategori").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/barang/addkategori.php",
			data	: "kategori="+kategori,
			timeout	: 3000,
			success	: function(data){
				$('#warningreg').show();
				$('#warningreg').html(data);
				$("#txtkategori").val('');
				tampilkategori();
			}	
		});
	}	
}

$('#satuan_add').click(function(){
		$("#regnewunit-modal").modal('toggle');
		$("#regnewunit-modal").modal('show');
		$('#warningreg').hide();
});
$('#btnadd').click(function(){
		regnewunit();
});

$('#subkat_add').click(function(){
		var namakategori = $("#cbokategori option:selected").text();
		var sdhdipilih = $("#cbokategori").val();
		if(sdhdipilih.length>0){
			$("#regnewsubcat-modal").modal('toggle');
			$("#regnewsubcat-modal").modal('show');
			$("#txtkategori2").val(namakategori);
			$("#lblkodekat").text(sdhdipilih);
			$('#warningregsub').hide();
		}
});

$('#btnaddsub').click(function(){
	addnewsubkat();
});

function addnewsubkat(){
	var kodekategori = $("#lblkodekat").text();
	var namasubkat = $("#txtsubkategori").val();
	var batalkan 	= false;
		
	if(namasubkat.length==0){
		$('#warningregsub').show();
		$('#warningregsub').html('Nama Sub Kategori masih kosong.');
		$("#txtsubkategori").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/barang/addsubkategori.php",
			data	: "kodekategori="+kodekategori+
					"&namasubkat="+namasubkat,
			timeout	: 3000,
			success	: function(data){
				$('#warningregsub').show();
				$('#warningregsub').html(data);
				var datax=data.trim();
				if(datax.length==0){
					$("#txtkategori2").val('');
					$("#txtsubkategori").val('');
					$("#regnewsubcat-modal").modal('hide');
					tampilsubkategori('');
				}
			}	
		});
	}	
}