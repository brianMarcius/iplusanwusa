$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappengeluaranbrg/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);				
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lappengeluaranbrg/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx").val(data.tgltrx);				
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx= $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappengeluaranbrg/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cboperuntukan').change(function(){		
	var ket = $('#cboperuntukan option:selected').text();
	var ketb = ket.toUpperCase();	
	$("#lbljudul").text('LAPORAN '+ketb);
	var kdket = $('#cboperuntukan').val();
	if(kdket==3){
		$("#lbljudul").text('LAPORAN PENGELUARAN BARANG');
	}
	settglarea();
});

$('#cboarea').change(function(){	
	settglarea();
});

$('#tgltrx').change(function(){
	settglarea();
});

function tampildata(){	
	var area = $("#cboarea").val();
	var tgltrx= $("#tgltrx").val();	
	var kdutk = $('#cboperuntukan').val();
	$.ajax({
		type	: "GET",		
		url		: "pages/lappengeluaranbrg/tampildata.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx+
				"&kdutk="+kdutk,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	
