$(document).ready(function() {	
	tampilbahan();
});						   

$("#txtbahan").click(function(){
	var namabrg = $("#txtbahan").val();
	
	if(namabrg.length==0){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);
	}
});

$("#txtbahan").keyup(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/obatracikan/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/obatracikan/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

$("#txtcaribarang").keyup(function(){
	 tampil_masterbrg(1);
});

function getkodebrg(kodebrg,namabrg){
	$('#txtkodebrg').val(kodebrg);
	$('#txtbahan').val(namabrg);
	isi_cbosatuan();
	$("#barang-modal").modal('hide');
	$("#txtjml").val('1');
	$("#txtjml").focus();
}

function tampilbahan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/obatracikan/tampilbahan.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbahan").html(data);
		}
	});
}

function isi_cbosatuanracikan(){
	var kodebrg = $('#txtkodebrg_racikan').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/obatracikan/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuanracikan').html(response); 
		}
	});			
}

function isi_cbosatuan(){
	var kodebrg = $('#txtkodebrg').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/obatracikan/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function addbarang(){
	
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();

	$('#warningx').html('');
	$('#warning').html('');
	var batalkan = false;

	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#txtbahan").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/obatracikan/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat,
			dataType: "json",
			success	: function(data){
				if(data.sukses==1){				
					tampilbahan();
					clearaddbrg();
					setbtnconfirm();
				}else{
					$('#warning').html(data.pesan);
					return false;
				}
			}	
		});
	}
}

function clearaddbrg(){
	$("#txtkodebrg").val('');
	$("#txtbahan").val('');
	$("#txtjml").val('');
	$("#cbosatuan").select2("val","");
}

function simpandata(){
	var kodebrg		= $("#txtkodebrg_racikan").val();
	var dokter 		= '';
	var pasien		= '';
	var jmljadi		= $("#txtjmljadi").val();
	var satuanracikan = $("#cbosatuanracikan").val();
	var batalkan 	= false;
	
	$("#warningx").text('');
	
	if(kodebrg.length==0){
		$("#warningx").text('Nama Racikan belum diisi!');
		$("#txtnamabrg").focus();
		var batalkan = true;
	}
	
	if(jmljadi<1){
		$("#warningx").text('Jumlah jadi tidak boleh kosong atau minus!');
		$("#txtjmljadi").focus();
		var batalkan = true;
	}
	
	if(satuanracikan<1){
		$("#warningx").text('Satuan racikan belum dipilih!');
		$("#cbosatuanracikan").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/obatracikan/simpan.php",
			data	: "kodebrg="+kodebrg+
						"&dokter="+dokter+
						"&pasien="+pasien+
						"&jmljadi="+jmljadi+
						"&satuanracikan="+satuanracikan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampilbahan();
				$("#txtnamabrg").val('');
				setbtnconfirm();
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	 window.location.assign("?mod=racik");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/obatracikan/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampilbahan();
				setbtnconfirm();				
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var nama	= $("#txtnamabrg").val();
	var jmljadi	= $("#txtjmljadi").val();
	var satuanracikan = $("#cbosatuanracikan").val();

	$("#warningx").text('');
	var batalkan = false;
	
	if(nama.length==0){
		$("#warningx").text('Nama Racikan belum diisi!');
		var batalkan = true;
		return false;
	}
	
	if(jmljadi<1){
		$("#warningx").text('Jumlah jadi tidak boleh kosong atau minus!');
		$("#txtjmljadi").focus();
		var batalkan = true;
	}
	
	if(satuanracikan<1){
		$("#warningx").text('Satuan racikan belum dipilih!');
		$("#cbosatuanracikan").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');	
	}
	
});

$('#btn_addbahan').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$("#txtnamabrg").click(function(){
	var namabrg = $("#txtnamabrg").val();
	
	if(namabrg.length==0){
		$('#cbosatuanracikan').select2('val','');
		$("#racikan-modal").modal('toggle');
		$("#racikan-modal").modal('show');
		tampil_masterracikan(1);
	}
});

$("#txtnamabrg").keyup(function(){
	$('#cbosatuanracikan').select2('val','');
	$("#racikan-modal").modal('toggle');
	$("#racikan-modal").modal('show');
	tampil_masterracikan(1);		
});

function tampil_masterracikan(pageno){
	var namabrg = $('#txtcariracikan').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/obatracikan/tampil_masterbrgracikan.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterracikan').html(data); 		
			pagingracikan(pageno);
		}
	});		
}

function pagingracikan(pageno){
	var namabrg = $('#txtcariracikan').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/obatracikan/pagingmasterbrgracikan.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterracikan").html(data);
		}
	});
}

$("#txtcariracikan").keyup(function(){
	 tampil_masterracikan(1);
});

function getkodebrgracikan(kodebrg,namabrg){
	$('#txtkodebrg_racikan').val(kodebrg);
	$('#txtnamabrg').val(namabrg);
	isi_cbosatuanracikan();
	$("#racikan-modal").modal('hide');
}