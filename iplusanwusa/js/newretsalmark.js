$(document).ready(function() {	
	isian_default();
	isi_cbopenerima();
});		

function isian_default(){
	var nonota = $('#txtnonota').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/returjualmarketing/setdefault.php',
		data: "nonota="+nonota,
		dataType: "json",
		success	: function(data){			
			var edit=data.kodejual;
			if(edit.length>0){
				$('#txtkodejual').val(data.kodejual);
				$('#txtnonota').val(data.nonota);
				$('#txttgljual').val(data.tgljual);
				$('#txttglretur').val(data.tgljthtempo);
				$('#txtkodecustomer').val(data.kodecustomer);
				$('#txtnamacustomer').val(data.namacustomer);
				$('#txtalamat').val(data.alamat);
				$('#txtdiscpersen').val(data.disctot);
				$('#txtdisc').val(data.rpdisctot);
				$('#txtppnpersen').val(data.ppn);
				$('#txtppn').val(data.rpppn);
				$('#txtsaldopiutang').val(data.rpgrandtot);
				$('#txtrppenerimaan').val(data.rppenerimaan);
				tampillistbrg();
			}else{
				$('#txttgljual').val(data.tgljual);		
				tampillistbrg();
			}			
		}
	});		
}

function isi_cbopenerima(){
	$.ajax({
		type: 'POST', 
		url: 'pages/returjualmarketing/tampilkan_karyawan.php',
		success: function(response) {
			$('#cbopenerima').html(response); 
		}
	});		
}

$('#txtnonota').keypress(function(e) {
	var nonota = $('#txtnonota').val();								  
    if(e.which == 13 && nonota.length>0) {
        isian_default();
    }
});

$('#txtnamabrg').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	var nonota = $('#txtnonota').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/returjualmarketing/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&nonota="+nonota+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	var nonota = $('#txtnonota').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/returjualmarketing/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&nonota="+nonota+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

function getkodebrg(kodebrg,namabrg){
	$('#txtkodebrg').val(kodebrg);
	$('#txtnamabrg').val(namabrg);
	$("#barang-modal").modal('hide');
	edit(kodebrg);
}

function edit(ID){
	var kodejual = $('#txtkodejual').val();
	$.ajax({
		type	: "POST",
		url		: "pages/returjualmarketing/edit.php",
		data	: "kodebrg="+ID+
					"&kodejual="+kodejual,
		dataType: "json",
		success	: function(data){	
			$('#txtkodebrg').val(data.kodebrg);
			$('#txtnamabrg').val(data.namabrg);
			$('#txtharga').val(data.hargabrg);
			$('#txtjml').val(data.jmlbrg);
			isi_cbosatuan(data.kodesatuan);
			$('#txtdisc_peritem').val(data.disc);
			$('#txtdiscrp_peritem').val(data.rpdisc);
			$('#txtrpppn_peritem').val(data.rpppn);				
		}
	});
}

$("#txtcaribarang").keyup(function(){
	 tampil_masterbrg(1);
});

function isi_cbosatuan(kode){
	var kodebrg = $('#txtkodebrg').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/returjualmarketing/tampilkan_satuan.php',
		data	: "kodebrg="+kodebrg+
					"&kodesatuan="+kode,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/returjualmarketing/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
			getrppiutang();
		}
	});
}

function addbarang(){

	var kodejual = $("#txtkodejual").val();
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var harga	= $("#txtharga").val();
	var disc	= $("#txtdisc_peritem").val();
	var rpdisc	= $("#txtdiscrp_peritem").val();	

	$('#warningx').html('');
	var batalkan = false;	
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum di pilih');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(harga<=0){
		$('#warningx').html('Harga tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}

	if(disc<0){
		$('#warningx').html('Disc tidak boleh kosong, nol atau minus.');
		$("#txtharga").focus();
		var batalkan = true;
	}
	
	if(rpdisc<0){
		$('#warningx').html('Rp Disc tidak valid.');
		$("#txtharga").focus();
		var batalkan = true;
	}

	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}

	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returjualmarketing/addbarang.php",
			data	: "kodejual="+kodejual+	
					"&kodebrg="+kodebrg+
					"&jml="+jml+
					"&kodesatuan="+kodesat+	
					"&harga="+harga+
					"&disc="+disc+
					"&rpdisc="+rpdisc,
			dataType: "json",
			success	: function(data){	

				if(data.sukses==1){
					 tampillistbrg();
					 clearaddbrg();
				}else{
					$('#warningx').text(data.pesan);
				}
			}	
		});
	}
}

function getrppiutang(){
	var disc  = $("#txtdiscpersen").val();
	var ppn  = $("#txtppnpersen").val();
	//alert('PPN :'+ppn);
	$.ajax({
		type	: "POST",
		url		: "pages/returjualmarketing/getrppiutang.php",
		data	: "disc="+disc+
					"&ppn="+ppn,
		dataType: "json",
		success	: function(data){
			//alert('RP PPN :'+data.rpppn);
			$("#txtdisc").val(data.rpdisc);
			$("#txtppn").val(data.rpppn);
			$("#txtsaldopiutang").val(data.jmlpiutang);			
			$("#txtrppenerimaan").val(data.rppenerimaan);	
		}
	});
}

function clearaddbrg(){
	$("#txtnamabrg").val('');
	$("#txtkodebrg").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	isi_cbosatuan('');
	$("#txtharga").val('');
	$("#txtdisc_peritem").val('');
	$("#txtdiscrp_peritem").val('');
	$("#txtrpppn_peritem").val('');
}

function simpandata(){
	var kodecustomer = $("#txtkodecustomer").val();
	var tglretur	= $("#txttglretur").val();	
	var kodejual	 = $("#txtkodejual").val();
	var disc 		= $("#txtdiscpersen").val();
	var rpdisc 		= $("#txtdisc").val();
	var ppn 		= $("#txtppnpersen").val();	
	var rpppn 		= $("#txtppn").val();
	var grandtot	= $("#txtsaldopiutang").val();
	var pembayaran	= $("#txtrppenerimaan").val();
	var marketing	= $("#cbopenerima").val();
	var ket 		= $("#txtket").val();
	
	var batalkan = false;	
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/returjualmarketing/simpan.php",
			data	: "kodecustomer="+kodecustomer+
						"&tglretur="+tglretur+
						"&kodejual="+kodejual+
						"&disc="+disc+
						"&rpdisc="+rpdisc+
						"&ppn="+ppn+
						"&rpppn="+rpppn+
						"&grandtot="+grandtot+
						"&pembayaran="+pembayaran+
						"&marketing="+marketing+
						"&ket="+ket,
			dataType: "json",
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);
			}	
		});	
	}	
}		

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/returjualmarketing/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				getrppiutang();			
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var tglretur	= $("#txttglretur").val();	
	var ket 		= $("#txtket").val();	
	var marketing	= $("#cbopenerima").val();
	
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(tglretur.length==0){
		$("#lblwarning_save").text('Tgl Retur belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(marketing.length==0){
		$("#lblwarning_save").text('Nama Penerima belum dipilih.');
		var batalkan = true;
		return false;
	}
	
	if(ket.length==0){
		$("#lblwarning_save").text('Alasan Retur harus diisi.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/returjualmarketing/datecompare.php",
			data	: "tglretur="+tglretur,
			dataType: "json",
			success	: function(data){
				if(data.lanjut==0){
					$("#lblwarning_save").text(data.pesan);
					return false;
				}else{
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$("#txtdiscpersen").keyup(function(){
	getrppiutang();
});

$("#txtppnpersen").keyup(function(){
	 getrppiutang();
});

$('#btnok').click(function(){
	 window.location.assign("?mod=newretjualmark");
});

function goBack() {
    window.history.back();
}

$('#cbosatuan').change(function(){
	konversi();
});

function konversi(){	
	var kodebrg = $("#txtkodebrg").val();
	var kodesatuan = $("#cbosatuan").val();
	var kodejual = $("#txtkodejual").val();
	
	$.ajax({
		type	: "POST",
		url		: "pages/returjualmarketing/konversi.php",
		data	: "kodebrg="+kodebrg+
					"&kodesatuan="+kodesatuan+
					"&kodejual="+kodejual,
		dataType: "json",
		success	: function(data){				
			$('#txtharga').val(data.hargabrg);
			$('#txtdiscrp_peritem').val(data.rpdisc);
			$('#txtrpppn_peritem').val(data.rpppn);			
		}
	});
	
}

function cleartemp(){	
	$.ajax({
		type	: "POST",
		url		: "pages/returjualmarketing/deltemp.php",
		dataType: "json",
		success	: function(data){				
			tampillistbrg();
		}
	});
	
}

function clearall(){
	clearaddbrg();	
	$("#txtkodejual").val('');
	$("#txtnonota").val('');
	$("#txttgljual").val('');
	$("#txtkodecustomer").val('');
	$("#txtnamacustomer").val('');
	$("#txtalamat").val('');
	$("#txttglretur").val('');	
	$("#cbopenerima").select2("val","");
	$("#txtket").val('');
	cleartemp();
	$("#txtdiscpersen").val('');
	$("#txtdisc").val('');
	$("#txtppnpersen").val('');	
	$("#txtppn").val('');
	$("#txtsaldopiutang").val('');
	$("#txtrppenerimaan").val('');		
}