$(document).ready(function(){
	list_cbperusahaan();
	gettglskg();
	tampilabsen();
});

function list_cbperusahaan(){
	$.ajax({
		type: 'POST',
		url: 'pages/payroll/absen/list_perusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
		}
	});	
}

function gettglskg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/absen/gettglskg.php",
		dataType: "json",
		success	: function(data){
			$('#tglkerja').val(data.tglskg);
		}	
	});
}

function tampilabsen(){	
	var	tglkerja = $("#tglkerja").val();
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/absentelat/tampil_absen_telat.php',
		data: {
          	tanggal: tglkerja,
          	kodearea: kodearea
        },
		success: function(response) {
			$('#tblabsensi').html(response); 
		}
	});	
}

function ubahketlain(tanggal,idkaryawan,ketlain,valtelat){
	$("#ubahketlain-modal").modal('toggle');
	$("#ubahketlain-modal").modal('show');
	$("#warningketlain").hide();
	$('#txttanggal').val(tanggal);
	$('#txtidkaryawan').val(idkaryawan);
	$('#txtketlain').val(ketlain);
	$("#cbopottelat").val('');
	if(valtelat==0){
		$("#cbopottelat").hide();
	} else {
		$("#cbopottelat").show();
	}
	$('#txtketlain').focus();
}

$('#btnupdateketlain').click(function(){
	var tanggal = $("#txttanggal").val();
	var idkaryawan = $("#txtidkaryawan").val();
	var ketlain = $("#txtketlain").val();
	var pottelat = $("#cbopottelat").val();
	$("#warningketlain").hide();
	var batalkan = false;
	
	if(ketlain.length==0){
		$("#warningketlain").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/absen/ubahketlain.php",
			data	: "idkaryawan="+idkaryawan+
						"&ketlain="+ketlain+
						"&pottelat="+pottelat+
						"&tanggal="+tanggal,
			timeout	: 3000,
			success	: function(data){				
				$("#ubahketlain-modal").modal('hide');
				tampilabsen();
			}	
		});
	}					
});

function cetakabsentelat(){
	var tgl	= $("#tglkerja").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(tgl.length==0){
		alert("Tanggal belum di input");
		var batalkan = true;
		return false;
	}

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type: 'POST', 
			url: 'pages/payroll/absentelat/lintasrpt_absentelat.php',
			data: {
          		tanggal: tgl,
          		kodearea: area
        	}
		});

		window.open("pages/payroll/absentelat/cetakabsentelat.php");
	}
}
