$(document).ready(function(){
	list_bulan();
	list_tahun();
	list_perusahaan();
	tampil_data();
});

function list_bulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbbulan.php',
		success: function(response) {
			$('#cbbulan').html(response);
		}
	});	
}

function list_tahun(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbtahun.php',
		success: function(response) {
			$('#cbtahun').html(response);
		}
	});	
}

function list_perusahaan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/hutangnonbpd/list_perusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
		}
	});	
}

function tampil_data(){
	var bulan = $("#cbbulan").val();
	var tahun = $("#cbtahun").val();
	var unit = $("#cboarea").val();		
	
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/hutangnonbpd/tampildata.php',
		data: {
          	bulan: bulan,
          	tahun: tahun,
          	unit: unit
        },
		success: function(response) {
			$('#tblhutang').html(response); 
		}
	});	
}

function ubahdata(nama,idkaryawan,bapas,bkkdempet,bkkkasdemak,koperasi){
	$("#ubahdata-modal").modal('toggle');
	$("#ubahdata-modal").modal('show');
	$("#warningtext").text('');
	$('#nmkry').val(nama);
	$('#txtidkaryawan').val(idkaryawan);
	$('#bapas').val(bapas);
	$('#bkkdempet').val(bkkdempet);
	$('#bkkkasdemak').val(bkkkasdemak);
	$('#koperasi').val(koperasi);
	$('#bapas').focus();
}

$('#btnupdate').click(function(){
	var bulan = $("#cbbulan").val();
	var tahun = $("#cbtahun").val();
	var idkaryawan = $("#txtidkaryawan").val();
	var bapas = $("#bapas").val();
	var bkkdempet = $("#bkkdempet").val();
	var bkkkasdemak = $("#bkkkasdemak").val();
	var koperasi = $("#koperasi").val();

	$("#warningtext").text('');
					
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/hutangnonbpd/ubahdata.php",
		data	: "idkaryawan="+idkaryawan+
					"&bulan="+bulan+
					"&tahun="+tahun+
					"&bapas="+bapas+
					"&bkkdempet="+bkkdempet+
					"&bkkkasdemak="+bkkkasdemak+
					"&koperasi="+koperasi,
		success	: function(){				
			$("#ubahdata-modal").modal('hide');
			tampil_data();
		}	
	});					
});

function cetak_hutang(){
	var bulan = $("#cbbulan").val();
	var tahun = $("#cbtahun").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(bulan.length==0){
		alert("Bulan belum dipilih");
		var batalkan = true;
		return false;
	}

	if(tahun.length==0){
		alert("Tahun belum dipilih");
		var batalkan = true;
		return false;
	}

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type: "POST",		
			url: "pages/payroll/hutangnonbpd/setsess_cetak.php",
			data: {
				bulan: bulan,
				tahun: tahun,
				area: area
			}
		});

		window.open("pages/payroll/hutangnonbpd/cetakhutang.php");
	}
}