$(document).ready(function() {
	isi_cboarea();					
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapreturbeli/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);
		}
	});		
}


function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lapreturbeli/tgldefa.php",
		dataType: "json",		
		success	: function(data){				
			$("#tgltrx").val(data.tgltrx);
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapreturbeli/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}


$('#cboarea').change(function(){							  
	tampildata();
	settglarea();
});

$('#tgltrx').change(function(){
	tampildata();
	settglarea();
});

$('#cbotrx').change(function(){
	tampildata();
});


function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx = $("#tgltrx").val();
	var jnstrx = $("#cbotrx").val();				
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapreturbeli/tampildata.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx+
				"&jnstrx="+jnstrx,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail_bl(ID,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=bldetail');
			}
		}
	});
}

function cetak(ID,kdcust){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpembelian/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=printrj'); 
			}
		}	
	});	
}