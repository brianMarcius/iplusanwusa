$(document).ready(function() {	
	tampillistbrg();
});		

$('#kodekirim').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		get_datapengiriman();
	}
});

$("#txttglexp").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/cetaksuratjalan/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function setsatuan(kdsat){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/cetaksuratjalan/tampilkan_satuanx.php',
		data: "kodebrg="+kodebrg+
				"&kodesatuan="+kdsat,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function get_datapengiriman(){
	var kodekirim = $('#kodekirim').val();	
	$.ajax({
		type	: "POST",
		url		: "pages/cetaksuratjalan/get_datapengiriman.php",
		data	: "kodekirim="+kodekirim,
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.ada==1){
				$("#txttglkirim").val(data.tglkirim);
				$("#kodekirim1").val(data.kodekirim);
				$("#kodeminta").val(data.noref);
				$("#requnit").val(data.requnit);
				$("#txtasal").val(data.kodeasal);
				$("#txttujuan").val(data.kodetuju);
				$("#txtpengirim").val(data.pengirim);
				$("#txtket").val(data.ket);
			}else if(data.ada==0){
				$("#warningx").text('Kode Pengiriman Tidak Valid');
			}else if(data.ada==2){
				$("#warningx").text('Surat Jalan ini Sudah Pernah Dicetak Sebelumnya.');
			}
			tampillistbrg()
		}
	});
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetaksuratjalan/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);			
		}
	});
}

function addbarang(){
	var kodebrg 	= $("#cbobarang").val();
	var jml 		= $("#txtjml").val();
	var kodesat 	= $("#cbosatuan").val();
	var tujuan  	= $("#txttujuan").val();
	var kodeminta  	= $("#kodeminta").val();
	var requnit 	= $('#requnit').val();
	var tgled 		= $('#txttglexp').val();
	
	$("#lblwarning_save").text('');
	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
		return false;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
		return false;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
		return false;
	}
	
	if(tujuan.length==0){
		$('#warningx').html('Tujuan pengiriman tidak valid.');
		var batalkan = true;
		return false;
	}
	
	if(kodeminta.length<3 && requnit==1){
		$('#warningx').html('No Referensi tidak valid.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/cetaksuratjalan/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&tujuan="+tujuan+
					"&kodeminta="+kodeminta+
					"&requnit="+requnit+
					"&tgled="+tgled+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){
				$("#warningx").html(data);
				var pesansuksesx = $("#warningx").html();
				var pesansukses = pesansuksesx.trim();
				if(pesansukses.length==0){
					tampillistbrg();
					clearaddbrg();
					 //setbtnconfirm();
				}
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#txttglexp").val('');
}

function simpandata(){
	var kodekirim	= $("#kodekirim1").val();
	var kodeminta	= $("#kodeminta").val();
	var kodetujuan	= $("#txttujuan").val();
	var pengirim	= $("#txtpengirim").val();
	var tglkirim	= $("#txttglkirim").val();
	var perihal		= $("#cbountuk").val();
	var ket			= $("#txtket").val();
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		$("#cbotujuan").focus();
		var batalkan = true;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		$("#cbopengirim").focus();
		var batalkan = true;
	}
	
	if(kodeminta.length<3){
		$('#lblwarning_save').text('No Referensi tidak valid.');
		$("#kodeminta").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/cetaksuratjalan/simpan.php",
			data	: "kodetujuan="+kodetujuan+
						"&pengirim="+pengirim+
						"&tglkirim="+tglkirim+
						"&perihal="+perihal+
						"&ket="+ket+
						"&kodekirim="+kodekirim+
						"&kodeminta="+kodeminta,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){		
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);	
				$("#lblsukses").text(data.sukses);
			}	
		});	
	}	
}		

$('#info-ok').click(function(){
	var sukses = $("#lblsukses").text();
	if(sukses==1){
		tampillistbrg();
		$("#txtpengirim").val('');
		$("#txttujuan").val('');
		$("#cbountuk").val('');
		$("#cbountuk").select2("val","0");
		$("#txttglkirim").val('');
		$("#kodeminta").val('');
		window.location.assign("?mod=srtjln");
	}
});

function del(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/cetaksuratjalan/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){			
			tampillistbrg();							
		}
	});
}

function edit(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/cetaksuratjalan/edit.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			$("#cbobarang").val(data.kodebrg);
			$("#barang").val(data.namabrg);
			$("#txtjml").val(data.jmlbrg);
			setsatuan(data.kodesatuan);
			$('#txttglexp').val(data.tglexpired);
		}
	});
}

$('#btnconfirm').click(function(){
	var kodeminta	= $("#kodeminta").val();							
	var perihal		= $("#cbountuk").val();
	var kodetujuan	= $("#txttujuan").val();
	var pengirim	= $("#txtpengirim").val();		
	var tglkirim	= $("#txttglkirim").val();
	
	$('#warningx').html('');
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(tglkirim.length==0){
		$("#lblwarning_save").text('Tanggal belum diisi!');
		var batalkan = true;
		return false;
	}
	
	if(kodeminta.length<3){
		$('#lblwarning_save').text('No Referensi tidak valid.');
		$("#kodeminta").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/cetaksuratjalan/datecompare.php",
			data	: "tglkirim="+tglkirim,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Kirim melebihi tanggal sekarang.');
					return false;
				}else {
					if(data.adarec==0){
						$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
						return false;
					}else {
						if(data.stok<0){
							$("#lblwarning_save").text('Jumlah pengiriman melebih stok yang ada.');
							tampillistbrg();
							return false;
						}else {	
							
							if(data.jmlkirim==0){
								$("#lblwarning_save").text('Jumlah pengiriman masih ada yang nol.');
								tampillistbrg();
								return false;
							}else{							
								if(data.sat==0){
									$("#lblwarning_save").text('Satuan barang ada yg tidak valid.');
									tampillistbrg();
									return false;
								}else {	
									$("#confirm-modal").modal('toggle');
									$("#confirm-modal").modal('show');	
								}	
							}
						}
					}					
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/cetaksuratjalan/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/cetaksuratjalan/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}
function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

function clearall(){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetaksuratjalan/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();				
			$("#kodekirim").val('');		
			$("#barang").val('');		
			$("#txtjml").val('');		
			$("#cbosatuan").val('');		
			$("#txttglexp").val('');
			$('#warningx').html('');
			$("#lblwarning_save").text('');
		}
	});
}

function goBack() {
    window.history.back();
}