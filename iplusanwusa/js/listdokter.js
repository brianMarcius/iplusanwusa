$(document).ready(function() {
	tampildata('1');
});	

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

$('#cari').keyup(function(){
	tampildata('1');
});

function tampildata(pageno){
	setCookie("nopageempl", pageno);
	if(getCookie("pageempl").length>0) {
		var pageno = getCookie("pageempl");
	}
	else {
		var pageno = pageno;
	}
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}
	
	var searchby = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/dokter/tampildata.php",
		data	: "kode="+kode+
					"&page="+pageno+
					"&searchby="+searchby,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}
	$.ajax({
		type	: "GET",		
		url		: "pages/dokter/paging.php",
		data	: "kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			delCookie("cariempl");
			delCookie("pageempl");
		}
	});
}

function edit(kode){
	setCookie("pageempl", getCookie("nopageempl"));
	setCookie("cariempl", $("#cari").val());
	
	$.ajax({
		type	: "POST", 
		url		: "pages/dokter/lintasform.php",
		data	: "kode="+kode,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newdokter');
			}
		}	
	});	
}

function hapus(kode){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/dokter/hapus.php",
		data	: "kode="+kode,
		success	: function(data){
			$("#overlayx").show();
			$("#loading-imgx").show();			
			$("#info-modal").modal('show');
			$("#infone").html(data);
			tampildata(1);
		}	
	});	
}