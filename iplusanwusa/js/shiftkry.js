$(document).ready(function() {
	isi_cboarea();
	tampil_data();
});

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cboarea(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbperusahaan.php',
		success: function(response) {			
			$('#cboarea').html(response);
			if(getCookie("areatmp").length>0) {
				$('#cboarea').val(getCookie("areatmp"));
				delCookie("areatmp");
			}
		}
	});		
}

function tampil_data(){
	if(getCookie("areatmp").length>0) {
		var kodearea = getCookie("areatmp");
	}
	else {
		var kodearea = $("#cboarea").val();		
	}
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shiftkry/tampildata.php',
		data: {
          	kodearea: kodearea
        },
		success: function(response) {
			$('#tampildata').html(response); 
		}
	});	
}

$('#cboarea').change(function(){
	tampil_data();
});

function tampil_karyawan(){
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shiftkry/listkaryawan.php',
		data: {
        	kodearea: kodearea
        },
		success: function(response) {
			$('#cbokaryawan').html(response);
		}
	});	
}

function ubah(shift,kdhari){
	$("#ubahkry-modal").modal('toggle');
	$("#ubahkry-modal").modal('show');
	tampil_karyawan();
	$('#txtshift').val(shift);
	$('#txtkdhari').val(kdhari);
	$('#cbokaryawan').focus();
}

$('#btnupdatekry').click(function(){
	var kodearea = $("#cboarea").val();
	var newidkaryawan = $("#cbokaryawan").val();
	var shift = $("#txtshift").val();
	var kdhari = $("#txtkdhari").val();
	$("#warningkry").hide();
	var batalkan = false;
	
	if(newidkaryawan.length==0){
		$("#warningkry").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/shiftkry/ubahkaryawan.php",
			data	: "newidkaryawan="+newidkaryawan+
						"&kodearea="+kodearea+
						"&shift="+shift+
						"&kdhari="+kdhari,
			timeout	: 3000,
			success	: function(data){				
				$("#ubahkry-modal").modal('hide');
				tampil_data();
			}	
		});
	}					
});