$(document).ready(function() {
	tampildata('1');
});	

$(function() {		   
	
	$("[data-mask]").inputmask();	
	
});

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function tampildata(){
	
	$.ajax({
		type	: "GET",		
		url		: "pages/jamkerja/tampildata.php",
		success	: function(data){
			$("#tampildata").html(data);
		}
	});
}	

function edit(kodearea,nojamkerja){
	$.ajax({
		type	: "POST", 
		url		: "pages/jamkerja/cari.php",
		data	: "kodearea="+kodearea+
					"&nojamkerja="+nojamkerja,
		dataType: "json",
		success	: function(data){
			$("#jamkerja-modal").modal('toggle');
			$("#jamkerja-modal").modal('show');	
			$("#txtjnsjamkerja").val(data.jamkerja);
			$("#txtjammasuk").val(data.jammasuk);
			$("#txtjampulang").val(data.jampulang);
			$("#txtkodearea").val(data.kodearea);
			$("#txtnojamkerja").val(data.nojamkerja);				
		}	
	});	
}

$('#btnupdate').click(function(){
	update();
});

function update(){
	var kodearea	= $("#txtkodearea").val();
	var nojamkerja	= $("#txtnojamkerja").val();
	var jammasuk	= $("#txtjammasuk").val();
	var jampulang	= $("#txtjampulang").val();
	var batalkan 	= false;

	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/jamkerja/simpan.php",
			data	: "kodearea="+kodearea+
						"&nojamkerja="+nojamkerja+
						"&jammasuk="+jammasuk+
						"&jampulang="+jampulang,
			timeout	: 30000,
			success	: function(data){
				$("#jamkerja-modal").modal('hide');
				tampildata();
			}	
		});
	}	
}