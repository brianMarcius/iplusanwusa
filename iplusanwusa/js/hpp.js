$(document).ready(function() {
	tampildata('1');	
});	

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}


function tampildata(pageno){
	var kode = $("#cari").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/hpp/tampildata.php",
		data	: "page="+pageno+
				"&kode="+kode,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
			setCookie("nopagex",pageno);
		}
	});
}	

function paging(pageno){
	var kode = $("#cari").val();		

	$.ajax({
		type	: "GET",		
		url		: "pages/hpp/paging.php",
		data	: "page="+pageno+
				"&kode="+kode,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampildata(pageno);						 
});

$('#cari').keyup(function(){
	tampildata('1');
});

function edithpp(kode){
	var cari = $("#cari").val();	
	setCookie("carix",cari); 	
	
	$("#labelkode_updhpp").html(kode);
	$("#txthpp").val('');
	$("#inputwarning_updhpp").hide();
	$("#updhpp-modal").modal('toggle');
	$("#updhpp-modal").modal('show');
}

$('#btnupdate_updhpp').click(function(){
	execedithpp();
});

function execedithpp(){
	var kode = $("#labelkode_updhpp").html();
	var hpp = $("#txthpp").val();
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/hpp/edithpp.php',
			data: "kode="+kode+
					"&hpp="+hpp,
			success: function(response) {
				$("#inputwarning_updhpp").hide();				
				$("#updhpp-modal").modal('hide');
				var pageno = getCookie("nopagex");
				tampildata(pageno);				
			}
		});	
	}
}

//----

function updatehpp(kode){
	var cari = $("#cari").val();	
	setCookie("carix",cari); 
	
	$("#lblkodebrg_upd").html(kode);
	$("#confirmupd-modal").modal('toggle');
	$("#confirmupd-modal").modal('show');
}

$('#btnsaveupd').click(function(){
	execupdatehpp();
});

function execupdatehpp(){
	var kodebrg = $("#lblkodebrg_upd").text();
	$.ajax({
		type: 'POST', 
		url: 'pages/hpp/updatehpp.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {			
			$("#confirmupd-modal").modal('hide');
			var pageno = getCookie("nopagex");
			tampildata(pageno);
		}
	});	
}

//---

function declinehpp(kode){
	var cari = $("#cari").val();	
	setCookie("carix",cari); 
	
	$("#lblkodebrg_dec").html(kode);
	$("#confirmdec-modal").modal('toggle');
	$("#confirmdec-modal").modal('show');
}

$('#btnsavedec').click(function(){
	execdeclinehpp();
});

function execdeclinehpp(){
	var kodebrg = $("#lblkodebrg_dec").text();
	$.ajax({
		type: 'POST', 
		url: 'pages/hpp/dechpp.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$("#confirmdec-modal").modal('hide');
			var pageno = getCookie("nopagex");
			tampildata(pageno);
		}
	});	
}


