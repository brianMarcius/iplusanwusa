$(document).ready(function() {	
	isi_cbojns();
	isi_cbopekerjaan();
	edit_data();
	isi_cbounit();
});						   


$(function() {

	"use strictz";
	
	$("#txttglmou").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		
});

function isi_cbojns(){
	$.ajax({
		type: 'POST', 
		url: 'pages/customer/tampilkan_jenis.php',
		success: function(response) {
			//alert(response);
			$('#cbojns').html(response); 
		}
	});
}

function isi_cbopekerjaan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/customer/tampilkan_pekerjaan.php',
		success: function(response) {
			//alert(response);
			$('#cbopekerjaan').html(response); 
		}
	});
}
function isi_cbounit(){
	$.ajax({
		type: 'POST', 
		url: 'pages/customer/tampilkan_cabang.php',
		success: function(data) {			
			$('#cbounit').html(data);
		}
	});		
}


function simpandata(){
	var kode		= $("#txtkode").val();
	var jns 		= $("#cbojns").val();
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= '';
	var ktp 	 	= $("#txtktp").val();
	var telp 	 	= $("#txttelp").val();
	var kodekerja	= $("#cbopekerjaan").val();
	var nomember	= $("#txtnomember").val();
	var tglmou		= $("#txttglmou").val();
	var unit		= $("#cbounit").val();
	
	var batalkan = false;
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/customer/simpan.php",
			data	: "nama="+nama+
					"&jns="+jns+
					"&kodecstmr="+kode+
					"&kota="+kota+
					"&ktp="+ktp+
					"&alamat="+alamat+
					"&telp="+telp+
					"&kodekerja="+kodekerja+
					"&tglmou="+tglmou+
					"&unit="+unit+
					"&nomember="+nomember,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				window.location.assign('?mod=custm');
				/*
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				*/
			}	
		});
	}	
}

$('#btnconfirm').click(function(){	
	$("#warningx").text('');
		
	var kode		= $("#txtkode").val();
	var jns 		= $("#cbojns").val();
	var nama 		= $("#txtnama").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= '';
	var ktp 	 	= $("#txtktp").val();
	var telp 	 	= $("#txttelp").val();
	var kodekerja	= $("#cbopekerjaan").val();
	var nomember	= $("#txtnomember").val();
	var tglmou		= $("#txttglmou").val();
	var unit		= $("#cbounit").val();
	var batalkan 	= false;
		
	if(jns.length==0){
		$("#warningx").text('Jenis Customer belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(nama.length==0){
		$("#warningx").text('Nama masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(alamat.length==0){
		$("#warningx").text('Alamat masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(telp.length==0){
		$("#warningx").text('Nomor Telepon masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(kodekerja.length==0){
		$("#warningx").text('Nama Pekerjaan masih kosong!');
		var batalkan = true;
		return false;
	}
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
	/*
	$("#txtkode").val('');
	$("#cbocabang").val('');
	$("#cbojns").val('');
	$("#txtnama").val('');
	$("#txtktp").val('');
	$("#alamat").val('');
	$("#txttelp").val('');
	$("#cbopekerjaan").val('');
	$("#txtnomember").val('');
	*/
});

$('#txtnama').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#txtktp').focus();
	}
});
$('#txtktp').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#alamat').focus();
	}
});
$('#alamat').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		$('#txttelp').focus();
	}
});

function edit_data(){
	$.ajax({
		type		: "POST",
		url			: "pages/customer/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#txtkode").val(data.kode_customer);
			$("#cbocabang").val(data.kodearea);
			$("#cbojns").val(data.jnscustomer);
			$("#txtnama").val(data.nama_customer);			
			$("#txtktp").val(data.ktp);
			$("#alamat").val(data.alamat);
			$("#txttelp").val(data.notelp);
			$("#cbopekerjaan").val(data.kodekerja);
			$("#txtnomember").val(data.nomember);
			$("#txttglmou").val(data.tglmou);
			$("#cbounit").val(data.unit);
		}
	});
}