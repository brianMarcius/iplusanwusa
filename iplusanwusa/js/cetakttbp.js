$(document).ready(function() {
	getdatapengambilan();	
});	

function getdatapengambilan(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/tandaterimaprol/datapengambilan.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblno").text(data.nosrtjalan);
			$("#lbltgltrx").text(data.tglkirim);
			$("#lblstaffgdg").text(data.staffgudang);
			$("#lblpenerima").text(data.penerima);
			tampildata();
		}
	});
}

function tampildata(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/tandaterimaprol/tampildata.php",
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function cetaksrtjalan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/tandaterimaprol/cetakttbp.php",
		success	: function(data){
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	// window.open("pages/tandaterimaprol/cetakttbp.php");
	window.open('?mod=cetttbp','_blank');
}

function goBack() {
    window.history.back();
}