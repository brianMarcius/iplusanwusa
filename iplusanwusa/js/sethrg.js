$(document).ready(function() {	
	isi_cbocabang();
	tampildata('1');	
});						   

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingharga/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbocabang").change(function() {
	tampildata('1');
})

$('#btnview').click(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	setCookie("nopagex", pageno);
	var cabang = $("#cbocabang").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/settingharga/tampildata.php",
		data	: "cabang="+cabang+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var cabang = $("#cbocabang").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/settingharga/paging.php",
		data	: "cabang="+cabang+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function del(kode){
	$("#kodehapus").val(kode);
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
}

$('#btndel').click(function(){
	hapus();
});

function hapus(){
	var kode = $("#kodehapus").val();
	var kodearea = $("#cbocabang").val();

	$.ajax({
		type: 'POST', 
		url: 'pages/settingharga/hapus.php',
		data: "kode="+kode+
				"&kodearea="+kodearea,
		success: function(response) {
			tampildata('1');
			$("#confirm-modal").modal('hide');
		}
	});		
}

function editharga(kode){
	$("#labelkode_updharga").html(kode);
	$("#txtharga").val('');
	$("#inputwarning_updharga").hide();
	$("#updharga-modal").modal('toggle');
	$("#updharga-modal").modal('show');
}

$('#btnupdate_updharga').click(function(){
	updateharga();
});

function updateharga(){
	var kode = $("#labelkode_updharga").html();
	var kodearea = $("#cbocabang").val();
	var harga = $("#txtharga").val();
	var batalkan = false;

	if(harga <= 0) {
		$("#inputwarning_updharga").show();
		$("#inputwarning_updharga").text("Nilai yang dimasukkan tidak valid");
		batalkan = true;
		return false;
	}

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settingharga/updateharga.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&harga="+harga,
			success: function(response) {
				tampildata('1');
				$("#inputwarning_updharga").hide();
				$("#updharga-modal").modal('hide');
			}
		});	
	}
}
