function simpandata(){
	var kode  		= $("#txtnopengajuan").val();
	var flampiran   = $("#flampiran").val();
	
	var batalkan = false;					
	
	if(kode.length==0){
		$('#lblwarning_save').html('No Pengajuan Masih Kosong.');
		$("#nopengajuan").focus();
		var batalkan = true;
	}
	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/upload/simpan.php",
			data	: "flampiran="+flampiran+
					"&kode="+kode,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
			}	
		});
	}	
}

$('#btnsave').click(function(){		
	simpandata();
	$("#txtnopengajuan").val('');
	dropboxrefresh();
});

function dropboxrefresh(){
	var message 	= $('.message');
	$("#dropbox").html(message.show());
}


