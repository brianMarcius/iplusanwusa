$(document).ready(function() {
	isi_cboarea();						   	
	isi_cbothn();	
	tampildata();
});	

function settgl(){
	var area = $("#cboarea").val();
	var thn = $("#cbothn").val();
	var bln = $("#cbobln").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbeliunit/settgl.php",
		data	: "thn="+thn+
				"&bln="+bln+
				"&area="+area,
		dataType: "json",		
		success	: function(data){		
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);			
		}
	});
}

function isi_cboarea(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapbeliunit/getarea.php',
		success: function(data) {			
			$('#cboarea').html(data);			
			isi_cbobln();
		}
	});		
}

function isi_cbothn(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapbeliunit/gettahun.php',
		success: function(data) {			
			$('#cbothn').html(data);			
			isi_cbobln();
		}
	});		
}

function isi_cbobln(){
	$.ajax({
		type	: "POST",		
		url		: "pages/lapbeliunit/getbulan.php",
		success	: function(data){	
			$("#cbobln").html(data);	
		}
	});
}

$('#cboarea').change(function(){	
	settgl();
	tampildata();
});

$('#cbothn').change(function(){	
	settgl();
	tampildata();
});

$('#cbobln').change(function(){
	settgl();
	tampildata();
});

function tampildata(){
	var area = $("#cboarea").val();
	var thn = $("#cbothn").val();
	var bln = $("#cbobln").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbeliunit/tampildata.php",
		data	: "thn="+thn+
				"&bln="+bln+
				"&area="+area,
		success	: function(data){					
			$("#tampildata").html(data);
		}
	});
}	