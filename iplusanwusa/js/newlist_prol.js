$(document).ready(function() {	
	getdatapengiriman();
});		

function getdatapengiriman(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/resepprolanis/dataprolanis.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblnotelp").text(data.notelp);
			$("#lblno").text(data.noresep);
			$("#lblnobpjs").text(data.nobpjs);
			$("#lbltgltrx").text(data.tglresep);
			$("#lblpasien").text(data.namapasien);
			$("#lbldokter").text(data.namadokter);
			tampillistbrg();
		}
	});
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/resepprolanis/tampillistbrg_prol.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function goBack() {
    window.history.back();
}