$(document).ready(function() {	
	getdata();
});	

function getdata(){	
	$.ajax({
		type	: "POST", 
		url		: "pages/detailpiutang/getdata.php",
		dataType: "json",
		success	: function(data){
			$("#txtkodecustomer").val(data.kode_customer);
			$("#txtnamacustomer").val(data.nama_customer);
			$("#txtalamat").val(data.alamat);
			tampil_list();
		}
	});
}

function tampil_list(){			
	$.ajax({
		type	: "POST", 
		url		: "pages/detailpiutang/tampil_listpiutang.php",
		success	: function(data){
			$("#tblpiutang").html(data);	
		}
	});	
}

function goBack() {
    window.history.back();
}