$(document).ready(function() {	
	document.getElementById("kodeminta").disabled = false;		
	isi_cbotujuan();
	get_permintaan();
});		

$(function() {

	"use strict";
	
	$("#chkrequnit").on('ifUnchecked', function(event) {
		document.getElementById("kodeminta").disabled = false;			
	});
	
	$("#chkrequnit").on('ifChecked', function(event) {
		document.getElementById("kodeminta").disabled = true;	
	});
		
});

function isi_cbotujuan(){	
	$.ajax({
		type: "POST", 
		url: "pages/pengirimanbrg/tampilkan_tujuan.php",
		success: function(response) {			
			$("#cbotujuan").html(response); 
			isi_cboarea();	
		}
	});		
}

function isi_cboarea(){
	$.ajax({
		type: "POST", 
		url: "pages/pengirimanbrg/tampilkan_areapengirim.php",
		success: function(response) {
			$("#cboarea").html(response); 
			tampillistbrg();
		}
	});		
}

function isi_cbopengirim(){
	var area = $('#cboarea').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/pengirimanbrg/tampilkan_pengirim.php',
		data: 'area='+area,
		success: function(response) {
			$('#cbopengirim').html(response); 
		}
	});		
}

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/pengirimanbrg/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

$('#cboarea').change(function(){
	isi_cbopengirim();
});

function setsatuan(kdsat){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/pengirimanbrg/tampilkan_satuanx.php',
		data: "kodebrg="+kodebrg+
				"&kodesatuan="+kdsat,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function tampillistbrg(){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pengirimanbrg/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);			
		}
	});
}

function get_permintaan(){
	$.ajax({
		type	: "POST",
		url		: "pages/pengirimanbrg/get_permintaan.php",
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			
			if(data.ada == 1) {
				$("#kodeminta").val(data.kodeminta);
				document.getElementById("kodeminta").disabled = true;
				$("#chkrequnit").iCheck("check");
				//$("#lblwarning_save").text('Info : Sumber data dari Permintaan '+data.kodeminta);
				$("#cbotujuan").select2("val",data.tujuan);
				document.getElementById("cbotujuan").disabled = true;
			}else{
				$("#chkrequnit").iCheck("uncheck");
			}
		}
	});
}

function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var tujuan  = $("#cbotujuan").val();
	var kodeminta  = $("#kodeminta").val();
	var tgled  	= $("#txted").val();
	var requnitx = $('#chkrequnit').is(':checked');
	
	if(requnitx==true){
		var requnit=1;
	}else{
		var requnit=0;
	}
	
	$("#lblwarning_save").text('');
	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
		return false;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
		return false;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
		return false;
	}
	
	if(tujuan.length==0){
		$('#warningx').html('Tujuan pengiriman tidak valid.');
		var batalkan = true;
		return false;
	}
	
	if(tgled.length==0){
		$('#warningx').html('Tgl Kadaluarsa masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(kodeminta.length<3 && requnit==1){
		$('#warningx').html('No Referensi tidak valid.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pengirimanbrg/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&tujuan="+tujuan+
					"&kodeminta="+kodeminta+
					"&requnit="+requnit+
					"&tgled="+tgled+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){
				$("#warningx").html(data);
				var pesansuksesx = $("#warningx").html();
				var pesansukses = pesansuksesx.trim();
				if(pesansukses.length==0){
					tampillistbrg();
					 clearaddbrg();
					 //setbtnconfirm();
				}
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#txted").val('');
}

function simpandata(){
	var kodeminta	= $("#kodeminta").val();
	var kodetujuan	= $("#cbotujuan").val();
	var pengirim	= $("#cbopengirim").val();
	var tglkirim	= $("#txttglkirim").val();
	var perihal		= $("#cbountuk").val();
	var ket			= $("#txtket").val();
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		$("#cbotujuan").focus();
		var batalkan = true;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		$("#cbopengirim").focus();
		var batalkan = true;
	}
	
	if(kodeminta.length<3){
		$('#lblwarning_save').text('No Referensi tidak valid.');
		$("#kodeminta").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/pengirimanbrg/simpan.php",
			data	: "kodetujuan="+kodetujuan+
						"&pengirim="+pengirim+
						"&tglkirim="+tglkirim+
						"&perihal="+perihal+
						"&ket="+ket+
						"&kodeminta="+kodeminta,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){		
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.pesan);	
				$("#lblsukses").text(data.sukses);
			}	
		});	
	}	
}		

$('#info-ok').click(function(){
	var sukses = $("#lblsukses").text();
	if(sukses==1){
		tampillistbrg();
		$("#cbopengirim").val('');
		$("#cbopengirim").select2("val","");
		$("#cbotujuan").val('');
		$("#cbotujuan").select2("val","");
		$("#cbountuk").val('');
		$("#cbountuk").select2("val","0");
		$("#txttglkirim").val('');
		$("#kodeminta").val('');
		window.location.assign("?mod=presrtjln");
	}
});

function del(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/pengirimanbrg/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){			
			tampillistbrg();							
		}
	});
}

function edit(ID){
	
	$.ajax({
		type	: "POST",
		url		: "pages/pengirimanbrg/edit.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			$("#cbobarang").val(data.kodebrg);
			$("#barang").val(data.namabrg);
			$("#txtjml").val(data.jmlbrg);
			setsatuan(data.kodesatuan);			
		}
	});
}

$('#btnconfirm').click(function(){
	var kodeminta	= $("#kodeminta").val();							
	var perihal		= $("#cbountuk").val();
	var kodetujuan	= $("#cbotujuan").val();
	var pengirim	= $("#cbopengirim").val();		
	var tglkirim	= $("#txttglkirim").val();
	
	$('#warningx').html('');
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(tglkirim.length==0){
		$("#lblwarning_save").text('Tanggal belum diisi!');
		var batalkan = true;
		return false;
	}
	
	if(kodeminta.length<3){
		$('#lblwarning_save').text('No Referensi tidak valid.');
		$("#kodeminta").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/pengirimanbrg/datecompare.php",
			data	: "tglkirim="+tglkirim,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Kirim melebihi tanggal sekarang.');
					return false;
				}else {
					if(data.adarec==0){
						$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
						return false;
					}else {
						if(data.stok<0){
							$("#lblwarning_save").text('Jumlah pengiriman melebih stok yang ada.');
							tampillistbrg();
							return false;
						}else {	
							
							if(data.jmlkirim==0){
								$("#lblwarning_save").text('Jumlah pengiriman masih ada yang nol.');
								tampillistbrg();
								return false;
							}else{							
								if(data.sat==0){
									$("#lblwarning_save").text('Satuan barang ada yg tidak valid.');
									tampillistbrg();
									return false;
								}else {	
									$("#confirm-modal").modal('toggle');
									$("#confirm-modal").modal('show');	
								}	
							}
						}
					}					
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/pengirimanbrg/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/pengirimanbrg/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}
function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

function clearall(){
	$.ajax({
		type	: "POST", 
		url		: "pages/pengirimanbrg/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();		
			$("#chkrequnit").iCheck("uncheck");
			document.getElementById("kodeminta").disabled = false;			
			$("#kodeminta").val('');
			document.getElementById("cbotujuan").disabled = false;
			isi_cbotujuan();
			$('#warningx').html('');
			$("#lblwarning_save").text('');
		}
	});
}

function goBack() {
    window.history.back();
}