$(document).ready(function() {
	isi_cbothn();	
	tampildata();
});	

function settgl(){
	var thn = $("#cbothn").val();
	var bln = $("#cbobln").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbayarviamark/settgl.php",
		data	: "thn="+thn+
				"&bln="+bln,
		dataType: "json",		
		success	: function(data){		
			$("#lbltgltrx").text(data.tgltrx);
		}
	});
}

function isi_cbothn(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapbayarviamark/gettahun.php',
		success: function(data) {			
			$('#cbothn').html(data);			
			isi_cbobln();
		}
	});		
}

function isi_cbobln(){
	$.ajax({
		type	: "POST",		
		url		: "pages/lapbayarviamark/getbulan.php",
		success	: function(data){	
			$("#cbobln").html(data);	
		}
	});
}

$('#cbothn').change(function(){	
	settgl();
	tampildata();
});

$('#cbobln').change(function(){
	settgl();
	tampildata();
});

function tampildata(){
	
	var thn = $("#cbothn").val();
	var bln = $("#cbobln").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbayarviamark/tampildata.php",
		data	: "thn="+thn+
				"&bln="+bln,
		success	: function(data){					
			$("#tampildata").html(data);
		}
	});
}	