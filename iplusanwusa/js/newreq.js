$(document).ready(function() {							 
	isi_cbotujuan();	
});		

function isi_cbotujuan(){
	$.ajax({
		type: "POST", 
		url: "pages/permintaan/tampilkan_tujuan.php",
		success: function(response) {
			$("#cbotujuan").html(response); 
			tampillistbrg();	
		}
	});		
}

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/permintaan/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function addbarang(){
	var tujuan = $("#cbotujuan").val();
	var tglsp  	= $("#txttglpermintaan").val();
	var kodebrg = $("#cbobarang").val();	
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var ket		= $("#txtket").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(tglsp.length==0){
		$('#warningx').html('Tgl Permintaan masih kosong.');
		$("#txttglpermintaan").focus();
		var batalkan = true;
	}
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}

	if(tujuan.length==0){
		$('#warningx').html('Tujuan permintaan belum dipilih.');
		$("#cbotujuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/permintaan/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+					
					"&kodesatuan="+kodesat+
					"&tujuan="+tujuan+
					"&tglsp="+tglsp+
					"&ket="+ket,
			timeout	: 3000,
			success	: function(data){	
				$('#warningx').html(data);
				var pesansuksesx = $("#warningx").html();
				var pesansukses = pesansuksesx.trim();
				if(pesansukses.length==0){
					tampillistbrg();
					 clearaddbrg();
				}
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#txtket").val('');
}

function simpandata(){
	var kodetujuan	= $("#cbotujuan").val();	
	var tglsp  	= $("#txttglpermintaan").val();
	var batalkan = false;
	
	if(kodetujuan.length==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		$("#cbotujuan").focus();
		var batalkan = true;
	}
	
	if(tglsp.length==0){
		$('#lblwarning_save').text('Tgl Permintaan masih kosong.');
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/permintaan/simpan.php",
			data	: "kodetujuan="+kodetujuan+
					"&tglsp="+tglsp,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);				
				$("#cbotujuan").val('');
				clearaddbrg();
				tampillistbrg();
			}	
		});	
	}	
}		
/*
$("#info-close").click(function() {
	window.location.assign("?mod=newminta");
});
*/
function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/permintaan/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();			
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var kodetujuan	= $("#cbotujuan").val();
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(kodetujuan.length==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/permintaan/datecompare.php",
			dataType: "json",
			success	: function(data){
				if(data.adarec==0){
					$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
					return false;
				}else {
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}									
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$('#txttglpermintaan').change(function(){
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			tampillistbrg();
		}
	});
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	var tujuan = $('#cbotujuan').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/permintaan/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&tujuan="+tujuan+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}
$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/permintaan/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

function clearall(){
	$.ajax({
		type	: "POST", 
		url		: "pages/permintaan/bersihkan.php",
		timeout	: 3000,
		success	: function(data){
			isi_cbotujuan();
		}
	});
}