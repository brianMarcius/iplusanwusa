$(document).ready(function() {
	getdatapengembalian();	
});	

function getdatapengembalian(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/buktipengembalian/datapengembalian.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblno").text(data.nosrtjalan);
			$("#lbltgltrx").text(data.tglkirim);
			$("#lblmarketing").text(data.marketing);
			$("#lblpenerima").text(data.penerima);
			tampildata();
		}
	});
}

function tampildata(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/buktipengembalian/tampildata.php",
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function cetaksrtjalan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/buktipengembalian/cetakbpb.php",
		success	: function(data){
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	window.open('?mod=cetbpb','_blank');
	//window.open("pages/buktipengembalian/cetakbpb.php");
}

function goBack() {
    window.history.back();
}