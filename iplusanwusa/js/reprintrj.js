$(document).ready(function() {	
	isi_cboarea();
});						   

$(function() {
	$('#tgltrx').daterangepicker({format: 'DD/MM/YYYY'});	
});		

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/cetakrj/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);
			tampildata('1');
		}
	});		
}	

$('#tgltrx').change(function(){
	tampildata('1');						 
});

$('#cari').click(function(){
	tampildata('1');						 
});	

$('#cboarea').click(function(){
	tampildata('1');						 
});


function tampildata(pageno){
	var tgl = $("#tgltrx").val();
	var area = $("#cboarea").val();
	var cari = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakrj/tampildata.php",
		data	: "tgl="+tgl+	
					"&area="+area+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);			
		}
	});
}	

function paging(pageno){
	var tgl = $("#tgltrx").val();
	var area = $("#cboarea").val();
	var cari = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakrj/paging.php",
		data	: "tgl="+tgl+	
					"&area="+area+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function cetak(ID,kdcust){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=printrj'); 
			}
		}	
	});	
}
