$(document).ready(function() {		
	tampillistbrg();		
});		

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/barangedso/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/barangedso/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var tgled	= $("#txttgled").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(tgled.length==0){
		$('#warningx').html('Tanggal Kadaluarsa barang belum dipilih.');
		var batalkan = true;
	}

	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/barangedso/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat+
					"&tgled="+tgled,
			timeout	: 3000,
			success	: function(data){	
				$('#warningx').html(data);
				tampillistbrg();
				 clearaddbrg();
				 //setbtnconfirm();
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	//$("#txttgled").val('');
}

function del(ID,tgled){
	$.ajax({
		type	: "POST",
		url		: "pages/barangedso/hapus.php",
		data	: "kodebrg="+ID+
				"&tgled="+tgled,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				//setbtnconfirm();				
			}				
		}
	});
}


$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

$('#btnconfirm').click(function(){		

	$("#lblwarning_save").text('');
	var batalkan = false;
		
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/barangedso/datecompare.php",
			dataType: "json",
			success	: function(data){
				if(data.adarec==0){
					$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
					return false;
				}else {
					$("#confirm-modal").modal('toggle');
					$("#confirm-modal").modal('show');	
				}		
			}
		});
	}
	
});

function simpandata(){
	//var tgl 		= $("#txttgl").val();
	var ket			= $("#txtket").val();
	var batalkan = false;
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/barangedso/simpan.php",
			data	: "ket="+ket,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampillistbrg();
				//$("#txttgl").val('');
			}	
		});	
	}	
}	

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/barangedso/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}
$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/barangedso/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}
function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	$("#barang-modal").modal('hide');
	isi_cbosatuan();
}

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newbrgedso");
});