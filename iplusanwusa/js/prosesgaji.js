$(document).ready(function(){
	$(".progress").css('display', 'none');
	isi_cboarea();
	list_cbbulan();
	list_cbtahun();
});

$(function() {
	$('#tglproses').daterangepicker({format: 'DD/MM/YYYY'});	
});

function isi_cboarea(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbperusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
		}
	});	
}

function list_cbbulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbbulan.php',
		success: function(response) {
			$('#cbobulan').html(response);
		}
	});	
}

function list_cbtahun(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/cbtahun.php',
		success: function(response) {
			$('#cbotahun').html(response);
		}
	});	
}

$('#cboarea').change(function(){
	$('#cbodivisi').val('');
	isi_cbodivisi();
	isi_cbokaryawan();
});

$('#cbodivisi').change(function(){
	isi_cbokaryawan();
});

function isi_cbodivisi(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/prosesgaji/listdivisi.php',
		success: function(response) {
			$('#cbodivisi').html(response);
		}
	});	
}

function isi_cbokaryawan(){
	var kodearea 	= $('#cboarea').val();
	var kodedivisi	= $("#cbodivisi").val();
	$.ajax({
		type 	: 'POST',
		url		: "pages/payroll/prosesgaji/getdatakaryawan.php",
		data	: "kodearea="+kodearea+
					"&kodedivisi="+kodedivisi,
		success : function(response) {
			$('#cbokaryawan').html(response);
		}
	});		
}

$('#btnproses').click(function(){
	prosesgaji();		
});

function prosesgaji(){
	var kodearea	= $("#cboarea").val();
	var kodedivisi	= $("#cbodivisi").val();
	var idkaryawan	= $("#cbokaryawan").val();
	var bulan 		= $("#cbobulan").val();
	var tahun 		= $("#cbotahun").val();
	var tanggal		= $("#tglproses").val();
	var btnproses 	= $("#btnproses").text();
	$("#labelsukses").text('');

	if(btnproses=='Proses'){
		var batalkan = false;

		if(kodearea.length==0){
			$("#labeleror").text('Area Kerja belum di pilih.');
			var batalkan = true;
			return false;
		}

		if(bulan.length==0){
			$("#labeleror").text('Bulan Penggajian belum di pilih.');
			var batalkan = true;
			return false;
		}

		if(tahun.length==0){
			$("#labeleror").text('Tahun Penggajian belum di pilih.');
			var batalkan = true;
			return false;
		}

		if(tanggal.length==0){
			$("#labeleror").text('Tanggal belum di pilih.');
			var batalkan = true;
			return false;
		}
	
		if(batalkan==false){
			$("#labeleror").text('');
            $("#labelsukses").text('');
			$("#cboarea").prop('disabled', true);
			$("#cbodivisi").prop('disabled', true);
			$("#cbokaryawan").prop('disabled', true);
			$("#cbobulan").prop('disabled', true);
			$("#cbotahun").prop('disabled', true);
			$("#tglproses").prop('disabled', true).css("background", "");
			$("#btnproses").text('Stop');
			$('#btnproses').removeClass('btn-success');
			$('#btnproses').addClass('btn-danger');
			$(".progress").css('display', 'none');
			mulaiproses();
		}
	}
	else{
		$("#batal-modal").modal('toggle');
		$("#batal-modal").modal('show');
	}	
}

function mulaiproses(){
	var kodearea	= $("#cboarea").val();
	var kodedivisi	= $("#cbodivisi").val();
	var idkaryawan	= $("#cbokaryawan").val();
	var bulan 		= $("#cbobulan").val();
	var tahun 		= $("#cbotahun").val();
	var tanggal		= $("#tglproses").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/prosesgaji/simpanproses.php",
		data: {
          	kodearea: kodearea,
          	kodedivisi: kodedivisi,
          	idkaryawan: idkaryawan,
          	bulan: bulan,
          	tahun: tahun,
          	tanggal: tanggal
        },
        beforeSend: function (){
            $("#imgload").html("<img src='img/loading.gif' height='30'>");
            $("#labelinfo").text("Menyiapkan data");
            $("#labeleror").text("");
            $("#labelinfo2").text("");
            $("#btnproses").text("Tunggu");
            $("#btnproses").prop('disabled', true);
        },
		success	: function(){
			$("#btnproses").text("Stop");
			$("#btnproses").prop('disabled', false);
			$("#imgload").html("");
            $("#labelinfo").text("");
			$(".progress").css('display', '');
            $(".progress").addClass('active');
			$(".progress-bar").removeClass('progress-bar-danger');
			$(".progress-bar").removeClass('progress-bar-success');
			cekproses();
		}	
	});
}

function cekproses(){
	timerHandle = setTimeout(cekproses,100);
	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/prosesgaji/cekproses.php",
		success	: function(data){
			if(data>=100){
				$("#cboarea").prop('disabled', false);
				$("#cbodivisi").prop('disabled', false);
				$("#cbokaryawan").prop('disabled', false);
				$("#cbobulan").prop('disabled', false);
				$("#cbotahun").prop('disabled', false);
				$("#tglproses").prop('disabled', false).css("background", "#FFFFFF");
				$("#btnproses").text('Proses');
				$('#btnproses').removeClass('btn-danger');
				$('#btnproses').addClass('btn-success');
				$("#imgload").html("");
				$("#labelinfo").text("");
				$("#labelinfo2").text("");
				$(".progress").removeClass('active');
				$(".progress-bar").addClass('progress-bar-success');
				$(".progress-bar").css('width', data+'%').attr('aria-valuenow', data).html(data+'%');
				$("#labelsukses").text("Data selesai diproses");
				clearTimeout(timerHandle);
			}
			else{
				$(".progress-bar").css('width', data+'%').attr('aria-valuenow', data).html(data+'%');
				//$("#labelinfo").text(data+"% selesai");
			}
		}	
	});
}

$('#btnyesstop').click(function(){
	$("#batal-modal").modal('hide');
	$("#cboarea").prop('disabled', false);
	$("#cbodivisi").prop('disabled', false);
	$("#cbokaryawan").prop('disabled', false);
	$("#cbobulan").prop('disabled', false);
	$("#cbotahun").prop('disabled', false);
	$("#tglproses").prop('disabled', false).css("background", "#FFFFFF");
	$("#btnproses").text('Proses');
	$('#btnproses').removeClass('btn-danger');
	$('#btnproses').addClass('btn-success');
	$(".progress").removeClass('active');
	$(".progress-bar").addClass('progress-bar-danger');
	$("#imgload").html("");
	$("#labelsukses").text("");

	clearTimeout(timerHandle);

    $.ajax({
		type	: "POST", 
		url		: "pages/payroll/prosesgaji/stopproses.php",
		success	: function(data){
			$("#imgload").html("");
			$(".progress-bar").css('width', data+'%').attr('aria-valuenow', data).html(data+'%');
			$("#labelinfo").text("Proses data dihentikan");
			//$("#labelinfo2").text(data+"% data berhasil diproses");
		}	
	});
});
