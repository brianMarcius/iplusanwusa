$(document).ready(function() {
	isi_cboarea();
});

function isi_cboarea(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shiftkry/listperusahaan.php',
		success: function(response) {			
			$('#cboarea').html(response);
		}
	});		
}

function tampil_karyawan(){
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shiftkry/listkaryawan.php',
		data: {
        	kodearea: kodearea
        },
		success: function(response) {
			$('#cbokaryawan').html(response);
		}
	});	
}

$('#cboarea').change(function(){
	var	kodearea = $("#cboarea").val();
	tampil_karyawan();
	$("#tglmulai").val('');
	$("#tglselesai").val('');
	$('#warningx').html('');
	viewshiftkry();
});

$('#tglmulai').change(function(){
	$('#warningx').html('');
	gettglselesai();
	viewshiftkry();
});

function gettglselesai(){
	var	tglmulai = $("#tglmulai").val();

	$.ajax({
		type	: "POST", 
		url		: "pages/payroll/shiftkry/gettglselesai.php",
		data	: "tglmulai="+tglmulai,
		dataType: "json",
		success	: function(data){
			$('#tglselesai').val(data.tglselesai);
		}	
	});
}

function viewshiftkry(){	
	var	kodearea = $("#cboarea").val();
	var	tglmulai = $("#tglmulai").val();

	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/shiftkry/lihatshiftkry.php',
		data: {
       		kodearea: kodearea,
       		tglmulai: tglmulai
       	},
		success: function(response) {
			$('#tblshiftkry').html(response); 
		}
	});
}

function ubah(tanggal,shift,idkaryawan){
	$("#ubahkry-modal").modal('toggle');
	$("#ubahkry-modal").modal('show');
	tampil_karyawan();
	$("#warningkry").hide();
	$('#txtshift').val(shift);
	$('#txttanggal').val(tanggal);
	$('#oldidkaryawan').val(idkaryawan);
	$('#cbokaryawan').select2('val',idkaryawan);
	$('#cbokaryawan').focus();
}

$('#cbokaryawan').change(function(){
	$("#warningkry").hide();
});

$('#btnupdatekry').click(function(){
	var kodearea = $("#cboarea").val();
	var shift = $("#txtshift").val();
	var tanggal = $("#txttanggal").val();
	var oldidkaryawan = $("#oldidkaryawan").val();
	var newidkaryawan = $("#cbokaryawan").val();
	$("#warningkry").hide();
	var batalkan = false;
	
	if(newidkaryawan.length==0){
		$("#warningkry").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/shiftkry/ubahkaryawan.php",
			data	: "newidkaryawan="+newidkaryawan+
						"&oldidkaryawan="+oldidkaryawan+
						"&kodearea="+kodearea+
						"&shift="+shift+
						"&tanggal="+tanggal,
			timeout	: 3000,
			success	: function(data){				
				$("#ubahkry-modal").modal('hide');
				viewshiftkry();
			}	
		});
	}					
});

function hapus(tanggal,shift,idkaryawan){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');
	$('#txtshift').val(shift);
	$('#txttanggal').val(tanggal);
	$('#oldidkaryawan').val(idkaryawan);
}

$('#btnhapuskry').click(function(){
	var kodearea = $("#cboarea").val();
	var shift = $("#txtshift").val();
	var tanggal = $("#txttanggal").val();
	var idkaryawan = $("#oldidkaryawan").val();
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/shiftkry/delete.php",
			data	: "idkaryawan="+idkaryawan+
						"&kodearea="+kodearea+
						"&shift="+shift+
						"&tanggal="+tanggal,
			timeout	: 3000,
			success	: function(data){				
				$("#confirmhapus-modal").modal('hide');
				viewshiftkry();
			}	
		});
	}					
});

function cetakshift(){
	var tglmulai = $("#tglmulai").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(tglmulai.length==0){
		alert("Tanggal mulai berlaku belum di input");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type: "POST",		
			url: "pages/payroll/shiftkry/setsess_cetak.php",
			data: {
				tglmulai: tglmulai,
				area: area
			}
		});

		window.open("pages/payroll/shiftkry/cetakshift.php");
	}
}

/*
function clearaddkry(){
	$("#cbokaryawan").select2('val','');
}

function addkry(){
	var	kodearea = $("#cboarea").val();
	var shift 	= $("#cboshift").val();
	var	tgl = $("#tanggal").val();
	var idkaryawan = $("#cbokaryawan").val();

	$('#warningx').html('');
	var batalkan = false;	

	if(kodearea.length==0){
		$('#warningx').html('Area Kerja belum dipilih.');
		var batalkan = true;
	}

	if(shift.length==0){
		$('#warningx').html('Shift belum dipilih.');
		var batalkan = true;
	}

	if(tgl.length==0){
		$('#warningx').html('Tanggal berlaku belum di input.');
		var batalkan = true;
	}

	if(idkaryawan.length==0){
		$('#warningx').html('Karyawan belum di input.');
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/shiftkry/simpanshiftkry.php",
			data	: "tgl="+tgl+	
					"&kodearea="+kodearea+
					"&shift="+shift+	
					"&idkaryawan="+idkaryawan,
			dataType: "json",
			success	: function(data){
				if(data.sukses==1){
					viewshiftkry();
					clearaddkry();
				}else{
					$('#warningx').html(data.pesan);
				}
			}	
		});
	}
}

$('#btn_addkry').click(function(){	
	addkry();						 
});

$('#btnclearall').click(function(){	
	clearallkry();						 
});

function clearallkry(){
	var	shift = $("#cboshift").val();
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type	: "POST",
		url		: "pages/payroll/shiftkry/deleteall.php",
		data	: "shift="+shift+
					"&kodearea="+kodearea,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				viewshiftkry();
				clearaddkry();			
			}				
		}
	});
}
*/