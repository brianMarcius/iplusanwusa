$(document).ready(function() {	
						 
	//document.getElementById("btnconfirm").disabled = true;	
	
	isi_cbotujuan();
	//get_permintaan();
	//isi_cbobarang();		
	tampillistbrg();	
	
});		

function isi_cbotujuan(){
	$.ajax({
		type: "POST", 
		url: "pages/transferout/tampilkan_tujuan.php",
		success: function(response) {
			$("#cbotujuan").html(response); 
			isi_cboarea();	
		}
	});		
}
/*
$('#cbotujuan').change(function(){
	setbtnconfirm();
});
*/

$('#cbountuk').change(function(){
	var peruntukan = $('#cbountuk').val(); 		
	if(peruntukan==0){
		document.getElementById("cbotujuan").disabled = false;
		document.getElementById("cboarea").disabled = false;
		document.getElementById("cbopengirim").disabled = false;
	}else{
		document.getElementById("cbotujuan").disabled = true;
		document.getElementById("cboarea").disabled = true;
		document.getElementById("cbopengirim").disabled = true;
	}
});

function isi_cboarea(){
	$.ajax({
		type: "POST", 
		url: "pages/transferout/tampilkan_areapengirim.php",
		success: function(response) {
			$("#cboarea").html(response); 			
		}
	});		
}

$('#cboarea').change(function(){
	isi_cbopengirim();
});

function isi_cbopengirim(){
	var area = $('#cboarea').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/transferout/tampilkan_pengirim.php',
		data: 'area='+area,
		success: function(response) {
			$('#cbopengirim').html(response); 
		}
	});		
}
/*
$('#cbopengirim').change(function(){
	setbtnconfirm();
});	
*/
/*
function isi_cbobarang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/transferout/tampilkan_barang.php',
		success: function(response) {
			$('#cbobarang').html(response); 
		}
	});		
}
*/

function isi_cbosatuan(){
	var kodebrg = $('#cbobarang').val();	
	$.ajax({
		type: 'POST', 
		url: 'pages/transferout/tampilkan_satuan.php',
		data: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
			$("#barang-modal").modal('hide');
			$("#txtjml").val('1');
			$("#txtjml").focus();
		}
	});		
}
/*
$('#cbobarang').change(function(){
	isi_cbosatuan();
});	
*/
function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/transferout/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function get_permintaan(){
	$.ajax({
		type	: "POST",
		url		: "pages/transferout/get_permintaan.php",
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.ada == 1) {
				$("#kodeminta").val(data.kodeminta);
				$("#lblwarning_save").text('Info : Sumber data dari Permintaan '+data.kodeminta);
				$("#cbotujuan").select2("val",data.tujuan);
				document.getElementById("cbotujuan").disabled = true;
			}
		}
	});
}
/*
function setbtnconfirm(){
	$.ajax({
		type	: "POST",
		url		: "pages/transferout/getdatacount.php",
		dataType: "json",
		success	: function(data){	
			var tujuan = $('#cbotujuan').val();	
			var pengirim = $('#cbopengirim').val();	
			
			if(tujuan.length>0 && pengirim.length>0 && data.valid>0){
				document.getElementById("btnconfirm").disabled = false;
			}else{
				document.getElementById("btnconfirm").disabled = true;
			}
		}
	});
}
*/
function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/transferout/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){	
				$('#warningx').html(data);
				tampillistbrg();
				 clearaddbrg();
				 //setbtnconfirm();
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#barang").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
}

function simpandata(){
	var kodeminta	= $("#kodeminta").val();
	var kodetujuan	= $("#cbotujuan").val();
	var pengirim	= $("#cbopengirim").val();
	var tglkirim	= $("#txttglkirim").val();
	var perihal		= $("#cbountuk").val();
	var ket			= $("#txtket").val();
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		$("#cbotujuan").focus();
		var batalkan = true;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		$("#cbopengirim").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/transferout/simpan.php",
			data	: "kodetujuan="+kodetujuan+
						"&pengirim="+pengirim+
						"&tglkirim="+tglkirim+
						"&perihal="+perihal+
						"&ket="+ket+
						"&kodeminta="+kodeminta,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampillistbrg();
				$("#cbopengirim").val('');
				$("#cbopengirim").select2("val","");
				$("#cbotujuan").val('');
				$("#cbotujuan").select2("val","");
				$("#cbountuk").val('');
				$("#cbountuk").select2("val","0");
				$("#txttglkirim").val('');
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newtransout");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/transferout/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampillistbrg();
				//setbtnconfirm();				
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var perihal		= $("#cbountuk").val();
	var kodetujuan	= $("#cbotujuan").val();
	var pengirim	= $("#cbopengirim").val();		
	var tglkirim	= $("#txttglkirim").val();

	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(kodetujuan.length==0 && perihal==0){
		$("#lblwarning_save").text('Tujuan belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(pengirim.length==0 && perihal==0){
		$("#lblwarning_save").text('Pengirim belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(tglkirim.length==0){
		$("#lblwarning_save").text('Tanggal belum diisi!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST",
			url		: "pages/transferout/datecompare.php",
			data	: "tglkirim="+tglkirim,
			dataType: "json",
			success	: function(data){
				if(data.melebihi<0){
					$("#lblwarning_save").text('Tgl Kirim melebihi tanggal sekarang.');
					return false;
				}else {
					if(data.adarec==0){
						$("#lblwarning_save").text('Tidak ada barang yang dipilih.');						
						return false;
					}else {
						$("#confirm-modal").modal('toggle');
						$("#confirm-modal").modal('show');	
					}					
				}
			}
		});
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});

function myFunction(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#barang-modal").modal('toggle');
		$("#barang-modal").modal('show');
		tampil_masterbrg(1);		
	}
}

$('#barang').click(function(){
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/transferout/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
			
		}
	});		
}
$("#txtcaribarang").keyup(function(){
	$("#lblwarning_save").text('');
	 tampil_masterbrg(1);
});

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/pembelian/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}
function getkodebrg(kodebrg,namabrg){
	$('#cbobarang').val(kodebrg);
	$('#barang').val(namabrg);
	isi_cbosatuan();
}