$(document).ready(function() {
	settglarea();
	tampildata();
});


function settglarea(){
	
	var area = $("#cboarea").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakharga/tglarea.php",
		data	: "area="+area,
		dataType: "json",		
		success	: function(data){			
			$("#lbltgltrx").text(data.tgltrx);
		}
	});
}

$('#searchkandungan').keyup(function(){
	tampildata();
});
$('#searchbarang').keyup(function(){
	tampildata();
});

function tampildata(){	

	var subkat 	= $("#searchkandungan").val();
	var namabrg = $("#searchbarang").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakharga/tampildata.php",
		data	: "subkat="+subkat+
					"&namabrg="+namabrg,
		success	: function(data){				
			$("#tampildata").html(data);
			
		}
	});
}	

function cetakpricelist(){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetakharga/cetakpricelist.php",
		success	: function(data){
			if(data.result==1){				
				//window.open('media.php'+data.hlink,'_blank');
			}
		}	
	});
	window.open("pages/cetakharga/cetakpricelist.php");
}
