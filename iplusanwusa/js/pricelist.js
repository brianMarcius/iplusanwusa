
$(document).ready(function() {
	isi_cbokategori();		
});	

function isi_cbokategori(){
	$.ajax({
		type: 'POST', 
		url: 'pages/pricelist/tampilkan_kategori.php',
		success: function(data) {			
			$('#cbokategori').html(data);
			tampildata('1');
		}
	});		
}

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

$('#cbokategori').change(function(){
	tampildata('1');
});

$('#cari').keyup(function(){
	tampildata('1');
});

$('#cbojnssatuan').change(function(){
	var pageno = getCookie("nopagex");							   
	tampildata(pageno);
});

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampildata(pageno);						 
});

function tampildata(pageno){
	setCookie("nopagex", pageno);
	var kode = $("#cari").val();	
	var kategori = $("#cbokategori").val();
	var jnssat = $("#cbojnssatuan").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/pricelist/tampildata.php",
		data	: "page="+pageno+
				"&kode="+kode+
				"&jnssat="+jnssat+
				"&kategori="+kategori,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var kode = $("#cari").val();		
	var kategori = $("#cbokategori").val();
	var jnssat = $("#cbojnssatuan").val();

	$.ajax({
		type	: "GET",		
		url		: "pages/pricelist/paging.php",
		data	: "page="+pageno+
				"&kode="+kode+
				"&jnssat="+jnssat+
				"&kategori="+kategori,
		success	: function(data){
			$("#paging").html(data);
			delCookie("carix");
		}
	});
}

function editmarginpersen(kode){
	var cari = $("#cari").val();	
	setCookie("carix",cari); 
	
	$("#labelkode_updmarginpersen").html(kode);
	$("#txtmarginpersen").val('');
	// $("#lbljnscustomerpersen").html(jns);
	$("#inputwarning_updmarginpersen").hide();
	$("#updmarginpersen-modal").modal('toggle');
	$("#updmarginpersen-modal").modal('show');
}

$('#btnupdate_updmarginpersen').click(function(){
	updatemarginpersen();
});

function updatemarginpersen(){
	var kode = $("#labelkode_updmarginpersen").html();
	// var jns = $("#lbljnscustomerpersen").html();
	var marginpersenx = $("#txtmarginpersen").val();
	var marginpersen = marginpersenx.replace(',','.');
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/pricelist/updatemarginpersen.php',
			data: "kode="+kode+
					"&marginpersen="+marginpersen,
			success: function(response) {
				var pageno = getCookie("nopagex");
				tampildata(pageno);
				$("#inputwarning_updmarginpersen").hide();
				$("#updmarginpersen-modal").modal('hide');
			}
		});	
	}
}

function editmarginrp(kode){
	var cari = $("#cari").val();	
	setCookie("carix",cari); 
	
	$("#labelkode_updmarginrp").html(kode);
	// $("#lbljnscustomerrp").html(jns);
	$("#txtmarginrp").val('');
	$("#inputwarning_updmarginrp").hide();
	$("#updmarginrp-modal").modal('toggle');
	$("#updmarginrp-modal").modal('show');
}

$('#btnupdate_updmarginrp').click(function(){	
	updatemarginrp();
});

function updatemarginrp(){	
	var kode = $("#labelkode_updmarginrp").html();
	// var jns = $("#lbljnscustomerrp").html();
	var marginrpy = $("#"+kode).val();
	var marginrpx = marginrpy.substr(0,marginrpy.length-3);
	var marginrp = $("#txtmarginrp").val();
	var max = (parseInt(marginrpx.substr(0,(marginrpx.length-2)))+1)*100;
	var min = parseInt(marginrpx.substr(0,(marginrpx.length-2)))*100;
	var batalkan = false;
	/*
	if(marginrp<min && marginrpy!=0) {
		alert(marginrp+' - '+min+' - '+marginrpy);
		$("#inputwarning_updmarginrp").html("Margin Rp melebihi batas minimal");
		$("#inputwarning_updmarginrp").show();
		batalkan = true;
	}

	if(marginrp>max && marginrpy!=0) {
		$("#inputwarning_updmarginrp").html("Margin Rp melebihi batas maksimal");
		$("#inputwarning_updmarginrp").show();
		batalkan = true;
	}
	*/
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/pricelist/updatemarginrp.php',
			data: "kode="+kode+
					"&marginrp="+marginrp,
			success: function(response) {
				$("#inputwarning_updmarginrp").hide();
				$("#updmarginrp-modal").modal('hide');
				var pageno = getCookie("nopagex");	
				tampildata(pageno);				
			}
		});	
	}
}

function editmarginpersen_res(kode){
	
	var cari = $("#cari").val();	
	setCookie("carix",cari); 
	
	$("#labelkode_updmarginpersenres").html(kode);
	$("#txtmarginpersenres").val('');
	// $("#lbljnscustomerpersen").html(jns);
	$("#inputwarning_updmarginpersenres").hide();
	$("#updmarginpersenres-modal").modal('toggle');
	$("#updmarginpersenres-modal").modal('show');
}

$('#btnupdate_updmarginpersenres').click(function(){
	updatemarginpersen_res();
});

function updatemarginpersen_res(){
	var kode = $("#labelkode_updmarginpersenres").html();
	// var jns = $("#lbljnscustomerpersen").html();
	var marginpersenx = $("#txtmarginpersenres").val();
	var marginpersen = marginpersenx.replace(',','.');
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/pricelist/updatemarginpersen_res.php',
			data: "kode="+kode+
					"&marginpersen="+marginpersen,
			success: function(response) {
				var pageno = getCookie("nopagex");
				tampildata(pageno);
				$("#inputwarning_updmarginpersenres").hide();
				$("#updmarginpersenres-modal").modal('hide');
			}
		});	
	}
}

function editmarginrp_res(kode){
	var cari = $("#cari").val();	
	setCookie("carix",cari); 
	
	$("#labelkode_updmarginrpres").html(kode);
	// $("#lbljnscustomerrp").html(jns);
	$("#txtmarginrpres").val('');
	$("#inputwarning_updmarginrpres").hide();
	$("#updmarginrpres-modal").modal('toggle');
	$("#updmarginrpres-modal").modal('show');
}

$('#btnupdate_updmarginrpres').click(function(){
	updatemarginrp_res();
});

function updatemarginrp_res(){
	var kode = $("#labelkode_updmarginrpres").html();
	// var jns = $("#lbljnscustomerrp").html();
	var marginrpy = $("#"+kode).val();
	var marginrpx = marginrpy.substr(0,marginrpy.length-3);
	var marginrp = $("#txtmarginrpres").val();
	var max = (parseInt(marginrpx.substr(0,(marginrpx.length-2)))+1)*100;
	var min = parseInt(marginrpx.substr(0,(marginrpx.length-2)))*100;
	var batalkan = false;

	if(marginrp<min && marginrpy!=0) {
		$("#inputwarning_updmarginrpres").html("Margin Rp melebihi batas minimal");
		$("#inputwarning_updmarginrpres").show();
		batalkan = true;
	}

	if(marginrp>max && marginrpy!=0) {
		$("#inputwarning_updmarginrpres").html("Margin Rp melebihi batas maksimal");
		$("#inputwarning_updmarginrpres").show();
		batalkan = true;
	}

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/pricelist/updatemarginrp_res.php',
			data: "kode="+kode+
					"&marginrp="+marginrp,
			success: function(response) {
				$("#inputwarning_updmarginrpres").hide();
				$("#updmarginrpres-modal").modal('hide');
				var pageno = getCookie("nopagex");
				tampildata(pageno);				
			}
		});	
	}
}

function edithpp(kode){
	var cari = $("#cari").val();	
	setCookie("carix",cari); 
	
	$("#labelkode_updhpp").html(kode);
	$("#txthpp").val('');
	$("#inputwarning_updhpp").hide();
	$("#updhpp-modal").modal('toggle');
	$("#updhpp-modal").modal('show');
}

$('#btnupdate_updhpp').click(function(){
	updatehpp();
});

function updatehpp(){
	var kode = $("#labelkode_updhpp").html();
	var hpp = $("#txthpp").val();
	var batalkan = false;
	
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/pricelist/updatehpp.php',
			data: "kode="+kode+
					"&hpp="+hpp,
			success: function(response) {
				$("#inputwarning_updhpp").hide();				
				$("#updhpp-modal").modal('hide');
				var pageno = getCookie("nopagex");
				tampildata(pageno);				
			}
		});	
	}
}
