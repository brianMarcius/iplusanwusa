$(document).ready(function() {						   
	getdata();
});		

$(function() {
	$("#txttgl1").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
	$("#txttgl2").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
});

function getdata(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/mutasikas/datastok.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			tampilhistory();
		}
	});
}

function tampilhistory(){
	$.ajax({
		type	: "POST", 
		url		: "pages/mutasikas/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function goBack() {
    window.history.back();
}