$(document).ready(function() {				
	get_default();
});		

function get_default(){
	$.ajax({
		type	: "POST",
		url		: "pages/settinghutang/get_default.php",
		dataType: "json",
		timeout	: 3000,
		success	: function(data){	
			$("#txtkodesupplier").val(data.kodesupplier);
			$("#txtnamasupplier").val(data.namasupplier);
			$("#txtalamat").val(data.alamat);
			$("#txtkodearea").val(data.kodearea);
			$("#txtnamaarea").val(data.namaarea);
			 tampillistbrg();	
			 isi_cboarea();
		}
	});
}

function get_hutang(){
	
	var jmlrpbeli = $("#txtjmlrpbeli").val();
	var totrpbayar = $("#txttotrpbayar").val();
	
	$.ajax({
		type	: "POST",
		url		: "pages/settinghutang/get_hutang.php",
		data	: "jmlrpbeli="+jmlrpbeli+
					"&totrpbayar="+totrpbayar,
		dataType: "json",
		timeout	: 3000,
		success	: function(data){	
			$("#txtsaldohutang").val(data.saldohutang);
		}
	});
}

function isi_cboarea(){
	var area = $('#txtkodearea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/settinghutang/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#txtkodearea').html(data);
		}
	});		
}

function tampillistbrg(){	
	$.ajax({
		type	: "POST", 
		url		: "pages/settinghutang/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

$('#txtjmlrpbeli').keyup(function(){
	get_hutang();		
});

$('#txttotrpbayar').keyup(function(){
	get_hutang();		
});

function addhutang(){
	var kodetrx  = $("#txtkodetrx").val();
	var kodesupp  = $("#txtkodesupplier").val();
	var kodearea  = $("#txtkodearea").val();
	var tglnota  = $("#txttglnota").val();
	var nonota  = $("#txtnonota").val();
	var jmlrpbeli = $("#txtjmlrpbeli").val();
	var totrpbayar = $("#txttotrpbayar").val();
	var saldohutang = $("#txtsaldohutang").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesupp.length==0){
		$('#warningx').html('Customer tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(kodearea.length==0){
		$('#warningx').html('Area Kerja tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(tglnota.length==0){
		$('#warningx').html('Tgl Faktur masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(nonota.length==0){
		$('#warningx').html('No Faktur masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(jmlrpbeli<=0){
		$('#warningx').html('Jumlah Rp Pembelian belum diisi.');
		$("#txtjmlrpbeli").focus();
		var batalkan = true;
		return false;
	}
	
	if(totrpbayar<0){
		$('#warningx').html('Total Rp Bayar tidak valid.');
		$("#txttotrpbayar").focus();
		var batalkan = true;
		return false;
	}
	
	if(saldohutang<=0){
		$('#warningx').html('Saldo Hutang belum diisi.');
		$("#txtsaldohutang").focus();
		var batalkan = true;
		return false;
	}
	/*
	if(jmlrpbeli<=saldohutang){
		$('#warningx').html('Saldo Hutang sama atau melebihi Rp Penjualan!');
		$("#txtsaldohutang").focus();
		var batalkan = true;
		return false;
	}
	*/

	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/settinghutang/addhutang.php",
			data	: "kodetrx="+kodetrx+
					"&kodesupplier="+kodesupp+	
					"&kodearea="+kodearea+
					"&tglnota="+tglnota+
					"&nonota="+nonota+
					"&jmlrpbeli="+jmlrpbeli+
					"&totrpbayar="+totrpbayar+
					"&saldohutang="+saldohutang,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){
				
				if(data.result==0){
					$('#warningx').html('Faktur tsb sudah ada pembayaran!');					
				}else{
					$("#confirm-modal").modal('hide');
					tampillistbrg();
					clearaddhutang();
				}
			}	
		});
	}
}

function clearaddhutang(){
	$("#txttglnota").val('');
	$("#txtnonota").val('');
	$("#txtjmlrpbeli").val('');
	$("#txttotrpbayar").val('');
	$("#txtsaldohutang").val('');
	$("#txtkodetrx").val('');
}

function del(ID,kodesupp){	
	var kodearea  = $("#txtkodearea").val();
	$('#warningx').html('');
	
	if(kodearea.length==0){
		$('#warningx').html('Area Kerja belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	$.ajax({
		type	: 'POST',
		url		: 'pages/settinghutang/hapus.php',
		data	: 'kodetrx='+ID+
				'&kodesupp='+kodesupp+
				'&kodearea='+kodearea,
		dataType: 'json',
		success	: function(data){	
			if(data.result==1){
				tampillistbrg();							
			}else{
				$('#warningx').html('Faktur tsb sudah ada pembayaran!');
			}
		}
	});
}

function edit(ID,kodesupp){	
	$('#warningx').html('');
	$.ajax({
		type	: "POST",
		url		: "pages/settinghutang/edit.php",
		data	: "kodetrx="+ID+
				"&kodesupp="+kodesupp,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				$("#txttglnota").val(data.tglnota);
				$("#txtnonota").val(data.nonota);
				$("#txtjmlrpbeli").val(data.jmlrpbeli);
				$("#txttotrpbayar").val(data.totrpbayar);
				$("#txtsaldohutang").val(data.saldohutang);
				$("#txtkodetrx").val(data.kodetrx);	
				$("#txtkodearea").val(data.kodearea);	
			}else{
				$('#warningx').html('Faktur tsb sudah ada pembayaran!');
			}
		}
	});
}

$('#btn_addhutang').click(function(){	
	var kodetrx  = $("#txtkodetrx").val();
	var kodesupp  = $("#txtkodesupplier").val();
	var kodearea  = $("#txtkodearea").val();
	var tglnota  = $("#txttglnota").val();
	var nonota  = $("#txtnonota").val();
	var jmlrpbeli = $("#txtjmlrpbeli").val();
	var totrpbayar = $("#txttotrpbayar").val();
	var saldohutang = $("#txtsaldohutang").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesupp.length==0){
		$('#warningx').html('Customer tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(kodearea.length==0){
		$('#warningx').html('Area Kerja tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(tglnota.length==0){
		$('#warningx').html('Tgl Faktur masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(nonota.length==0){
		$('#warningx').html('No Faktur masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(jmlrpbeli<=0){
		$('#warningx').html('Jumlah Rp Penjualan belum diisi.');
		$("#txtjmlrpbeli").focus();
		var batalkan = true;
		return false;
	}
	
	if(totrpbayar<0){
		$('#warningx').html('Total Rp Bayar tidak valid.');
		$("#txttotrpbayar").focus();
		var batalkan = true;
		return false;
	}
	
	if(saldohutang<=0){
		$('#warningx').html('Saldo Hutang tidak boleh nol.');
		$("#txtsaldohutang").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/settinghutang/validasi.php",
			data	: "kodetrx="+kodetrx+
					"&kodesupplier="+kodesupp+	
					"&kodearea="+kodearea+
					"&tglnota="+tglnota+
					"&nonota="+nonota+
					"&jmlrpbeli="+jmlrpbeli+
					"&totrpbayar="+totrpbayar+
					"&saldohutang="+saldohutang,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){
				if(data.result==0){
					$('#warningx').html('Faktur tsb sudah ada pembayaran!');					
				}else{
					if(data.result==2){
						$('#warningx').html('Saldo Hutang sama atau melebihi Rp Penjualan!');					
					}else{
						if(data.result==3){
							$('#warningx').html('Saldo Hutang tidak valid!');					
						}else{
							$("#confirm-modal").modal('toggle');
							$("#confirm-modal").modal('show');
						}
					}
				}
			}	
		});
	}			 
});

$('#btnsave').click(function(){
	addhutang();		
});

function goBack() {
    window.history.back();
}