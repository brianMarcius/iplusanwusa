$(document).ready(function() {
	document.getElementById("btnconfirm").disabled = true;
	isi_cbodokter();
	isi_cbopasien();
	isi_cbobarang();
	isi_cbosatuan();
	tampilbahan();
});

$(function() {
	$("#txttglresep").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
});

$('#txttglresep').keyup(function() {
	setbtnconfirm();
});

function isi_cbodokter(){
	$.ajax({
		type	: "POST", 
		url		: "pages/resepdokter/tampildokter.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbodokter").html(data);
		}
	});
}

function isi_cbopasien(){
	$.ajax({
		type	: "POST", 
		url		: "pages/resepdokter/tampilpasien.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbopasien").html(data);
		}
	});
}

function tampilbahan(){
	$.ajax({
		type	: "POST", 
		url		: "pages/resepdokter/tampilbahan.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbahan").html(data);
		}
	});
}

function isi_cbobarang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/resepdokter/tampilkan_barang.php',
		success: function(response) {
			$('#cbobarang').html(response); 
		}
	});		
}

function isi_cbosatuan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/resepdokter/tampilkan_satuan.php',
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function setbtnconfirm(){
	$.ajax({
		type	: "POST",
		url		: "pages/resepdokter/getdatacount.php",
		dataType: "json",
		success	: function(data){	
			var dokter = $('#cbodokter').val();
			var pasien = $('#cbopasien').val();
			var tglresep = $('#txttglresep').val();
			
			if(dokter.length>0 && pasien.length>0 && tglresep.length>0 && data.valid>0){
				document.getElementById("btnconfirm").disabled = false;
			}else{
				document.getElementById("btnconfirm").disabled = true;
			}
		}
	});
}

function addbarang(){
	var kodebrg = $("#cbobarang").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum diisi.');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/resepdokter/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat,
			timeout	: 3000,
			success	: function(data){				
				tampilbahan();
				 clearaddbrg();
				 setbtnconfirm();
			}	
		});
	}
}

function clearaddbrg(){
	$("#cbobarang").val('');
	$("#cbobarang").select2("val","");
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#cbosatuan").select2("val","");
}

function simpandata(){
	var dokter = $('#cbodokter').val();
	var pasien = $('#cbopasien').val();
	var tglresep = $('#txttglresep').val();
	var batalkan 	= false;
	
	if(dokter.length==0){
		$("#lblwarning_save").text('Nama Dokter belum diisi!');
		$("#cbodokter").focus();
		var batalkan = true;
	}
	if(pasien.length==0){
		$("#lblwarning_save").text('Nama Pasien belum diisi!');
		$("#cbopasien").focus();
		var batalkan = true;
	}
	if(tglresep.length==0){
		$("#lblwarning_save").text('Tgl Resep belum diisi!');
		$("#txttglresep").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/resepdokter/simpan.php",
			data	: "dokter="+dokter+
						"&pasien="+pasien+
						"&tglresep="+tglresep,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				tampilbahan();
				$("#cbodokter").val('');
				$("#cbodokter").select2("val","");
				$("#cbopasien").val('');
				$("#cbopasien").select2("val","");
				$("#txttglresep").val('');
			}	
		});	
	}	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=newminta");
});

function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/resepdokter/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampilbahan();
				setbtnconfirm();				
			}				
		}
	});
}

$('#btnconfirm').click(function(){
	var dokter = $('#cbodokter').val();
	var pasien = $('#cbopasien').val();
	var tglresep = $('#txttglresep').val();
	var batalkan 	= false;
	
	if(dokter.length==0){
		$("#lblwarning_save").text('Nama Dokter belum diisi!');
		$("#cbodokter").focus();
		var batalkan = true;
	}
	if(pasien.length==0){
		$("#lblwarning_save").text('Nama Pasien belum diisi!');
		$("#cbopasien").focus();
		var batalkan = true;
	}
	if(tglresep.length==0){
		$("#lblwarning_save").text('Tgl Resep belum diisi!');
		$("#txttglresep").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');	
	}
	
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnsave').click(function(){
	simpandata();		
});