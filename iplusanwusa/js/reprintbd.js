$(document).ready(function() {	
	isi_cbosupplier();
});						   

$(function() {
	$('#tgltrx').daterangepicker({format: 'DD/MM/YYYY'});	
});		

function isi_cbosupplier(){
	var supplier = $('#cbosupplier').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/cetakbd/tampilkan_supplier.php',
		data: 'supplier='+supplier,
		success: function(data) {			
			$('#cbosupplier').html(data);
			tampildata('1');
		}
	});		
}	

$('#tgltrx').change(function(){
	tampildata('1');						 
});

$('#cari').click(function(){
	tampildata('1');						 
});	

$('#cbosupplier').click(function(){
	tampildata('1');						 
});


function tampildata(pageno){
	var tgl = $("#tgltrx").val();
	var supplier = $("#cbosupplier").val();
	var cari = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakbd/tampillistbarangdatang.php",
		data	: "tgl="+tgl+	
					"&supplier="+supplier+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);			
		}
	});
}	

function paging(pageno){
	var tgl = $("#tgltrx").val();
	var supplier = $("#cbosupplier").val();
	var cari = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakbd/paging.php",
		data	: "tgl="+tgl+	
					"&supplier="+supplier+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function cetak(ID,kdsupplier){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetakbd/lintasform.php",
		data	: "kode="+ID+
					"&kdsupplier="+kdsupplier,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=printbd'); 
			}
		}	
	});	
}
