$(document).ready(function() {
	isi_cbomarketing();					
});	

function isi_cbomarketing(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappenjualanmarkrekap/tampilkan_marketing.php',
		success: function(data) {			
			$('#cbomarketing').html(data);
			isi_cbojnscustomer();
		}
	});		
}

function isi_cbojnscustomer(){
	$.ajax({
		type: 'POST', 
		url: 'pages/lappenjualanmarkrekap/tampilkan_jenis.php',
		success: function(data) {
			$('#cbojnscustomer').html(data); 
				settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lappenjualanmarkrekap/tgldefa.php",
		dataType: "json",		
		success	: function(data){				
			$("#tgltrx1").val(data.tgltrx);
			$("#tgltrx2").val(data.tgltrx);
			settglarea();
		}
	});
}

function settglarea(){
	
	var tgltrx1 = $("#tgltrx1").val();	
	var tgltrx2 = $("#tgltrx2").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappenjualanmarkrekap/tglarea.php",
		data	: "tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cbomarketing').change(function(){							  
	tampildata();
	settglarea();
});

$('#tgltrx1').change(function(){
	tampildata();
	settglarea();
});

$('#tgltrx2').change(function(){
	tampildata();
	settglarea();
});

$('#cbotrx').change(function(){
	tampildata();
});

$('#cbojnscustomer').change(function(){
	tampildata();
});

function tampildata(){
	
	var marketing = $("#cbomarketing").val();
	var tgltrx1 = $("#tgltrx1").val();
	var tgltrx2 = $("#tgltrx2").val();
	var jnstrx = $("#cbotrx").val();		
	var jnscustomer = $("#cbojnscustomer").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappenjualanmarkrekap/tampildata.php",
		data	: "marketing="+marketing+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2+
				"&jnstrx="+jnstrx+
				"&jnscustomer="+jnscustomer,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail_jl(ID,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/lintasform.php",
		data	: "kode="+ID+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=jldetailmark');
			}
		}
	});
}