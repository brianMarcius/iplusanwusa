$(document).ready(function() {
	isi_cbocabang();
	tampildata('1');
	isi_cbokaryawan();
});	

$(function() {
	$("#tglkeluar").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
});

function isi_cbokaryawan() {
	$.ajax({
		type: 'POST', 
		url: 'pages/logfinger/tampilkan_karyawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response); 
		}
	});		
}

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/logfinger/tampilkan_cabangx.php',
		success: function(response) {			
			$('#cboarea').html(response);
			if(getCookie("areaempl").length>0) {
				$('#cboarea').val(getCookie("areaempl"));
			}
		}
	});		
}

$('#cbokaryawan').change(function(){
	tampildata('1');
});

$('#txttglakhir').change(function(){
	tampildata('1');
});

function tampildata(pageno){
	setCookie("nopageempl", pageno);
	if(getCookie("pageempl").length>0) {
		var pageno = getCookie("pageempl");
	}
	else {
		var pageno = pageno;
	}
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}	
	if(getCookie("areaempl").length>0) {
		var area = getCookie("areaempl");
	}
	else {
		var area = $("#cboarea").val();		
	}

	var karyawan = $('#cbokaryawan').val();
	var tglawal = $('#txttglawal').val();
	var tglakhir = $('#txttglakhir').val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/logfinger/tampildata.php",
		data	: "idkaryawan="+karyawan+
					"&tglawal="+tglawal+
					"&tglakhir="+tglakhir,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}else {
		var kode = $("#cari").val();		
	}
	
	if(getCookie("areaempl").length>0) {
		var area = getCookie("areaempl");
	}
	else {
		var area = $("#cboarea").val();		
	}
	
	$.ajax({
		type	: "GET",		
		url		: "pages/logfinger/paging.php",
		data	: "kode="+kode+
					"&area="+area+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			delCookie("cariempl");
			delCookie("pageempl");
			delCookie("areaempl");
		}
	});
}

function editempl(kode){
	setCookie("pageempl", getCookie("nopageempl"));
	setCookie("cariempl", $("#cari").val());
	setCookie("areaempl", $("#cboarea").val());
	
	$.ajax({
		type	: "POST", 
		url		: "pages/logfinger/lintasform.php",
		data	: "kode="+kode,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newempl');
			}
		}	
	});	
}

function resignempl(kode){
	$("#resign-modal").modal('toggle');
	$("#resign-modal").modal('show');
	$("#txtidlogfinger").val(kode);
}

function resign(){
	var idlogfinger	= $("#txtidlogfinger").val();
	var tglkeluar	= $("#tglkeluar").val();
	var batalkan 	= false;
	
	setCookie("pageempl", getCookie("nopageempl"));
	setCookie("cariempl", $("#cari").val());

	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/logfinger/resign.php",
			data	: "idlogfinger="+idlogfinger+
						"&tglkeluar="+tglkeluar,
			dataType: "json",
			success	: function(data){
				$("#resign-modal").modal('hide');
				tampildata('1');
			}	
		});
	}	
}

$('#btnupdate').click(function(){
	resign();
});

function delemp(kode){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkode").val(kode);
}

$('#btnyes').click(function(){
	del();
});

function del(){
	var idlogfinger	= $("#txtkode").val();
	var batalkan 	= false;
	
	setCookie("pageempl", getCookie("nopageempl"));
	setCookie("cariempl", $("#cari").val());

	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/logfinger/hapus.php",
			data	: "idlogfinger="+idlogfinger,
			dataType: "json",
			success	: function(data){
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data.result);
				tampildata('1');
			}	
		});
	}	
}