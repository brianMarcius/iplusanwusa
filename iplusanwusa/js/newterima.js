$(document).ready(function() {
	//document.getElementById("btnconfirm").disabled = true;
	tampil_listbarang();
	$("#txtnotrx").focus();
});	

function EnterJml(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		$("#cbosatuan").focus();				
	}
}

$("#btnlistbrg").click(function() {
	$("#barang-modal").modal('toggle');
	$("#barang-modal").modal('show');
	tampil_masterbrg(1);		
});

function tampil_masterbrg(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type: 'GET', 
		url: 'pages/penerimaan/tampil_masterbrg.php',
		data: "namabrg="+namabrg+
				"&page="+pageno,	
		success: function(data) {
			$('#tblmasterbrg').html(data); 		
			paging(pageno);
		}
	});		
}

function paging(pageno){
	var namabrg = $('#txtcaribarang').val();	
	$.ajax({
		type	: "GET",		
		url		: "pages/penerimaan/pagingmasterbrg.php",
		data	: "namabrg="+namabrg+
					"&page="+pageno,
		success	: function(data){
			$("#pagingmasterbrg").html(data);
		}
	});
}

$("#txtcaribarang").keyup(function(){
	 tampil_masterbrg(1);
});

function getkodebrg(kodebrg,namabrg,namagdg){
	$("#txtkodebrg").val(kodebrg);
	$("#txtnamabrg").val(namabrg);
	isi_cbosatuan();
	$("#barang-modal").modal('hide');
	$("#txtjml").val('1');
	$("#txtgudang").val(namagdg);
	$("#txtjml").focus();
}

function isi_cbosatuan(){
	var kodebrg = $('#txtkodebrg').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/penerimaan/tampilkan_satuan.php',
		data	: "kodebrg="+kodebrg,
		success: function(response) {
			$('#cbosatuan').html(response); 
		}
	});		
}

function tampil_listbarang(){

	$.ajax({
		type	: "POST", 
		url		: "pages/penerimaan/tampilbarang_ok.php",
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

function addbarang(){
	var kodebrg = $("#txtkodebrg").val();
	var jml 	= $("#txtjml").val();
	var kodesat = $("#cbosatuan").val();
	var gudang	= $("#txtgudang").val();

	$('#warningx').html('');
	var batalkan = false;	
	
	if(kodesat.length==0){
		$('#warningx').html('Satuan barang belum dipilih.');
		$("#cbosatuan").focus();
		var batalkan = true;
	}
	
	if(jml<=0){
		$('#warningx').html('Jumlah belum di pilih');
		$("#txtjml").focus();
		var batalkan = true;
	}
	
	if(gudang<=0){
		$('#warningx').html('Gudang belum dipilih.');
		$("#cbogudang").focus();
		var batalkan = true;
	}
	
	if(kodebrg.length<=1){
		$('#warningx').html('Barang belum dipilih.');
		$("#cbobarang").focus();
		var batalkan = true;
	}
		
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/penerimaan/addbarang.php",
			data	: "kodebrg="+kodebrg+	
					"&jml="+jml+
					"&kodesatuan="+kodesat+	
					"&gudang="+gudang,	
			timeout	: 3000,
			success	: function(data){				
 				 tampil_listbarang();
				 clearaddbrg();
			}	
		});
	}
}

function clearaddbrg(){
	$("#txtnamabrg").val('');
	$("#txtkodebrg").val('');
	$("#txtjml").val('');
	$("#cbosatuan").val('');
	$("#txtgudang").val('');
}

function simpandata() {
	var notrx = $("#txtnotrx").val();
	var ket = $("#txtket").val();
	
	$.ajax({
		type	: "POST",		
		url		: "pages/penerimaan/simpan.php",
		data	: "kodetrx="+notrx+
					"&ket="+ket,
		success	: function(data){
			$("#confirm-modal").modal('hide');
			$("#info-modal").modal('toggle');
			$("#info-modal").modal('show');
			$("#infone").html(data);
		}
	});
}

function tampil_konfirmexec(){	
	var notrx = $("#txtnotrx").val();
	var ket = $("#txtket").val();

	$.ajax({
		type	: "POST",		
		url		: "pages/penerimaan/getdatacount.php",
		dataType: "json",
		success	: function(data){
			if(data.jmlrec<1) {
				$('#warningx').html('Barang belum dipilih.');
			}
			else if(notrx.length<1) {
				$('#warningx').html('No Transaksi belum diisi.');
			}
			else if(ket.length<1) {
				$('#warningx').html('Keterangan belum diisi.');
			}
			else {
				$('#warningx').html('');
				$("#confirm-modal").modal('toggle');
				$("#confirm-modal").modal('show');	
			}
		}
	});
}


function del(ID){
	$.ajax({
		type	: "POST",
		url		: "pages/penerimaan/hapus.php",
		data	: "kodebrg="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				tampil_listbarang();
			}				
		}
	});
}

$('#btnexec').click(function(){		
	simpandata();
});

$('#btn_addbrg').click(function(){	
	addbarang();						 
});

$('#btnconfirm').click(function(){		
	tampil_konfirmexec();
});

$('#btnok').click(function(){		
	location.reload(true);
});