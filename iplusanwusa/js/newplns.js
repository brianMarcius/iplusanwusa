$(document).ready(function() {		
	tampildata_pelunasan();
});	

$(function() {
	$("#txttglbayar").inputmask("dd-mm-yyyy", {"placeholder": "dd/mm/yyyy"});
});	

function tampildata_pelunasan(){
	$.ajax({
		type		: "POST",
		url			: "pages/pelunasan/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#txtkodesupplier").val(data.kode_supplier);
			$("#txtnamasupplier").val(data.nama_supplier);
			$("#txtalamat").val(data.alamat);
			$("#txttglbayar").val(data.tglbayar);			
			$("#txtjmlhutang").val(data.jmlhutang);
			//$("#txtsaldohutang").val(data.jmlhutang);
			tampil_list();
			
		}
	});
}

function tampil_list(){		
	$.ajax({
		type	: "POST", 
		url		: "pages/pelunasan/tampil_listplns.php",
		success	: function(data){
			$("#tblplns").html(data);
			getsaldohutang();
		}
	});
}

function tampil_banktujuan(){				
	$.ajax({
		type	: "POST", 
		url		: "pages/pelunasan/tampilkan_bank.php",
		success	: function(data){
			$("#cbobank_tujuan").html(data);
		}
	});
}

$("#cbosistembyr").change(function() {
	var viabayar = $("#cbosistembyr").val();	
	
	if(viabayar!=2){
		$("#cbobank_tujuan").val('');
		$("#txtnorek_tujuan").val('');
//		$("#txtownrek").val('');
		tampil_banktujuan();
		$("#bank_add").hide();
		document.getElementById("cbobank_tujuan").disabled = true;
		document.getElementById("txtnorek_tujuan").disabled = true;
//		document.getElementById("txtownrek").disabled = true;
	}else{		
		document.getElementById("cbobank_tujuan").disabled = false;
		document.getElementById("txtnorek_tujuan").disabled = false;
//		document.getElementById("txtownrek").disabled = false;
		tampil_banktujuan();
		$("#bank_add").show();
	}			
});

$("#txtjmlbayar").keyup(function() {
	getsaldohutang();
});

function datediff(tgl) {
	var dx = tgl.split("/");
	var d = dx[1]+"/"+dx[0]+"/"+dx[2];
	var now = new Date();
	var tglx = new Date(d);
	var a = now.getTime() - tglx.getTime();
	var diff = Math.floor(a/(1000*60*60*24));
	return diff;
}

$('#btnconfirm').click(function(){
	var tglbayar 	= $("#txttglbayar").val();
	var sistembyr	= $("#cbosistembyr").val();
	var banktujuan	= $("#cbobank_tujuan").val();
	var norektujuan = $("#txtnorek_tujuan").val();
//	var pemilikrek	= $("#txtownrek").val();
	var jmlhutangx 	= $("#txtjmlhutang").val();
	var jmlhutang	= jmlhutangx.substring(0,1);
	var jmlbayarx 	= $("#txtjmlbayar").val();
	var jmlbayar	= jmlbayarx.substring(0,2);
	var saldohutangx= $("#txtsaldohutang").val();
	var saldohutang	= saldohutangx.substring(0,2);

	$("#lblwarning_save").text('');
	
	if(jmlhutang<=0){
		$("#lblwarning_save").text('Tidak ada nota yang dipilih.');
		$("#txtjmlbayar").focus();
		return false;
	}							
							
	if(tglbayar.length==0){
		$("#lblwarning_save").text('Tgl Bayar belum diisi.');
		$("#txttglbayar").focus();
		return false;
	}

	if(datediff(tglbayar)<0) {
		$("#lblwarning_save").text('Tgl Bayar melebihi tanggal sekarang.');
		$("#txttglbayar").focus();
		return false;
	}
	
	if(sistembyr.length==0){
		$("#lblwarning_save").text('Cara Pembayaran belum dipilih.');
		$("#cbosistembyr").focus();
		return false;
	}
	
	if(sistembyr==2){
		if(banktujuan.length==0){
			$("#lblwarning_save").text('Bank Tujuan belum dipilih.');
			$("#cbobank_tujuan").focus();
			return false;
		}
		if(norektujuan.length==0){
			$("#lblwarning_save").text('No Rekening belum diisi.');
			$("#txtnorek_tujuan").focus();
			return false;
		}
		// if(pemilikrek.length==0){
		// 	$("#lblwarning_save").text('Nama Pemilik Rekening masih kosong.');
		// 	$("#txtownrek").focus();
		// 	return false;
		// }
	}
	
	if(jmlbayar<=0){
		$("#lblwarning_save").text('Jml Bayar tidak valid.');
		$("#txtjmlbayar").focus();
		return false;
	}
	
	if(saldohutang<0){
		$("#lblwarning_save").text('Jml bayar melebihi sisa hutang lalu.');
		$("#txtjmlbayar").focus();
		return false;
	}							

	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#overlayx").hide();
	$("#loading-imgx").hide();
	
});

function simpandata(){	
	
	var kodesupplier = $("#txtkodesupplier").val();
	var tglbayar 	= $("#txttglbayar").val();
	var sistembyr	= $("#cbosistembyr").val();
	var banktujuan	= $("#cbobank_tujuan").val();
	var norektujuan = $("#txtnorek_tujuan").val();
	var jmlbayar 	= $("#txtjmlbayar").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pelunasan/simpan.php",
		data	: "kodesupplier="+kodesupplier+
					"&tglbayar="+tglbayar+
					"&sistembyr="+sistembyr+
					"&banktujuan="+banktujuan+
					"&norektujuan="+norektujuan+
					"&jmlbayar="+jmlbayar,
		timeout	: 3000,
		beforeSend	: function(){		
			$("#overlayx").show();
			$("#loading-imgx").show();			
		},
		success	: function(data){
			$("#confirm-modal").modal('hide');
			$("#info-modal").modal('toggle');
			$("#info-modal").modal('show');
			$("#infone").html(data);
		}	
	});		
}		

$('#btnsave').click(function(){
	simpandata();		
	$("#txttglbayar").val('');
	$("#txtnorek_tujuan").val('');
//	$("#txtownrek").val('');
	$("#txtjmlbayar").val('');
	$("#txtsaldohutang").val('');
	tampil_list();
	tampildata_pelunasan();
	
	//getsaldohutang();
});							 

$('#btnclose').click(function(){
	window.history.back();
});	

function myfunction(obj){
	var cek = document.getElementById(obj).checked;
	if(cek==true){
		addpay(obj);
	}else{
		delpay(obj);
	}
}

function addpay(kode){	
	var kodesupplier = $("#txtkodesupplier").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pelunasan/addpay.php",
		data	: "kode="+kode+
					"&kodesupplier="+kodesupplier,
		dataType: "json",
		success	: function(data){			
			$("#txtjmlhutang").val(data.jmlhutang);
			getsaldohutang();
		}
	});
}

function delpay(kode){	
	var kodesupplier = $("#txtkodesupplier").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pelunasan/delpay.php",
		data	: "kode="+kode+
					"&kodesupplier="+kodesupplier,
		dataType: "json",
		success	: function(data){
			$("#txtjmlhutang").val(data.jmlhutang);
			getsaldohutang();
		}
	});
}

function getsaldohutang(){
	var jmlhutanglalu 	= $("#txtjmlhutang").val();
	var jmlbayar  		= $("#txtjmlbayar").val();
	
	$.ajax({
		type		: "POST",
		url			: "pages/pelunasan/getsaldohutang.php",
		data		: "jmlhutanglalu="+jmlhutanglalu+
						"&jmlbayar="+jmlbayar,	
		dataType	: "json",
		success		: function(data){
			$("#txtsaldohutang").val(data.jmlhutang);
		}
	});
}

function regnewbank(){
	var bank	 	= $("#txtbank").val();	
	var batalkan 	= false;

	if(bank<=1){
		$('#warningreg').html('Nama bank harus di inputkan!!');
		$("#txtbank").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/pelunasan/addbank.php",
			data	: "bank="+bank,
			timeout	: 3000,
			success	: function(data){
				$('#warningreg').show();
				$('#warningreg').html(data)
				tampil_banktujuan();
				$("#txtbank").val('');
				
			}	
		});
	}	
}


$('#bank_add').click(function(){		
	$("#regnewbank-modal").modal('toggle');
	$("#regnewbank-modal").modal('show');
	$('#warningreg').hide();
});

$('#btnadd').click(function(){
	regnewbank();
});