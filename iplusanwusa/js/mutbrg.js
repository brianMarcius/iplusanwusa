$(document).ready(function() {
	isi_cboarea();	
	isi_cbogudang();
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/mutasibarang/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);				
			settglarea();
		}
	});		
}

function isi_cbogudang(){
	var gudang = $('#cbogudang').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/mutasibarang/tampilkan_gudang.php',
		data: 'gudang='+gudang,
		success: function(data) {			
			$('#cbogudang').html(data);				
			tampildata();
		}
	});		
}

function settglarea(){
	
	var area = $("#cboarea").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/mutasibarang/tglarea.php",
		data	: "area="+area,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

function cekupd(){
	$.ajax({		
		url		: "pages/mutasibarang/timeupd.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblwaktu").text(data.waktu);
		}
	});
}

$('#cboarea').change(function(){	
	settglarea();
});

$('#cbogudang').change(function(){
	tampildata();
});

$('#cboshift').change(function(){
	tampildata();
});

function tampildata(){
	var area   = $("#cboarea").val();
	var gudang = $("#cbogudang").val();	
	var shift = $("#cboshift").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/mutasibarang/tampildata.php",
		data	: "area="+area+
				"&shift="+shift+
				"&gudang="+gudang,
		success	: function(data){				
			$("#tampildata").html(data);
			cekupd();
		}
	});
}

function cetakmutbrg(){
	var area   = $("#cboarea").val();
	var gudang = $("#cbogudang").val();
	var shift = $("#cboshift").val();
	
	// window.open('?mod=cetmutbrg','_blank');
	window.open('pages/mutasibarang/cetmutbrg.php?area='+area+'&shift='+shift+'&gudang='+gudang,'_blank');
	//window.open("pages/tandaterima/cetakttb.php");
}