$(document).ready(function() {						   
	tampildata('1');
});						   

$(function() {
	$('#tglnota').daterangepicker({format: 'DD/MM/YYYY'});	
});		

$('#tglnota').change(function(){
	tampildata('1');						 
});	

$('#txtkode').keyup(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl = $("#tglnota").val();
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/pembelian/tampildata.php",
		data	: "tgl="+tgl+	
					"&kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var tgl = $('#tglnota').val();
	var kode = $("#txtkode").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/pembelian/paging.php",
		data	: "tgl="+tgl+	
					"&kode="+kode+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function tampillistbrg(){
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/tampildetail.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

function retur(ID){		
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newretur');
			}
		}
	});
}

function brgretur(kode){		
	$('#retur-modal').modal('toggle');
	$("#retur-modal").modal('show');		
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/tampil_detailretur.php",
		data	: "kode="+kode,
		timeout	: 3000,
		beforeSend	: function(){
			$("#overlayy").show();
			$("#loading-imgy").show();
		},
		success	: function(data){
			$("#info-modal").modal('hide');		
			$("#overlayy").hide();
			$("#loading-imgy").hide();
			$("#inforetur").html(data);
		}	
	});
}

function edit(ID){	
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/lintasform.php",
		data	: "kode="+ID,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newbuy');
			}
		}
	});
}

function hapus(ID){			
	$('#confirm-modal').modal('toggle');
	$("#confirm-modal").modal('show');
	$("#txtkodebeli").val(ID);
}

$('#btnsave').click(function(){
	var kode = $("#txtkodebeli").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pembelian/hapus_trx.php",
		data	: "kode="+kode,
		dataType: "json",
		success	: function(data){
			$("#confirm-modal").modal('hide');
			$('#info-modal').modal('toggle');
			$("#info-modal").modal('show');
			$("#infone").html(data.pesan);
			if(data.sukses==1){
				tampildata('1');
			}
		}
	});
});

function changecheck(kodebeli){
	$.ajax({
		type: 'POST', 
		url: 'pages/pembelian/changecheck.php',
		data: "kodebeli="+kodebeli,
		success: function(response) {

		}
	});		
}