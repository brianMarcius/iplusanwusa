$(document).ready(function() {						   
	isi_cbocabang();
	delCookie("nopagex");		
});			

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settinghutang/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
			tampildata('1');
		}
	});		
}

$("#cbocabang").change(function() {
	tampildata('1');
})

$("#namasupplier").keyup(function() {
	tampildata('1');
})

function tampildata(pageno){
	
	setCookie("nopagex", pageno);
	
	var cabang = $("#cbocabang").val();
	var namasupplier = $("#namasupplier").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/settinghutang/tampildata.php",
		data	: "cabang="+cabang+
					"&namasupplier="+namasupplier+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			var pageno = getCookie("nopagex");
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var cabang = $("#cbocabang").val();
	var namasupplier = $("#namasupplier").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/settinghutang/paging.php",
		data	: "cabang="+cabang+
					"&namasupplier="+namasupplier+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			//delCookie("nopagex");
		}
	});
}

function del(kode){
	$("#kodehapus").val(kode);
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
}

$('#btndel').click(function(){
	hapus();
});

function hapus(){
	var kode = $("#kodehapus").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/settinghutang/hapus.php',
		data: "kode="+kode,
		dataType: "json",
		success: function(response) {
			tampildata('1');						
			$("#confirm-modal").modal('hide');
		}
	});		
}

function editsaldo(kode,kodearea){
	/*
	$("#labelkode_updsaldo").html(kode);
	$("#lblkodearea").html(kodearea);
	$("#txtsaldo").val('');
	$("#inputwarning_updsaldo").hide();
	$("#updsaldo-modal").modal('toggle');
	$("#updsaldo-modal").modal('show');
	*/
	
	$.ajax({
		type	: "POST", 
		url		: "pages/settinghutang/lintasform.php",
		data	: "kode="+kode,
		dataType: "json",
		timeout	: 3000,
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=newsethut');
			}
		}
	});
	
}

$('#btnupdate_updsaldo').click(function(){
	updatesaldo();
});

function updatesaldo(){
	var cabang = $("#lblkodearea").html();
	var kodesupplier = $("#labelkode_updsaldo").html();
	var saldo = $("#txtsaldo").val();
	var batalkan = false;
		
	if(saldo < 0) {
		$("#inputwarning_updsaldo").show();
		$("#inputwarning_updsaldo").text("Nilai yang dimasukkan tidak valid");
		batalkan = true;
		return false;
	}

	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/settinghutang/updatesaldo.php',
			data: "kode="+kodesupplier+
					"&cabang="+cabang+
					"&saldo="+saldo,
			success: function(response){
				var pageno = getCookie("nopagex");
				tampildata(pageno);
				$("#inputwarning_updsaldo").hide();
				$("#updsaldo-modal").modal('hide');
			}
		});	
	}
}
