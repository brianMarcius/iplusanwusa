$(document).ready(function() {	
	tampildata('1');
});						   

$(function() {
	$('#tgltrx').daterangepicker({format: 'DD/MM/YYYY'});	
});			

$('#tgltrx').change(function(){
	tampildata('1');						 
});

$('#cari').click(function(){
	tampildata('1');						 
});	

function tampildata(pageno){
	var tgl  = $("#tgltrx").val();
	var cari = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakrb/tampillistreturbeli.php",
		data	: "tgl="+tgl+	
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);			
		}
	});
}	

function paging(pageno){
	var tgl = $("#tgltrx").val();
	var cari = $("#cari").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakrb/paging.php",
		data	: "tgl="+tgl+	
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function cetak(ID,kdsupplier){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetakrb/lintasform.php",
		data	: "kode="+ID+
					"&kdsupplier="+kdsupplier,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=printrb'); 
			}
		}	
	});	
}
