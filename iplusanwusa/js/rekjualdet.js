$(document).ready(function() {
	tampildata();
});	

function tampildata(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/rekpenjualan_detail/tampildata.php",
		success	: function(data){				
			$("#tampildata").html(data);
			settglarea();
		}
	});		
}	

function settglarea(){	
	$.ajax({
		type	: "GET",		
		url		: "pages/rekpenjualan_detail/tglarea.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			$("#lblnamacustomer").text(data.namacustomer);
		}
	});
}