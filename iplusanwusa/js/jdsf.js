$(document).ready(function() {
	isi_cbocabang();
	isi_cbobulan();	
});	

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/jadwalshift/tampilkan_cabangx.php',
		success: function(response) {			
			$('#cboarea').html(response);
			if(getCookie("areaempl").length>0) {
				$('#cboarea').val(getCookie("areaempl"));
			}
		}
	});		
}

function isi_cbobulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/jadwalshift/tampilkan_bulan.php',
		success: function(response) {			
			$('#cbobulan').html(response);
			if(getCookie("bulanx").length>0) {
				$('#cbobulan').val(getCookie("bulanx"));
			}
			isi_cbominggu();			
		}
	});		
}

function isi_cbominggu(){
	var thnbln = $('#cbobulan').val();
	
	$.ajax({
		type: "POST", 
		url: "pages/jadwalshift/tampilkan_mingguke.php",
		data: "thnbln="+thnbln,
		success: function(response) {	
			$('#cbominggu').html(response);
			if(getCookie("minggux").length>0) {
				$('#cbominggu').val(getCookie("minggux"));
			}
			tampildata('1');
		}
	});		
}

$('#cboarea').click(function(){
	tampildata('1');
});

$('#cbobulan').click(function(){
	isi_cbominggu();						  
	tampildata('1');
});

$('#cbominggu').click(function(){
	tampildata('1');
});

$('#cboreadonly').click(function(){
	tampildata('1');
});

$('#cari').keyup(function(){
	tampildata('1');
});

function tampildata(pageno){
	setCookie("nopageempl", pageno);
	if(getCookie("pageempl").length>0) {
		var pageno = getCookie("pageempl");
	}
	else {
		var pageno = pageno;
	}
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}
	else {
		var kode = $("#cari").val();		
	}	
	if(getCookie("areaempl").length>0) {
		var area = getCookie("areaempl");
	}
	else {
		var area = $("#cboarea").val();		
	}
	
	var bulan = $("#cbobulan").val();
	var mingguke = $("#cbominggu").val();
	var readonly = $("#cboreadonly").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/jadwalshift/tampildata.php",
		data	: "kode="+kode+
					"&area="+area+
					"&bulan="+bulan+
					"&mingguke="+mingguke+
					"&readonly="+readonly+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}

function modalshift(kode,karyawan,tgl,shiftx){
	$.ajax({
		type	: "POST",		
		url		: "pages/jadwalshift/tampilkan_shift.php",
		data	: {kodearea:kode,
					karyawan:karyawan,
					tgl:tgl,
					shiftx:shiftx},
		success	: function(data){
			$("#list_shift").html(data);
			$("#myModal").modal('show');
		}
	});
}

function update_shift(karyawan,tgl,shift,shiftx){

	$.ajax({
		type : "POST",
		url : "pages/jadwalshift/simpan.php",
		data  : {
			karyawan:karyawan,
			tgl:tgl,
			shift:shift,
			shiftx:shiftx
		},
		success:function(data){
			$("#myModal").modal('hide');
			tampildata('1');
		} 
	})
}

function delshift(karyawan,tgl,shiftx){

	$.ajax({
		type : "POST",
		url : "pages/jadwalshift/hapus.php",
		data  : {
			karyawan:karyawan,
			tgl:tgl,
			shiftx:shiftx
		},
		success:function(data){
			$("#myModal").modal('hide');
			tampildata('1');
		} 
	})
}


// $("#cboarea").change(function(){
// 	var kode = $("#cboarea").val();
// 	var bulan = $("#cbobulan").val();
// 	var mingguke = $("#cbominggu").val();
// 	console.log(kode);
// 	$.ajax({
// 		type	: "POST",		
// 		url		: "pages/jadwalshift/tampilkan_shift.php",
// 		data	: {kodearea:kode,
// 					bulan:bulan,
// 					minggu:mingguke},
// 		success	: function(data){
// 			$("#list_shift").html(data);
// 		}
// 	});
// });


function paging(pageno){
	
	if(getCookie("cariempl").length>0) {
		var kode = getCookie("cariempl");
	}else {
		var kode = $("#cari").val();		
	}
	
	if(getCookie("areaempl").length>0) {
		var area = getCookie("areaempl");
	}
	else {
		var area = $("#cboarea").val();		
	}
	
	$.ajax({
		type	: "GET",		
		url		: "pages/jadwalshift/paging.php",
		data	: "kode="+kode+
					"&area="+area+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
			delCookie("cariempl");
			delCookie("pageempl");
			delCookie("areaempl");
		}
	});
}

function edit(nik,tgl,shiftx){	
	$.ajax({
		type	: "POST", 
		url		: "pages/jadwalshift/simpan.php",
		data	: "nik="+nik+
				"&tgl="+tgl+
				"&shiftx="+shiftx,
		success	: function(data){
			//alert(data);
			tampildata('1');
		}	
	});	
}

function editlongshit(nik,tgl,shiftx){	
	$.ajax({
		type	: "POST", 
		url		: "pages/jadwalshift/simpan.php",
		data	: "nik="+nik+
				"&tgl="+tgl+
				"&shiftx="+shiftx,
		success	: function(data){
			//alert(data);
			tampildata('1');
		}	
	});	
}