$(document).ready(function() {
  isi_cbokaryawan();
});						   

$(function() {	

	"use strict";
	
	$("#chkmsk").on('ifUnchecked', function(event) {
		document.getElementById("txtjammasuk").disabled = true;
		$("#txtjammasuk").val('');		
	});
	
	$("#chkmsk").on('ifChecked', function(event) {
		document.getElementById("txtjammasuk").disabled = false;
		$("#txtjammasuk").val('');	
	});	
	
	$("#chkplg").on('ifUnchecked', function(event) {
		document.getElementById("txtjampulang").disabled = true;
		$("#txtjampulang").val('');
	});
	
	$("#chkplg").on('ifChecked', function(event) {
		document.getElementById("txtjampulang").disabled = false;	
		$("#txtjampulang").val('');
	});	
	
	$("[data-mask]").inputmask();
});

function isi_cbokaryawan() {
	$.ajax({
		type: 'POST', 
		url: 'pages/absenmanual/tampilkan_karyawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response); 
		}
	});		
}

$('#cbokaryawan').click(function(){
	getdivisi();
});

function getdivisi(){
	var nik = $("#cbokaryawan").val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/absenmanual/getdivisi.php',
		data: "nik="+nik,
		dataType: "json",
		success: function(data){
			if(data.jmldata>0){					
				$("#txtdivisi").val(data.divisi);
			}
		}
	});		
}

function simpandata(){	
	var nik 		= $("#cbokaryawan").val();
	var tglkerja 	= $("#txttglkerja").val();
	var alasan  	= $("#cboalasan").val();
	var jammasuk 	= $("#txtjammasuk").val();
	var jampulang 	= $("#txtjampulang").val();
	var ket		  	= $("#txtket").val();
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/absenmanual/simpan.php",
			data	: "nik="+nik+
					"&tglkerja="+tglkerja+
					"&alasan="+alasan+
					"&jammasuk="+jammasuk+
					"&jampulang="+jampulang+
					"&ket="+ket,
			timeout	: 30000,
			success	: function(data){
				if (data=='1') {
					toastr["success"]("Insert Success", "Success");
					$("#confirm-modal").modal('hide');
					window.location.assign("?mod=abmn");
				}else{
					toastr["error"]("Insert failed ","Error");	
					$("#confirm-modal").modal('hide');	
				}		
			}	
		});
	}	
}

$('#btnconfirm').click(function(){
	$("#warningx").text('');
	var nik 		= $("#cbokaryawan").val();
	var tglkerja 	= $("#txttglkerja").val();
	var alasan  	= $("#cboalasan").val();
	var jammasuk 	= $("#txtjammasuk").val();
	var jampulang 	= $("#txtjampulang").val();
	var ket		  	= $("#txtket").val();
	var batalkan = false;

	if(nik.length==0){
		$("#warningx").text('Pilih nama karyawan dulu!');
		var batalkan = true;
		return false;
	}
	
	if(tglkerja.length==0){
		$("#warningx").text('Tgl kerja masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(alasan.length==0){
		$("#warningx").text('Alasan belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(jammasuk.length==0 && jampulang.length==0){
		$("#warningx").text('Tanggal tidak boleh kosong semua!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
});