$(document).ready(function() {	
	isi_cbogudang();
	isi_cbokatbarang();
});						   

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbogudang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settinggudang/tampil_gudang.php',
		success: function(response) {
			$('#cbogudang').html(response); 
			tampildata('1');
		}
	});		
}

function getpic(){	
	var gudang 	= $("#cbogudang").val();
	$.ajax({
		type: "POST", 
		url: "pages/settinggudang/picgudang.php",
		data	: "kodegudang="+gudang,
		dataType: "json",
		success: function(data) {
			$('#txtpic').val(data.pic);
		}
	});		
}

function isi_cbopic(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/settinggudang/tampil_karyawan.php',
		success: function(response) {
			$('#cbopic').html(response);
			tampildata('1');	
		}
	});		
}

function isi_cbopic2(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/settinggudang/tampil_karyawan.php',
		success: function(response) {
			$('#cbopic2').html(response);
		}
	});		
}

function isi_cbokatbarang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settinggudang/tampilkan_kategori.php',
		success: function(response) {
			$('#cbokatbarang').html(response); 
		}
	});		
}

$('#cbogudang').change(function() {
	getpic();							
	tampildata('1');
});

$('#cbokatbarang').change(function() {
	tampildata('1');
});

$('#txtcari').keyup(function() {
	tampildata('1');
});

function tampildata(pageno){	
	
	setCookie("pagech", pageno);
	var gudang 	= $("#cbogudang").val();
	var katbrg 	= $("#cbokatbarang").val();
	var keyword	= $("#txtcari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/settinggudang/tampildata.php",
		data	: "gudang="+gudang+
					"&katbrg="+katbrg+
					"&keyword="+keyword+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			var pageno = getCookie("pagech");
			paging(pageno);
		}
	});
}	

function paging(pageno){	
	
	var gudang = $("#cbogudang").val();
	var katbrg 	= $("#cbokatbarang").val();
	var keyword	= $("#txtcari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/settinggudang/paging.php",
		data	: "gudang="+gudang+
					"&katbrg="+katbrg+
					"&keyword="+keyword+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

$('#gudang_add').click(function(){
	$("#addgudang-modal").modal('toggle');
	$("#addgudang-modal").modal('show');
	isi_cbopic();
	$('#txtgudangbaru').focus();
});

$('#btnupdategud').click(function(){
	var gudang = $("#txtgudangbaru").val();
	var pic = $("#cbopic").val();
	
	$("#warninggud").text('');
	$("#warninggud").hide();
	var batalkan = false;
	
	if(gudang.length==0){
		$("#warninggud").text('Nama Gudang masih kosong!');
		$("#warninggud").show();
		var batalkan = true;
		return false;
	}
	
	if(pic.length==0){
		$("#warninggud").text('Penanggungjawab belum dipilih!');
		$("#warninggud").show();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/settinggudang/addgudang.php",
			data	: "gudang="+gudang+
						"&pic="+pic,
			timeout	: 3000,
			success	: function(data){	
				$("#addgudang-modal").modal('hide');
				isi_cbogudang();
			}	
		});
	}					
});

$('#edit_pic').click(function(){
	var gudang 	= $("#cbogudang").val();	
	
	if(gudang.length==0){
		return false;
	}
	
	$("#editpic-modal").modal('toggle');
	$("#editpic-modal").modal('show');	
	$("#kdgud").text(gudang);
	isi_cbopic2();
});

$('#btneditpic').click(function(){
	var gudang 	= $("#kdgud").text();
	var pic 	= $("#cbopic2").val();
	
	$("#warningpic").hide();
	var batalkan = false;
		
	if(pic.length==0){
		$("#warningpic").show();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/settinggudang/editpic.php",
			data	: "gudang="+gudang+
						"&pic="+pic,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){	
				$("#editpic-modal").modal('hide');
				$('#txtpic').val(data.pic);
			}	
		});
	}					
});

function changelock(kodebrg){		
	var gudang = $("#cbogudang").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/settinggudang/changelock.php',
		data: "kode="+kodebrg+
				"&gudang="+gudang,
		success: function(response) {
			//var pageno = getCookie("pagech");
			//tampildata(pageno);
			
		}
	});		
}

function changelock_all(x){		
	var gudang = $("#cbogudang").val();
	var katbrg 	= $("#cbokatbarang").val();
	var keyword	= $("#txtcari").val();
	var checkall= $('#check_all').prop('checked');
	
	if(checkall==true){
		var checkall=1;
	}else{
		var checkall=0;
	}
	
	$.ajax({
		type: 'POST', 
		url: 'pages/settinggudang/changelock_all.php',
		data: "katbrg="+katbrg+
				"&keyword="+keyword+
				"&gudang="+gudang+
				"&checkall="+checkall,
		success: function(response) {
			tampildata(x);			
		}
	});		
}