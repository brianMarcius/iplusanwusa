$(document).ready(function() {				
	get_default();
});		

function get_default(){
	$.ajax({
		type	: "POST",
		url		: "pages/settingpiutang/get_default.php",
		dataType: "json",
		timeout	: 3000,
		success	: function(data){	
			$("#txtkodecustomer").val(data.kodecustomer);
			$("#txtnamacustomer").val(data.namacustomer);
			$("#txtalamat").val(data.alamat);
			$("#txtkodearea").val(data.kodearea);
			$("#txtnamaarea").val(data.namaarea);
			 tampillistbrg();	
			 isi_cboarea();
		}
	});
}

function get_piutang(){
	
	var jmlrpjual = $("#txtjmlrpjual").val();
	var totrpbayar = $("#txttotrpbayar").val();
	
	$.ajax({
		type	: "POST",
		url		: "pages/settingpiutang/get_piutang.php",
		data	: "jmlrpjual="+jmlrpjual+
					"&totrpbayar="+totrpbayar,
		dataType: "json",
		timeout	: 3000,
		success	: function(data){	
			$("#txtsaldopiutang").val(data.saldopiutang);
		}
	});
}

function isi_cboarea(){
	var area = $('#txtkodearea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpiutang/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#txtkodearea').html(data);
		}
	});		
}

function tampillistbrg(){	
	$.ajax({
		type	: "POST", 
		url		: "pages/settingpiutang/tampillistbrg.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

$('#txtjmlrpjual').keyup(function(){
	get_piutang();		
});

$('#txttotrpbayar').keyup(function(){
	get_piutang();		
});

function addpiutang(){
	var kodetrx  = $("#txtkodetrx").val();
	var kodecust  = $("#txtkodecustomer").val();
	var kodearea  = $("#txtkodearea").val();
	var tglnota  = $("#txttglnota").val();
	var nonota  = $("#txtnonota").val();
	var jmlrpjual = $("#txtjmlrpjual").val();
	var totrpbayar = $("#txttotrpbayar").val();
	var saldopiutang = $("#txtsaldopiutang").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodecust.length==0){
		$('#warningx').html('Customer tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(kodearea.length==0){
		$('#warningx').html('Area Kerja tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(tglnota.length==0){
		$('#warningx').html('Tgl Nota masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(nonota.length==0){
		$('#warningx').html('No Nota masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(jmlrpjual<=0){
		$('#warningx').html('Jumlah Rp Penjualan belum diisi.');
		$("#txtjmlrpjual").focus();
		var batalkan = true;
		return false;
	}
	
	if(totrpbayar<0){
		$('#warningx').html('Total Rp Bayar tidak valid.');
		$("#txttotrpbayar").focus();
		var batalkan = true;
		return false;
	}
	
	if(saldopiutang<=0){
		$('#warningx').html('Saldo Piutang belum diisi.');
		$("#txtsaldopiutang").focus();
		var batalkan = true;
		return false;
	}
	/*
	if(jmlrpjual<=saldopiutang){
		$('#warningx').html('Saldo Piutang sama atau melebihi Rp Penjualan!');
		$("#txtsaldopiutang").focus();
		var batalkan = true;
		return false;
	}
	*/
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/settingpiutang/addpiutang.php",
			data	: "kodetrx="+kodetrx+
					"&kodecustomer="+kodecust+	
					"&kodearea="+kodearea+
					"&tglnota="+tglnota+
					"&nonota="+nonota+
					"&jmlrpjual="+jmlrpjual+
					"&totrpbayar="+totrpbayar+
					"&saldopiutang="+saldopiutang,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){
				if(data.lanjut==0){
					$('#warningx').html('Nota tsb sudah ada pembayaran!');					
				}else{
					$("#confirm-modal").modal('hide');
					tampillistbrg();
					clearaddpiutang();
				}
			}	
		});
	}
}

function clearaddpiutang(){
	$("#txttglnota").val('');
	$("#txtnonota").val('');
	$("#txtjmlrpjual").val('');
	$("#txttotrpbayar").val('');
	$("#txtsaldopiutang").val('');
	$("#txtkodetrx").val('');
}

function del(ID,kodecust){	
	
	var kodearea  = $("#txtkodearea").val(); 
	$('#warningx').html('');
	
	$.ajax({
		type	: "POST",
		url		: "pages/settingpiutang/hapus.php",
		data	: "kodetrx="+ID+
				"&kodecust="+kodecust+
				"&kodearea="+kodearea,
		dataType: "json",
		success	: function(data){		
			if(data.result==1){
				tampillistbrg();							
			}else{
				$('#warningx').html('Nota tsb sudah ada pembayaran!');
			}
		}
	});
}

function edit(ID,kodecust){	
	var kodearea  = $("#txtkodearea").val(); 
	$('#warningx').html('');
	
	$.ajax({
		type	: "POST",
		url		: "pages/settingpiutang/edit.php",
		data	: "kodetrx="+ID+
				"&kodecust="+kodecust+
				"&kodearea="+kodearea,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				$("#txttglnota").val(data.tglnota);
				$("#txtnonota").val(data.nonota);
				$("#txtjmlrpjual").val(data.jmlrpjual);
				$("#txttotrpbayar").val(data.totrpbayar);
				$("#txtsaldopiutang").val(data.saldopiutang);
				$("#txtkodetrx").val(data.kodetrx);	
				$("#txtkodearea").val(data.kodearea);	
			}else{
				$('#warningx').html('Nota tsb sudah ada pembayaran!');
			}
		}
	});
}

$('#btn_addpiutang').click(function(){	
	var kodetrx  = $("#txtkodetrx").val();
	var kodecust  = $("#txtkodecustomer").val();
	var kodearea  = $("#txtkodearea").val();
	var tglnota  = $("#txttglnota").val();
	var nonota  = $("#txtnonota").val();
	var jmlrpjual = $("#txtjmlrpjual").val();
	var totrpbayar = $("#txttotrpbayar").val();
	var saldopiutang = $("#txtsaldopiutang").val();

	$('#warningx').html('');
	var batalkan = false;
	
	if(kodecust.length==0){
		$('#warningx').html('Customer tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(kodearea.length==0){
		$('#warningx').html('Area Kerja tidak terdeteksi.');
		var batalkan = true;
		return false;
	}
	
	if(tglnota.length==0){
		$('#warningx').html('Tgl Nota masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(nonota.length==0){
		$('#warningx').html('No Nota masih kosong.');
		var batalkan = true;
		return false;
	}
	
	if(jmlrpjual<=0){
		$('#warningx').html('Jumlah Rp Penjualan belum diisi.');
		$("#txtjmlrpjual").focus();
		var batalkan = true;
		return false;
	}
	
	if(totrpbayar<0){
		$('#warningx').html('Total Rp Bayar tidak valid.');
		$("#txttotrpbayar").focus();
		var batalkan = true;
		return false;
	}
	
	if(saldopiutang<=0){
		$('#warningx').html('Saldo Piutang tidak boleh nol.');
		$("#txtsaldopiutang").focus();
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/settingpiutang/validasi.php",
			data	: "kodetrx="+kodetrx+
					"&kodecustomer="+kodecust+	
					"&kodearea="+kodearea+
					"&tglnota="+tglnota+
					"&nonota="+nonota+
					"&jmlrpjual="+jmlrpjual+
					"&totrpbayar="+totrpbayar+
					"&saldopiutang="+saldopiutang,
			dataType: "json",
			timeout	: 3000,
			success	: function(data){
				if(data.result==0){
					$('#warningx').html('Nota tsb sudah ada pembayaran!');					
				}else{
					if(data.result==2){
						$('#warningx').html('Saldo Piutang sama atau melebihi Rp Penjualan!');					
					}else{
						if(data.result==3){
							$('#warningx').html('Saldo Piutang tidak valid!');					
						}else{
							$("#confirm-modal").modal('toggle');
							$("#confirm-modal").modal('show');
						}
					}
				}
			}	
		});
	}			 
});

$('#btnsave').click(function(){
	addpiutang();		
});

function goBack() {
    window.history.back();
}