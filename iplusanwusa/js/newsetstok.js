$(document).ready(function() {	
	document.getElementById("btnconfirm").disabled = true;
	isi_cbocabang();
});		

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingstok/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbocabang").change(function() {
	isi_cbobarang();
	setbtnconfirm();
})

$("#cbobarang").change(function() {
	setbtnconfirm();
})

$("#txtsaldoawal, #txtminstok").keyup(function() {
	setbtnconfirm();
})

function isi_cbobarang(){
	var kodecabang =  $("#cbocabang").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/settingstok/tampilkan_barang.php',
		data: 'kodecabang='+kodecabang,
		success: function(response) {
			$('#cbobarang').html(response); 
		}
	});		
}

function setbtnconfirm(){
	var kodecabang = $("#cbocabang").val();
	var kodebrg = $("#cbobarang").val();
	var saldoawal = $("#txtsaldoawal").val();
	var minstok = $("#txtminstok").val();

	if(kodecabang.length>0 && kodebrg.length>0 && saldoawal.length>0 && minstok.length>0){
		document.getElementById("btnconfirm").disabled = false;
	}else{
		document.getElementById("btnconfirm").disabled = true;
	}
}

function simpandata(){
	var kodecabang = $("#cbocabang").val();
	var kodebrg = $("#cbobarang").val();
	var saldoawal = $("#txtsaldoawal").val();
	var minstok = $("#txtminstok").val();

		$.ajax({
			type	: "POST", 
			url		: "pages/settingstok/simpan.php",
			data	: "kodecabang="+kodecabang+
						"&kodebrg="+kodebrg+
						"&saldoawal="+saldoawal+
						"&minstok="+minstok,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				$("#cbocabang").select2('val','');
				$("#cbobarang").select2('val','');
				$("#txtsaldoawal").val('');
				$("#txtminstok").val('');
				setbtnconfirm();
			}	
		});	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=setstok");
});

$('#btnconfirm').click(function(){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');	
});

$('#btnsave').click(function(){
	simpandata();		
});
