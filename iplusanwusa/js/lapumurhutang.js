$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapumurhutang/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);			
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	var area = $("#cboarea").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapumurhutang/tglarea.php",
		data	: "area="+area,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cboarea').change(function(){	
	settgldefa();
});

$('#cbojnscustomer').change(function(){
	settgldefa();
});

function tampildata(){
	
	var area = $("#cboarea").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapumurhutang/tampildata.php",
		data	: "area="+area,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail(kodearea,kodesupplier,umurhutang){	
	
	$.ajax({
		type	: "POST", 
		url		: "pages/lapumurhutang/lintasform.php",
		data	: "kodearea="+kodearea+
					"&kodesupplier="+kodesupplier+
					"&umurhutang="+umurhutang,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=dethut');
			}
		}
	});
}