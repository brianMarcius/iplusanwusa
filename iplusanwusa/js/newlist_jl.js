$(document).ready(function() {	
	getdatapengiriman();
});		

function getdatapengiriman(){	
	$.ajax({
		type	: "POST",		
		url		: "pages/returpenjualan/datapenjualan.php",
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.namaunit);
			$("#lblalamat").text(data.alamat);
			$("#lblnotelp").text(data.notelp);
			$("#lblno").text(data.nosrtjalan);
			$("#lbltgltrx").text(data.tglminta);
			tampillistbrg();
		}
	});
}

function tampillistbrg(){
	$.ajax({
		type	: "POST", 
		url		: "pages/returpenjualan/tampillistbrg_jl.php",
		timeout	: 3000,
		success	: function(data){
			$("#tblbrg").html(data);
		}
	});
}

function goBack() {
    window.history.back();
}