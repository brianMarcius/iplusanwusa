$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lapbarangkosong/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);			
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lapbarangkosong/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx1").val(data.tgltrx1);
			$("#tgltrx2").val(data.tgltrx2);				
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx1 = $("#tgltrx1").val();	
	var tgltrx2 = $("#tgltrx2").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbarangkosong/tglarea.php",
		data	: "area="+area+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cboarea').change(function(){	
	settglarea();
});

$('#tgltrx1').change(function(){
	settglarea();
});

$('#tgltrx2').change(function(){
	settglarea();
});

$('#cbosortby').change(function(){
	tampildata();
});

function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx1 = $("#tgltrx1").val();
	var tgltrx2 = $("#tgltrx2").val();
	var sortby = $("#cbosortby").val();		
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lapbarangkosong/tampildata.php",
		data	: "area="+area+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2+
				"&sortby="+sortby,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	


function changecheck(kodebeli){
	$.ajax({
		type: 'POST', 
		url: 'pages/lapbarangkosong/changecheck.php',
		data: "kodebeli="+kodebeli,
		success: function(response) {

		}
	});		
}