$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappiutang/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);			
			settgldefa();
		}
	});		
}

function isi_cboarea(){
	var area = $('#cboarea').val();
	
	$.ajax({
		type: 'POST', 
		url: 'pages/lappiutang/tampilkan_area.php',
		data: 'area='+area,
		success: function(data) {			
			$('#cboarea').html(data);			
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/lappiutang/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx").val(data.tgltrx);				
			settglarea();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx= $("#tgltrx").val();	
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappiutang/tglarea.php",
		data	: "area="+area+
				"&tgltrx="+tgltrx,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
			tampildata();
		}
	});
}

$('#cboarea').change(function(){	
	settglarea();
});

$('#tgltrx').change(function(){
	settglarea();
});

$('#cbojnscustomer').change(function(){
	settglarea();
});

function tampildata(){
	
	var area = $("#cboarea").val();
	var jnscustomer = $("#cbojnscustomer").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/lappiutang/tampildata.php",
		data	: "area="+area+
					"&jnscustomer="+jnscustomer,
		success	: function(data){				
			$("#tampildata").html(data);
		}
	});
}	

function showdetail(kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/lappiutang/lintasform.php",
		data	: "kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=detpiut');
			}
		}
	});
}