$(document).ready(function() {
   isi_cbokota();
	isi_cbocabang();
	isi_cbodivisi();
	isi_cbojabatan();
	tampil_data();
});						   

$(function() {
	
	"use strict";
		   
	$("#txttgllahir").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
	$("[data-mask]").inputmask();	
	
	$("#chkshift").on('ifUnchecked', function(event) {
		$("#txtshift").val('0');		
	});
	
	$("#chkshift").on('ifChecked', function(event) {
		$("#txtshift").val('1');
	});	
	
});

function isi_cbokota(){
	$.ajax({
		type: 'POST', 
		url: 'pages/karyawan/tampilkan_kota.php',
		success: function(response) {
			$('#cbokotalahir').html(response); 
		}
	});	
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/karyawan/tampilkan_cabang.php',
		success: function(response) {
			//alert(response);
			$('#cbocabang').html(response); 
		}
	});		
}

function isi_cbodivisi(){
	$.ajax({
		type: 'POST', 
		url: 'pages/karyawan/tampilkan_divisi.php',
		success: function(response) {
			$('#cbodivisi').html(response); 
		}
	});	
}
function isi_cbojabatan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/karyawan/tampilkan_jabatan.php',
		success: function(response) {
			$('#cbojabatan').html(response); 
		}
	});	
}

$('#btnconfirm').click(function(){
	$("#warningx").text('');
	var idkaryawan	= $("#txtidkaryawan").val();
	var nama 		= $("#txtnama").val();
	var nik 		= $("#txtnik").val();
	var pin		 	= $("#txtpin").val();
	var kotalahir	= $("#cbokotalahir").val();
	var tgllahir 	= $("#txttgllahir").val();
	var kdjekel  	= $("#cbojekel").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= '';
	var telp 	 	= $("#txttelp").val();
	var kodecabang	= $("#cbocabang").val();
	var kddivisi  	= $("#cbodivisi").val();
	var kdjbtn  	= $("#cbojabatan").val();
	var tglmsk 	 	= $("#tglmasuk").val();
	var jenjang		= $("#cbojenjang").val();
	var jurusan		= $("#cbojurusan").val();
	var gaji		= $("#cbogaji").val();
	var shifting	= $("#txtshift").val();
	
	var batalkan = false;

	if(nama.length==0){
		$("#warningx").text('Nama masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(nik.length==0){
		$("#warningx").text('NIK masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(pin.length==0){
		$("#warningx").text('PIN masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(kotalahir.length==0){
		$("#warningx").text('Kota kelahiran belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(tgllahir.length==0){
		$("#warningx").text('Tgl lahir masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(kdjekel.length==0){
		$("#warningx").text('Jenis Kelamin belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(alamat.length==0){
		$("#warningx").text('Alamat masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(telp.length==0){
		$("#warningx").text('Nomor Telepon masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(kodecabang.length==0){
		$("#warningx").text('Cabang belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(kdjbtn.length==0){
		$("#warningx").text('Jabatan belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(kddivisi.length==0){
		$("#warningx").text('Divisi belum dipilih!');
		var batalkan = true;
		return false;
	}
	if(tglmsk.length==0){
		$("#warningx").text('Tanggal Masuk masih kosong!');
		var batalkan = true;
		return false;
	}
	
	if(jenjang.length==0){
		$("#warningx").text('Jenjang Pendidikan belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(jurusan.length==0){
		$("#warningx").text('Jurusan belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(gaji.length==0){
		$("#warningx").text('Sistem Penggajian belum dipilih!');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){					
		$("#confirm-modal").modal('toggle');
		$("#confirm-modal").modal('show');
	}	
});

$('#btnsave').click(function(){
	simpandata();
});

function simpandata(){
	var idkaryawan	= $("#txtidkaryawan").val();
	var nama 		= $("#txtnama").val();
	var nik 		= $("#txtnik").val();
	var pin		 	= $("#txtpin").val();
	var kotalahir	= $("#cbokotalahir").val();
	var tgllahir 	= $("#txttgllahir").val();
	var kdjekel  	= $("#cbojekel").val();
	var alamat 	 	= $("#alamat").val();
	var kota 	 	= '';
	var telp 	 	= $("#txttelp").val();
	var kodecabang	= $("#cbocabang").val();
	var kddivisi  	= $("#cbodivisi").val();
	var kdjbtn  	= $("#cbojabatan").val();
	var tglmsk 	 	= $("#tglmasuk").val();
	var jenjang		= $("#cbojenjang").val();
	var jurusan		= $("#cbojurusan").val();
	var gaji		= $("#cbogaji").val();
	var shifting	= $("#txtshift").val();
	var batalkan = false;
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/karyawan/simpan.php",
			data	: "idkaryawan="+idkaryawan+
					"&nama="+nama+
					"&nik="+nik+
					"&pin="+pin+
					"&kotalahir="+kotalahir+
					"&tgllahir="+tgllahir+
					"&kdjekel="+kdjekel+
					"&alamat="+alamat+
					"&kota="+kota+
					"&telp="+telp+
					"&kdcabang="+kodecabang+	
					"&kddivisi="+kddivisi+
					"&kdjbtn="+kdjbtn+
					"&tglmsk="+tglmsk+
					"&jenjang="+jenjang+
					"&jurusan="+jurusan+
					"&gaji="+gaji+
					"&shifting="+shifting,
			timeout	: 3000,
			success	: function(data){				
				$("#confirm-modal").modal('hide');				
				//window.location.assign('?mod=empl');
				
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				
			}	
		});
	}	
}

$('#btnok').click(function(){
	window.location.assign('?mod=empl');
});

function tampil_data(){		
	$.ajax({
		type		: "POST",
		url			: "pages/karyawan/cari.php",
		dataType	: "json",
		success		: function(data){
			if(data.jmldata>0){					
				$("#txtidkaryawan").val(data.idkaryawan);
				$("#txtnik").val(data.nik);
				$("#txtnama").val(data.nama);
				$("#txtpin").val(data.idfinger);
				$("#cbokotalahir").select2('val',data.tempatlahir);
				$("#txttgllahir").val(data.tgllahir);
				$("#cbojekel").val(data.jnskelamin);
				$("#alamat").val(data.alamat);				
				$("#txttelp").val(data.notelp);
				$("#cbocabang").val(data.kodearea);
				$("#cbodivisi").val(data.kodedivisi);
				$("#cbojabatan").val(data.kodejabatan);
				$("#tglmasuk").val(data.tglmasuk);	
				$("#cbojenjang").val(data.jenjang);
				$("#cbojurusan").val(data.jurusan);
				$("#cbogaji").val(data.bulanan);
				
				if(data.shifting==1){
					$("#chkshift").iCheck("check");
				}else{					
					$("#chkshift").iCheck("uncheck");
				}
			}
		}
	});
}

$('#divisi_add').click(function(){
	$("#adddivisi-modal").modal('toggle');
	$("#adddivisi-modal").modal('show');
	$('#txtdivisibaru').focus();
});

$('#btnupdatediv').click(function(){
	var divisibaru = $("#txtdivisibaru").val();
	$("#warningdiv").hide();
	var batalkan = false;
	
	if(divisibaru.length==0){
		$("#warningdiv").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/karyawan/adddivisi.php",
			data	: "divisibaru="+divisibaru,
			timeout	: 3000,
			success	: function(data){				
				$("#adddivisi-modal").modal('hide');
				isi_cbodivisi();
			}	
		});
	}		
});

$('#jabatan_add').click(function(){
	$("#addjabatan-modal").modal('toggle');
	$("#addjabatan-modal").modal('show');
	$('#txtjabatanbaru').focus();
});

$('#btnupdatejab').click(function(){
	var jabatanbaru = $("#txtjabatanbaru").val();
	$("#warningjab").hide();
	var batalkan = false;
	
	if(jabatanbaru.length==0){
		$("#warningjab").show();
		var batalkan = true;
		return false;
	}	
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/karyawan/addjabatan.php",
			data	: "jabatanbaru="+jabatanbaru,
			timeout	: 3000,
			success	: function(data){				
				$("#addjabatan-modal").modal('hide');
				isi_cbojabatan();
			}	
		});
	}					
});