$(document).ready(function() {
	isi_cboarea();	
});	

function isi_cboarea(){		
	
	$.ajax({
		type: 'POST', 
		url: 'pages/rekpenjualan/tampilkan_area.php',
		success: function(data) {				
			$('#cboarea').html(data);	
			settgldefa();
		}
	});		
}

function settgldefa(){
	
	$.ajax({
		type	: "POST",		
		url		: "pages/rekpenjualan/tgldefa.php",
		dataType: "json",		
		success	: function(data){	
			$("#tgltrx1").val(data.tgltrx1);
			$("#tgltrx2").val(data.tgltrx2);				
			tampildata();
		}
	});
}

function settglarea(){
	
	var area = $("#cboarea").val();
	var tgltrx1 = $("#tgltrx1").val();	
	var tgltrx2 = $("#tgltrx2").val();
	$.ajax({
		type	: "GET",		
		url		: "pages/rekpenjualan/tglarea.php",
		data	: "area="+area+
				"&tgltrx1="+tgltrx1+
				"&tgltrx2="+tgltrx2,
		dataType: "json",		
		success	: function(data){				
			$("#lblarea").text(data.area);
			$("#lblalamat").text(data.alamat);
			$("#lbltgltrx").text(data.tgltrx);
		}
	});
}

$('#cboarea').change(function(){	
	tampildata();
});

$('#tgltrx1').change(function(){
	tampildata();
});

$('#tgltrx2').change(function(){
	tampildata();
});

$('#cboformat').change(function(){	
	tampildata();
});


function tampildata(){
	
	var area = $("#cboarea").val();
	var tgltrx1 = $("#tgltrx1").val();
	var tgltrx2 = $("#tgltrx2").val();
	var format = $("#cboformat").val();
	
	if(area!='XX' && tgltrx1.length>0 && tgltrx2.length>0 && format.length>0){
	
		$.ajax({
			type	: "GET",		
			url		: "pages/rekpenjualan/tampildata.php",
			data	: "area="+area+
					"&tgltrx1="+tgltrx1+
					"&tgltrx2="+tgltrx2+
					"&formatby="+format,
			success	: function(data){				
				$("#tampildata").html(data);
				settglarea();
			}
		});
	}
}	

function showdetail(tglawal,tglakhir,kdarea,kdcust){	
	$.ajax({
		type	: "POST", 
		url		: "pages/rekpenjualan_detail/lintasform.php",
		data	: "tglawal="+tglawal+
					"&tglakhir="+tglakhir+
					"&kdarea="+kdarea+
					"&kdcust="+kdcust,		
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=rekjualdet');
			}
		}
	});
}