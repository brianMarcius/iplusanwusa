$(document).ready(function() {	
	tampildata_pelunasan();
	tampil_list();
});	

$(function() {
	$("#txttglbayar").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
});	

function tampildata_pelunasan(){
	$.ajax({
		type		: "POST",
		url			: "pages/pembayaran/cari.php",
		dataType	: "json",
		success		: function(data){
			$("#txtkodecustomer").val(data.kode_customer);
			$("#txtnamacustomer").val(data.nama_customer);			
			$("#txtalamat").val(data.alamat);			
			$("#txtnotelp").val(data.notelp);
			$("#txttglbayar").val(data.tglbayar);
			$("#txtjmlpiutang").val(data.jmlpiutang);		
		}
	});
}

function tampil_list(){		
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaran/tampil_listplns.php",
		success	: function(data){
			$("#tblplns").html(data);
			getsaldopiutang();
		}
	});
}

function tampil_banktujuan(){				
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaran/tampilkan_bank.php",
		success	: function(data){
			$("#cbobank_tujuan").html(data);
		}
	});
}

$("#cbosistembyr").change(function() {
	var viabayar = $("#cbosistembyr").val();	
	
	if(viabayar!=2){
		$("#cbobank_tujuan").val('');
		$("#txtnorek_tujuan").val('');
//		$("#txtownrek").val('');
		tampil_banktujuan();
		$("#bank_add").hide();
		document.getElementById("cbobank_tujuan").disabled = true;
		document.getElementById("txtnorek_tujuan").disabled = true;
//		document.getElementById("txtownrek").disabled = true;
	}else{		
		document.getElementById("cbobank_tujuan").disabled = false;
		document.getElementById("txtnorek_tujuan").disabled = false;
//		document.getElementById("txtownrek").disabled = false;
		tampil_banktujuan();
		$("#bank_add").show();
	}			
});

$("#txtjmlbayar").keyup(function() {
	getsaldopiutang();
});

function datediff(tgl) {
	var dx = tgl.split("/");
	var d = dx[1]+"/"+dx[0]+"/"+dx[2];
	var now = new Date();
	var tglx = new Date(d);
	var a = now.getTime() - tglx.getTime();
	var diff = Math.floor(a/(1000*60*60*24));
	return diff;
}

$('#btnconfirm').click(function(){
	var tglbayar 	= $("#txttglbayar").val();
	var sistembyr	= $("#cbosistembyr").val();
	var banktujuan	= $("#cbobank_tujuan").val();
	var norektujuan = $("#txtnorek_tujuan").val();
//	var pemilikrek	= $("#txtownrek").val();
	var jmlpiutangx 	= $("#txtjmlpiutang").val();
	var jmlpiutang	= jmlpiutangx.substring(0,1);
	var jmlbayarx 	= $("#txtjmlbayar").val();
	var jmlbayar	= jmlbayarx.substring(0,2);
	var saldopiutangx= $("#txtsaldopiutang").val();
	var saldopiutang	= saldopiutangx.substring(0,2);

	$("#lblwarning_save").text('');
	
	if(jmlpiutang<=0){
		$("#lblwarning_save").text('Tidak ada nota yang dipilih.');
		$("#txtjmlbayar").focus();
		return false;
	}							
							
	if(tglbayar.length==0){
		$("#lblwarning_save").text('Tgl Bayar belum diisi.');
		$("#txttglbayar").focus();
		return false;
	}

	if(datediff(tglbayar)<0) {
		$("#lblwarning_save").text('Tgl Bayar melebihi tanggal sekarang.');
		$("#txttglbayar").focus();
		return false;
	}
	
	if(sistembyr.length==0){
		$("#lblwarning_save").text('Cara Pembayaran belum dipilih.');
		$("#cbosistembyr").focus();
		return false;
	}
	
	if(sistembyr==2){
		if(banktujuan.length==0){
			$("#lblwarning_save").text('Bank Tujuan belum dipilih.');
			$("#cbobank_tujuan").focus();
			return false;
		}
		if(norektujuan.length==0){
			$("#lblwarning_save").text('No Rekening belum diisi.');
			$("#txtnorek_tujuan").focus();
			return false;
		}
		// if(pemilikrek.length==0){
		// 	$("#lblwarning_save").text('Nama Pemilik Rekening masih kosong.');
		// 	$("#txtownrek").focus();
		// 	return false;
		// }
	}
	
	if(jmlbayar<=0){
		$("#lblwarning_save").text('Jml Bayar tidak valid.');
		$("#txtjmlbayar").focus();
		return false;
	}
	
	if(saldopiutang<0){
		$("#lblwarning_save").text('Jml bayar melebihi sisa piutang lalu.');
		$("#txtjmlbayar").focus();
		return false;
	}							

	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');
	$("#overlayx").hide();
	$("#loading-imgx").hide();
	
});

function simpandata(){	
	
	var kodecustomer	= $("#txtkodecustomer").val();
	var tglbayar 	= $("#txttglbayar").val();
	var sistembyr	= $("#cbosistembyr").val();
	var banktujuan	= $("#cbobank_tujuan").val();
	var norektujuan = $("#txtnorek_tujuan").val();
//	var pemilikrek	= $("#txtownrek").val();
	var jmlbayar 	= $("#txtjmlbayar").val();
	
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaran/simpan.php",
		data	: "kodecustomer="+kodecustomer+
					"&tglbayar="+tglbayar+
					"&sistembyr="+sistembyr+
					"&banktujuan="+banktujuan+
					"&norektujuan="+norektujuan+
					//"&pemilikrek="+pemilikrek+
					"&jmlbayar="+jmlbayar,
		timeout	: 3000,
		beforeSend	: function(){		
			$("#overlayx").show();
			$("#loading-imgx").show();			
		},
		success	: function(data){
			$("#lblwarning_save").text(data);
		}	
	});		
}		

$('#btnsave').click(function(){
	simpandata();	
	$("#confirm-modal").modal('hide');
	$("#txttglbayar").val('');
	$("#txtnorek_tujuan").val('');
//	$("#txtownrek").val('');
	$("#txtjmlbayar").val('');
	tampildata_pelunasan();
	tampil_list();
	getsaldopiutang();
});							 

$('#btnclose').click(function(){
	window.history.back();
});	

function myfunction(obj){	
	var cek = document.getElementById(obj).checked;	
	if(cek==true){
		addpay(obj);
	}else{
		delpay(obj);
	}
}

function addpay(kode){	
	var kodecustomer = $("#txtkodecustomer").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaran/addpay.php",
		data	: "kode="+kode+
					"&kodecustomer="+kodecustomer,
		dataType: "json",
		success	: function(data){
			$("#txtjmlpiutang").val(data.jmlpiutang);
			getsaldopiutang();
		}
	});
}

function delpay(kode){	
	var kodecustomer = $("#txtkodecustomer").val();
	$.ajax({
		type	: "POST", 
		url		: "pages/pembayaran/delpay.php",
		data	: "kode="+kode+
					"&kodecustomer="+kodecustomer,
		dataType: "json",
		success	: function(data){
			$("#txtjmlpiutang").val(data.jmlpiutang);
			getsaldopiutang();
		}
	});
}

function getsaldopiutang(){
	var jmlpiutanglalu 	= $("#txtjmlpiutang").val();
	var jmlbayar  		= $("#txtjmlbayar").val();
	
	$.ajax({
		type		: "POST",
		url			: "pages/pembayaran/getsaldopiutang.php",
		data		: "jmlpiutanglalu="+jmlpiutanglalu+
						"&jmlbayar="+jmlbayar,	
		dataType	: "json",
		success		: function(data){
			$("#txtsaldopiutang").val(data.jmlpiutang);
		}
	});
}

function regnewbank(){
	var bank	 	= $("#txtbank").val();	
	var batalkan 	= false;

	if(bank<=1){
		$('#warningreg').html('Nama bank harus di inputkan!!');
		$("#txtbank").focus();
		var batalkan = true;
	}
	
	if(batalkan==false){						
		$.ajax({
			type	: "POST", 
			url		: "pages/pembayaran/addbank.php",
			data	: "bank="+bank,
			timeout	: 3000,
			success	: function(data){
				$('#warningreg').show();
				$('#warningreg').html(data)
				tampil_banktujuan();
				$("#txtbank").val('');
				
			}	
		});
	}	
}


$('#bank_add').click(function(){		
	$("#regnewbank-modal").modal('toggle');
	$("#regnewbank-modal").modal('show');
	$('#warningreg').hide();
});

$('#btnadd').click(function(){
		regnewbank();
});