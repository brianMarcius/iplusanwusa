$(document).ready(function(){
	isi_cbobulan();
	isi_cbotahun();
	isi_cboarea();
	tampildata();
});

function isi_cbobulan(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/reportgaji/listbulan.php',
		success: function(response) {
			$('#cbobulan').html(response);
		}
	});	
}

function isi_cbotahun(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/reportgaji/listtahun.php',
		success: function(response) {
			$('#cbotahun').html(response);
		}
	});	
}

function isi_cboarea(){
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/bankjateng/list_perusahaan.php',
		success: function(response) {
			$('#cboarea').html(response);
		}
	});	
}

function tampildata(){	
	var	bulan = $("#cbobulan").val();
	var	tahun = $("#cbotahun").val();
	var	kodearea = $("#cboarea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/bankjateng/tampildata.php',
		data: {
          	bulan: bulan,
          	tahun: tahun,
          	kodearea: kodearea
        },
		success: function(response) {
			$('#tblpayroll').html(response); 
		}
	});	
}

$('#cbobulan').change(function(){
	tampildata();				 
});

$('#cbotahun').change(function(){
	tampildata();				 
});

$('#cboarea').change(function(){
	tampildata();				 
});

function cetakpayroll(){
	var bulan = $("#cbobulan").val();
	var tahun = $("#cbotahun").val();
	var area = $("#cboarea").val();	
	
	var batalkan = false;

	if(bulan.length==0){
		alert("Bulan belum dipilih");
		var batalkan = true;
		return false;
	}

	if(tahun.length==0){
		alert("Tahun belum dipilih");
		var batalkan = true;
		return false;
	}

	if(area.length==0){
		alert("Area Kerja belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type: "POST",		
			url: "pages/payroll/bankjateng/setsess_cetakpayroll.php",
			data: {
				bulan: bulan,
				tahun: tahun,
				area: area
			}
		});

		window.open("pages/payroll/bankjateng/cetakpayroll.php");
	}
}

function cetakpayrollall(){
	var bulan = $("#cbobulan").val();
	var tahun = $("#cbotahun").val();
	
	var batalkan = false;

	if(bulan.length==0){
		alert("Bulan belum dipilih");
		var batalkan = true;
		return false;
	}

	if(tahun.length==0){
		alert("Tahun belum dipilih");
		var batalkan = true;
		return false;
	}

	if(batalkan==false){
		$.ajax({
			type: "POST",		
			url: "pages/payroll/bankjateng/setsess_cetakpayroll.php",
			data: {
				bulan: bulan,
				tahun: tahun
			}
		});

		window.open("pages/payroll/bankjateng/cetakpayrollall.php");
	}
}