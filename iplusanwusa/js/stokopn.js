$(document).ready(function() {	
	isi_cbocabang();	
});						   

function setCookie(cname, cvalue) {
    var expires = "expires=0";
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delCookie(cname) {
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/stokopname/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
			setdefa();
		}
	});		
}

function setdefa(){
	$.ajax({
		type: 'POST', 
		url: 'pages/stokopname/setdefa.php',
		dataType: "json",		
		success: function(data) {
			$('#tgltrx').val(data.tgltrx); 
			tampildata('1');
		}
	});		
}


$("#cbocabang").change(function() {
	tampildata('1');
});

$("#tgltrx").change(function() {
	tampildata('1');
});

$("#cari").keyup(function(){
	tampildata('1');						 
});	

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length==0){
		var pageno = 1;
	}
	tampildata(pageno);						 
});

function tampildata(pageno){	
	
	setCookie("pagech", pageno);
	var cabang = $("#cbocabang").val();
	var tgltrx = $("#tgltrx").val();
	var cari = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/stokopname/tampildata.php",
		data	: "cabang="+cabang+
					"&tgltrx="+tgltrx+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#tampildata").html(data);
			var pageno = getCookie("pagech");
			paging(pageno);
		}
	});
}	

function paging(pageno){	
	
	var cabang = $("#cbocabang").val();
	var tgltrx = $("#tgltrx").val();
	var cari = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/stokopname/paging.php",
		data	: "cabang="+cabang+
					"&tgltrx="+tgltrx+
					"&cari="+cari+
					"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function EnterJml(event) {
    var x = event.which || event.keyCode;
    if(x==13){
		updatesaldo();		
	}
}

$('#btnupdate_updsaldo').click(function(){
	updatesaldo();
});

function updatesaldo(){	

	var kode = $("#labelkode_updsaldo").html();
	var saldo = $("#txtsaldo").val();
	var kodearea = $("#cbocabang").val();
	var tgltrx = $("#lbltgltrx").html();
	
	var batalkan = false;
	if(batalkan==false) {
		$.ajax({
			type: 'POST', 
			url: 'pages/stokopname/updatesaldo.php',
			data: "kode="+kode+
					"&kodearea="+kodearea+
					"&tgltrx="+tgltrx+
					"&saldo="+saldo,
			success: function(response) {
				var pageno = getCookie("pagech");			
				tampildata(pageno);
				$("#inputwarning_updsaldo").hide();
				$("#updsaldo-modal").modal('hide');
			}
		});	
	}
}   

function editsaldo(kode,tgl){
	setCookie("pagechtamp", getCookie("pagech"));	
	
	$("#labelkode_updsaldo").html(kode);
	$("#lbltgltrx").html(tgl);
	$("#txtsaldo").val('');
	$("#inputwarning_updsaldo").hide();
	
	$("#updsaldo-modal").modal('toggle');
	$("#updsaldo-modal").modal('show');
}

