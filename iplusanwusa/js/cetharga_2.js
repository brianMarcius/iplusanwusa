$(document).ready(function() {
	isi_cbokategori1();	
	settglarea();
});

function isi_cbokategori1(){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetakharga_2/tampilkan_kategori1.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbokategori1").html(data);	
			isi_cbokategori2();
		}
	});
}

function isi_cbokategori2(){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetakharga_2/tampilkan_kategori2.php",
		timeout	: 3000,
		success	: function(data){
			$("#cbokategori2").html(data);
			tampildata('1');
		}
	});
}

function settglarea(){;	
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakharga_2/tglarea.php",
		dataType: "json",
		success	: function(data){		
			$("#lbltgltrx").text(data.tgltrx);
		}
	});
}

$('#cbokategori1').change(function(){
	tampildata('1');
});

$('#cbokategori2').change(function(){
	tampildata('1');
});

$('#cari').keyup(function(){
	tampildata('1');
});

$("#cari_bynopage").keyup(function(){
	var pageno = $("#cari_bynopage").val();	
	if(pageno.length>0){
		tampildata(pageno);
	}							 
});

function tampildata(pageno){	

	var kat1 = $("#cbokategori1").val();
	var kat2 = $("#cbokategori2").val();
	var cari = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakharga_2/tampildata.php",
		data	: "kat1="+kat1+
				"&kat2="+kat2+
				"&cari="+cari+
				"&page="+pageno,
		success	: function(data){				
			$("#tampildata").html(data);
			paging(pageno);
		}
	});
}	

function paging(pageno){
	var kat1 = $("#cbokategori1").val();
	var kat2 = $("#cbokategori2").val();
	var cari = $("#cari").val();
	
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakharga_2/paging.php",
		data	: "kat1="+kat1+
				"&kat2="+kat2+
				"&cari="+cari+
				"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}