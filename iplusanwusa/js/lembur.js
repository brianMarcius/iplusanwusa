$(document).ready(function(){
	isi_cbokaryawan();
	$("#jammulai").inputmask("h:s",{ "placeholder": "hh/mm" });
	$("#jamselesai").inputmask("h:s",{ "placeholder": "hh/mm" });
});

function clearall(){
	$.ajax({
		timeout	: 3000,
		success	: function(data){
			$("#cbokaryawan").select2('val','');
			$("#txtarea").val('');
			$("#kodearea").val('');
			$("#tgllembur").val('');
			$("#cboshift").val('');
			$("#jammulai").val('');
			$("#jamselesai").val('');
			$("#cboket").val('');
		}
	});
}

function simpanlembur(){
	var idkaryawan	= $("#cbokaryawan").val();
	var tgllembur	= $("#tgllembur").val();
	var shift		= $("#cboshift").val();
	var mulai		= $("#jammulai").val();	
	var selesai		= $("#jamselesai").val();
	var ket			= $("#cboket").val();
	var kodearea	= $("#kodearea").val();
	
	var batalkan = false;	
	
	if(batalkan==false){
		$.ajax({
			type	: "POST", 
			url		: "pages/payroll/lembur/simpanlembur.php",
			data: {
          		idkaryawan: idkaryawan,
          		tgllembur: tgllembur,
          		shift: shift,
          		mulai: mulai,
          		selesai: selesai,
          		ket: ket,
          		kodearea: kodearea
        	},
			timeout	: 3000,
			success	: function(data){	
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);		
			}	
		});	
	}	
}

function isi_cbokaryawan(){	
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/lembur/tampil_karyawan.php',
		success: function(response) {
			$('#cbokaryawan').html(response);	
		}
	});		
}

$('#cbokaryawan').change(function(){
	var idkaryawan = $('#cbokaryawan').val();			
	$.ajax({
		type	: "POST",
		url		: "pages/payroll/lembur/getdatakaryawan.php",
		data	: "idkaryawan="+idkaryawan,			
		dataType: "json",
		success	: function(data){
			$('#txtarea').val(data.unit);
			$('#kodearea').val(data.kodearea);
			list_shift();
		}
	});					 
});	

$('#btnclearall').click(function(){
	$("#confirmhapus-modal").modal('toggle');
	$("#confirmhapus-modal").modal('show');	
});

$('#btnsavehapus').click(function(){	
	$("#confirmhapus-modal").modal('hide');
	clearall();	
});

$('#btnconfirm').click(function(){
	var idkaryawan	= $("#cbokaryawan").val();
	var tgllembur	= $("#tgllembur").val();
	var shift		= $("#cboshift").val();
	var mulai		= $("#jammulai").val();	
	var selesai		= $("#jamselesai").val();
	var ket			= $("#cboket").val();
	var kodearea	= $("#kodearea").val();
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
	
	$("#lblwarning_save").text('');
	var batalkan = false;
	
	if(idkaryawan.length==0){
		$("#lblwarning_save").text('Nama Karyawan belum di pilih.');
		var batalkan = true;
		return false;
	}
	
	if(tgllembur.length==0){
		$("#lblwarning_save").text('Tanggal belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(mulai.length==0){
		$("#lblwarning_save").text('Jam mulai lembur belum di inputkan.');
		var batalkan = true;
		return false;
	}

	if(mulai.length!=0 && !mulai.match(jamValid)){
		$('#lblwarning_save').text('Jam mulai lembur tidak valid');
		var batalkan = true;
		return false;
	}
	
	if(selesai.length==0){
		$("#lblwarning_save").text('Jam selesai lembur belum di inputkan.');
		var batalkan = true;
		return false;
	}

	if(selesai.length!=0 && !selesai.match(jamValid)){
		$('#lblwarning_save').text('Jam selesai lembur tidak valid');
		var batalkan = true;
		return false;
	}

	if(ket.length==0){
		$("#lblwarning_save").text('Keterangan lembur belum dipilih.');
		var batalkan = true;
		return false;
	}

	if(kodearea.length==0){
		$("#lblwarning_save").text('Kode area belum di inputkan.');
		var batalkan = true;
		return false;
	}
	
	if(batalkan==false){
		$.ajax({
			success	: function(data){
				$("#confirm-modal").modal('toggle');
				$("#confirm-modal").modal('show');	
			}
		});
	}
});

$('#btnsave').click(function(){
	simpanlembur();		
});

$('#btnokinfo').click(function(){
	$("#info-modal").modal('hide');
	isi_cbokaryawan();
	$("#cbokaryawan").select2('val','');
	$("#txtarea").val('');
	$("#kodearea").val('');
	$("#tgllembur").val('');
	$("#cboshift").val('');
	$("#jammulai").val('');
	$("#jamselesai").val('');
	$("#cboket").val('');
	clearall();
	window.location("media.php?mod=inlembur");
});

function list_shift(){
	var	kodearea = $("#kodearea").val();
	$.ajax({
		type: 'POST', 
		url: 'pages/payroll/listshift.php',
		data: {
        	kodearea: kodearea
        },
		success: function(response) {
			$('#cboshift').html(response); 
		}
	});	
}

function CekJamMulai() {
	$("#lblwarning_save").text('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jammulai = $("#jammulai").val();
    if(jammulai.length!=0 && !jammulai.match(jamValid)){
		$('#lblwarning_save').text('Jam mulai lembur tidak valid');
	}
}

function CekJamSelesai() {
	$("#lblwarning_save").text('');
	var jamValid = /([01]\d|2[0-3]):([0-5]\d)/;
    var	jamselesai = $("#jamselesai").val();
    if(jamselesai.length!=0 && !jamselesai.match(jamValid)){
		$('#lblwarning_save').text('Jam selesai lembur tidak valid');
	}
}

/**
function CekJamMulai(event) {
    var	jammulai = $("#jammulai").val();
    var x = event.which || event.keyCode;
    if(jammulai.length==2 && x!=8){
		$("#jammulai").val(jammulai+':');
	}
}
*/