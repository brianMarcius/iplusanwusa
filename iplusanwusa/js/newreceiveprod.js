$(document).ready(function() {
	gettrx();
	insert_temp();
});	
	
function setgudang(kodebrg, kodegudang) {
	$.ajax({
		type	: "POST", 
		url		: "pages/barangdatang/setgudang.php",
		data 	: "kodebrg="+kodebrg+
					"&kodegudang="+kodegudang,
		success	: function(data){
			$("#gudang_"+kodebrg).val(data);
		}
	});
}	

function gettrx() {
	$.ajax({
		type	: "GET", 
		url		: "pages/barangdatang/gettrx.php",
		success	: function(data){
			$("#txtnotrx").val(data);
		}
	});
}

function insert_temp() {
	$.ajax({
		type	: "POST", 
		url		: "pages/barangdatang/inserttemp.php",
		success	: function(data){
			tampil_listbarang();
		}
	});
}

function tampil_listbarang(){

	$.ajax({
		type	: "GET", 
		url		: "pages/barangdatang/tampilbarang.php",
		success	: function(data){
			$("#tblbarang").html(data);
		}
	});
}

function simpandata() {
	var kodetrx = $("#lblkodetrx").text();
	
	$.ajax({
		type	: "POST",		
		url		: "pages/barangdatang/simpan.php",
		data	: "kodetrx="+kodetrx,
		success	: function(data){
			$("#confirm-modal").modal('hide');
			$("#info-modal").modal('toggle');
			$("#info-modal").modal('show');
			$("#infone").html(data);
		}
	});
}

function tampil_konfirmexec(){	
	var kodetrx = $("#txtnotrx").val();
	$("#lblkodetrx").text(kodetrx);
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');	
}

$('#btnexec').click(function(){		
	simpandata();
});

$('#btnconfirm').click(function(){		
	tampil_konfirmexec();
});

$('#btnok').click(function(){		
	window.location.assign('?mod=prodarr');
});