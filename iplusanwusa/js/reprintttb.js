$(document).ready(function() {
	isi_cbomarketing();	
	tampildata('1');				
});	

$(function() {
	$('#tgltrx').daterangepicker({format: 'DD/MM/YYYY'});	
});	

function isi_cbomarketing(){
	var marketing = $('#cbomarketing').val();
	$.ajax({
		type: 'POST', 
		url: 'pages/cetakttb/tampilkan_marketing.php',
		data: 'marketing='+marketing,
		success: function(data) {			
			$('#cbomarketing').html(data);
		}
	});		
}

$('#tgltrx').change(function(){
	tampildata('1');
});

$('#cari').change(function(){
	tampildata('1');
});

$('#cbomarketing').change(function(){							  
	tampildata('1');
});

function tampildata(pageno){
	var marketing = $("#cbomarketing").val();
	var tgltrx = $("#tgltrx").val();
	var cari = $("#cari").val();			
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakttb/tampildata.php",
		data	: "marketing="+marketing+
				"&tgltrx="+tgltrx+
				"&cari="+cari+
				"&page="+pageno,
		success	: function(data){				
			$("#tampildata").html(data);
			paging(pageno);			
		}
	});
}	

function paging(pageno){
	var marketing = $("#cbomarketing").val();
	var tgltrx 	  = $("#tgltrx").val();
	var cari 	  = $("#cari").val();			
	$.ajax({
		type	: "GET",		
		url		: "pages/cetakttb/paging.php",
		data	: "marketing="+marketing+
				"&tgltrx="+tgltrx+
				"&cari="+cari+
				"&page="+pageno,
		success	: function(data){
			$("#paging").html(data);
		}
	});
}

function cetak(ID){
	$.ajax({
		type	: "POST", 
		url		: "pages/cetakttb/lintasform.php",
		data	: "kode="+ID,
		dataType: "json",
		success	: function(data){
			if(data.result==1){
				window.location.assign('?mod=ttb'); 
			}
		}	
	});	
}
