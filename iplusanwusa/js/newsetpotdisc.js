$(document).ready(function() {	
	document.getElementById("btnconfirm").disabled = true;
	isi_cbocabang();
});		

$(function() {
	$("#txttglpot").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
	$("#txttgldisc").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
});		

function isi_cbocabang(){
	$.ajax({
		type: 'POST', 
		url: 'pages/settingpotongandisc/tampilkan_cabang.php',
		success: function(response) {
			$('#cbocabang').html(response); 
		}
	});		
}

$("#cbocustomer").change(function() {
	isi_cbobarang();
	setbtnconfirm();
})

$("#cbocabang, #cbojns").change(function() {
	isi_cbocustomer();
	setbtnconfirm();
})

$("#txtpotpersen, #txtpotrp, #txttglpot, #txtdiscpersen, #txtdiscrp, #txttgldisc").keyup(function() {
	setbtnconfirm();
})

function isi_cbocustomer(){
	var kodearea = $("#cbocabang").val();
	var jns = $("#cbojns").val();
	$.ajax({
		type: 'POST', 
		data: "jns="+jns+
				"&kodearea="+kodearea,
		url: 'pages/settingpotongandisc/tampilkan_customer.php',
		success: function(response) {
			$('#cbocustomer').html(response);
			$('#cbocustomer').select2('val','');
		}
	});		
}

function isi_cbobarang(){
	var kodearea = $("#cbocabang").val();
	var jns = $("#cbojns").val();
	var kodecstmr = $("#cbocustomer").val();

	$.ajax({
		type: 'POST', 
		data: "jns="+jns+
				"&kodearea="+kodearea+
				"&kodecstmr="+kodecstmr,
		url: 'pages/settingpotongandisc/tampilkan_barang.php',
		success: function(response) {
			$('#cbobarang').html(response); 
			$('#cbobarang').select2('val','');
		}
	});		
}

function setbtnconfirm(){
	var kodecabang = $("#cbocabang").val();
	var jns = $("#cbojns").val();
	var kodecstmr = $("#cbocustomer").val();
	var kodebrg = $("#cbobarang").val();
	var potpersen = $("#txtpotpersen").val();
	var potrp = $("#txtpotrp").val();
	var tglpot = $("#txttglpot").val();
	var discpersen = $("#txtdiscpersen").val();
	var discrp = $("#txtdiscrp").val();
	var tgldisc = $("#txttgldisc").val();

	if(kodecabang.length>0 && jns.length>0 && kodecstmr.length>0 && kodebrg.length>0 && (((potpersen.length>0 || potrp.length>0) && tglpot.length>0) || ((discpersen.length>0 || discrp.length>0) && tgldisc.length>0))){
		document.getElementById("btnconfirm").disabled = false;
	}else{
		document.getElementById("btnconfirm").disabled = true;
	}
}

function simpandata(){
	var kodecabang = $("#cbocabang").val();
	var jns = $("#cbojns").val();
	var kodecstmr = $("#cbocustomer").val();
	var kodebrg = $("#cbobarang").val();
	var potpersen = $("#txtpotpersen").val();
	var potrp = $("#txtpotrp").val();
	var tglpot = $("#txttglpot").val();
	var discpersen = $("#txtdiscpersen").val();
	var discrp = $("#txtdiscrp").val();
	var tgldisc = $("#txttgldisc").val();

		$.ajax({
			type	: "POST", 
			url		: "pages/settingpotongandisc/simpan.php",
			data	: "kodecabang="+kodecabang+
						"&jns="+jns+
						"&kodecstmr="+kodecstmr+
						"&kodebrg="+kodebrg+
						"&potpersen="+potpersen+
						"&potrp="+potrp+
						"&tglpot="+tglpot+
						"&discpersen="+discpersen+
						"&discrp="+discrp+
						"&tgldisc="+tgldisc,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#overlayx").show();
				$("#loading-imgx").show();			
			},
			success	: function(data){
				$("#overlayx").hide();
				$("#loading-imgx").hide();			
				$("#confirm-modal").modal('hide');
				$("#info-modal").modal('toggle');
				$("#info-modal").modal('show');
				$("#infone").html(data);
				$("#cbocabang").select2('val','');
				$("#cbojns").select2('val','');
				$("#cbocustomer").select2('val','');
				$("#cbobarang").select2('val','');
				$("#txtpotpersen").val('');
				$("#txtpotrp").val('');
				$("#txttglpot").val('');
				$("#txtdiscpersen").val('');
				$("#txtdiscrp").val('');
				$("#txttgldisc").val('');
				setbtnconfirm();
			}	
		});	
}		

$("#info-ok, #info-close").click(function() {
	window.location.assign("?mod=setpotdisc");
});

$('#btnconfirm').click(function(){
	$("#confirm-modal").modal('toggle');
	$("#confirm-modal").modal('show');	
});

$('#btnsave').click(function(){
	simpandata();		
});
