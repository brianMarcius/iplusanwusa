<?php
session_start();
include "../../inc/inc.koneksi.php";
include "../../inc/fungsi_hdt.php";
include "../../inc/fungsi_tanggal.php";


$kode = $_GET[kode];
$username = $_SESSION[username];

$dataPerPage = 10;
if(isset($_GET['page']))
{
$noPage = $_GET['page'];
}
else $noPage = 1;

$offset = ($noPage - 1) * $dataPerPage;

$text 	= "SELECT idkaryawan,username,nama_lengkap,CONCAT('Tgl. ',DATE_FORMAT(lastlogin,'%d-%m-%Y'),' Pkl. ',DATE_FORMAT(lastlogin,'%H:%i:%s'),' WIB') 
		AS lastlogin,ipaddress,IF(online=1,'Online','Offline') AS status
		FROM userapp WHERE username NOT IN ('superuser','root') ";			

if(strlen($kode)>0) {
	$text 	= $text. "AND (nama_lengkap LIKE '%$kode%' OR username LIKE '%$kode%') ";
}
// if($kdprshn!='ITRD'){
// 	$text 	= $text. "AND a.kodeperusahaan='$kdprshn' ";
// }	 

$text 	= $text. "ORDER BY nama_lengkap ASC LIMIT $offset, $dataPerPage";					
$sql 	= mysql_query($text);	
echo "
	<table class='table table-bordered'>
		<thead style='background-color:#f9f9f9'>
			<tr>
				<th style='width:20px' class='text-center'>No</th>				
				<th class='text-center'>Pemilik User</th>	
				<th style='width:100px' class='text-center'>Username</th>				
				<th style='width:90px' class='text-center'>Status</th>
				<th style='width:140px' class='text-center'>Area Kerja</th>
				<th style='width:220px' class='text-center'>Login Terakhir</th>
				<th style='width:200px' class='text-center'>Aksi</th>
			</tr>
		</thead>
		<tbody>";		
	
		$no=1+$offset;
		while($rec = mysql_fetch_array($sql)){	
			
			$text 	= "SELECT b.namaperusahaan FROM karyawan a LEFT JOIN perusahaan b ON b.kodearea=a.kodearea WHERE a.idkaryawan='$rec[idkaryawan]'";					
			$q 		= mysql_query($text);
			$rs 	= mysql_fetch_array($q);
			$namaarea = $rs[namaperusahaan];
			
			echo "
				<tr>
					<td class='text-center'>$no.</td>                 	
					<td>".ucwords($rec[nama_lengkap])."</td>
					<td class='text-center'>$rec[username]</td>                 	
					<td class='text-center'>$rec[status]</td>
					<td class='text-center'>$namaarea</td>
					<td class='text-center'>$rec[lastlogin]</td>
					<td class='text-center'>
					<a href='javascript:void(0)' onClick=\"edit('$rec[username]')\">Edit</a> &nbsp;
					| &nbsp;<a href='javascript:void(0)' onClick=\"confirmdel('$rec[username]')\">Hapus</a>&nbsp;
					| &nbsp;<a href='javascript:void(0)' onClick=\"resetuser('$rec[username]')\">Reset Login</a>
					</td>
                </tr>";	
				
			$no++;						
		}	
		
echo "</tbody>
	</table>";
?>