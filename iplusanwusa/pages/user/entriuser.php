<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>iplus | Pengguna Aplikasi</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<link href="js/assets/css/styles.css" rel="stylesheet" type="text/css" />
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
		<link href="assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
		<link href="assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
		<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>		
		<link href="img/favicon.png" rel="shortcut icon" type="image/png" />
    </head>
    <body class="skin-blue">
        <header class="header">
            <a href="#" class="logo">
                PD. Anwusa Demak
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
					
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>
									<?php echo $_SESSION[namalengkap];?>
								<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-olive">
                                    <img src="<?php echo $_SESSION[pathavatar];?>" class="img-circle" alt="User Image" />
                                    <p>									
										<?php 
											if(strtolower($_SESSION[namalengkap])=='administrator'){
												echo "Programmer";
											}else{			
												echo $_SESSION[posisi];
											}
										?>
                                        <small></small>
                                    </p>
                                </li>
								
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="?mod=changepass" class="btn btn-default btn-flat">Ganti Password</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="?mod=exit" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <?php				
							session_start();
							include "inc/inc.koneksi.php";
							include "inc/fungsi_hdt.php";
							
							$text = "SELECT UPPER(namaperusahaan) AS nama FROM perusahaan WHERE kodearea='$_SESSION[kodearea]'";																		
							$sql = mysql_query($text);
							$rec = mysql_fetch_array($sql);
							$namaprshn = $rec['nama'];							
					?>		
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">                        
                    	<input type="text" class="form-control text-center" value="<?php echo $namaprshn ?>" disabled="disabled"/>
                    </form>
					
                    <ul class="sidebar-menu">
						<?php
							$idinduk = "4";							
							
							$input = "DELETE FROM aksesmenu_temp WHERE userid='$_SESSION[namauser]'";
							mysql_query($input);	
							
							$queryx="SELECT b.link,b.menu_class,b.menu_caption,a.id_induk,a.id_anak FROM aksesmenu a 
									LEFT JOIN menu_induk b ON b.id_induk=a.id_induk 
									WHERE a.username='$_SESSION[namauser]' GROUP BY a.id_induk ORDER BY a.id_induk";																		
							$sql_ = mysql_query($queryx);
							while($menu = mysql_fetch_array($sql_)){
								if($menu[id_induk]==$idinduk){
									if($menu[id_anak]>0){
										 echo "
										 <li class='treeview active'>";
									}else{
										echo "
										 <li class='active'>";
									}	 									 
								}else{	 
									if($menu[id_anak]>0){
										echo "
										 <li class='treeview'>";
									}else{
										echo "<li>";
									}	 			
								}
								
								echo "									
									<a href='$menu[link]'>
										<i class='$menu[menu_class]'></i><span> $menu[menu_caption]</span>";
										if($menu[id_anak]>0){
											echo "
												<i class='fa fa-angle-left pull-right'></i>";
										}
								echo "</a>";
								
								if($menu[id_anak]>0){
									echo "
										<ul class='treeview-menu'>";					
										
									$sqlx = mysql_query("SELECT b.link,b.menu_class,b.menu_caption FROM aksesmenu a LEFT JOIN menu_anak b 
														ON b.id_anak=a.id_anak WHERE a.id_induk=$menu[id_induk] 
														AND a.username='$_SESSION[namauser]' ORDER BY a.id_anak");
									while($menu_a = mysql_fetch_array($sqlx)){
										echo "
											<li><a href='$menu_a[link]'><i class='$menu_a[menu_class]'></i> $menu_a[menu_caption]</a></li>";							
									}
									
									echo "</ul>";
								}
								echo " 									
								</li>";	
							}	
						?>						
						<li>
							<a href='?mod=exit'><i class='fa fa-sign-out'></i><span> Sign Out</span></a>
						</li>
                    </ul>
                </section>
            </aside>
			
            <aside class="right-side">
                <section class="content-header">
                    <h1>
                        Pengguna Aplikasi
                        <small>Input Data</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-cog"></i>Pengaturan</a></li>
                        <li class="active">Pengguna Aplikasi</li>
                    </ol>
                </section>				
                <section class="content">
					<div class="box box-solid"><br>
						<div class="box-header" >
							<h4 style="font-family: 'Kaushan Script', cursive;font-size:18px;color:#CCCCCC" class="box-title">&nbsp; 
							Form User Aplikasi</h4>							
						</div><hr>
						
						<div class="box-body">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="cbokaryawan" class="col-sm-2 control-label">Nama Karyawan</label>
									<div class="col-sm-5">
										<select id="cbokaryawan" class="form-control input-medium select2me">
											<option value="">- Silakan Pilih -</option>
										</select>										
									</div>	
								</div>
								<div class="form-group">									
									<label for="txtusername" class="col-sm-2 control-label">Username</label>
									<div class="col-sm-4">
										<input type="text" id="txtusername" class="form-control pull-right" value=""/>
									</div>	
								</div>
								<div class="form-group">									
									<label for="txtpassword" class="col-sm-2 control-label">Password</label>
									<div class="col-sm-4">
										<input type="password" id="txtpassword" class="form-control pull-right" value=""/>
									</div>	
								</div>
								<div class="form-group">									
									<label for="txtrepassword" class="col-sm-2 control-label">Ulangi Password</label>
									<div class="col-sm-4">
										<input type="password" id="txtrepassword" class="form-control pull-right" value=""/>
									</div>	
								</div>						
								 <br>
							</form>	
						</div>						
						<div class="row">							
							<div class="col-xs-12">
								<div class="col-xs-1"></div>
								<div class="box-body col-sm-10">
									<div class="table-responsive" id="tblmenu"></div>
								</div>
							</div>														
						</div>
						<div class="row">
							<div class="col-xs-12 text-center">
								<label id="lblwarning_save" style="color:#FF0000"></label>
							</div>
						</div><br>
						<div class="box-footer text-right">                                  
							<button type="button" onClick="window.location='?mod=user'" class="btn btn-danger" id="btnclose">
							<i class="fa  fa-dot-circle-o"></i>&nbsp;Tutup</button>									
							<button type="button" class="btn btn-success" id="btnconfirm">
							<i class="fa fa-save (alias)"></i>&nbsp;Simpan</button>
						</div>
					</div>	
                </section>
            </aside>
        </div> 
		<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Konfirmasi</h4>
					</div>
					<div class="modal-body text-center">
						<h3>Yakin akan disimpan ?</h3>											
					</div>
					<div class="modal-footer clearfix" id="btnexec">
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
						<button type="button" class="btn btn-primary pull-left" id="btnsave"><i class="fa fa-check"></i> Ya</button>
					</div>
					<div class="overlay" id="overlayx"></div>
					<div class="loading-img" id="loading-imgx"></div>						
                </div>
            </div>
        </div>
		<div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="info-close">&times;</button>
						<h4 class="modal-title">Informasi</h4>
					</div>
					<div class="modal-body text-center" id="infone"></div>
					<div class="modal-footer text-center">
						<button type="button" class="btn btn-primary" data-dismiss="modal" id="info-ok"><i class="fa fa-check"></i> OK</button>
					</div>					
                </div>
            </div>
        </div>
       
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
		<script src="js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
		<script src="js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="js/newuser.js" type="text/javascript"></script> 
		<script src="js/ribuan.js" type="text/javascript"></script>
        <script src="js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
		<script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="js/assets/js/jquery.filedrop.js"></script>
        <script src="js/assets/js/script.js"></script>
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		<script src="assets/global/plugins/select2/select2.min.js" type="text/javascript" ></script>
		<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
		<script src="js/components-dropdowns.js" type="text/javascript" ></script>
		<script>
				jQuery(document).ready(function() { 
				   Metronic.init(); 
				   ComponentsDropdowns.init();
				});   
		</script>       	
    </body>
</html>