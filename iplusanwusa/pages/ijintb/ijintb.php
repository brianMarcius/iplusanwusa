<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>iplus | Ijin Tidak Bekerja</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />	
		<link href="img/favicon.png" rel="shortcut icon" type="image/png" />
		<link href="css/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
        <link href="css/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
		<link href="css/jquery.ui.datepicker.min.css" rel="stylesheet" type="text/css">
    </head>
    <body class="skin-blue">
        <header class="header">
            <a href="#" class="logo">
                PD. Anwusa Demak
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
					
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>
									<?php echo $_SESSION[namalengkap];?>
								<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-olive">
                                    <img src="<?php echo $_SESSION[pathavatar];?>" class="img-circle" alt="User Image" />
                                    <p>									
										<?php 
											if(strtolower($_SESSION[namalengkap])=='administrator'){
												echo "Programmer";
											}else{			
												echo $_SESSION[posisi];
											}
										?>
                                        <small></small>
                                    </p>
                                </li>
								
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Ganti Password</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="?mod=exit" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <?php				
							session_start();
							include "inc/inc.koneksi.php";
							include "inc/fungsi_hdt.php";
							
							$text = "SELECT UPPER(namaperusahaan) AS nama FROM perusahaan WHERE kodearea='$_SESSION[kodearea]'";																		
							$sql = mysql_query($text);
							$rec = mysql_fetch_array($sql);
							$namaprshn = $rec['nama'];							
					?>		
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">                        
                    	<input type="text" class="form-control text-center" value="<?php echo $namaprshn ?>" disabled="disabled"/>
                    </form>
					
                    <ul class="sidebar-menu">
						<?php
							$idinduk="2";					
							
							$input = "DELETE FROM lintasform WHERE userid='$_SESSION[namauser]'";
							mysql_query($input);
							
							$queryx="SELECT b.link,b.menu_class,b.menu_caption,a.id_induk,a.id_anak FROM aksesmenu a 
									LEFT JOIN menu_induk b ON b.id_induk=a.id_induk 
									WHERE a.username='$_SESSION[namauser]' GROUP BY a.id_induk ORDER BY a.id_induk";																		
							$sql_ = mysql_query($queryx);
							while($menu = mysql_fetch_array($sql_)){
								if($menu[id_induk]==$idinduk){
									if($menu[id_anak]>0){
										 echo "
										 <li class='treeview active'>";
									}else{
										echo "
										 <li class='active'>";
									}	 									 
								}else{	 
									if($menu[id_anak]>0){
										echo "
										 <li class='treeview'>";
									}else{
										echo "<li>";
									}	 			
								}
								
								echo "									
									<a href='$menu[link]'>
										<i class='$menu[menu_class]'></i><span> $menu[menu_caption]</span>";
										if($menu[id_anak]>0){
											echo "
												<i class='fa fa-angle-left pull-right'></i>";
										}
								echo "</a>";
								
								if($menu[id_anak]>0){
									echo "
										<ul class='treeview-menu'>";					
										
									$sqlx = mysql_query("SELECT b.link,b.menu_class,b.menu_caption FROM aksesmenu a LEFT JOIN menu_anak b 
														ON b.id_anak=a.id_anak WHERE a.id_induk=$menu[id_induk] 
														AND a.username='$_SESSION[namauser]' ORDER BY a.id_anak");
									while($menu_a = mysql_fetch_array($sqlx)){
										echo "
											<li><a href='$menu_a[link]'><i class='$menu_a[menu_class]'></i> $menu_a[menu_caption]</a></li>";							
									}
									
									echo "</ul>";
								}
								echo " 									
								</li>";	
							}	
						?>						
						<li>
							<a href='?mod=exit'><i class='fa fa-sign-out'></i><span> Sign Out</span></a>
						</li>
                    </ul>
                </section>
            </aside>
			
            <aside class="right-side">
                <section class="content-header">
                    <h1>
                        Ijin Tidak Bekerja
                        <small>View Data</small>                    
					</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-user"></i>Kepegawaian</a></li>
                        <li class="active">Ijin Tidak Bekerja</li>
                    </ol>
                </section>
				
                <section class="content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-solid">										
								<div class="box-body">
									<label></label>
									<div class="row">
										<div class="col-lg-3">		
											<select id="cboarea" class="form-control">
												<option value='' selected="selected">- Pilih Area -</option>
											</select>		
										</div>
										<div class="col-lg-2">		
											<select id="cbobulan" class="form-control">
												<option value='' selected="selected">- Bulan -</option>
											</select>		
										</div>
										<div class="col-lg-2">		
											<select id="cbominggu" class="form-control">
												<option value='1' selected> Minggu Ke 1 </option>
												<option value='2' > Minggu Ke 2 </option>
												<option value='3' > Minggu Ke 3 </option>
												<option value='4' > Minggu Ke 4 </option>
											</select>		
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-search"></i>
												</div>
												<input type="text" class="form-control" id="cari"/>
											</div>	
										</div>	
										<div class="col-lg-2">		
											<select id="cbolock" class="form-control">
												<option value='1' selected> Terkunci </option>
												<option value='2' > Bisa Diedit </option>
											</select>		
										</div>
									</div><br>									
									<div class="table-responsive" id="tampildata"></div>
									<div class="col-xs-12"></div>
									<div class="col-xs-12"></div>
									<div class="row">
										<div class="col-xs-12 text-right" id="paging"></div>
									</div><br>
								</div>					
								<div class="box-footer text-right" >								                                  
									<button type="button" class="btn btn-primary" onClick="window.location='?mod=home'">Tutup</button>
								</div>																																						
							</div>							
						</div>		
                    </div>						
                </section>
				
            </aside>
        </div>
		<div class="modal fade" id="resign-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Input Tgl Resign</h4>
					</div>
					<div class="modal-body">
						<div class="col-sm-8 text-center">
							<div class="input-group">										
								<input type="text" id="tglkeluar" class="form-control" placeholder="Tgl Resign" />
								<input type="hidden" id="txtidkaryawan" class="form-control"/>
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div><br>
							<div class="form-group text-center" style="color:#FF0033;display: none;" id="warningreg">
						</div>
					</div>
					<div class="modal-footer clearfix">
						<button type="button" class="btn btn-primary pull-left" id="btnupdate">
						<i class="fa fa-plus"></i> Simpan</button>
						<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">
						<i class="fa fa-times"></i> Batal</button>										
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Konfirmasi</h4>
					</div>
					<div class="modal-body text-center">
						<h3>Yakin akan dihapus ?</h3>											
					</div>
					<input type="hidden" id="txtkode" class="form-control"/>
					<div class="modal-footer clearfix" id="btnexec">
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
						<button type="button" class="btn btn-primary pull-left" id="btnyes"><i class="fa fa-check"></i> Ya</button>
					</div>
					<div class="overlay" id="overlayx"></div>
					<div class="loading-img" id="loading-imgx"></div>						
                </div>
            </div>
        </div>
        <div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Informasi</h4>
					</div>
					<div class="modal-body text-center" id="infone"></div>
					<div class="modal-footer text-center">
						<button type="button" class="btn btn-primary" id="btnok" data-dismiss="modal"><i class="fa fa-check"></i> OK</button>
					</div>					
                </div>
            </div>
        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
		<script src="js/jdsf.js" type="text/javascript"></script>
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
    	<script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
		<script src="js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
  		<script src="js/jQueryAssets/jquery-ui-1.9.2.datepicker.custom.min.js" type="text/javascript"></script>

    </body>
</html>