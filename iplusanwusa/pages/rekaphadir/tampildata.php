<?php
session_start();
include "../../inc/inc.koneksi.php";
include "../../inc/fungsi_hdt.php";
include "../../inc/fungsi_tanggal.php";

$kode = $_GET[kode];
$kodecabang = $_GET[area];
$tglawal = jin_date_sql($_GET['tglawal']);
$tglakhir = jin_date_sql($_GET['tglakhir']);
if (empty($tglawal)) {
	$tglawal = jin_date_sql(date('01-m-Y'));
}
if (empty($tglakhir)) {
	$tglakhir = jin_date_sql(date('t-m-Y'));
	// echo $tglakhir;
}

$dataPerPage = 10;
if(isset($_GET['page']))
{
$noPage = $_GET['page'];
}
else $noPage = 1;

$offset = ($noPage - 1) * $dataPerPage;

$sqljml ="SELECT COUNT(datefield) jml FROM calendar
WHERE datefield BETWEEN '$tglawal' AND '$tglakhir'";
$querjml = mysql_query($sqljml);
$row = mysql_fetch_array($querjml);

$totalhari = $row['jml'];

$text 	= "SELECT a.idkaryawan,
	a.nama,
	a.idfinger,
	a.kodejabatan,
	(SELECT COUNT(b.idkaryawan)
	FROM kehadiran b
	WHERE b.idkaryawan = a.idkaryawan
	AND b.tglkerja BETWEEN '$tglawal' AND '$tglakhir'
	) hadir,
	(SELECT COUNT(c.idkaryawan)
	FROM ijintdkbekerja c
	WHERE c.idkaryawan = a.idkaryawan
	AND c.tglijin BETWEEN '$tglawal' AND '$tglakhir'
	) ijin,
	(SELECT COUNT(d.idkaryawan)
	FROM pengambilancuti d
	WHERE d.idkaryawan = a.idkaryawan
	AND d.tglijin BETWEEN '$tglawal' AND '$tglakhir'
	) cuti,
	-- (SELECT COUNT(e.nik)
	-- FROM absensi e
	-- WHERE e.nik = a.idkaryawan
	-- AND e.telat != '00:00:00'
	-- AND e.tglkerja BETWEEN '$tglawal' AND '$tglakhir'
	-- ) telat,
	(SELECT COUNT(f.idkaryawan)
	FROM absenmanual f
	WHERE f.idkaryawan = a.idkaryawan
	AND f.tglkerja BETWEEN '$tglawal' AND '$tglakhir') manual,
	(SELECT COUNT(g.idkaryawan)
	FROM ijinkehadiran g
	WHERE g.idkaryawan = a.idkaryawan
	AND g.tglijin BETWEEN '$tglawal' AND '$tglakhir'
	AND kodeijin=1) plgawal,
	(SELECT COUNT(g.idkaryawan)
	FROM ijinkehadiran g
	WHERE g.idkaryawan = a.idkaryawan
	AND g.tglijin BETWEEN '$tglawal' AND '$tglakhir'
	AND kodeijin=0) brgkttelat,
	(SELECT COUNT(h.idkaryawan)
	FROM dinasluar h
	WHERE h.idkaryawan = a.idkaryawan
	AND h.tglijin BETWEEN '$tglawal' AND '$tglakhir') dinasluar
FROM karyawan a
WHERE 1=1";			

if(!empty($kode)){
	$text 	= $text. "AND (a.nama LIKE '%$kode%' OR a.nik LIKE '%$kode%') ";
}

if(!empty($kodecabang)){
	$text 	= $text. " AND a.kodearea='$kodecabang' ";
}

$text 	= $text. " ORDER BY a.kodejabatan, a.idkaryawan,a.nama DESC LIMIT $offset, $dataPerPage";					
$sql 	= mysql_query($text);


echo "
	<table class='table table-bordered'>
		<tr style='background-color:#f9f9f9'>
			<th style='width:10px;vertical-align:middle;' class='text-center'>NO</th>						
			<th style='vertical-align:middle;' class='text-center'>NAMA</th>
			<th style='width:80px;vertical-align:middle;' class='text-center'>HADIR</th>					
			<th style='width:80px;vertical-align:middle;' class='text-center'>IJIN</th>
			<th style='width:80px;vertical-align:middle;' class='text-center'>CUTI</th>
			<th style='width:80px;vertical-align:middle;' class='text-center'>ALPA</th>		
			<th style='width:80px;vertical-align:middle;' class='text-center'>TERLAMBAT</th>				
			<th style='width:80px;vertical-align:middle;' class='text-center'>ABSENSI MANUAL</th>
			<th style='vertical-align:middle;' class='text-center'>KETERANGAN</th>
		</tr>";		
		
		$no=1+$offset;
		while($rec = mysql_fetch_array($sql)){
				$alpa = $totalhari-$rec['hadir']-$rec['ijin']-$rec['cuti']-$rec['telat']-$rec['manual'];
				$hadir = $rec['hadir'];

				if ($alpa>5||$rec['manual']>5) {
					$peringatan = "<h4 style='color:red'>PERINGATAN</h4>";
				}else{
					$peringatan = "<h4 style='color:green'>BAIK</h4>";
				}

			echo "
				<tr >
					<td class='text-center'>$no</td>                 	
					<td>".$rec['nama']."</td>
					<td class='text-center'>".$hadir."</td>									
					<td class='text-center'>".$rec['ijin']."</td>
					<td class='text-center'>".$rec['cuti']."</td>
					<td class='text-center'>".$alpa."</td>		
					<td class='text-center'></td>								
					<td class='text-center'>".$rec['manual']."</td>
					<td class='text-center'>$peringatan</td>
                </tr>
				";	
				
			$no++;						
		}	
		
echo "
	</table>
	";

?>