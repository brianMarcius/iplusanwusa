<?php
session_start();
include "../../inc/inc.koneksi.php";
include "../../inc/fungsi_hdt.php";
include "../../inc/fungsi_tanggal.php";

$idkaryawan = $_GET['idkaryawan'];
$tglawal = jin_date_sql($_GET['tglawal']);
$tglakhir = jin_date_sql($_GET['tglakhir']);

$dataPerPage = 10;
if(isset($_GET['page']))
{
$noPage = $_GET['page'];
}
else $noPage = 1;

$offset = ($noPage - 1) * $dataPerPage;


$text 	= "SELECT distinct a.datefield,
				c.nama,
			IF(c.shifting=0,(
							SELECT masuk 
							FROM jamkerja
							WHERE kodearea = c.kodearea
							AND kodejam = IF((DATE_FORMAT(a.datefield,'%w')-1)=-1,6,(DATE_FORMAT(a.datefield,'%w')-1))),
							(
							SELECT jammasuk
							FROM jadwalshift
							WHERE kodearea = c.kodearea
							AND idkaryawan = c.idkaryawan
							AND tglefektif = a.datefield
				)) jdmasuk,
			IF(c.shifting=0,(
							SELECT pulang 
							FROM jamkerja
							WHERE kodearea = c.kodearea
							AND kodejam = IF((DATE_FORMAT(a.datefield,'%w')-1)=-1,6,(DATE_FORMAT(a.datefield,'%w')-1))),
							(
							SELECT jampulang
							FROM jadwalshift
							WHERE kodearea = c.kodearea
							AND idkaryawan = c.idkaryawan
							AND tglefektif = a.datefield
				)) jdpulang,
			(SELECT DATE_FORMAT(waktuscan,'%H:%i:%s')
					FROM logmesin
					WHERE pin = c.idkaryawan
					AND DATE_FORMAT(waktuscan,'%Y-%m-%d') = a.datefield
					AND kodescan = 1) masuk,
			(SELECT DATE_FORMAT(waktuscan,'%H:%i:%s')
					FROM logmesin
					WHERE pin = c.idkaryawan
					AND DATE_FORMAT(waktuscan,'%Y-%m-%d') = a.datefield
					AND kodescan = 2) pulang
FROM calendar a,
	logmesin b,
	karyawan c
WHERE a.datefield = STR_TO_DATE(b.waktuscan,'%Y-%m-%d')
AND b.pin = c.idkaryawan";			

if(!empty($idkaryawan)){
	$text 	= $text. " AND c.idkaryawan = '$idkaryawan' ";
}

if(!empty($tglawal)&&!empty($tglakhir)){
	$text 	= $text. " AND a.datefield between '$tglawal' and '$tglakhir' ";
}

$text 	= $text. " ORDER BY a.datefield DESC LIMIT $offset, $dataPerPage";					
$sql 	= mysql_query($text);


echo "
	<table class='table table-bordered'>
		<tr style='background-color:#f9f9f9'>
			<th rowspan=2 style='width:10px;vertical-align:middle;' class='text-center'>NO</th>						
			<th rowspan=2 style='vertical-align:middle;' class='text-center'>HARI / TANGGAL</th>
			<th colspan=2 style='width:260px;vertical-align:middle;' class='text-center'>JADWAL</th>					
			<th colspan=2 style='width:260px;vertical-align:middle;' class='text-center'>MESIN ABSENSI</th>
			<th rowspan=2 style='width:140px;vertical-align:middle;' class='text-center'>TERLAMBAT</th>
			<th rowspan=2 style='width:140px;vertical-align:middle;' class='text-center'>KELEBIHAN JAM</th>
		</tr>
		<tr style='background-color:#f9f9f9'>	
			<th style='width:130px;vertical-align:middle;' class='text-center'>MASUK</th>
			<th style='width:130px;vertical-align:middle;' class='text-center'>PULANG</th>		
			<th style='width:130px;vertical-align:middle;' class='text-center'>MASUK</th>				
			<th style='width:130px;vertical-align:middle;' class='text-center'>PULANG</th>			
		</tr>";		
		
		$no=1+$offset;
		while($rec = mysql_fetch_array($sql)){				
			echo "
				<tr >
					<td class='text-center'>$no.</td>                 	
					<td >".$rec['datefield']."</td>
					<td class='text-center'>".$rec['jdmasuk']."</td>									
					<td class='text-center'>".$rec['jdpulang']."</td>
					<td class='text-center'>".$rec['masuk']."</td>
					<td class='text-center'>".$rec['pulang']."</td>		
					<td class='text-center'></td>								
					<td class='text-center'></td>
                </tr>";	
				
			$no++;						
		}	
		
echo "
	</table>
	";

?>