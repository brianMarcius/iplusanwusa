<?php
session_start();
include "../../inc/inc.koneksi.php";
include "../../inc/fungsi_hdt.php";

$kode = $_GET[kode];
$kodecabang = $_GET[area];

$dataPerPage = 10;
if(isset($_GET['page']))
{
$noPage = $_GET['page'];
}
else $noPage = 1;

$offset = ($noPage - 1) * $dataPerPage;


$text 	= "SELECT a.idkaryawan,a.nik,a.idfinger,a.nama,d.namaperusahaan,a.notelp,b.namajabatan AS jabatan,
			c.namadivisi AS divisi,IF(FLOOR(PERIOD_DIFF(DATE_FORMAT(NOW(), '%Y%m'), DATE_FORMAT(a.tglmasuk, '%Y%m'))/12)>0,			
			CONCAT(FLOOR(PERIOD_DIFF(DATE_FORMAT(NOW(), '%Y%m'), DATE_FORMAT(a.tglmasuk, '%Y%m'))/12),' Tahun '),'') AS masakerja1,
			CONCAT(MOD(PERIOD_DIFF(DATE_FORMAT(NOW(), '%Y%m'), DATE_FORMAT(a.tglmasuk, '%Y%m')),12), ' Bulan') AS masakerja2
			FROM karyawan a LEFT JOIN jabatan b ON b.kode_jabatan=a.kodejabatan
		   	LEFT JOIN divisi c ON c.kode_divisi=a.kodedivisi
			LEFT JOIN perusahaan d ON d.kodearea=a.kodearea
			WHERE a.tglresign='0000-00-00' AND a.onview=1 ";			

if(!empty($kode)){
	$text 	= $text. "AND (a.nama LIKE '%$kode%' OR a.nik LIKE '%$kode%') ";
}

if(!empty($kodecabang)){
	$text 	= $text. "AND a.kodearea='$kodecabang' ";
}

$text 	= $text. "ORDER BY d.index_perusahaan,a.kodejabatan,a.nama DESC LIMIT $offset, $dataPerPage";					
$sql 	= mysql_query($text);	


echo "
	<table class='table table-bordered'>
		<tr style='background-color:#f9f9f9'>
			<th style='width:10px;vertical-align:middle;' class='text-center'>NO</th>						
			<th style='vertical-align:middle;' class='text-center'>NAMA</th>
			<th style='width:140px;vertical-align:middle;' class='text-center'>NIK</th>					
			<th style='width:100px;vertical-align:middle;' class='text-center'>JABATAN</th>
			<th style='width:100px;vertical-align:middle;' class='text-center'>DIVISI</th>		
			<th style='width:120px;vertical-align:middle;' class='text-center'>NO TELP</th>				
			<th style='width:50px;vertical-align:middle;' class='text-center'>ID FINGER</th>
			<th style='width:140px;vertical-align:middle;' class='text-center'>MASA KERJA</th>
			<th style='width:120px;vertical-align:middle;' class='text-center'>AKSI</th>
		</tr>";		
		
		$no=1+$offset;
		while($rec = mysql_fetch_array($sql)){				
			echo "
				<tr >
					<td class='text-center'>$no.</td>                 	
					<td >$rec[nama]</td>
					<td class='text-center'>$rec[nik]</td>									
					<td class='text-center'>$rec[jabatan]</td>
					<td class='text-center'>$rec[divisi]</td>		
					<td class='text-center'>$rec[notelp]</td>								
					<td class='text-center'>$rec[idfinger]</td>
					<td class='text-center'>$rec[masakerja1] &nbsp; $rec[masakerja2] </td>
					<td class='text-center'>&nbsp;
						<a href='javascript:void(0)' onClick=\"editempl('$rec[idkaryawan]')\" title='Edit'>Edit</a> &nbsp;&nbsp;| &nbsp;";
						
						//<a href='javascript:void(0)' onClick=\"delemp('$rec[idkaryawan]')\" title='Hapus'>Hapus</a> &nbsp;&nbsp;| &nbsp;
						echo "
						<a href='javascript:void(0)' onClick=\"resignempl('$rec[idkaryawan]')\" title='Resign'>Resign</a>
					</td>
                </tr>";	
				
			$no++;						
		}	
		
echo "
	</table>
	";

?>