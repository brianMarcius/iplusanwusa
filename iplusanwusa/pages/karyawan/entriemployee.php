<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>iplus | Data Karyawan</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
		<link href="assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
		<link href="assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
		<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>	
    	<link href="img/favicon.png" rel="shortcut icon" type="image/png" />
    </head>
    <body class="skin-blue">
        <header class="header">
            <a href="#" class="logo">
                PD. Anwusa Demak
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
					
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>
									<?php echo $_SESSION[namalengkap];?>
								<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-olive">
                                    <img src="<?php echo $_SESSION[pathavatar];?>" class="img-circle" alt="User Image" />
                                    <p>									
										<?php 
											if(strtolower($_SESSION[namalengkap])=='administrator'){
												echo "Programmer";
											}else{			
												echo $_SESSION[posisi];
											}
										?>
                                        <small></small>
                                    </p>
                                </li>
								
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="?mod=changepass" class="btn btn-default btn-flat">Ganti Password</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="?mod=exit" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <?php				
							session_start();
							include "inc/inc.koneksi.php";
							include "inc/fungsi_hdt.php";
							
							$text = "SELECT UPPER(namaperusahaan) AS nama FROM perusahaan WHERE kodearea='$_SESSION[kodearea]'";																		
							$sql = mysql_query($text);
							$rec = mysql_fetch_array($sql);
							$namaprshn = $rec['nama'];							
					?>		
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">                        
                    	<input type="text" class="form-control text-center" value="<?php echo $namaprshn ?>" disabled="disabled"/>
                    </form>
					
                    <ul class="sidebar-menu">
						<?php
							$idinduk = "2";								
							
							$queryx="SELECT b.link,b.menu_class,b.menu_caption,a.id_induk,a.id_anak FROM aksesmenu a 
									LEFT JOIN menu_induk b ON b.id_induk=a.id_induk 
									WHERE a.username='$_SESSION[namauser]' GROUP BY a.id_induk ORDER BY a.id_induk";																		
							$sql_ = mysql_query($queryx);
							while($menu = mysql_fetch_array($sql_)){
								if($menu[id_induk]==$idinduk){
									if($menu[id_anak]>0){
										 echo "
										 <li class='treeview active'>";
									}else{
										echo "
										 <li class='active'>";
									}	 									 
								}else{	 
									if($menu[id_anak]>0){
										echo "
										 <li class='treeview'>";
									}else{
										echo "<li>";
									}	 			
								}
								
								echo "									
									<a href='$menu[link]'>
										<i class='$menu[menu_class]'></i><span> $menu[menu_caption]</span>";
										if($menu[id_anak]>0){
											echo "
												<i class='fa fa-angle-left pull-right'></i>";
										}
								echo "</a>";
								
								if($menu[id_anak]>0){
									echo "
										<ul class='treeview-menu'>";					
										
									$sqlx = mysql_query("SELECT b.link,b.menu_class,b.menu_caption FROM aksesmenu a LEFT JOIN menu_anak b 
														ON b.id_anak=a.id_anak WHERE a.id_induk=$menu[id_induk] AND a.username='$_SESSION[namauser]' 
														ORDER BY a.id_anak");
									while($menu_a = mysql_fetch_array($sqlx)){
										echo "
											<li><a href='$menu_a[link]'><i class='$menu_a[menu_class]'></i> $menu_a[menu_caption]</a></li>";							
									}
									
									echo "</ul>";
								}
								echo " 									
								</li>";	
							}	
						?>						
						<li>
							<a href='?mod=exit'><i class='fa fa-sign-out'></i><span> Sign Out</span></a>
						</li>
                    </ul>
                </section>
            </aside>
			
            <aside class="right-side">
                <section class="content-header">
                    <h1>
                        Data Karyawan
                        <small>Input Data</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-user"></i>Kepegawaian</a></li>
                        <li class="active">Data Karyawan</li>
                    </ol>
                </section>				
                <section class="content">
					<div class="box box-solid"><br>
						<div class="box-header" >
							<h4 style="font-family: 'Kaushan Script', cursive;font-size:18px;color:#CCCCCC" class="box-title">&nbsp; Form Karyawan</h4>							
						</div><hr>
						<br>
						<div class="box-body">
							<form class="form-horizontal">		
								<div class="form-group">	
									<label for="txtnik" class="col-sm-3 control-label">NIK</label>	
									<div class="col-sm-3">		
										<input type="text" class="form-control" id="txtnik"/>									
									</div>	
								</div>		
								<div class="form-group">	
									<label for="txtpin" class="col-sm-3 control-label">ID Finger (PIN)</label>	
									<div class="col-sm-3">										
										<input type="number" class="form-control" id="txtpin"/>									
									</div>										
								</div>				
								<div class="form-group">	
									<label for="txtnama" class="col-sm-3 control-label">Nama Lengkap</label>	
									<div class="col-sm-6">										
										<input type="text" class="form-control" id="txtnama"/>
										<input type="hidden" class="form-control" id="txtidkaryawan"/>									
									</div>	
								</div>						
								<div class="form-group">	
									<label for="cbokotalahir" class="col-sm-3 control-label">TTL</label>	
									<div class="col-sm-3">
										<select id="cbokotalahir" class="form-control select2me">
											<option value='' selected="selected">- Kota Kelahiran -</option>
										</select>										
									</div>
									<div class="col-sm-3">
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type='text' class='form-control text-center' id='txttgllahir'
											data-inputmask="'alias': 'dd-mm-yyyy'" data-mask />
										</div>
									</div>
								</div>
								<div class="form-group">	
									<label for="txtpin" class="col-sm-3 control-label">Jenis Kelamin</label>	
									<div class="col-sm-3">
										<select id="cbojekel" class="form-control">
											<option value='' selected="selected">- Silakan Pilih -</option>
											<option value='0' >Perempuan</option>
											<option value='1' >Laki - Laki</option>
										</select>										
									</div>
								</div>	
								<div class="form-group">	
									<label for="alamat" class="col-sm-3 control-label">Alamat</label>	
									<div class="col-sm-6">										
										<textarea rows='3' class="form-control" id='alamat'></textarea>									
									</div>	
								</div>
								<div class="form-group">	
									<label for="txttelp" class="col-sm-3 control-label">No Telepon</label>	
									<div class="col-sm-4">	
										<div class="input-group">
											<div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>										
												<input type="text" class="form-control" id="txttelp" onKeyDown="return numbersonly(this, event);" 
												onKeyUp="javascript:tandaPemisahTitik(this);"/>	
										</div>								
									</div>	
								</div>
								<div class="form-group">
									<label for="cbocabang" class="col-sm-3 control-label">Wilayah Kerja</label>
									<div class="col-sm-4">
										<select id="cbocabang" class="form-control">
											<option value="" selected>- Silakan Pilih -</option>
										</select>										
									</div>
								</div>
								<div class="form-group">
									<label for="cbodivisi" class="col-sm-3 control-label">Divisi</label>
									<div class="col-sm-3">
										<select id="cbodivisi" class="form-control">
											<option value="" selected>- Silakan Pilih -</option>
										</select>										
									</div>													
									<div class="col-sm-3">		
										<span class="help-block"><a href="#" id="divisi_add">| &nbsp; Tambah Divisi Baru</a></span>
									</div>							
								</div>
								<div class="form-group">
									<label for="cbodivisi" class="col-sm-3 control-label">Jabatan</label>
									<div class="col-sm-3">
										<select id="cbojabatan" class="form-control">
											<option value="" selected>- Silakan Pilih -</option>
										</select>										
									</div>													
									<div class="col-sm-3">		
										<span class="help-block"><a href="#" id="jabatan_add">| &nbsp; Tambah Jabatan Baru</a></span>
									</div>	
								</div>
								<div class="form-group">
									<label for="tglmasuk" class="col-sm-3 control-label">Tgl Bergabung</label>
									<div class="col-sm-3">
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type='text' class='form-control text-center' id='tglmasuk'
											data-inputmask="'alias': 'dd-mm-yyyy'" data-mask />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="cbojenjang" class="col-sm-3 control-label">Pendidikan</label>
									<div class="col-sm-2">
										<select id="cbojenjang" class="form-control">
											<option value="" selected>- Silakan Pilih -</option>
											<option value="SD" > SD </option>
											<option value="SMP" > SMP </option>
											<option value="SMA" > SMA </option>
											<option value="D1" > D1 </option>
											<option value="D3" > D3 </option>
											<option value="S1" > S1 </option>
											<option value="S2" > S2 </option>
											<option value="S3" > S3 </option>
										</select>										
									</div>
									<div class="col-sm-4">
										<select id="cbojurusan" class="form-control">
											<option value='' selected="selected">- Silakan Pilih -</option>
											<option value='0' >Non Jurusan</option>
											<option value='1' >Ekonomi</option>
											<option value='2' >Hukum</option>
											<option value='3' >Bahasa</option>
											<option value='4' >Komputer</option>
											<option value='5' >Teknik Mesin</option>
											<option value='6' >Teknik Elektro</option>
											<option value='7' >Teknik Sipil</option>
										</select>
									</div>
								</div>
								<div class="form-group">	
									<label for="cbogaji" class="col-sm-3 control-label">Penggajian</label>	
									<div class="col-sm-3">
										<select id="cbogaji" class="form-control">
											<option value='0' >Harian</option>
											<option value='1' >Mingguan</option>
											<option value='2' selected="selected">Bulanan</option>
										</select>										
									</div>
								</div>
								<div class="form-group">	
									<label for="chkshift" class="col-sm-3 control-label"></label>	
									<div class="col-sm-3">
										<input id="chkshift" type="checkbox" />	Shifting
									</div>
									<div class="col-sm-1">
										<input type="hidden" class="form-control" id="txtshift" value="0"/>
									</div>
								</div>
							</form>	
						</div>						
						<br>						
						<div class="row">
							<div class="col-xs-12 text-center">
								<label id="warningx" style="color:#FF0000"></label>
							</div>
						</div>							
						<div class="box-footer text-right" >	
							<button type="button" class="btn btn-danger" onClick="window.location='?mod=empl'">
								<i class="fa fa-dot-circle-o"></i>&nbsp; Tutup</button>
							<button type="button" id="btnconfirm" class="btn btn-success" >
								<i class="fa fa-save (alias)"></i>&nbsp; Simpan</button>							
						</div>
					</div>	
                </section>
            </aside>
        </div>  
		<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Konfirmasi</h4>
					</div>
					<div class="modal-body text-center">
						<h3>Yakin akan disimpan ?</h3>											
					</div>
					<div class="modal-footer clearfix" id="btnexec">
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
						<button type="button" class="btn btn-primary pull-left" id="btnsave"><i class="fa fa-check"></i> Ya</button>
					</div>
					<div class="overlay" id="overlayx"></div>
					<div class="loading-img" id="loading-imgx"></div>						
                </div>
            </div>
        </div>
        <div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Informasi</h4>
					</div>
					<div class="modal-body text-center" id="infone"></div>
					<div class="modal-footer text-center">
						<button type="button" class="btn btn-primary" id="btnok" data-dismiss="modal"><i class="fa fa-check"></i> OK</button>
					</div>					
                </div>
            </div>
        </div> 
		<div class="modal fade" id="adddivisi-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Tambah Divisi Baru</h4>
					</div>
					<div class="modal-body" >
						<div class="form-group"> 							
							<input type="text" class="form-control text-center" id="txtdivisibaru" value=""/> 								
			 				<label class="control-label" style="color:#FF9933; display:none;" id="warningdiv">Nama Divisi belum diisi</label> 
						</div>
					</div>
					<div class="modal-footer clearfix"> 
						<button type="button" class="btn btn-danger pull-left" data-dismiss="modal" ><i class="fa fa-times"> Batal</i></button> 
						<button type="button" class="btn btn-success" id="btnupdatediv"><i class="fa fa-save (alias)">  Simpan</i></button> 
					</div> 
				</div>
			</div>
		</div>
		<div class="modal fade" id="addjabatan-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Tambah Jabatan Baru</h4>
					</div>
					<div class="modal-body" >
						<div class="form-group"> 
							<input type="text" class="form-control text-center" id="txtjabatanbaru" value=""/> 
			 				<label class="control-label" style="color:#FF9933; display:none;" id="warningjab">Nama Jabatan belum diisi</label> 
						</div>
					</div>
					<div class="modal-footer clearfix"> 
						<button type="button" class="btn btn-danger pull-left" data-dismiss="modal" ><i class="fa fa-times"> Batal</i></button> 
						<button type="button" class="btn btn-success" id="btnupdatejab"><i class="fa fa-save (alias)">  Simpan</i></button> 
					</div> 
				</div>
			</div>
		</div>
		<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
		<script src="js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
		<script src="js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="js/newempl.js?v=1.0.0" type="text/javascript"></script>	
		<script src="js/ribuan.js" type="text/javascript"></script>
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>	
		<script src="assets/admin/pages/scripts/components-pickers.js"></script>
		<script src="assets/global/plugins/select2/select2.min.js" type="text/javascript" ></script>
		<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
		<script src="js/components-dropdowns.js" type="text/javascript" ></script>
		<script>
				jQuery(document).ready(function() { 
				   	Metronic.init(); 
				    ComponentsPickers.init();
					ComponentsDropdowns.init();	
				});   
		</script>
    </body>
</html>