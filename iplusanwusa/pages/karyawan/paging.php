<?php
session_start();
include "../../inc/inc.koneksi.php";
include "../../inc/fungsi_hdt.php";

$kode = $_GET[kode];
$kodecabang = $_GET[area];

$dataPerPage = 10;
if(isset($_GET['page']))
{
$noPage = $_GET['page'];
}
else $noPage = 1;

$offset = ($noPage - 1) * $dataPerPage;
	
$text 	= "SELECT COUNT(*) AS jumData
		   FROM karyawan a LEFT JOIN jabatan b ON b.kode_jabatan=a.kodejabatan
		   	LEFT JOIN divisi c ON c.kode_divisi=a.kodedivisi
			WHERE a.tglresign='0000-00-00' AND a.onview=1 ";				

if(!empty($kode)){
	$text 	= $text. "AND (a.nama LIKE '%$kode%' OR a.nik LIKE '%$kode%') ";
}

if(!empty($kodecabang)){
	$text 	= $text. "AND a.kodearea='$kodecabang' ";
}

$hasil  = mysql_query($text);
$data   = mysql_fetch_array($hasil);

$jumData = $data['jumData'];
$jumPage = ceil($jumData/$dataPerPage);

// menampilkan navigasi paging
echo "<ul class='pagination pagination-sm no-margin pull-right'>";
if ($noPage > 1) echo  "<li><a href='javascript:void(0)' onClick=\"tampildata('".($noPage-1)."')\"><< Prev</a></li>";
for($page = 1; $page <= $jumPage; $page++)
{
	if ((($page >= $noPage - 2) && ($page <= $noPage + 2)) || ($page == 1) || ($page == $jumPage))
	{
	  if (($showPage == 1) && ($page != 2))  echo "<li class='disabled'><a href='#'>...</a></li>";
	  if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  echo "<li class='disabled'><a href='#'>...</a></li>";
	  if ($page == $noPage) echo "<li class='active'><a href='#'>".$page."</a></li>";
	  else echo "<li><a href='javascript:void(0)' onClick=\"tampildata('".$page."')\">".$page."</a></li>";
	  $showPage = $page;
	}
}

if ($noPage < $jumPage) echo "<li><a href='javascript:void(0)' onClick=\"tampildata('".($noPage+1)."')\">Next >></a></li>";

echo "</ul>";   
	
?>