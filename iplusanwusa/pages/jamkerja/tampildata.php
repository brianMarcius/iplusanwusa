<?php
session_start();
include "../../inc/inc.koneksi.php";
include "../../inc/fungsi_hdt.php";

$kode = $_GET[kode];
$kodecabang = $_GET[area];

$text 	= "SELECT a.kodearea,a.namaperusahaan,
			IF(COALESCE(TIME_FORMAT(b.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(b.masuk,'%H:%i'),'-')) AS normal1_msk,
			IF(COALESCE(TIME_FORMAT(b.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(b.pulang,'%H:%i'),'-')) AS normal1_plg,			
			IF(COALESCE(TIME_FORMAT(c.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(c.masuk,'%H:%i'),'-')) AS normal2_msk,
			IF(COALESCE(TIME_FORMAT(c.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(c.pulang,'%H:%i'),'-')) AS normal2_plg,
			IF(COALESCE(TIME_FORMAT(m.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(m.masuk,'%H:%i'),'-')) AS normal3_msk,
			IF(COALESCE(TIME_FORMAT(m.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(m.pulang,'%H:%i'),'-')) AS normal3_plg,
			IF(COALESCE(TIME_FORMAT(n.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(n.masuk,'%H:%i'),'-')) AS normal4_msk,
			IF(COALESCE(TIME_FORMAT(n.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(n.pulang,'%H:%i'),'-')) AS normal4_plg,
			IF(COALESCE(TIME_FORMAT(o.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(o.masuk,'%H:%i'),'-')) AS normal5_msk,
			IF(COALESCE(TIME_FORMAT(o.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(o.pulang,'%H:%i'),'-')) AS normal5_plg,
			IF(COALESCE(TIME_FORMAT(p.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(p.masuk,'%H:%i'),'-')) AS normal6_msk,
			IF(COALESCE(TIME_FORMAT(p.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(p.pulang,'%H:%i'),'-')) AS normal6_plg,
			IF(COALESCE(TIME_FORMAT(q.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(q.masuk,'%H:%i'),'-')) AS normal7_msk,
			IF(COALESCE(TIME_FORMAT(q.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(q.pulang,'%H:%i'),'-')) AS normal7_plg,
			IF(COALESCE(TIME_FORMAT(d.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(d.masuk,'%H:%i'),'-')) AS pagi1_msk,
			IF(COALESCE(TIME_FORMAT(d.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(d.pulang,'%H:%i'),'-')) AS pagi1_plg,			
			IF(COALESCE(TIME_FORMAT(e.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(e.masuk,'%H:%i'),'-')) AS pagi2_msk,
			IF(COALESCE(TIME_FORMAT(e.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(e.pulang,'%H:%i'),'-')) AS pagi2_plg,
			IF(COALESCE(TIME_FORMAT(f.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(f.masuk,'%H:%i'),'-')) AS pagi3_msk,
			IF(COALESCE(TIME_FORMAT(f.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(f.pulang,'%H:%i'),'-')) AS pagi3_plg,
			IF(COALESCE(TIME_FORMAT(g.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(g.masuk,'%H:%i'),'-')) AS siang1_msk,
			IF(COALESCE(TIME_FORMAT(g.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(g.pulang,'%H:%i'),'-')) AS siang1_plg,			
			IF(COALESCE(TIME_FORMAT(h.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(h.masuk,'%H:%i'),'-')) AS siang2_msk,
			IF(COALESCE(TIME_FORMAT(h.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(h.pulang,'%H:%i'),'-')) AS siang2_plg,
			IF(COALESCE(TIME_FORMAT(i.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(i.masuk,'%H:%i'),'-')) AS siang3_msk,
			IF(COALESCE(TIME_FORMAT(i.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(i.pulang,'%H:%i'),'-')) AS siang3_plg,
			IF(COALESCE(TIME_FORMAT(j.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(j.masuk,'%H:%i'),'-')) AS malam1_msk,
			IF(COALESCE(TIME_FORMAT(j.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(j.pulang,'%H:%i'),'-')) AS malam1_plg,			
			IF(COALESCE(TIME_FORMAT(k.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(k.masuk,'%H:%i'),'-')) AS malam2_msk,
			IF(COALESCE(TIME_FORMAT(k.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(k.pulang,'%H:%i'),'-')) AS malam2_plg,
			IF(COALESCE(TIME_FORMAT(l.masuk,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(l.masuk,'%H:%i'),'-')) AS malam3_msk,
			IF(COALESCE(TIME_FORMAT(l.pulang,'%H:%i'),'-')='00:00','-',COALESCE(TIME_FORMAT(l.pulang,'%H:%i'),'-')) AS malam3_plg
			FROM perusahaan a 
			LEFT JOIN jamkerja b ON b.kodearea=a.kodearea AND b.kodejam=0
			LEFT JOIN jamkerja c ON c.kodearea=a.kodearea AND c.kodejam=1
			LEFT JOIN jamkerja m ON m.kodearea=a.kodearea AND m.kodejam=2
			LEFT JOIN jamkerja n ON n.kodearea=a.kodearea AND n.kodejam=3
			LEFT JOIN jamkerja o ON o.kodearea=a.kodearea AND o.kodejam=4
			LEFT JOIN jamkerja p ON p.kodearea=a.kodearea AND p.kodejam=5
			LEFT JOIN jamkerja q ON q.kodearea=a.kodearea AND q.kodejam=6
			LEFT JOIN jamkerja d ON d.kodearea=a.kodearea AND d.kodejam=11
			LEFT JOIN jamkerja e ON e.kodearea=a.kodearea AND e.kodejam=12
			LEFT JOIN jamkerja f ON f.kodearea=a.kodearea AND f.kodejam=13
			LEFT JOIN jamkerja g ON g.kodearea=a.kodearea AND g.kodejam=21
			LEFT JOIN jamkerja h ON h.kodearea=a.kodearea AND h.kodejam=22
			LEFT JOIN jamkerja i ON i.kodearea=a.kodearea AND i.kodejam=23
			LEFT JOIN jamkerja j ON j.kodearea=a.kodearea AND j.kodejam=31
			LEFT JOIN jamkerja k ON k.kodearea=a.kodearea AND k.kodejam=32
			LEFT JOIN jamkerja l ON l.kodearea=a.kodearea AND l.kodejam=33
			ORDER BY a.index_perusahaan,b.kodejam";					
$sql 	= mysql_query($text);	

echo "
	<table class='table table-bordered'>
		<tr style='background-color:#f9f9f9'>
			<th rowspan=2 style='width:10px;vertical-align:middle;' class='text-center'>NO</th>						
			<th rowspan=2 style='vertical-align:middle;' class='text-center'>WILAYAH KERJA</th>
			<th colspan=7 style='vertical-align:middle;' class='text-center'>JAM NORMAL</th>					
			<th colspan=9 style='vertical-align:middle;' class='text-center'>SHIFT</th>
		</tr>
		<tr style='background-color:#f9f9f9'>
			<th style='width:60px;vertical-align:middle;' class='text-center'>SEN</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>SEL</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>RAB</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>KAM</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>JUM</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>SAB</th>					
			<th style='width:60px;vertical-align:middle;' class='text-center'>MIN</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>P1</th>					
			<th style='width:60px;vertical-align:middle;' class='text-center'>P2</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>P3</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>S1</th>					
			<th style='width:60px;vertical-align:middle;' class='text-center'>S2</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>S3</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>M1</th>					
			<th style='width:60px;vertical-align:middle;' class='text-center'>M2</th>
			<th style='width:60px;vertical-align:middle;' class='text-center'>M3</th>
		</tr>
		";		
		
		$no=1;
		while($rec = mysql_fetch_array($sql)){	
						
			echo "
				<tr >
					<td style='vertical-align:middle;' class='text-center'>$no.</td>                 	
					<td style='vertical-align:middle;' >$rec[namaperusahaan]</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','0')\">$rec[normal1_msk]<br/>$rec[normal1_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','1')\">$rec[normal2_msk]<br/>$rec[normal2_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','2')\">$rec[normal3_msk]<br/>$rec[normal3_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','3')\">$rec[normal4_msk]<br/>$rec[normal4_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','4')\">$rec[normal5_msk]<br/>$rec[normal5_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','5')\">$rec[normal6_msk]<br/>$rec[normal6_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','6')\">$rec[normal7_msk]<br/>$rec[normal7_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','11')\">$rec[pagi1_msk]<br/>$rec[pagi1_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','12')\">$rec[pagi2_msk]<br/>$rec[pagi2_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','13')\">$rec[pagi3_msk]<br/>$rec[pagi3_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','21')\">$rec[siang1_msk]<br/>$rec[siang1_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','22')\">$rec[siang2_msk]<br/>$rec[siang2_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','23')\">$rec[siang3_msk]<br/>$rec[siang3_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','31')\">$rec[malam1_msk]<br/>$rec[malam1_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','32')\">$rec[malam2_msk]<br/>$rec[malam2_plg]</a>
					</td>
					<td class='text-center'> 
						<a href='javascript:void(0)' onClick=\"edit('$rec[kodearea]','33')\">$rec[malam3_msk]<br/>$rec[malam3_plg]</a>
					</td>
                </tr>";	
				
			$no++;						
		}	
		
echo "
	</table>
	";

?>