<?php

$mod 		= $_GET['mod'];
$kodearea 	= $_SESSION[kodearea];
$username 	= $_SESSION[namauser];
$leveluser  = $_SESSION[leveluser];

if(empty($kodearea) or empty($username)){
	header('location:index.html');
}

if($mod=='home'){
	include "pages/info/info.php";	
}elseif ($mod=='changepass'){
	include "pages/updateacc/changepass.php";
}elseif ($mod=='empl'){
	include "pages/karyawan/employee.php";
}elseif($mod=='newempl'){
	include "pages/karyawan/entriemployee.php";
}elseif($mod=='jmkr'){
	include "pages/jamkerja/jamkerja.php";
}elseif($mod=='jdsf'){
	include "pages/jadwalshift/jadwalshift.php";	
}elseif($mod=='ijtb'){
	include "pages/ijintb/entriijintb.php";
}elseif($mod=='amct'){
	include "pages/cuti/entricuti.php";
}elseif($mod=='abmn'){
	include "pages/absenmanual/entriabsenmanual.php";
}elseif($mod=='dspn'){
	include "pages/dispen/entridispen.php";
}elseif($mod=='dnlk'){
	include "pages/dinluarkota/entridinluarkota.php";	
}elseif($mod=='rkhd'){
	include "pages/rekaphadir/rekaphadir.php";
}elseif($mod=='logfg'){
	include "pages/logfinger/logfinger.php";
}elseif ($mod=='user'){
	include "pages/user/user.php";
}elseif ($mod=='newuser'){
	include "pages/user/entriuser.php";
}elseif ($mod=='exit'){
    include "pages/logout.php";	
}else{
  header('location:index.html');
}

?>
<script src="js/addlinkChangepass.js" type="text/javascript"></script>